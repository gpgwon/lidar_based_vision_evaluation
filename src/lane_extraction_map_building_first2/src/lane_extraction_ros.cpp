#include <lane_extraction/lane_extraction.h>

// ROS headers
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>

// std headers
#include <iostream>
#include <string>
#include <vector>
#include <stdint.h>
#include <fstream>
#include <exception>
#include <sstream>
#include <algorithm>
#include <iterator>

// Message headers
#include <lane_extraction/NetworkFrame.h>
#include <lane_extraction/PoseMeasurement.h>
#include <sensor_msgs/Image.h>

// 3rd party headers
#include <yaml-cpp/yaml.h>
#include <boost/filesystem.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/tokenizer.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

// Constants
const uint32_t kTimeDifferenceBetweenGPSAndUTCTimes = 315964800;
const uint32_t kVeryOldGPSINSMeasurementSkipTime = 20;
const uint32_t kOldGPSINSMeasurementSkipTime = 10;

// Global variables
int gps_utc_time_offset_sec;

typedef boost::tokenizer<boost::escaped_list_separator<char>> Tokenizer;


// Shared pointer to LidarLaneExtraction object.
std::shared_ptr<LidarLaneExtraction> lane_extraction_ptr{nullptr};

// Shared pointer to a ROS publisher to display a debug cloud.
std::shared_ptr<ros::Publisher> pub_debug_cloud_full{nullptr};

// Shared pointer to a ROS publisher to display lanes.
std::shared_ptr<ros::Publisher> pub_debug_lanes{nullptr};

// Function to convert the OXTS data to SimpleGPSINSMeasurement format.
bool convertOXTSCSVLineToSimpleGPSINSMeasurement(const std::vector<std::string>& str_vec, SimpleGPSINSMeasurement& simple_gpsins);

// Callback to receive point cloud from the Velodyne parser.
void getParsedLidarPointCloudCallback(const PointCloudf& point_cloud);

// Function to publish the visualization topics for debugging.
void publishDebugInfo(const pcl::PointCloud<pcl::PointXYZI>& cloud_full, const std::vector<pcl::PointCloud<pcl::PointXYZI>>& cloud_lanes);

// Function to load the configuration file.
void loadConfigFile(LaneExtractionParams& params);

// Function to read the input arguments.
void readInputArguments(int& argc, char* argv[], LaneExtractionParams& params);

// Function to print out input parameters for checking up.
void printOutInputParameters(const LaneExtractionParams& params);

// Function to list up the bag files by order.
bool listUpPointCloudFiles(LaneExtractionParams& params, std::vector<std::string>& bag_file_list);

// Read the timestamp file.
void readTimeStampFile(const LaneExtractionParams& params, std::vector<double>& timestamps);

// Read OXTS files.
bool readOXTSFiles(const LaneExtractionParams& params, const double& dataset_start_timestamp, const double& dataset_end_timestamp,
  std::string& gpsins_file_name);

bool moveToStartOfLine(std::ifstream& fs);

std::string getLastLineInFile(std::ifstream& fs);

void skipNLinesOfTextFile(std::ifstream& fin, const size_t& n);

double getGPSINSTimestampFromLineString(const std::string line_str);

bool readPointCloudFile(const std::string& file_name, PointCloudf& out_point_cloud);

int main(int argc, char* argv[])
{
  std::cout << std::fixed;
  std::cout.precision(6);

  // Read the config. file.
  LaneExtractionParams params;
  loadConfigFile(params);

  // Read input arguments.
  // If the input arguments exist, overwrite the configurations imported from the existing Yaml file.
  readInputArguments(argc, argv, params);

  //Print out input parameters.
  printOutInputParameters(params);

  // Initialize the ROS node.
  boost::posix_time::ptime timeUTC = boost::posix_time::second_clock::universal_time();
  ros::Time ros_time = ros::Time::fromBoost(timeUTC);
  ros::init(argc, argv, "lane_extraction_" + std::to_string(ros_time.toNSec()));

  // Set the ROS node handle.
  ros::NodeHandle nh;

  // Publishers to display debug info.
  pub_debug_cloud_full = std::make_shared<ros::Publisher>(nh.advertise<sensor_msgs::PointCloud2>("debug_cloud_full", 5));
  pub_debug_lanes = std::make_shared<ros::Publisher>(nh.advertise<sensor_msgs::PointCloud2>("debug_lanes", 5));


  // List up the pointcloud files in a vector of strings by order.
  std::vector<std::string> pointcloud_file_list;
  listUpPointCloudFiles(params, pointcloud_file_list);

  // Get the start and end timestamp of the dataset.
  double dataset_start_timestamp, dataset_end_timestamp;
  double file_time_sec = std::stod(pointcloud_file_list[0].substr(pointcloud_file_list[0].size() - 23, 10));
  double file_time_ns = std::stod(pointcloud_file_list[0].substr(pointcloud_file_list[0].size() - 12, 9));
  dataset_start_timestamp = file_time_sec + 1E-9 * file_time_ns;
  file_time_sec = std::stod(pointcloud_file_list.back().substr(pointcloud_file_list.back().size() - 23, 10));
  file_time_ns = std::stod(pointcloud_file_list.back().substr(pointcloud_file_list.back().size() - 12, 9));
  dataset_end_timestamp = file_time_sec + 1E-9 * file_time_ns;

  // Find corresponding OXTS file
  std::string gpsins_file_path;
  if(!readOXTSFiles(params, dataset_start_timestamp, dataset_end_timestamp, gpsins_file_path))
  {
    std::cout << "Fail to find corresponding GPS/INS file." << std::endl;
    return 0;
  }
  else
  {
    std::cout << "Selected GPS/INS file - " << gpsins_file_path << std::endl;
  }

  // Read the timestamp file.
  int cam_timestamp_index = 0;
  std::vector<double> cam_timestamp;
  readTimeStampFile(params, cam_timestamp);

  // Declare a Velodyne128 parser object.
  VelodyneVLS128Parser velodyne_parser;

  // Initialize the Velodyne parser.
  velodyne_parser.initialize();
  velodyne_parser.setPublishFullScan(false);

  // Register the publish pointcloud callback.
  velodyne_parser.registerPublishCallback(getParsedLidarPointCloudCallback);

  // Declare LidarLaneExtraction object.
  lane_extraction_ptr = std::make_shared<LidarLaneExtraction>(params);

  // Set topics to extract from the rosbag files.
  std::vector<std::string> topics;
  topics.push_back("/network/vls_128_data");
  topics.push_back("/csi_cam/front_center/image_raw");
  topics.push_back("/csi_cam/rear_center/image_raw");


  /* Read and process the pointcloud files one-by-one */
  for(size_t i = 0; i < pointcloud_file_list.size(); ++i)
  {
    if(!ros::ok())
    {
      break;
    }

    // Read the GPS/INS file.
    std::ifstream fin(gpsins_file_path);
    std::string line_str;
    std::getline(fin, line_str); // Skip the first row (names of columns)
    std::getline(fin, line_str);
    double gps_ins_timestamp = getGPSINSTimestampFromLineString(line_str);


    // Get the velodyne packet topics from the bag file and parse them.
    for(const auto& pc_file_path_str : pointcloud_file_list)
    {
      if(!ros::ok())
      {
        break;
      }

      //Read the pointcloud file.
      PointCloudf cloud;
      readPointCloudFile(pc_file_path_str, cloud);

      // Timestamp at the last point
      double pc_timestamp = cloud.points_.back().timestamp;

      // Read and update GPS/INS measurements baed on the Velodyne packet timestamp.
      while(ros::ok())
      {
        // Skip GPS/INS measurements that are too old from current time.
        if(pc_timestamp - gps_ins_timestamp > kVeryOldGPSINSMeasurementSkipTime)
        {
          // Skip about 10 sec.
          fin.ignore(300000);
          fin.ignore(400, '\n');
          std::getline(fin, line_str);
          gps_ins_timestamp = getGPSINSTimestampFromLineString(line_str);
          continue;
        }
        else if(pc_timestamp - gps_ins_timestamp > kOldGPSINSMeasurementSkipTime)
        {
          // Skip about 1 sec.
          fin.ignore(30000);
          fin.ignore(400, '\n');
          std::getline(fin, line_str);
          gps_ins_timestamp = getGPSINSTimestampFromLineString(line_str);

          continue;
        }
        else if(gps_ins_timestamp >= pc_timestamp)
        {
          break;
        }

        std::getline(fin, line_str);
        Tokenizer tok(line_str);
        std::vector<std::string> str_vec;
        str_vec.assign(tok.begin(), tok.end());
        uint64_t gps_time = std::stoul(str_vec[0]);
        gps_ins_timestamp = 1e-9 * (gps_time) + kTimeDifferenceBetweenGPSAndUTCTimes + gps_utc_time_offset_sec;

        SimpleGPSINSMeasurement simple_gpsins;
        convertOXTSCSVLineToSimpleGPSINSMeasurement(str_vec, simple_gpsins);
        lane_extraction_ptr->pushNewGPSINS(simple_gpsins);
      }

//      PclCloudType pcl_cloud;
//      for(const auto& point : cloud.points_)
//      {
//        pcl::PointXYZI pcl_point;
//        pcl_point.x = point.x;
//        pcl_point.y = point.y;
//        pcl_point.z = point.z;
//        pcl_point.intensity = static_cast<float>(point.intensity);
//        pcl_cloud.push_back(pcl_point);
//      }

      lane_extraction_ptr->pushNewPointCloud(cloud);
      publishDebugInfo(lane_extraction_ptr->debug_cloud_full_, lane_extraction_ptr->debug_cloud_lanes_);
      ros::spinOnce();
    }
  }


  return 0;
}


bool convertOXTSCSVLineToSimpleGPSINSMeasurement(const std::vector<std::string>& str_vec, SimpleGPSINSMeasurement& simple_gpsins)
{
  // Check if this measurement is valid.
  if(str_vec[17].compare("") == 0 || str_vec[18].compare("") == 0 || str_vec[25].compare("") == 0 || str_vec[26].compare("") == 0)
  {
    return false;
  }

  // Get the timestamp of this measurement.
  uint64_t gps_time = std::stoul(str_vec[0]);
  double gps_ins_timestamp = 1e-9 * (gps_time) + kTimeDifferenceBetweenGPSAndUTCTimes + gps_utc_time_offset_sec;
  simple_gpsins.timestamp_ = gps_ins_timestamp;

  // Calculate the UTM coordinates.
  double calculated_utm_x, calculated_utm_y;
  long utm_zone;
  char utm_hemisphere;
  Convert_Geodetic_To_UTM(degree_to_radian(std::stod(str_vec[4])),
                          degree_to_radian(std::stod(str_vec[5])),
                          &utm_zone,
                          &utm_hemisphere,
                          &calculated_utm_x, &calculated_utm_y);
  simple_gpsins.utm_x_meters_ = calculated_utm_x;
  simple_gpsins.utm_y_meters_ = calculated_utm_y;
  simple_gpsins.utm_z_meters_ = std::stod(str_vec[6]);

  // Position covariance.
  simple_gpsins.position_covariance_ = std::sqrt(std::pow(std::stod(str_vec[17]), 2) + std::pow(std::stod(str_vec[18]), 2));

  // Velocities.
  simple_gpsins.velocity_x_mps_ = std::stod(str_vec[8]);
  simple_gpsins.velocity_y_mps_ = std::stod(str_vec[7]);
  simple_gpsins.velocity_z_mps_ = -std::stod(str_vec[9]);

  // Ground-plane speed.
  simple_gpsins.xy_speed_mps_ = std::sqrt(std::pow(simple_gpsins.velocity_x_mps_, 2) + std::pow(simple_gpsins.velocity_y_mps_, 2));

  // Euler angles.
  simple_gpsins.roll_rad_  = degree_to_radian(std::stod(str_vec[15]));
  simple_gpsins.pitch_rad_ = degree_to_radian(-std::stod(str_vec[14]));
  simple_gpsins.yaw_rad_   = -degree_to_radian((std::stod(str_vec[13]) + std::stod(str_vec[25]))) + HalfPI;
  if(simple_gpsins.yaw_rad_ > TwoPI)
    simple_gpsins.yaw_rad_ -= TwoPI;
  else if(simple_gpsins.yaw_rad_ < 0.0)
    simple_gpsins.yaw_rad_ +=  TwoPI;

  return true;
}

void getParsedLidarPointCloudCallback(const PointCloudf& point_cloud)
{
  lane_extraction_ptr->pushNewPointCloud(point_cloud);
}

void publishDebugInfo(const pcl::PointCloud<pcl::PointXYZI>& in_cloud_full, const std::vector<pcl::PointCloud<pcl::PointXYZI>>& in_cloud_lanes)
{
  pcl::PointCloud<pcl::PointXYZI> cloud_full = in_cloud_full;
//  for(auto& point : cloud_full.points)
//  {
//    point.y += 40.0f;
//  }
  sensor_msgs::PointCloud2 cloud_full_msg;
  pcl::toROSMsg(cloud_full, cloud_full_msg);

  cloud_full_msg.header.frame_id = "map";
  cloud_full_msg.header.stamp = ros::Time::now();

  pub_debug_cloud_full->publish(cloud_full_msg);


  std::vector<pcl::PointCloud<pcl::PointXYZI>> clustered_cloud_lanes = in_cloud_lanes;
  pcl::PointCloud<pcl::PointXYZI> cloud_lanes;

//  for(const auto cluster : clustered_cloud_lanes)
//  {
//    std::cout << cluster.points[0].x << ", " << cluster.points.back().x << ", "
//              << cluster.points[0].y << ", " << cluster.points.back().y << std::endl;
//  }

  for(size_t i = 0; i < clustered_cloud_lanes.size(); ++i)
  {
    float inten = 255.0f * static_cast<float>(i) / static_cast<float>(clustered_cloud_lanes.size());
    for(auto& point : clustered_cloud_lanes[i].points)
    {
      point.intensity = inten;
    }
    cloud_lanes += clustered_cloud_lanes[i];
  }
  sensor_msgs::PointCloud2 cloud_lane_msg;
  pcl::toROSMsg(cloud_lanes, cloud_lane_msg);

  cloud_lane_msg.header.frame_id = "map";
  cloud_lane_msg.header.stamp = ros::Time::now();

  pub_debug_lanes->publish(cloud_lane_msg);
}

void loadConfigFile(LaneExtractionParams& params)
{
  YAML::Node yaml_node;
  try
  {
    std::cout << "Try to read a yaml file - " << std::string(LIDAR_BASED_VISION_EVALUATION_PARAM_DIR)
                 + "/lane_extraction_map_building_first/lane_extraction_map_building_first_params.yaml" << std::endl;
    yaml_node = YAML::LoadFile(std::string(LIDAR_BASED_VISION_EVALUATION_PARAM_DIR)
                               + "/lane_extraction_map_building_first2/lane_extraction_map_building_first_params.yaml");
  }
  catch (const YAML::Exception& e)
  {
    std::cout << "Fail to read the yaml file." << std::endl;
  }

  params.pointcloud_file_time_range_.resize(2);

  params.pointcloud_folder_ = yaml_node["pointcloud_folder"].as<std::string>();
  params.pointcloud_file_time_range_ = yaml_node["pointcloud_file_time_range"].as<std::vector<double>>();
  params.gps_ins_folder_ = yaml_node["gps_ins_folder"].as<std::string>();
  params.output_folder_ = yaml_node["output_folder"].as<std::string>();
  params.use_timestamp_file_ = yaml_node["use_timestamp_file"].as<bool>();
  params.timestamp_file_path_ = yaml_node["timestamp_file_path"].as<std::string>();
  params.save_result_image_ = yaml_node["save_result_image"].as<bool>();
  params.publish_viz_topics_ = yaml_node["publish_viz_topics"].as<bool>();
  params.gps_ins_measurement_frequency_hz_ = yaml_node["gps_ins_measurement_frequency_hz"].as<uint32_t>();
  params.camera_measurement_frequency_hz_ = yaml_node["camera_measurement_frequency_hz"].as<uint32_t>();
  params.lidar_fov_x_range_meter_ = yaml_node["lidar_fov_x_range_meter"].as<float>();
  params.lidar_fov_y_range_meter_ = yaml_node["lidar_fov_y_range_meter"].as<float>();
  params.lidar_data_max_accumulation_time_sec_ = yaml_node["lidar_data_max_accumulation_time_sec"].as<uint32_t>();
  params.size_local_grid_x_meter_ = yaml_node["size_local_grid_x_meter"].as<uint32_t>();
  params.size_local_grid_y_meter_ = yaml_node["size_local_grid_y_meter"].as<uint32_t>();
  params.num_local_grid_cell_per_meter_ = yaml_node["num_local_grid_cell_per_meter"].as<uint32_t>();
  params.size_clustering_grid_x_meter_ = yaml_node["size_clustering_grid_x_meter"].as<uint32_t>();
  params.size_clustering_grid_y_meter_ = yaml_node["size_clustering_grid_y_meter"].as<uint32_t>();
  params.num_clustering_grid_cell_per_meter_x_ = yaml_node["num_clustering_grid_cell_per_meter_x"].as<uint32_t>();
  params.num_clustering_grid_cell_per_meter_y_ = yaml_node["num_clustering_grid_cell_per_meter_y"].as<uint32_t>();
  params.min_num_points_in_clustering_cell_threshold_ = yaml_node["min_num_points_in_clustering_cell_threshold"].as<uint32_t>();
  params.min_num_points_in_cluster_threshold_ = yaml_node["min_num_points_in_cluster_threshold"].as<uint32_t>();
  params.cluster_connection_check_maximum_distance_ = yaml_node["cluster_connection_check_maximum_distance"].as<float>();
  params.pcd_process_ = yaml_node["pcd_process"].as<int>();
  params.pcd_process_timestamp_ = yaml_node["pcd_process_timestamp"].as<double>();

}

void readInputArguments(int& argc, char* argv[], LaneExtractionParams& params)
{
  std::cout << std::endl;
  std::cout << "Note: Use the parameter file or set command line parameters." << std::endl;
  std::cout << "#### Command line options #### " << std::endl;
  std::cout << "-i [input pointcloud folder]" << std::endl;
  std::cout << "-st [start time of pointcloud reading. (real number in sec.)]" << std::endl;
  std::cout << "-et [end time of pointcloud reading. (real number in sec.)]" << std::endl;
  std::cout << "-g [end index of the rosbag files to process (natural number)]" << std::endl;
  std::cout << "-o [output folder path]" << std::endl;
  std::cout << "-t [timestamp list file path]" << std::endl << std::endl;

  std::cout << "#### Usage example ####" << std::endl;
  std::cout << "lane_extraction_ros -i /home/bagfiles -si 0 -ei 99 -o /home/results -t timestamps.txt" << std::endl;

  if(argc > 1)
  {
    if(params.pointcloud_file_time_range_.size() == 0)
    {
      params.pointcloud_file_time_range_.resize(2);
    }

    for (int i = 1; i < argc; i++)
    {
      std::string str(argv[i]);

      if(str.compare("-i") == 0)
      {
        std::string str(argv[i + 1]);
        params.pointcloud_folder_ = str;
        ++i;
      }
      else if(str.compare("-st") == 0)
      {
        params.pointcloud_file_time_range_[0] = std::stod(argv[i + 1]);
        ++i;
      }
      else if(str.compare("-et") == 0)
      {
        params.pointcloud_file_time_range_[1] = std::stod(argv[i + 1]);
        ++i;
      }
      else if(str.compare("-g") == 0)
      {
        std::string str(argv[i + 1]);
        params.gps_ins_folder_ = str;
        ++i;
      }
      else if(str.compare("-o") == 0)
      {
        std::string str(argv[i + 1]);
        params.output_folder_ = str;
        ++i;
      }
      else if(str.compare("-t") == 0)
      {
        std::string str(argv[i + 1]);
        params.use_timestamp_file_ = true;
        params.timestamp_file_path_ = str;
        ++i;
      }
    }
  }


  // Eliminate '/' character at the end of the folder paths.
  if(params.pointcloud_folder_.back() == '/')
  {
    params.pointcloud_folder_.resize(params.pointcloud_folder_.size() - 1);
  }

  if(params.gps_ins_folder_.back() == '/')
  {
    params.gps_ins_folder_.resize(params.gps_ins_folder_.size() - 1);
  }

  if(params.output_folder_.back() == '/')
  {
    params.output_folder_.resize(params.output_folder_.size() - 1);
  }
}

bool listUpPointCloudFiles(LaneExtractionParams& params, std::vector<std::string>& pointcloud_file_list)
{
  double start_time, end_time;
  if(params.pointcloud_file_time_range_[0] == 0 && params.pointcloud_file_time_range_[1] == 0)
  {
    start_time = 0;
    end_time = 3000000000.0;
  }
  else
  {
    start_time = params.pointcloud_file_time_range_[0];
    end_time = params.pointcloud_file_time_range_[1];
  }

  pointcloud_file_list.clear();
  std::vector<boost::filesystem::path> file_path_vec;

  // Get an ordered list of all the pointcloud files (*.pc) in the designated folder.
  boost::filesystem::path folder_path(params.pointcloud_folder_);
  if(boost::filesystem::exists(folder_path))
  {
    std::copy(boost::filesystem::directory_iterator(folder_path),
      boost::filesystem::directory_iterator(),
      std::back_inserter(file_path_vec));
    std::sort(file_path_vec.begin(), file_path_vec.end());

    pointcloud_file_list.reserve(file_path_vec.size());
    for(const auto& file_path : file_path_vec)
    {
      std::string file_path_str(file_path.c_str());
      std::string file_extention = file_path_str.substr(file_path_str.size() - 2, 2);

      if(file_extention.compare("pc") != 0)
      {
        continue;
      }

      double file_time_sec = std::stod(file_path_str.substr(file_path_str.size() - 23, 10));
      double file_time_ns = std::stod(file_path_str.substr(file_path_str.size() - 12, 9));
      double file_time = file_time_sec + 1E-9 * file_time_ns;

      if(file_time >= start_time && file_time <= end_time)
      {
        pointcloud_file_list.push_back(file_path.c_str());
      }
      else if(file_time > end_time)
      {
        break;
      }
    }
  }
  else
  {
    std::cout << "[Error] The pointcloud folder doesn't exist." << std::endl;
    return false;
  }
}

void printOutInputParameters(const LaneExtractionParams& params)
{
  std::cout << std::endl;
  std::cout << "#### Parameters ####" << std::endl;
  std::cout << "[Rosbag folder path]\t " << params.pointcloud_folder_ << std::endl;

  if(params.pointcloud_file_time_range_.size() == 0)
  {
    std::cout << "[Rosbag file range]\t Incorrect" << std::endl;
  }
  else if(params.pointcloud_file_time_range_[0] == 0 && params.pointcloud_file_time_range_[1] == 0)
  {
    std::cout << "[Rosbag file range]\t All files" << std::endl;
  }
  else
  {
    std::cout << "[Rosbag file range]\t [" << params.pointcloud_file_time_range_[0] << ", "
              << params.pointcloud_file_time_range_[1] << "]" << std::endl;
  }

  std::cout << "[GPS/INS folder path]\t " << params.gps_ins_folder_ << std::endl;

  std::cout << "[Output folder path]\t " << params.output_folder_ << std::endl;

  std::cout << "[Timestamp file]\t " << params.timestamp_file_path_ << std::endl;

  std::cout << std::endl << std::endl;
}

void readTimeStampFile(const LaneExtractionParams& params, std::vector<double>& timestamps)
{
  std::ifstream fin(params.timestamp_file_path_, std::ios::in);

  if(fin.is_open() == false)
  {
    std::cout << "Attempted to read the timestamp file, but the file doesn't exist." << std::endl;
    return;
  }

  uint64_t timestamp;
  while(fin >> timestamp)
  {
    double timestamp_double = 1E-3 * static_cast<double>(timestamp);
    timestamps.push_back(timestamp_double);
  }
  fin.close();

  if(timestamps.size() < 1)
  {
    std::cout << "No timestamp exist in the timestamp file." << std::endl;
  }

  std::sort(timestamps.begin(), timestamps.end());
}

bool readOXTSFiles(const LaneExtractionParams& params, const double& dataset_start_timestamp, const double& dataset_end_timestamp,
  std::string& gpsins_file_name)
{
  /* Read all csv files and check the timestamp.
   * Find the gps/ins file corresponding to the dataset.
   */

  boost::filesystem::path targetDir(params.gps_ins_folder_);
  boost::filesystem::directory_iterator it(targetDir), eod;
  BOOST_FOREACH(boost::filesystem::path const &p, std::make_pair(it, eod))
  {
    double gps_ins_start_time_sec, gps_ins_end_time_sec;

    // Get the paht of the file.
    std::string file_full_path = it->path().string();

    // Skip non-csv files.
    std::string file_extention = file_full_path.substr(file_full_path.size() - 3, 3);
    if(file_extention.compare("csv") != 0)
    {
      continue;
    }

    // Open the file
    std::ifstream fin(file_full_path);
    std::string line_str;

    // Skip the first row which include column names.
    std::getline(fin, line_str);

    // Get the start timestamp of the GPS/INS file.
    std::getline(fin, line_str);
    Tokenizer tok(line_str);
    std::vector<std::string> str_vec;
    str_vec.assign(tok.begin(), tok.end());

    if(str_vec.size() < 20) // Skip invalid GPS/INS file.
    {
      continue;
    }

    gps_utc_time_offset_sec = std::stoul(str_vec[2]); // GPS to UTC time offset. This is provided by OXTS device.
    uint64_t gps_time = std::stoul(str_vec[0]);
    gps_ins_start_time_sec = 1e-9 * gps_time + kTimeDifferenceBetweenGPSAndUTCTimes + gps_utc_time_offset_sec;

    // Get the end timestamp of the GPS/INS file.
    line_str = getLastLineInFile(fin);    
    gps_ins_end_time_sec = getGPSINSTimestampFromLineString(line_str);

    fin.close();

    if(gps_ins_start_time_sec < dataset_start_timestamp && gps_ins_end_time_sec > dataset_end_timestamp)
    {      
      gpsins_file_name = file_full_path;
      return true;
    }
  }

  return false;
}

bool moveToStartOfLine(std::ifstream& fs)
{
  fs.seekg(-1, std::ios_base::cur);
  for(long i = fs.tellg(); i > 0; i--)
  {
    if(fs.peek() == '\n')
    {
      fs.get();
      return true;
    }
    fs.seekg(i, std::ios_base::beg);
  }
  return false;
}

std::string getLastLineInFile(std::ifstream& fs)
{
  // Go to the last character before EOF
  fs.seekg(-1, std::ios_base::end);
  if (!moveToStartOfLine(fs))
    return "";

  std::string lastline = "";
  getline(fs, lastline);
  return lastline;
}

void skipNLinesOfTextFile(std::ifstream& fin, const size_t& n)
{
  size_t i = 0;
  while(i++ < n)
  {
    fin.ignore(400, '/n');
  }
}

double getGPSINSTimestampFromLineString(const std::string line_str)
{
  Tokenizer tok(line_str);
  std::vector<std::string> str_vec;
  str_vec.assign(tok.begin(), tok.end());

  uint64_t gps_time = std::stoul(str_vec[0]);
  return 1e-9 * gps_time + kTimeDifferenceBetweenGPSAndUTCTimes + gps_utc_time_offset_sec;
}

bool readPointCloudFile(const std::string& file_name, PointCloudf& out_point_cloud)
{
  // Get the timestamp at the first point.
  double file_time_sec = std::stod(file_name.substr(file_name.size() - 23, 10));
  double file_time_ns = std::stod(file_name.substr(file_name.size() - 12, 9));
  double timestamp_at_first_point = file_time_sec + 1E-9 * file_time_ns;

  // Read the file.
  std::ifstream fin(file_name, std::ios::in | std::ios::binary);

  // Get length of the file:
  fin.seekg (0, std::ios::end);
  int length = fin.tellg();
  fin.seekg (0, std::ios::beg);

  // Copy to a buffer.
  //char* buff = new char[length];
  std::vector<char> buff(length);
  fin.read(buff.data(), length);
  fin.close();

  // Copy to the out pointcloud.
  uint32_t num_points = length / 17;
  out_point_cloud.resize(num_points);
  int pos = 0;
  for(size_t i = 0; i < num_points; ++i)
  {
    std::memcpy(&(out_point_cloud.points_[i].x), &buff[pos], 4);
    std::memcpy(&(out_point_cloud.points_[i].y), &buff[pos + 4], 4);
    std::memcpy(&(out_point_cloud.points_[i].z), &buff[pos + 8], 4);
    std::memcpy(&(out_point_cloud.points_[i].intensity), &buff[pos + 12], 1);

    uint32_t time_offset_nsec;
    std::memcpy(&time_offset_nsec, &buff[pos + 13], 4);
    out_point_cloud.points_[i].timestamp = timestamp_at_first_point + 1E-9 * time_offset_nsec;

    pos += 17;
  }

  return true;
}




