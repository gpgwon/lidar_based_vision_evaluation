#include <lane_extraction/lane_extraction.h>
#include <pcl/io/pcd_io.h>
#include <array>

#define kImprobableBigClusterIndexNumber 9999
const std::array<uint32_t, 128> vls128_channel_vertical_angle_order =
{36, 69, 54, 87, 0, 97, 18, 115, 44, 77, 62, 95, 8, 105, 26, 123, 100, 21, 118, 39, 64, 49, 82, 3, 108, 29, 126, 47,
 72, 57, 90, 11, 52, 85, 6, 103, 16, 113, 34, 67, 60, 93, 14, 111, 24, 121, 42, 75, 116, 37, 70, 55, 80, 1, 98, 19,
 124, 45, 78, 63, 88, 9, 106, 27, 4, 101, 22, 119, 32, 65, 50, 83, 12, 33, 109, 30, 127, 40, 73, 58, 91, 68, 53, 86,
 7, 96, 17, 114, 35, 76, 61, 94, 15, 104, 25, 122, 43, 20, 117, 38, 71, 48, 81, 2, 99, 28, 125, 46, 79, 56, 89, 10,
  107, 84, 5, 102, 23, 112, 66, 51, 92, 13, 110, 31, 120, 41, 74, 59};

Eigen::Matrix4f getRotMatrix(double roll, double pitch, double yaw);
Eigen::Matrix4f getTransformMatrix(double x, double y, double z, double roll, double pitch, double yaw);
pcl::PointXYZI doInverseRotation(const pcl::PointXYZI& p, const double& roll, const double& pitch, const double& yaw);
float xyDistanceBetweenTwoPclPoints(const pcl::PointXYZI& p1, const pcl::PointXYZI& p2);
void convertPointCloudfToPclPointCloud(const PointCloudf& cloudf, pcl::PointCloud<pcl::PointXYZI>& pcl_cloud);

bool point_comp_in_x_direction(const pcl::PointXYZI& p1, const pcl::PointXYZI& p2);
void xyToImageuv(const float& x, const float& y, int& u, int& v);

LidarLaneExtraction::LidarLaneExtraction(const LaneExtractionParams& params)
  : pcd_file_saved_(false),
    pos_offset_set_(false),
    params_(params)
{
  oxts_buffer_.set_capacity((params_.gps_ins_measurement_frequency_hz_ * params_.lidar_data_max_accumulation_time_sec_));
  pointcloud_buffer_.set_capacity((VLS128_NUM_BLOCKS_PER_SEC * params_.lidar_data_max_accumulation_time_sec_));
  front_cam_time_stamp_buffer_.set_capacity((params_.camera_measurement_frequency_hz_ * params_.lidar_data_max_accumulation_time_sec_));
  yaw_bias_buff_.set_capacity(300);

  local_grid_size_x_ = params_.size_local_grid_x_meter_ * params_.num_local_grid_cell_per_meter_ + 1;
  local_grid_size_y_ = params_.size_local_grid_y_meter_ * params_.num_local_grid_cell_per_meter_ + 1;

  pcl::console::setVerbosityLevel(pcl::console::L_ALWAYS);
}

LidarLaneExtraction::~LidarLaneExtraction()
{
}

void LidarLaneExtraction::pushNewPointCloud(const PointCloudf& input)
{
  PointCloudf lane_marking_candidates;

  convertPointCloudfToPclPointCloud(input, debug_cloud_full_);
  return;

  extractLaneMarkingCandidates(input, lane_marking_candidates);

//  PclCloudType cloud_global;
//  for(size_t i = 0; i < lane_marking_candidates.getSize(); ++i)
//  {
//    SimpleGPSINSMeasurement oxts0, oxts1;
//    for(size_t j = 0; j < oxts_buffer_.size() - 1; ++j)
//    {
//      if(lane_marking_candidates.points_[i].timestamp >= oxts_buffer_[j + 1].timestamp_
//         && lane_marking_candidates.points_[i].timestamp < oxts_buffer_[j].timestamp_)
//      {
//        oxts0 = oxts_buffer_[j];
//        oxts1 = oxts_buffer_[j + 1];
//        break;
//      }
//    }

//    double oxts_time_gap = oxts0.timestamp_ - oxts1.timestamp_;

//    // A factor to calculate arithmatical average between two GPS/INS measurements to calculate GPS/INS measurement at point time.
//    double r = (oxts0.timestamp_ - lane_marking_candidates.points_[i].timestamp) / oxts_time_gap;
//    SimpleGPSINSMeasurement gpsins_at_point = (1 - r) * oxts0 + r * oxts1;


//    const float& x = lane_marking_candidates.points_[i].x;
//    const float& y = lane_marking_candidates.points_[i].y;
//    const float& z = lane_marking_candidates.points_[i].z;

//    pcl::PointXYZI pcl_point;
//    // Transform the points to the UTM coordinate sytem.
//    Eigen::Vector4f translation(
//      static_cast<float>(gpsins_at_point.utm_x_meters_ - pos_offset_[0]),
//      static_cast<float>(gpsins_at_point.utm_y_meters_ - pos_offset_[1]),
//      static_cast<float>(gpsins_at_point.utm_z_meters_ - pos_offset_[2]), 0);
//    Eigen::Matrix4f rotation = getRotMatrix(gpsins_at_point.roll_rad_, gpsins_at_point.pitch_rad_, gpsins_at_point.yaw_rad_);
//    Eigen::Vector4f point_local(lane_marking_candidates.points_[i].x, lane_marking_candidates.points_[i].y, lane_marking_candidates.points_[i].z, 1);
//    Eigen::Vector4f point_world = rotation * point_local + translation;

//    pcl_point.x = point_world[0];
//    pcl_point.y = point_world[1];
//    pcl_point.z = point_world[2];
//    pcl_point.intensity = static_cast<float>(lane_marking_candidates.points_[i].intensity);

//    cloud_global.push_back(pcl_point);
//  }


//  debug_cloud_full_ += cloud_global;
}

void LidarLaneExtraction::pushNewGPSINS(const SimpleGPSINSMeasurement& input)
{
  SimpleGPSINSMeasurement _input = input;

  if(pos_offset_set_ == false)
  {
    pos_offset_ << input.utm_x_meters_, input.utm_y_meters_, input.utm_z_meters_;
    pos_offset_set_ = true;
  }

  //  double avg_bias(0.0);
  //  if(oxts_buffer_.size() > 0)
  //  {
  //    double estimated_yaw = std::atan2(_input.utm_y_meters_ - oxts_buffer_[0].utm_y_meters_, _input.utm_x_meters_ - oxts_buffer_[0].utm_x_meters_);

  //    if(estimated_yaw < 0)
  //    {
  //      estimated_yaw += 2 * M_PI;
  //    }

  //    double yaw_bias = estimated_yaw - _input.yaw_rad_;
  //    if(yaw_bias > M_PI)
  //    {
  //      yaw_bias -= 2 * M_PI;
  //    }
  //    else if(yaw_bias < -M_PI)
  //    {
  //      yaw_bias += 2 * M_PI;
  //    }

  //    yaw_bias_buff_.push_back(yaw_bias);

  //    for(size_t i = 0; i < yaw_bias_buff_.size(); ++i)
  //    {
  //      avg_bias += yaw_bias_buff_[i];
  //    }
  //    avg_bias = avg_bias / static_cast<double>(yaw_bias_buff_.size());

  //    std::cout << "Calculated yaw offset: " << avg_bias << std::endl;

  //  }
  //      {
  //        if(0.3 * cloud_43ch.points_[j - 1].intensity > cloud_43ch.points_[j].intensity)
  //        {
  //          if(j - peak_start_index < 10)

  //  _input.yaw_rad_ += avg_bias;
  oxts_buffer_.push_front(_input);
}

void LidarLaneExtraction::extractLaneMarkingCandidates(const PointCloudf& cloud, PointCloudf& lane_marking_candidates)
{
  /* Bring up only bottom 43 channels. Upper channels are not useful for lane extraction.
   */
  PointCloudf debug_cloud;
  std::vector<PointCloudf> cloud_43ch;
  cloud_43ch.resize(43);
  uint32_t num_points_per_ch = cloud.getSize() / 128;
  for(size_t i = 1; i < 44; ++i) // Discard the first channel (i == 0) points that the intensity values are incorrect.
  {
    cloud_43ch[i - 1].reserve(num_points_per_ch);
    const uint32_t& ch_num = vls128_channel_vertical_angle_order[i];
    for(size_t j = 0; j < num_points_per_ch; ++j)
    {
      cloud_43ch[i - 1].push_back(cloud.points_[ch_num + 128 * j]);
    }
  }


  /* Extract points that show the shape of a peak from an intensity point of view.
   */
  lane_marking_candidates.clear();
  for(size_t i = 0; i < 43; ++i)
  {
    uint32_t ch_start_index = num_points_per_ch * i;
    bool peak_start(false);
    uint32_t peak_start_index;

    for(size_t j = 10; j < num_points_per_ch - 10; ++j)
    {
      float avg_intensity(0.0f);
      for(int k = -10; k <= 10; ++k)
      {
        int index = static_cast<int>(j) + k;
        avg_intensity += cloud_43ch[i].points_[index].intensity;
      }
      avg_intensity /= 21.0f;

      if(cloud_43ch[i].points_[j].intensity > avg_intensity
        && cloud_43ch[i].points_[j].y > -8.0f && cloud_43ch[i].points_[j].y < 8.0f)
      {
        lane_marking_candidates.push_back(cloud_43ch[i].points_[j]);
      }
//      if(peak_start == false)
//      {
//        if(0.3 * cloud_43ch[i].points_[j].intensity > cloud_43ch[i].points_[j - 1].intensity
//           && cloud_43ch[i].points_[j].intensity > 3.0f)
//        {
//          peak_start = true;
//          peak_start_index = j;
//        }
//      }
//      else
//      {
//        if(0.3 * cloud_43ch[i].points_[j - 1].intensity > cloud_43ch[i].points_[j].intensity)
//        {
//          if((j - peak_start_index) >= 3 && (j - peak_start_index) < 10)
//          {
//            for(size_t k = peak_start_index; k < j; ++k)
//            {
//              if(cloud_43ch[i].points_[k].y > -8.0f && cloud_43ch[i].points_[k].y < 8.0f)
//              {
//                lane_marking_candidates.push_back(cloud_43ch[i].points_[k]);
//              }

//            }
//          }
//          peak_start = false;
//        }
//      }
    }
  }

  /* Filter out nonground points.
   */
  std::vector<std::vector<LocalGridCell>> local_grid;
  int grid_size_x = 80 * 2 + 1;
  int grid_size_y = 20 * 5 + 1;

  local_grid.resize(grid_size_x);
  for(auto& grid_vec_y : local_grid)
  {
    grid_vec_y.resize(grid_size_y);
  }

  for(const auto& scan : cloud_43ch)
  {
    for(const auto& point : scan.points_)
    {
      // Skip useless points.
      if((point.x > -2.5f && point.x < 3.0f && point.y > -1.0f && point.y < 1.0f)
        ||(point.y < -10.0f || point.y > 10.0f)
        ||(point.z > 2.0))
      {
        continue;
      }

      int x_index, y_index;
      xyToNonGroundObjectFilteringGridIndex(point.x, point.y, x_index, y_index);
      if(x_index >= 0 && x_index < grid_size_x
         && y_index >= 0 && y_index < grid_size_y)
      {
        local_grid[x_index][y_index].pointsf_.push_back(point);
      }
    }
  }


  // Calculate maximum, minimum z values of the points in each cell.
  for(size_t grid_x = 0; grid_x < grid_size_x; ++grid_x)
  {
    for(size_t grid_y = 0; grid_y < grid_size_y; ++grid_y)
    {

      for(size_t i = 0; i < local_grid[grid_x][grid_y].pointsf_.getSize(); ++i)
      {
        if(local_grid[grid_x][grid_y].pointsf_.points_[i].z < local_grid[grid_x][grid_y].min_z_)
        {
          local_grid[grid_x][grid_y].min_z_ = local_grid[grid_x][grid_y].pointsf_.points_[i].z;
        }

        if(local_grid[grid_x][grid_y].pointsf_.points_[i].z > local_grid[grid_x][grid_y].max_z_)
        {
          local_grid[grid_x][grid_y].max_z_ = local_grid[grid_x][grid_y].pointsf_.points_[i].z;
        }
      }

      if(local_grid[grid_x][grid_y].max_z_ - local_grid[grid_x][grid_y].max_z_ < 0.1f)
      {
        local_grid[grid_x][grid_y].is_ground_ = true;
      }
    }
  }

  PointCloudf temp_cloud = lane_marking_candidates;
  lane_marking_candidates.clear();
  for(const auto& point : temp_cloud.points_)
  {
    int x_index, y_index;
    xyToNonGroundObjectFilteringGridIndex(point.x, point.y, x_index, y_index);

    if(x_index >= 0 && x_index < grid_size_x
       && y_index >= 0 && y_index < grid_size_y
       && local_grid[x_index][y_index].is_ground_ == true)
    {
      lane_marking_candidates.push_back(point);
    }
  }

  convertPointCloudfToPclPointCloud(lane_marking_candidates, debug_cloud_full_);
}


PclCloudType LidarLaneExtraction::transformPointCloudToGlobalCoordinates(const PointCloudf& input_cloud)
{
  // PCL format pointcloud to return.
  PclCloudType ret_cloud;

  // Initialize the return pointcloud.
  size_t num_points = input_cloud.getSize();
  ret_cloud.resize(num_points);
  ret_cloud.header.stamp = static_cast<uint64_t>(1E6 * input_cloud.points_[0].timestamp);

  // Nearest two GPS/INS measurements.
  SimpleGPSINSMeasurement oxts0 = oxts_buffer_[0];
  SimpleGPSINSMeasurement oxts1 = oxts_buffer_[1];

  // Oxts time gap.
  double oxts_time_gap = oxts0.timestamp_ - oxts1.timestamp_;

  // GPS/INS at point measurement time.
  SimpleGPSINSMeasurement gpsins_at_point;


  // Point-wise local-to-global transformation.
  #pragma omp parallel for
  for(size_t i = 0; i < num_points; ++i)
  {
    // Eight points are acquired in one instant, so we can calculate GPS/INS every eight times.
    if(i % 8 == 0)
    {
      // A factor to calculate arithmatical average between two GPS/INS measurements to calculate GPS/INS measurement at point time.
      double r = (oxts0.timestamp_ - input_cloud.points_[i].timestamp) / oxts_time_gap;
      gpsins_at_point = (1 - r) * oxts0 + r * oxts1;
    }

    const float& x = input_cloud.points_[i].x;
    const float& y = input_cloud.points_[i].y;
    const float& z = input_cloud.points_[i].z;

    pcl::PointXYZI pcl_point;

    // Ignore the points corresponding to the ego-vehicle body.
    if(x > -2.5f && x < 3.0f && y > -1.0f && y < 1.0f)
    {
      pcl_point.x = 0;
      pcl_point.y = 0;
      pcl_point.z = 0;
      pcl_point.intensity = -1;
    }

    // Ignore distant or non-ground points.
    else if(z <= -5.0f || z >= 5.0f || x <= -120.0f || x >= 120.0f || y <= -30.0f || y >= 30.0f)
    {
      pcl_point.x = 0;
      pcl_point.y = 0;
      pcl_point.z = 0;
      pcl_point.intensity = -1;
    }

    else
    {
      // Transform the points to the UTM coordinate sytem.
      Eigen::Vector4f translation(
        static_cast<float>(gpsins_at_point.utm_x_meters_ - pos_offset_[0]),
        static_cast<float>(gpsins_at_point.utm_y_meters_ - pos_offset_[1]),
        static_cast<float>(gpsins_at_point.utm_z_meters_ - pos_offset_[2]), 0);
      Eigen::Matrix4f rotation = getRotMatrix(gpsins_at_point.roll_rad_, gpsins_at_point.pitch_rad_, gpsins_at_point.yaw_rad_);
      Eigen::Vector4f point_local(input_cloud.points_[i].x, input_cloud.points_[i].y, input_cloud.points_[i].z, 1);
      Eigen::Vector4f point_world = rotation * point_local + translation;

      pcl_point.x = point_world[0];
      pcl_point.y = point_world[1];
      pcl_point.z = point_world[2];
      pcl_point.intensity = static_cast<float>(input_cloud.points_[i].intensity);
    }

    ret_cloud.points[i] = pcl_point;
  }
  return ret_cloud;
}

void LidarLaneExtraction::extractLanesAtGivenTimestamp(const double& timestamp)
{
  std::cout << "Extract lanes at " << timestamp << std::endl;

  size_t num_points;
  PclCloudType cloud;

  /* Calculate the GPS/INS measurement at the timestamp
   */
  SimpleGPSINSMeasurement oxts0, oxts1;
  for(size_t i = 1; i < oxts_buffer_.size(); ++i)
  {
    if(timestamp > oxts_buffer_[i].timestamp_)
    {
      oxts1 = oxts_buffer_[i];
      oxts0 = oxts_buffer_[i - 1];
      break;
    }
  }
  double oxts_time_gap = oxts0.timestamp_ - oxts1.timestamp_;
  SimpleGPSINSMeasurement gpsins_at_cam_timestamp;
  double r = (oxts0.timestamp_ - timestamp) / oxts_time_gap;
  gpsins_at_cam_timestamp = (1 - r) * oxts0 + r * oxts1;

  if(params_.pcd_process_ == 2)
  {
    std::cout << "The PCD file was loaded." << std::endl;
    pcl::io::loadPCDFile("cloud.pcd", cloud);
  }
  else
  {
    /* Aggregate the pointclouds.
     */
    for(size_t i = 0; i < pointcloud_buffer_.size(); ++i)
    {
      cloud += pointcloud_buffer_[i];
    }

    if(params_.pcd_process_ == 1)
    {
      if(fabs(timestamp - params_.pcd_process_timestamp_) < 0.001)
      {
        pcl::io::savePCDFileBinary("cloud.pcd", cloud);
        std::fstream fout("pcd_timestamp.txt");
        fout << timestamp;
        fout.close();
        std::cout << "A PCD file was saved" << std::endl;
        pcd_file_saved_ = true;
      }

      return;
    }
  }

  num_points = cloud.size();


  /* Extract ground points.
   */
  // Initialize a local grid.
  std::vector<std::vector<LocalGridCell>> local_grid;
  local_grid.resize(local_grid_size_x_);
  for(size_t i = 0; i < local_grid.size(); ++i)
  {
    local_grid[i].resize(local_grid_size_y_);
  }

  // Current vehicle position.
  double veh_pos_x = gpsins_at_cam_timestamp.utm_x_meters_ - pos_offset_[0];
  double veh_pos_y = gpsins_at_cam_timestamp.utm_y_meters_ - pos_offset_[1];
  double veh_pos_z = gpsins_at_cam_timestamp.utm_y_meters_ - pos_offset_[2];

  // Build a local grid and fill the points.
  for(size_t i = 0; i < num_points; ++i)
  {
    // Skip useless points.
    if((cloud.points[i].x == 0 && cloud.points[i].y == 0)
       || cloud.points[i].intensity == -1 || i % 128 == 36)
    {
      continue;
    }

    int x_index, y_index;
    float x = cloud.points[i].x - veh_pos_x;
    float y = cloud.points[i].y - veh_pos_y;
    float z = cloud.points[i].z - veh_pos_z;

    if(fabs(x) < static_cast<float>(params_.size_local_grid_x_meter_)
      && fabs(y) < static_cast<float>(params_.size_local_grid_y_meter_))
    {
      xyToLocalGridIndex(x, y, x_index, y_index);
      if(x_index >= 0 && x_index < local_grid_size_x_
         && y_index >= 0 && y_index < local_grid_size_y_)
      {
        local_grid[x_index][y_index].points_.push_back(cloud.points[i]);
      }
    }
  }


  // Calculate maximum, minimum z values of the points in each cell.
  for(size_t grid_x = 0; grid_x < local_grid_size_x_; ++grid_x)
  {
    for(size_t grid_y = 0; grid_y < local_grid_size_y_; ++grid_y)
    {
      float sum_z = 0.0f;

      for(size_t i = 0; i < local_grid[grid_x][grid_y].points_.size(); ++i)
      {
        sum_z += local_grid[grid_x][grid_y].points_[i].z;
        if(local_grid[grid_x][grid_y].points_[i].z < local_grid[grid_x][grid_y].min_z_)
        {
          local_grid[grid_x][grid_y].min_z_ = local_grid[grid_x][grid_y].points_[i].z;
        }

        if(local_grid[grid_x][grid_y].points_[i].z > local_grid[grid_x][grid_y].max_z_)
        {
          local_grid[grid_x][grid_y].max_z_ = local_grid[grid_x][grid_y].points_[i].z;
        }
      }
    }
  }


  // Remove high-height points within each cell
  for(size_t grid_x = 0; grid_x < local_grid_size_x_; ++grid_x)
  {
    for(size_t grid_y = 0; grid_y < local_grid_size_y_; ++grid_y)
    {
      PclCloudType acceptable_points;
      for(size_t i = 0; i < local_grid[grid_x][grid_y].points_.size(); ++i)
      {
        if(local_grid[grid_x][grid_y].points_[i].z < (local_grid[grid_x][grid_y].min_z_ + 0.05))
        {
          acceptable_points.push_back(local_grid[grid_x][grid_y].points_[i]);
        }
      }

      if(acceptable_points.size() > 0)
      {
        local_grid[grid_x][grid_y].points_ = acceptable_points;
      }
      else
      {
        local_grid[grid_x][grid_y].points_.clear();
      }
    }
  }

  // Recalculate the maximum z in each cell, and calculate the mean z of the cell.
  for(size_t grid_x = 0; grid_x < local_grid_size_x_; ++grid_x)
  {
    for(size_t grid_y = 0; grid_y < local_grid_size_y_; ++grid_y)
    {
      local_grid[grid_x][grid_y].max_z_ = -9999.0f;

      for(size_t i = 0; i < local_grid[grid_x][grid_y].points_.size(); ++i)
      {
        local_grid[grid_x][grid_y].mean_z_ += local_grid[grid_x][grid_y].points_[i].z;

        if(local_grid[grid_x][grid_y].points_[i].z > local_grid[grid_x][grid_y].max_z_)
        {
          local_grid[grid_x][grid_y].max_z_ = local_grid[grid_x][grid_y].points_[i].z;
        }
      }

      local_grid[grid_x][grid_y].mean_z_ /= static_cast<float>(local_grid[grid_x][grid_y].points_.size());
    }
  }


  // Find ground cells using cell propagation method.
  // Firstly select a ground cell.
  int max_num_of_cells_to_search = 2 * params_.num_local_grid_cell_per_meter_;
  int x_index_at_veh_pos = (local_grid_size_x_ - 1) / 2;
  int y_index_at_veh_pos = (local_grid_size_y_ - 1) / 2;

  CellIndices seed_gnd_cell_indices;
  bool gnd_cell_found(false);
  for(int grid_x = x_index_at_veh_pos - max_num_of_cells_to_search; grid_x <= x_index_at_veh_pos + max_num_of_cells_to_search; ++grid_x)
  {
    for(int grid_y = y_index_at_veh_pos - max_num_of_cells_to_search; grid_y <= y_index_at_veh_pos + max_num_of_cells_to_search; ++grid_y)
    {
      bool is_ground(true);
      for(int i = -1; i <= 1; ++i)
      {
        for(int j = -1; j <= 1; ++j)
        {
          if(i == 0 && j == 0)
          {
            continue;
          }

          if(fabs(local_grid[grid_x][grid_y].min_z_ - local_grid[grid_x + i][grid_y + j].max_z_) > 0.05f
            && fabs(local_grid[grid_x][grid_y].max_z_ - local_grid[grid_x + i][grid_y + j].min_z_) > 0.05f)
          {
            is_ground = false;
            break;
          }
        }
        if(is_ground == false)
        {
          break;
        }
      }

      if(is_ground == true)
      {
        seed_gnd_cell_indices.x_index_ = grid_x;
        seed_gnd_cell_indices.y_index_ = grid_y;
        gnd_cell_found = true;
        local_grid[grid_x][grid_y].is_ground_ = true;
        break;
      }
    }

    if(gnd_cell_found == true)
    {
      break;
    }
  }

  // Find ground cells starting from the seed ground cell.
  std::vector<CellIndices> found_gnd_cells;
  std::vector<CellIndices> newly_found_gnd_cells;
  std::vector<CellIndices> gnd_cells_at_prev_step;
  found_gnd_cells.push_back(seed_gnd_cell_indices);
  gnd_cells_at_prev_step.push_back(seed_gnd_cell_indices);

  while(true)
  {
    for(const auto cell_indices : gnd_cells_at_prev_step)
    {
      for(int i = -1; i <= 1; ++i)
      {
        for(int j = -1; j <= 1; ++j)
        {
          if((i == 0 && j == 0)
            || (cell_indices.x_index_ + i) < 0 || (cell_indices.x_index_ + i) >= local_grid_size_x_
            || (cell_indices.y_index_ + j) < 0 || (cell_indices.y_index_ + j) >= local_grid_size_y_)
          {
            continue;
          }
          else if(local_grid[cell_indices.x_index_ + i][cell_indices.y_index_ + j].is_ground_ == true)
          {
             continue;
          }

          if(fabs(local_grid[cell_indices.x_index_][cell_indices.y_index_].min_z_ - local_grid[cell_indices.x_index_ + i][cell_indices.y_index_ + j].max_z_) < 0.05f
            || fabs(local_grid[cell_indices.x_index_][cell_indices.y_index_].max_z_ - local_grid[cell_indices.x_index_ + i][cell_indices.y_index_ + j].min_z_) < 0.05f)
//          if(fabs(local_grid[cell_indices.x_index_][cell_indices.y_index_].mean_z_ - local_grid[cell_indices.x_index_ + i][cell_indices.y_index_ + j].mean_z_) < 0.1f)
          {
            local_grid[cell_indices.x_index_ + i][cell_indices.y_index_ + j].is_ground_ = true;
            newly_found_gnd_cells.emplace_back(cell_indices.x_index_ + i, cell_indices.y_index_ + j);
          }
        }
      }
    }

    if(newly_found_gnd_cells.size() == 0)
    {
      break;
    }

    found_gnd_cells.insert(found_gnd_cells.end(), newly_found_gnd_cells.begin(), newly_found_gnd_cells.end());
    gnd_cells_at_prev_step = newly_found_gnd_cells;
    newly_found_gnd_cells.clear();
  }


  PclCloudType disp_cloud;
  for(size_t grid_x = 0; grid_x < local_grid_size_x_; ++grid_x)
  {
    for(size_t grid_y = 0; grid_y < local_grid_size_y_; ++grid_y)
    {
      if(local_grid[grid_x][grid_y].is_ground_)
      {
        disp_cloud += local_grid[grid_x][grid_y].points_;
      }
    }
  }


  /* Extract lane candidates. */
  PclCloudType lane_marking_candidates;
  for(int i = 0; i < num_points; ++i)
  {
    if(cloud.points[i].intensity == -1 || (i % 128) == 36)
    {
      continue;
    }

    float x = cloud.points[i].x - veh_pos_x;
    float y = cloud.points[i].y - veh_pos_y;

    if(fabs(x) >= 150.0f || fabs(y) >= 150.0f)
      continue;

    int idx_x, idx_y;
    xyToLocalGridIndex(x, y, idx_x, idx_y);

    if(local_grid[idx_x][idx_y].is_ground_ != true || fabs(local_grid[idx_x][idx_y].min_z_ - cloud.points[i].z) > 0.1)
    {
      continue;
    }

    bool skip_point(false);
    if(i > 128 * 50 && i < num_points - 128 * 50 - 1)
    {
      float avg_intensity(0.0f);
      int added_point_cnt(0);
      for(int j = -1; j > -50; --j)
      {
        if(cloud.points[i + 128 * j].intensity < 0)
        {
          continue;
        }

        float xy_distance_bw_two_points = xyDistanceBetweenTwoPclPoints(cloud.points[i], cloud.points[i + 128 * j]);
        if(xy_distance_bw_two_points >= 0.15)
        {
          avg_intensity += static_cast<float>(cloud.points[i + 128 * j].intensity);
          added_point_cnt++;
        }
        else if(xy_distance_bw_two_points >= 0.2)
        {
          avg_intensity += static_cast<float>(cloud.points[i + 128 * j].intensity);
          added_point_cnt++;
        }
        else if(xy_distance_bw_two_points > 0.25)
        {
          avg_intensity += static_cast<float>(cloud.points[i + 128 * j].intensity);
          added_point_cnt++;
          break;
        }
      }

      for(int j = 1; j < 50; ++j)
      {
        if(cloud.points[i + 128 * j].intensity < 0)
        {
          continue;
        }

        float xy_distance_bw_two_points = xyDistanceBetweenTwoPclPoints(cloud.points[i], cloud.points[i + 128 * j]);
        if(xy_distance_bw_two_points >= 0.15)
        {
          avg_intensity += static_cast<float>(cloud.points[i + 128 * j].intensity);
          added_point_cnt++;
        }
        else if(xy_distance_bw_two_points >= 0.2)
        {
          avg_intensity += static_cast<float>(cloud.points[i + 128 * j].intensity);
          added_point_cnt++;
        }
        else if(xy_distance_bw_two_points > 0.25)
        {
          avg_intensity += static_cast<float>(cloud.points[i + 128 * j].intensity);
          added_point_cnt++;
          break;
        }
      }

      if(added_point_cnt > 0)
      {
        avg_intensity /= static_cast<float>(added_point_cnt);
      }
      else
      {
        continue;
      }


//      for(int j = -5; j <= 5; ++j)
//      {
//        if(j >= -1 && j <= 1)
//        {
//          continue;
//        }

//        float inten = cloud.points[i + 128 * j].intensity;
//        if(inten < 0)
//        {
//          inten = 0;
//          skip_point = true;
//          break;
//        }
//        avg_intensity += static_cast<float>(inten);
//      }

//      if(skip_point)
//      {
//        continue;
//      }

//      avg_intensity /= 8.0f;

      if(0.1 * cloud.points[i].intensity > avg_intensity
         && cloud.points[i].intensity > 0.0f)
//         && avg_intensity < 20.0f)
      {
        //cloud_global.points[i].intensity = 255;
        lane_marking_candidates.push_back(cloud.points[i]);
      }
    }
  }


//  PclCloudType temp_cloud;
//  for(size_t i = 0; i < lane_marking_candidates.size(); ++i)
//  {
//    float x = lane_marking_candidates.points[i].x - veh_pos_x;
//    float y = lane_marking_candidates.points[i].y - veh_pos_y;

//    if(fabs(x) >= 150.0f || fabs(y) >= 150.0f)
//      continue;

//    int idx_x, idx_y;
//    xyToLocalGridIndex(x, y, idx_x, idx_y);

//    if(local_grid[idx_x][idx_y].is_ground_ == true && fabs(local_grid[idx_x][idx_y].mean_z_ - lane_marking_candidates.points[i].z) < 0.1f)
//    {
//      temp_cloud.push_back(lane_marking_candidates.points[i]);
//    }
//  }

//  cloud = lane_marking_candidates;

  pcl::RadiusOutlierRemoval<pcl::PointXYZI> outrem;
  outrem.setInputCloud(PclCloudType::ConstPtr(new PclCloudType(lane_marking_candidates)));
  outrem.setRadiusSearch(0.2);
  outrem.setMinNeighborsInRadius (5);
  outrem.filter (cloud);


  /* Transform to vehicle coordinates at camera timestamp. */
  Eigen::Vector4f translation(gpsins_at_cam_timestamp.utm_x_meters_ - pos_offset_[0],
      gpsins_at_cam_timestamp.utm_y_meters_ - pos_offset_[1], gpsins_at_cam_timestamp.utm_z_meters_ - pos_offset_[2], 0);
  Eigen::Matrix4f rotation = getRotMatrix(-gpsins_at_cam_timestamp.roll_rad_, -gpsins_at_cam_timestamp.pitch_rad_, -gpsins_at_cam_timestamp.yaw_rad_);

  num_points = cloud.size();
  PclCloudType temp_cloud = cloud;
  cloud.clear();
  //#pragma omp parallel for
  for(size_t i = 0; i < num_points; ++i)
  {
    if(cloud.points[i].intensity > 0)
    {
      Eigen::Vector4f global_point(temp_cloud.points[i].x, temp_cloud.points[i].y, temp_cloud.points[i].z, 1);
      Eigen::Vector4f diff_point = global_point - translation;

      Eigen::Vector4f local_point = rotation * diff_point;
      pcl::PointXYZI point;
      point.x = local_point[0];
      point.y = local_point[1];
      point.z = local_point[2];
      point.intensity = temp_cloud.points[i].intensity;
      cloud.push_back(point);
    }   
  }

  setVizDebugCloudFull(cloud);



  /* Clustering
   */
  // Regenerate the local grid with filtered cloud.
  std::vector<std::vector<LocalGridCell>> filtered_local_grid;
  int clustering_grid_size_x = params_.size_clustering_grid_x_meter_ * params_.num_clustering_grid_cell_per_meter_x_ + 1;
  int clustering_grid_size_y = params_.size_clustering_grid_y_meter_ * params_.num_clustering_grid_cell_per_meter_y_ + 1;

  filtered_local_grid.resize(clustering_grid_size_x);
  for(size_t i = 0; i < filtered_local_grid.size(); ++i)
  {
    filtered_local_grid[i].resize(clustering_grid_size_y);
  }

  for(size_t i = 0; i < cloud.size(); ++i)
  {
    int x_index, y_index;
    xyToClusteringGridIndex(cloud.points[i].x, cloud.points[i].y, x_index, y_index);
    if(x_index >= 0 && x_index < clustering_grid_size_x && y_index >= 0 && y_index < clustering_grid_size_y)
    {
      filtered_local_grid[x_index][y_index].points_.push_back(cloud.points[i]);
    }
  }

  // Distance-based naive clustering
  std::vector<PclCloudType> dist_clusters;
  for(size_t grid_x = 0; grid_x < clustering_grid_size_x; ++grid_x)
  {
    for(size_t grid_y = 0; grid_y < clustering_grid_size_y; ++grid_y)
    {
      if(filtered_local_grid[grid_x][grid_y].is_clustered_ == false
         && filtered_local_grid[grid_x][grid_y].points_.size() > params_.min_num_points_in_clustering_cell_threshold_)
      {
        filtered_local_grid[grid_x][grid_y].is_clustered_ = true;
        std::vector<CellIndices> clustered_cell_indices;
        std::vector<CellIndices> prev_clustered_cell_indices;
        std::vector<CellIndices> newly_clustered_cell_indices;
        clustered_cell_indices.emplace_back(grid_x, grid_y);
        prev_clustered_cell_indices.emplace_back(grid_x, grid_y);
        size_t start_index_to_explore = 0;

        while(true)
        {
          for(size_t i = 0; i < prev_clustered_cell_indices.size(); ++i)
          {
            for(int dx = 0; dx <= 2; ++dx)
            {
              int x_index = prev_clustered_cell_indices[i].x_index_ + dx;
              if(x_index < 0 || x_index >= clustering_grid_size_x)
              {
                continue;
              }

              for(int dy = -1; dy <= 1; ++dy)
              {
                if(dx == 0 && dy == 0)
                {
                  continue;
                }

                int y_index = prev_clustered_cell_indices[i].y_index_ + dy;
                if(y_index < 0 || y_index >= static_cast<int>(clustering_grid_size_y))
                {
                  continue;
                }

                if(filtered_local_grid[x_index][y_index].is_clustered_ == false
                  && filtered_local_grid[x_index][y_index].points_.size() > params_.min_num_points_in_clustering_cell_threshold_)
                {
                  newly_clustered_cell_indices.emplace_back(x_index, y_index);
                  filtered_local_grid[x_index][y_index].is_clustered_ = true;
                }
              }
            }
          }

          if(newly_clustered_cell_indices.size() > 0)
          {
            clustered_cell_indices.insert(clustered_cell_indices.end(),
              newly_clustered_cell_indices.begin(), newly_clustered_cell_indices.end());
            prev_clustered_cell_indices = newly_clustered_cell_indices;
            newly_clustered_cell_indices.clear();
          }
          else
          {
            PclCloudType cluster_cloud;
            for(size_t i = 0; i < clustered_cell_indices.size(); ++i)
            {
              const int& x_index = clustered_cell_indices[i].x_index_;
              const int& y_index = clustered_cell_indices[i].y_index_;
              cluster_cloud += filtered_local_grid[x_index][y_index].points_;
            }

            if(cluster_cloud.size() > params_.min_num_points_in_cluster_threshold_)
            {
              dist_clusters.push_back(cluster_cloud);
            }
            break;
          }
        }
      }
    }
  }


  // Sorting points in x direction.
  for(size_t i = 0; i < dist_clusters.size(); ++i)
  {
    std::sort(dist_clusters[i].begin(), dist_clusters[i].end(), point_comp_in_x_direction);
  }


  // Filter out clusters that are too narrow in x direction.
  //  std::vector<PclCloudType> copied_dist_clusters = dist_clusters;
  //  dist_clusters.clear();
  //  for(size_t i = 0; i < copied_dist_clusters.size(); ++i)
  //  {
  //    if(copied_dist_clusters[i].points.back().x - copied_dist_clusters[i].points[0].x >= 1.0)
  //    {
  //      dist_clusters.push_back(copied_dist_clusters[i]);
  //    }
  //  }


  // Lane clustering
  std::vector<PclCloudType> lane_clusters;
  std::vector<PclCloudType> left_lanes;
  std::vector<PclCloudType> right_lanes;

  if(dist_clusters.size() > 1)
  {
    std::vector<size_t> cluster_connectivity(dist_clusters.size(), kImprobableBigClusterIndexNumber);
    std::vector<bool> is_connected(dist_clusters.size(), false);

    for(size_t i = 0; i < dist_clusters.size() - 1; ++i)
    {
      for(size_t j = i + 1; j < dist_clusters.size(); ++j)
      {
        if((dist_clusters[j].front().x - dist_clusters[i].back().x) < params_.cluster_connection_check_maximum_distance_
          && (dist_clusters[j].front().x - dist_clusters[i].back().x) > 0.0f
          && fabs(dist_clusters[j].front().y - dist_clusters[i].back().y) < 2.0)
        {          
//          if(is_connected[j] == false)
//          {
            cluster_connectivity[i] = j;
//            is_connected[j] = true;
            break;
//          }
        }
      }
    }

    for(size_t i = 0; i < dist_clusters.size() - 1; ++i)
    {
      for(size_t j = i + 1; j < dist_clusters.size(); ++j)
      {
        if(cluster_connectivity[i] != kImprobableBigClusterIndexNumber && cluster_connectivity[i] == cluster_connectivity[j])
        {
          float dist_i = xyDistanceBetweenTwoPclPoints(dist_clusters[i].points.back(), dist_clusters[cluster_connectivity[i]].points[0]);
          float dist_j = xyDistanceBetweenTwoPclPoints(dist_clusters[j].points.back(), dist_clusters[cluster_connectivity[j]].points[0]);

          if(dist_i <= dist_j)
          {
            cluster_connectivity[j] = kImprobableBigClusterIndexNumber;
          }
          else
          {
            cluster_connectivity[i] = kImprobableBigClusterIndexNumber;
          }
        }
      }
    }


    is_connected = std::vector<bool>(dist_clusters.size(), false);
    for(size_t i = 0; i < dist_clusters.size(); ++i)
    {
      if(is_connected[i] == true)
      {
        continue;
      }

      if(cluster_connectivity[i] != kImprobableBigClusterIndexNumber)
      {
        is_connected[i] = true;
        lane_clusters.push_back(dist_clusters[i]);
        size_t idx_next_connected_cluster = cluster_connectivity[i];

        while(true)
        {
          lane_clusters.back() += dist_clusters[idx_next_connected_cluster];
          is_connected[idx_next_connected_cluster] = true;
          if(cluster_connectivity[idx_next_connected_cluster] == kImprobableBigClusterIndexNumber)
          {
            break;
          }
          else
          {
            idx_next_connected_cluster = cluster_connectivity[idx_next_connected_cluster];
          }
        }
      }
    }

    for(size_t i = 0; i < dist_clusters.size(); ++i)
    {
      if(is_connected[i] == false)
      {
        lane_clusters.push_back(dist_clusters[i]);
      }
    }
  }


  // Sorting points in x direction.
  for(size_t i = 0; i < lane_clusters.size(); ++i)
  {
    std::sort(lane_clusters[i].begin(), lane_clusters[i].end(), point_comp_in_x_direction);
  }


  //
  std::vector<float> y0_of_cluster_curves;
  std::vector<Eigen::Vector3f> curve_cooeficients;
  std::vector<bool> is_combined;
  std::vector<PclCloudType> copied_lane_clusters = lane_clusters;
  lane_clusters.clear();
  for(size_t i = 0; i < copied_lane_clusters.size(); ++i)
  {
    PclCloudType downsampled_points;

    if(copied_lane_clusters[i].size() > 1000)
    {
      // Find a point that is closest to the vehicle position.
      float min_dist = 999.0f;
      size_t min_idx = 999999;
      for(size_t j = 0; j < copied_lane_clusters[i].size(); ++j)
      {
        float dist = std::sqrt(std::pow(copied_lane_clusters[i].points[j].x, 2) + std::pow(copied_lane_clusters[i].points[j].y, 2));
        if(dist < min_dist)
        {
          min_dist = dist;
          min_idx = j;
        }
      }

      downsampled_points.push_back(copied_lane_clusters[i][min_idx]);
      for(size_t j = min_idx; j < copied_lane_clusters[i].size(); ++j)
      {
        if(copied_lane_clusters[i][j].x - downsampled_points.back().x > 0.5
           && fabs(copied_lane_clusters[i][j].y - downsampled_points.back().y) < 0.3)
        {
          downsampled_points.push_back(copied_lane_clusters[i][j]);
        }
      }

      for(size_t j = min_idx; j > 0; --j)
      {
        if(copied_lane_clusters[i][j].x - downsampled_points.back().x < -0.5
           && fabs(copied_lane_clusters[i][j].y - downsampled_points.back().y) < 0.3)
        {
          downsampled_points.push_back(copied_lane_clusters[i][j]);
        }
      }
    }
    else
    {
      downsampled_points = copied_lane_clusters[i];
    }

    //Check if this cluster includes lane points or outliers.
    Eigen::MatrixXf A(downsampled_points.size(), 3);
    Eigen::VectorXf b(downsampled_points.size());

    for(size_t j = 0; j < downsampled_points.size(); ++j)
    {
      A(j, 0) = std::pow(downsampled_points.points[j].x, 2);
      A(j, 1) = downsampled_points.points[j].x;
      A(j, 2) = 1.0f;
      b(j) = downsampled_points.points[j].y;
    }

    Eigen::VectorXf c = (A.transpose() * A).inverse() * A.transpose() * b;

    PclCloudType inliers;
    for(size_t j = 0; j < copied_lane_clusters[i].size(); ++j)
    {
      float px = copied_lane_clusters[i].points[j].x;
      float py = copied_lane_clusters[i].points[j].y;

      float curve_y = c[0] * std::pow(px, 2) + c[1] * px + c[2];
      if(fabs(curve_y - py) < 0.3f)
      {
        inliers.push_back(copied_lane_clusters[i].points[j]);
      }
    }
    //    if(static_cast<float>(inliers.size()) > 0.5f * copied_lane_clusters[i].size())
    if(inliers.size() > 0)
    {
      lane_clusters.push_back(inliers);
      y0_of_cluster_curves.push_back(c[2]);
      curve_cooeficients.push_back(c);
      is_combined.push_back(false);
    }
  }



  if(lane_clusters.size() > 1)
  {
    // Final clustering
    for(size_t i = 0; i < lane_clusters.size() - 1; ++i)
    {
      for(size_t j = i + 1; j < lane_clusters.size(); ++j)
      {
        const float x0 = lane_clusters[j][0].x;
        const float y0 = lane_clusters[j][0].y;
        const float xe = lane_clusters[j].back().x;
        const float ye = lane_clusters[j].back().y;

        if(is_combined[j] == false
           && fabs(curve_cooeficients[i][0] * std::pow(x0, 2) + curve_cooeficients[i][1] * x0 + curve_cooeficients[i][2] - y0) < 1.5
           && fabs(curve_cooeficients[i][0] * std::pow(xe, 2) + curve_cooeficients[i][1] * xe + curve_cooeficients[i][2] - ye) < 1.5)
        {
          lane_clusters[i] += lane_clusters[j];
          is_combined[j] = true;
        }
      }
    }



    // Eliminate too small clusters.
    copied_lane_clusters = lane_clusters;
    std::vector<float> copied_y0_of_cluster_curves = y0_of_cluster_curves;
    y0_of_cluster_curves.clear();
    lane_clusters.clear();
    for(size_t i = 0; i < is_combined.size(); ++i)
    {
      if(is_combined[i] == false && copied_lane_clusters[i].size() > 30)
      {
        lane_clusters.push_back(copied_lane_clusters[i]);
        y0_of_cluster_curves.push_back(copied_y0_of_cluster_curves[i]);
      }
    }



    // Filter out clusters that are too narrow in x direction.
    copied_lane_clusters = lane_clusters;
    lane_clusters.clear();
    for(size_t i = 0; i < copied_lane_clusters.size(); ++i)
    {
      if(copied_lane_clusters[i].points.back().x - copied_lane_clusters[i].points[0].x >= 10.0)
      {
        lane_clusters.push_back(copied_lane_clusters[i]);
      }
    }



    // Outlier removal -- again
//    copied_lane_clusters = lane_clusters;
//    lane_clusters.clear();
//    y0_of_cluster_curves.clear();
//    for(size_t i = 0; i < copied_lane_clusters.size(); ++i)
//    {
//      PclCloudType downsampled_points;
//      downsampled_points.push_back(copied_lane_clusters[i][0]);
//      if(copied_lane_clusters[i].size() > 1000)
//      {
//        for(size_t j = 0; j < copied_lane_clusters[i].size(); ++j)
//        {
//          if(copied_lane_clusters[i][j].x - downsampled_points.back().x > 0.5
//             && fabs(copied_lane_clusters[i][j].y - downsampled_points.back().y) < 0.5)
//          {
//            downsampled_points.push_back(copied_lane_clusters[i][j]);
//          }
//        }
//      }
//      else if(copied_lane_clusters[i].size() > 100)
//      {
//        for(size_t j = 0; j < copied_lane_clusters[i].size(); ++j)
//        {
//          if(copied_lane_clusters[i][j].x - downsampled_points.back().x > 0.1
//             && fabs(copied_lane_clusters[i][j].y - downsampled_points.back().y) < 0.5
//             || copied_lane_clusters[i][j].x - downsampled_points.back().x > 10)
//          {
//            downsampled_points.push_back(copied_lane_clusters[i][j]);
//          }
//        }
//      }
//      else
//      {
//        downsampled_points = copied_lane_clusters[i];
//      }

//      //Check if this cluster includes lane points or outliers.
//      Eigen::MatrixXf A(downsampled_points.size(), 3);
//      Eigen::VectorXf b(downsampled_points.size());

//      for(size_t j = 0; j < downsampled_points.size(); ++j)
//      {
//        A(j, 0) = std::pow(downsampled_points.points[j].x, 2);
//        A(j, 1) = downsampled_points.points[j].x;
//        A(j, 2) = 1.0f;
//        b(j) = downsampled_points.points[j].y;
//      }

//      Eigen::VectorXf c = (A.transpose() * A).inverse() * A.transpose() * b;

//      PclCloudType inliers;
//      for(size_t j = 0; j < copied_lane_clusters[i].size(); ++j)
//      {
//        float px = copied_lane_clusters[i].points[j].x;
//        float py = copied_lane_clusters[i].points[j].y;

//        float curve_y = c[0] * std::pow(px, 2) + c[1] * px + c[2];
//        if(fabs(curve_y - py) < 0.3f)
//        {
//          inliers.push_back(copied_lane_clusters[i].points[j]);
//        }
//      }
//      if(static_cast<float>(inliers.size()) > 0.6f * copied_lane_clusters[i].size())
//      {
//        lane_clusters.push_back(inliers);
//        y0_of_cluster_curves.push_back(c[2]);
//      }
//    }

    // Sort the clusters along y direction.
    copied_lane_clusters = lane_clusters;
    copied_y0_of_cluster_curves = y0_of_cluster_curves;
    y0_of_cluster_curves.clear();
    lane_clusters.clear();
    lane_clusters.push_back(copied_lane_clusters[0]);
    y0_of_cluster_curves.push_back(copied_y0_of_cluster_curves[0]);
    std::vector<PclCloudType>::iterator it;
    std::vector<float>::iterator it_y0;

    if(copied_lane_clusters.size() > 1)
    {
      for(size_t i = 1; i < copied_lane_clusters.size(); ++i)
      {
        it = lane_clusters.begin();
        it_y0 = y0_of_cluster_curves.begin();
        size_t insert_index = 0;
        for(size_t j = 0; j < lane_clusters.size(); ++j)
        {
          if(j == 0)
          {
            if(copied_y0_of_cluster_curves[i] < y0_of_cluster_curves[j])
            {
              insert_index = j;
              break;
            }
          }

          if(j == lane_clusters.size() - 1)
          {
            if(copied_y0_of_cluster_curves[i] > y0_of_cluster_curves[j])
            {
              insert_index = j + 1;
              break;
            }
          }

          if(copied_y0_of_cluster_curves[i] > y0_of_cluster_curves[j]
             && copied_y0_of_cluster_curves[i] < y0_of_cluster_curves[j + 1])
          {
            insert_index = j + 1;
          }
        }

        lane_clusters.insert(it + insert_index, copied_lane_clusters[i]);
        y0_of_cluster_curves.insert(it_y0 + insert_index, copied_y0_of_cluster_curves[i]);
      }
    }


    std::vector<float> y0_left, y0_right;

    for(size_t i = 0; i < lane_clusters.size(); ++i)
    {
      if(y0_of_cluster_curves[i] > 0)
      {
        if(i - 1 >= 0)
        {
          for(int j = i - 1; j >= 0; --j)
          {
            right_lanes.push_back(lane_clusters[j]);
            y0_right.push_back(y0_of_cluster_curves[j]);
          }
        }

        for(size_t j = i; j < lane_clusters.size(); ++j)
        {
          left_lanes.push_back(lane_clusters[j]);
          y0_left.push_back(y0_of_cluster_curves[j]);
        }

        break;
      }
    }

    lane_clusters.clear();

    if(right_lanes.size() > 0)
    {
      lane_clusters.push_back(right_lanes[0]);

      if(right_lanes.size() > 1)
      {
        for(size_t i = 1; i < right_lanes.size(); ++i)
        {
          if(y0_right[i - 1] - y0_right[i] > 5.0)
          {
            std::vector<PclCloudType>::iterator it_start = right_lanes.begin() + i;
            right_lanes.erase(it_start, right_lanes.end());
            break;
          }
          else
          {
            lane_clusters.push_back(right_lanes[i]);
          }
        }
      }
    }

    if(left_lanes.size() > 0)
    {
      lane_clusters.push_back(left_lanes[0]);

      if(left_lanes.size() > 1)
      {
        for(size_t i = 1; i < left_lanes.size(); ++i)
        {
          if(y0_left[i] - y0_left[i - 1] > 5.0)
          {
            std::vector<PclCloudType>::iterator it_start = left_lanes.begin() + i;
            left_lanes.erase(it_start, left_lanes.end());
            break;
          }
          else
          {
            lane_clusters.push_back(left_lanes[i]);
          }
        }
      }
    }

    std::cout << "Number of lanes: left - " << left_lanes.size() << ", right - " << right_lanes.size() << std::endl;

  }


  cloud.clear();
  if(params_.publish_viz_topics_)
  {
    setVizLanes(lane_clusters);
  }

  if(lane_clusters.size() > 0)
  {
    if(params_.output_folder_.back() == '/')
    {
      params_.output_folder_.resize(params_.output_folder_.size() - 1);
    }

    // Convert the timestamp in a string format.
    uint64_t time_sec = static_cast<uint64_t>(timestamp);
    uint64_t time_nsec = static_cast<uint64_t>(static_cast<uint64_t>(1E3 * timestamp)) - 1E3 * time_sec;
    time_nsec *= 1E6;

    std::string str_timestamp_sec = std::to_string(time_sec);
    std::string str_timestamp_nsec = std::to_string(time_nsec);

    int diff_str_length = 9 - static_cast<int>(str_timestamp_nsec.size());

    for(int i = 0; i < diff_str_length; ++i)
    {
      str_timestamp_nsec.insert(str_timestamp_nsec.begin(), 1, '0');
    }

    std::string str_timestamp = str_timestamp_sec + "_" + str_timestamp_nsec;


    if(params_.save_result_image_)
    {
      // Save image.
      cv::Mat image(1200, 2000, CV_8UC3, cv::Scalar(0,0,0));

      cv::line(image, cv::Point(1000, 300), cv::Point(1050, 300), cv::Scalar(0,0,255));
      cv::line(image, cv::Point(1000, 300), cv::Point(1000, 250), cv::Scalar(0,0,255));

      int num_clusters = static_cast<int>(lane_clusters.size());
      for(int i = 0; i < num_clusters; ++i)
      {
        float i_f = static_cast<float>(i);
        float num_f = static_cast<float>(num_clusters);

        int r, g, b;

        float ratio = i_f / num_f;
        int a = static_cast<int>((1.0f - ratio) / 0.25f);	//invert and group
        int X = static_cast<int>(a);	//this is the integer part
        int Y = static_cast<int>(255.0f * (a - X)); //fractional part from 0 to 255
        switch(X)
        {
        case 4: r=255; g=Y; b=0; break;
        case 3: r=255-Y; g=255; b=0; break;
        case 2: r=0; g=255; b=Y; break;
        case 1: r=0; g=255-Y; b=255; break;
        case 0: r=0; g=0; b=255; break;
        }

        for(size_t j = 0; j < lane_clusters[i].size(); ++j)
        {
          const float& x = lane_clusters[i].points[j].x;
          const float& y = lane_clusters[i].points[j].y;
          if(x < -100.0f || y < -30.0f || x >= 100.0f || y >= 30.0f)
            continue;

          int u, v;
          xyToImageuv(x, y, u, v);

          image.at<cv::Vec3b>(v, u)[0] = static_cast<unsigned char>(b);
          image.at<cv::Vec3b>(v, u)[1] = static_cast<unsigned char>(g);
          image.at<cv::Vec3b>(v, u)[2] = static_cast<unsigned char>(r);
        }
      }


      for(int i = 0; i < debug_cloud_full_.size(); ++i)
      {
        float& x = debug_cloud_full_.points[i].x;
        float& y = debug_cloud_full_.points[i].y;
        if(x < -100.0f || y < -30.0f || x >= 100.0f || y >= 30.0f)
          continue;

        int u, v;
        xyToImageuv(x, y, u, v);

        v += 600;

        image.at<cv::Vec3b>(v, u)[0] = static_cast<unsigned char>(0);
        image.at<cv::Vec3b>(v, u)[1] = static_cast<unsigned char>(0);
        image.at<cv::Vec3b>(v, u)[2] = static_cast<unsigned char>(255);
      }

      cv::imwrite(params_.output_folder_ + "/" + str_timestamp + ".jpg", image);
    }

    fout_front_lanes_ << std::fixed;
    fout_front_lanes_.precision(3);

    fout_front_lanes_.open(params_.output_folder_ + "/" + str_timestamp + ".txt", std::ios::out|std::ios::trunc);

    for(size_t i = 0; i < left_lanes.size(); ++i)
    {
      fout_front_lanes_ << i + 1;
      for(size_t j = 0; j < left_lanes[i].size(); ++j)
      {
        fout_front_lanes_ << "\t" << left_lanes[i].points[j].x << "\t" << left_lanes[i].points[j].y;
      }
      fout_front_lanes_ << std::endl;
    }

    for(size_t i = 0; i < right_lanes.size(); ++i)
    {
      fout_front_lanes_ << -static_cast<int>(i) - 1;
      for(size_t j = 0; j < right_lanes[i].size(); ++j)
      {
        fout_front_lanes_ << "\t" << right_lanes[i].points[j].x << "\t" << right_lanes[i].points[j].y;
      }
      fout_front_lanes_ << std::endl;
    }


    fout_front_lanes_.close();
  }
}

void LidarLaneExtraction::extractGroundPoints(const PclCloudType& input, PclCloudType& output)
{
  std::cout << "Start ground extraction." << std::endl;

  pcl::PointCloud<pcl::PointXYZI>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZI>);
  pcl::PointIndicesPtr ground (new pcl::PointIndices);

  pcl::ProgressiveMorphologicalFilter<pcl::PointXYZI> pmf;
  PclCloudType::Ptr cloud(new PclCloudType(input));

  pmf.setInputCloud(cloud);
  pmf.setMaxWindowSize (20);
  pmf.setSlope (1.0f);
  pmf.setInitialDistance (0.5f);
  pmf.setMaxDistance (3.0f);
  pmf.extract(ground->indices);

  // Create the filtering object
  pcl::ExtractIndices<pcl::PointXYZI> extract;
  extract.setInputCloud (cloud);
  extract.setIndices (ground);
  extract.filter (*cloud_filtered);

  output = *cloud_filtered;

  std::cout << "Ground extraction finished." << std::endl;
}

void LidarLaneExtraction::xyToLocalGridIndex(const float& x, const float& y, int& x_index, int& y_index)
{
  x_index = static_cast<int>(static_cast<float>(params_.num_local_grid_cell_per_meter_) * (x + static_cast<float>(params_.size_local_grid_x_meter_ / 2)));
  y_index = static_cast<int>(static_cast<float>(params_.num_local_grid_cell_per_meter_) * (y + static_cast<float>(params_.size_local_grid_y_meter_ / 2)));
}

void LidarLaneExtraction::xyToClusteringGridIndex(const float& x, const float& y, int& x_index, int& y_index)
{
  x_index = static_cast<int>(static_cast<float>(params_.num_clustering_grid_cell_per_meter_x_) * (x + static_cast<float>(params_.size_clustering_grid_x_meter_ / 2)));
  y_index = static_cast<int>(static_cast<float>(params_.num_clustering_grid_cell_per_meter_y_) * (y + static_cast<float>(params_.size_clustering_grid_y_meter_ / 2)));
}

void LidarLaneExtraction::xyToNonGroundObjectFilteringGridIndex(const float& x, const float& y, int& x_index, int& y_index)
{
  x_index = static_cast<int>(2 * (x + 40.0f));
  y_index = static_cast<int>(5 * (y + 20.0f));
}


void LidarLaneExtraction::setVizDebugCloudFull(const PclCloudType& viz_cloud)
{
  debug_cloud_full_ = viz_cloud;
}

void LidarLaneExtraction::setVizLanes(const std::vector<PclCloudType>& lanes)
{
  debug_cloud_lanes_ = lanes;
}

bool point_comp_in_x_direction(const pcl::PointXYZI& p1, const pcl::PointXYZI& p2)
{
  if(p1.x < p2.x)
  {
    return true;
  }
  else
  {
    return false;
  }
}

Eigen::Matrix4f getRotMatrix(double roll, double pitch, double yaw)
{
  Eigen::Matrix4f matrix;
  double cos_yaw = std::cos(yaw);
  double sin_yaw = std::sin(yaw);
  double cos_pitch = std::cos(pitch);
  double sin_pitch = std::sin(pitch);
  double cos_roll = std::cos(roll);
  double sin_roll = std::sin(roll);

  double comb_a = cos_yaw * sin_pitch;
  double comb_b = sin_yaw * sin_pitch;
  matrix(0, 0) = cos_yaw * cos_pitch;
  matrix(0, 1) = comb_a * sin_roll - sin_yaw * cos_roll;
  matrix(0, 2) = comb_a * cos_roll + sin_yaw * sin_roll;

  matrix(1, 0) = sin_yaw * cos_pitch;
  matrix(1, 1) = comb_b * sin_roll + cos_yaw * cos_roll;
  matrix(1, 2) = comb_b * cos_roll - cos_yaw * sin_roll;

  matrix(2, 0) = -sin_pitch;
  matrix(2, 1) = cos_pitch * sin_roll;
  matrix(2, 2) = cos_pitch * cos_roll;

  matrix(3, 0) = 0;
  matrix(3, 1) = 0;
  matrix(3, 2) = 0;

  matrix(0, 3) = 0;
  matrix(1, 3) = 0;
  matrix(2, 3) = 0;
  matrix(3, 3) = 1;

  return matrix;
}

Eigen::Matrix4f getTransformMatrix(double x, double y, double z, double roll, double pitch, double yaw)
{
  Eigen::Matrix4f matrix;
  double cos_yaw = std::cos(yaw);
  double sin_yaw = std::sin(yaw);
  double cos_pitch = std::cos(pitch);
  double sin_pitch = std::sin(pitch);
  double cos_roll = std::cos(roll);
  double sin_roll = std::sin(roll);


  double comb_a = cos_yaw * sin_pitch;
  double comb_b = sin_yaw * sin_pitch;
  matrix(0, 0) = cos_yaw * cos_pitch;
  matrix(0, 1) = comb_a * sin_roll - sin_yaw * cos_roll;
  matrix(0, 2) = comb_a * cos_roll + sin_yaw * sin_roll;

  matrix(1, 0) = sin_yaw * cos_pitch;
  matrix(1, 1) = comb_b * sin_roll + cos_yaw * cos_roll;
  matrix(1, 2) = comb_b * cos_roll - cos_yaw * sin_roll;

  matrix(2, 0) = -sin_pitch;
  matrix(2, 1) = cos_pitch * sin_roll;
  matrix(2, 2) = cos_pitch * cos_roll;

  matrix(3, 0) = 0;
  matrix(3, 1) = 0;
  matrix(3, 2) = 0;

  matrix(0, 3) = x;
  matrix(1, 3) = y;
  matrix(2, 3) = z;
  matrix(3, 3) = 1;

  return matrix;
}

void xyToImageuv(const float& x, const float& y, int& u, int& v)
{
  u = static_cast<int>(10 * x) + 1000;
  v = -static_cast<int>(10 * y) + 300;
}

pcl::PointXYZI doInverseRotation(const pcl::PointXYZI& p, const double& roll, const double& pitch, const double& yaw)
{
  const float& x = p.x;
  const float& y = p.y;
  const float& z = p.z;

  double x1 = cos(yaw) * x + sin(yaw) * y;
  double y1 = -sin(yaw) * x + cos(yaw) * y;
  double z1 = z;

  double x2 = cos(pitch) * x1 - sin(pitch) * z1;
  double y2 = y1;
  double z2 = +sin(pitch) * x1 + cos(pitch) * z1;

  pcl::PointXYZI out;
  out.x = x2;
  out.y = cos(roll) * y2 + sin(roll) * z2;
  out.z = -sin(roll) * y2 + cos(roll) * z2;
  out.intensity = p.intensity;

  return out;
}

float xyDistanceBetweenTwoPclPoints(const pcl::PointXYZI& p1, const pcl::PointXYZI& p2)
{
  return std::sqrt(std::pow((p1.x - p2.x), 2) + std::pow((p1.y - p2.y), 2));
}

void convertPointCloudfToPclPointCloud(const PointCloudf& cloudf, pcl::PointCloud<pcl::PointXYZI>& pcl_cloud)
{
  pcl_cloud.clear();
  pcl_cloud.reserve(cloudf.getSize());
  for(size_t i = 0; i < cloudf.getSize(); ++i)
  {
    pcl::PointXYZI pcl_point;
    pcl_point.x = cloudf.points_[i].x;
    pcl_point.y = cloudf.points_[i].y;
    pcl_point.z = cloudf.points_[i].z;
    pcl_point.intensity = static_cast<float>(cloudf.points_[i].intensity);

    pcl_cloud.push_back(pcl_point);
  }
}


