/*******************************************************************************
* @file    map_handler.h
* @date    06/15/2018
*
* @attention Copyright (c) 2017
* @attention Phantom AI, Inc.
* @attention All rights reserved.
*******************************************************************************/

#ifndef PHANTOM_AI_TILE_MAP_H_
#define PHANTOM_AI_TILE_MAP_H_

#include <vector>
#include <thread>
#include <boost/foreach.hpp>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>

enum class LocalizationMapCellLocFeatureType{
  NONE = 0,
  INTEN = 100,
  PERP = 200
};

enum class LocalizationMapCellAttribute{
  UNKNOWN = 0,

  ROAD = 1,
  BOUNDARY = 2,
  BICYCLE_LANE = 3,
  SIDEWALK = 4,
  ISLAND = 5,
  PARL_PARK = 6,

  LANE_WHITE_SOLID = 10,
  LANE_WHITE_DASHED = 11,
  LANE_WHITE_DOUBLE_SOLID = 12,
  LANE_WHITE_DOUBLE_DASHED = 13,
  LANE_WHITE_DOUBLE_LEFT_DASHED = 14,
  LANE_WHITE_DOUBLE_RIGHT_DASHED = 15,
  LANE_WHITE_LEFT_TURN = 16,

  LANE_YELLOW_SOLID = 20,
  LANE_YELLOW_DASHED = 21,
  LANE_YELLOW_DOUBLE_SOLID = 22,
  LANE_YELLOW_DOUBLE_DASHED = 23,
  LANE_YELLOW_DOUBLE_LEFT_DASHED = 24,
  LANE_YELLOW_DOUBLE_RIGHT_DASHED = 25,
  LANE_YELLOW_LEFT_TURN = 26,

  UNDEFINED = 99
};

class PointXYT
{
public:
  PointXYT()
  {}

  PointXYT(double x_arg, double y_arg, uint8_t type_arg) :
    x_(x_arg), y_(y_arg), type_(type_arg)
  {}

  double x_;
  double y_;
  uint8_t type_;
};

// Class denoting a single tile
class GridMapTile
{
public:
    GridMapTile(uint32_t x0_arg, uint32_t y0_arg)
      : x0_(x0_arg), y0_(y0_arg), cell_{0}, is_updated_(false)
    {}
    ~GridMapTile(){}

    // Get UTM x and y coordinates from a cell index
    inline void getUTMXYFromIndex(uint32_t idx_x_arg, uint32_t idx_y_arg, double* utm_x_ptr, double* utm_y_ptr)
    {
      *utm_x_ptr = x0_ + 0.05 * idx_x_arg;
      *utm_y_ptr = y0_ + 0.05 * idx_y_arg;
    }

    // Update a cell with a cell index and a cell value
    inline void updateCell(uint32_t idx_x, uint32_t idx_y, uint8_t type)
    {
      if(idx_x >= 2000)
        idx_x = 1999;
      if(idx_y >= 2000)
        idx_y = 1999;

      cell_[idx_x][idx_y] = type;
      if((type / 100) * 100 == (int)LocalizationMapCellLocFeatureType::INTEN || (type / 100) * 100 == (int)LocalizationMapCellLocFeatureType::PERP)
      {
        double utm_x, utm_y;
        getUTMXYFromIndex(idx_x, idx_y, &utm_x, &utm_y);
        occupied_points_.emplace_back(utm_x, utm_y, type);
      }
    }

    // Coordinates of the bottom-left corner of the tile
    uint32_t x0_;
    uint32_t y0_;

    // 2D array of cells.
    std::array<std::array<uint8_t, 2000>, 2000> cell_;

    // Set of localization feature points
    std::vector<PointXYT> occupied_points_;

    // Flat to indicate that the tile has been modified since initialization
    bool is_updated_;
};


// Class of a tile map
class LocalizationGridMap
{
public:
  LocalizationGridMap();
  ~LocalizationGridMap();

  void run();

  void stop();

  void initialize();

  void mapUpdateThread();

  void loadWholeMapFiles(const std::string& tile_map_folder_name);

  void loadAMapFile(const uint32_t& tile_idx_x, const uint32_t& tile_idx_y);

  void updateLocalMap();

  void updatePose(const double& pose_x, const double& pose_y)
  {
    pose_x_ = pose_x;
    pose_y_ = pose_y;
    is_pose_updated_ = true;
  }

  void initTileAtIndex(const uint32_t& x_idx_arg, const uint32_t& y_idx_arg)
  {
    if(tiles_.at(x_idx_arg).at(y_idx_arg) == NULL)
      tiles_.at(x_idx_arg).at(y_idx_arg) = new GridMapTile(tile_map_origin_.x + 100 * x_idx_arg, tile_map_origin_.y + 100 * y_idx_arg);
  }

  void initTileAtPoint(const double& utm_x_arg, const double& utm_y_arg)
  {
    uint32_t tile_x_idx = static_cast<uint32_t>((utm_x_arg - tile_map_origin_.x) / 100.0);
    uint32_t tile_y_idx = static_cast<uint32_t>((utm_y_arg - tile_map_origin_.y) / 100.0);
    if(tiles_.at(tile_x_idx).at(tile_y_idx) == NULL)
      tiles_.at(tile_x_idx).at(tile_y_idx) = new GridMapTile(tile_map_origin_.x + 100 * tile_x_idx, tile_map_origin_.y + 100 * tile_y_idx);
  }


  bool updateCellWithUTMPointAndValue(const double& utm_x_arg, const double& utm_y_arg, uint8_t value_arg)
  {
    uint32_t tile_x_idx = static_cast<uint32_t>((utm_x_arg - tile_map_origin_.x) / 100.0);
    uint32_t tile_y_idx = static_cast<uint32_t>((utm_y_arg - tile_map_origin_.y) / 100.0);

    if(tiles_.at(tile_x_idx).at(tile_y_idx) == NULL)
      tiles_.at(tile_x_idx).at(tile_y_idx) = new GridMapTile(tile_map_origin_.x + 100 * tile_x_idx, tile_map_origin_.y + 100 * tile_y_idx);

    uint32_t cell_idx_x = static_cast<uint32_t>(std::floor((utm_x_arg - tiles_.at(tile_x_idx).at(tile_y_idx)->x0_) * 20.0));// + 0.5));
    uint32_t cell_idx_y = static_cast<uint32_t>(std::floor((utm_y_arg - tiles_.at(tile_x_idx).at(tile_y_idx)->y0_) * 20.0));// + 0.5));

    tiles_.at(tile_x_idx).at(tile_y_idx)->is_updated_ = true;
    tiles_.at(tile_x_idx).at(tile_y_idx)->updateCell(cell_idx_x, cell_idx_y, value_arg);

    return true;
  }

  void updateCellWithMapPlanePointAndValue(const double& x_arg, const double& y_arg, uint8_t value_arg)
  {
    double utm_x = x_arg + map_viz_origin_.x;
    double utm_y = y_arg + map_viz_origin_.y;
    updateCellWithUTMPointAndValue(utm_x, utm_y, value_arg);
  }

  void updateCellWithUTMPointAndFeatureType(const double& utm_x_arg, const double& utm_y_arg, LocalizationMapCellLocFeatureType feature_type)
  {
    uint8_t cell_value = getCellValueAtUTMPoint(utm_x_arg, utm_y_arg);
    uint8_t attribute_value = cell_value - (cell_value / 100) * 100;
    updateCellWithUTMPointAndValue(utm_x_arg, utm_y_arg, (uint8_t)feature_type + attribute_value);
  }

  void updateCellWithMapPlanePointAndFeatureType(const double& x_arg, const double& y_arg, LocalizationMapCellLocFeatureType feature_type)
  {
    double utm_x = x_arg + map_viz_origin_.x;
    double utm_y = y_arg + map_viz_origin_.y;
    updateCellWithUTMPointAndFeatureType(utm_x, utm_y, feature_type);
  }

  void updateCellWithUTMPointAndAttribute(const double& utm_x_arg, const double& utm_y_arg, LocalizationMapCellAttribute attribute)
  {
    uint8_t cell_value = getCellValueAtUTMPoint(utm_x_arg, utm_y_arg);
    uint8_t feature_type_value = (cell_value / 100) * 100;
    updateCellWithUTMPointAndValue(utm_x_arg, utm_y_arg, feature_type_value + (uint8_t)attribute);
  }

  void updateCellWithMapPlanePointAndAttribute(const double& x_arg, const double& y_arg, LocalizationMapCellAttribute attribute)
  {
    double utm_x = x_arg + map_viz_origin_.x;
    double utm_y = y_arg + map_viz_origin_.y;
    updateCellWithUTMPointAndAttribute(utm_x, utm_y, attribute);
  }

  uint8_t getCellValueAtUTMPoint(const double& utm_x_arg, const double& utm_y_arg)
  {
    uint32_t tile_x_idx = static_cast<uint32_t>((utm_x_arg - tile_map_origin_.x) / 100.0);
    uint32_t tile_y_idx = static_cast<uint32_t>((utm_y_arg - tile_map_origin_.y) / 100.0);

    if(tile_x_idx < 0 || tile_x_idx >= tile_size_.x || tile_y_idx < 0 || tile_y_idx >= tile_size_.y)
    {
      return 0;
    }

    if(tiles_.at(tile_x_idx).at(tile_y_idx) == NULL)
      return 0;
    else
    {
      uint32_t cell_idx_x = static_cast<uint32_t>(std::floor((utm_x_arg - tiles_.at(tile_x_idx).at(tile_y_idx)->x0_) * 20.0));
      uint32_t cell_idx_y = static_cast<uint32_t>(std::floor((utm_y_arg - tiles_.at(tile_x_idx).at(tile_y_idx)->y0_) * 20.0));
      return tiles_.at(tile_x_idx).at(tile_y_idx)->cell_.at(cell_idx_x).at(cell_idx_y);
    }    
  }

  uint8_t getCellValueAtMapPoint(const double& map_plane_x_arg, const double& map_plane_y_arg)
  {
    double utm_x = map_plane_x_arg + map_viz_origin_.x;
    double utm_y = map_plane_y_arg + map_viz_origin_.y;
    return getCellValueAtUTMPoint(utm_x, utm_y);
  }

  LocalizationMapCellLocFeatureType getCellFeatureTypeAtUTMPoint(const double& utm_x_arg, const double& utm_y_arg)
  {
    uint8_t cell_value = getCellValueAtUTMPoint(utm_x_arg, utm_y_arg);
    uint8_t feature_type_value = cell_value / 100;

    if(feature_type_value == 1)
      return LocalizationMapCellLocFeatureType::INTEN;
    else if(feature_type_value == 2)
      return LocalizationMapCellLocFeatureType::PERP;
    else
      return LocalizationMapCellLocFeatureType::NONE;
  }

  LocalizationMapCellLocFeatureType getCellFeatureTypeAtMapPlanePoint(const double& map_plane_x_arg, const double& map_plane_y_arg)
  {
    double utm_x = map_plane_x_arg + map_viz_origin_.x;
    double utm_y = map_plane_y_arg + map_viz_origin_.y;
    return getCellFeatureTypeAtUTMPoint(utm_x, utm_y);
  }

  LocalizationMapCellAttribute  getCellAttributeAtUTMPoint(const double& utm_x_arg, const double& utm_y_arg)
  {
    uint8_t cell_value = getCellValueAtUTMPoint(utm_x_arg, utm_y_arg);
    uint8_t attribute_value = cell_value - (cell_value / 100) * 100;

    if(attribute_value == 0)
      return LocalizationMapCellAttribute::UNKNOWN;
    else if(attribute_value == 1)
      return LocalizationMapCellAttribute::ROAD;
    else if(attribute_value == 2)
      return LocalizationMapCellAttribute::BOUNDARY;
    else if(attribute_value == 3)
      return LocalizationMapCellAttribute::BICYCLE_LANE;
    else if(attribute_value == 4)
      return LocalizationMapCellAttribute::SIDEWALK;
    else if(attribute_value == 5)
      return LocalizationMapCellAttribute::ISLAND;
    else if(attribute_value == 6)
      return LocalizationMapCellAttribute::PARL_PARK;
    else if(attribute_value == 10)
      return LocalizationMapCellAttribute::LANE_WHITE_SOLID;
    else if(attribute_value == 11)
      return LocalizationMapCellAttribute::LANE_WHITE_DASHED;
    else if(attribute_value == 12)
      return LocalizationMapCellAttribute::LANE_WHITE_DOUBLE_SOLID;
    else if(attribute_value == 13)
      return LocalizationMapCellAttribute::LANE_WHITE_DOUBLE_DASHED;
    else if(attribute_value == 14)
      return LocalizationMapCellAttribute::LANE_WHITE_DOUBLE_LEFT_DASHED;
    else if(attribute_value == 15)
      return LocalizationMapCellAttribute::LANE_WHITE_DOUBLE_RIGHT_DASHED;
    else if(attribute_value == 20)
      return LocalizationMapCellAttribute::LANE_YELLOW_SOLID;
    else if(attribute_value == 21)
      return LocalizationMapCellAttribute::LANE_YELLOW_DASHED;
    else if(attribute_value == 22)
      return LocalizationMapCellAttribute::LANE_YELLOW_DOUBLE_SOLID;
    else if(attribute_value == 23)
      return LocalizationMapCellAttribute::LANE_YELLOW_DOUBLE_DASHED;
    else if(attribute_value == 24)
      return LocalizationMapCellAttribute::LANE_YELLOW_DOUBLE_LEFT_DASHED;
    else if(attribute_value == 25)
      return LocalizationMapCellAttribute::LANE_YELLOW_DOUBLE_RIGHT_DASHED;
    else
      return LocalizationMapCellAttribute::UNDEFINED;
  }

  LocalizationMapCellAttribute getCellAttributeAtMapPlanePoint(const double& map_plane_x_arg, const double& map_plane_y_arg)
  {
    double utm_x = map_plane_x_arg + map_viz_origin_.x;
    double utm_y = map_plane_y_arg + map_viz_origin_.y;
    return getCellAttributeAtUTMPoint(utm_x, utm_y);
  }

  void getTileIndexFromUTMPoint(const double& utm_x_arg, const double& utm_y_arg, uint32_t* tile_x_idx_ptr, uint32_t* tile_y_idx_ptr)
  {
    *tile_x_idx_ptr = static_cast<uint32_t>((utm_x_arg - tile_map_origin_.x) / 100.0);
    *tile_y_idx_ptr = static_cast<uint32_t>((utm_y_arg - tile_map_origin_.y) / 100.0);
  }

  void getTileIndexFromMapPoint(const double& map_plane_x_arg, const double& map_plane_y_arg, uint32_t* tile_x_idx_ptr, uint32_t* tile_y_idx_ptr)
  {
    double utm_x = map_plane_x_arg + map_viz_origin_.x;
    double utm_y = map_plane_y_arg + map_viz_origin_.y;
    getTileIndexFromUTMPoint(utm_x, utm_y, tile_x_idx_ptr, tile_y_idx_ptr);
  }

  void saveMap(const std::string& save_map_folder_name);

  void clearAllTiles();

  void loadYamlFile(const std::string& tile_map_folder_name);


  Point2T<uint32_t> tile_map_origin_;
  Point2T<uint32_t> tile_size_;
  Point2d map_viz_origin_;

  std::vector< std::vector<GridMapTile*> > tiles_;

private:
  bool is_pose_updated_;

  double pose_x_;

  double pose_y_;

  std::thread map_update_thread_;

  bool trigger_thread_stop_;

  std::string map_folder_name_;

  Timer map_update_timer_;
};


#endif
