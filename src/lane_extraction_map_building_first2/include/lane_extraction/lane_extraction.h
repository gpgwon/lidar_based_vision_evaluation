#ifndef LIDAR_LANE_EXTRACTION_H
#define LIDAR_LANE_EXTRACTION_H

#include <velodyne_parser/velodyne_vls_128_parser.h>
#include <lane_extraction/simple_gps_ins_measurement.h>
#include <vector>
#include <deque>
#include <thread>
#include <fstream>

#include <boost/circular_buffer.hpp>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/segmentation/progressive_morphological_filter.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/common/transforms.h>
#include <pcl/filters/radius_outlier_removal.h>
#include <pcl/filters/conditional_removal.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/surface/mls.h>
#include <pcl/io/pcd_io.h>

#include <utm/utm.h>
#include <Eigen/Dense>
#include <opencv2/opencv.hpp>

#define VLS128_NUM_BLOCKS_PER_SEC  18000

typedef pcl::PointCloud<pcl::PointXYZI> PclCloudType;

class CellIndices
{
public:
  CellIndices()
  {}

  CellIndices(const int& x_index, const int& y_index)
    : x_index_(x_index), y_index_(y_index)
  {}

  int x_index_;
  int y_index_;
};

struct LocalGridCell
{
  bool is_ground_;
  PclCloudType points_;
  PointCloudf pointsf_;
  float max_z_;
  float min_z_;
  float mean_z_;
  bool is_clustered_;

  LocalGridCell() : is_ground_(false), max_z_(-9999.0), min_z_(9999.0), mean_z_(0.0), is_clustered_(false)
  {}
};

struct LaneExtractionParams
{
  // Path of the folder where the input rosbag files are located.
  std::string pointcloud_folder_;

  // Start and end indicies of rosbag files to process. E.g., [0, 100]
  std::vector<double> pointcloud_file_time_range_;

  // Path of the folder where the post-processed GPS/INS files are located.
  std::string gps_ins_folder_;

  // Path of the folder where the output results are stored.
  std::string output_folder_;

  // Flag indicating whether to use a timestamp file.
  bool use_timestamp_file_;

  // File path of the timestamp list file.
  std::string timestamp_file_path_;

  // Flag indicating whether to save result images.
  bool save_result_image_;

  // Flag indicating whether to publish visualization topics for debugging.
  bool publish_viz_topics_;

  // Measurement frequency of the GPS/INS.
  uint32_t gps_ins_measurement_frequency_hz_;

  // Measurement frequency of the camera.
  uint32_t camera_measurement_frequency_hz_;

  float lidar_fov_x_range_meter_;
  float lidar_fov_y_range_meter_;

  // Max time duration to accumulate the lidar data.
  uint32_t lidar_data_max_accumulation_time_sec_;

  // The x-directional size of the local grid map.
  uint32_t size_local_grid_x_meter_;

  // The y-directional size of the local grid map.
  uint32_t size_local_grid_y_meter_;

  // The number of local grid cells in x and y direction.
  uint32_t num_local_grid_cell_per_meter_;

  // The x-directional size of the local grid map for clustering.
  uint32_t size_clustering_grid_x_meter_;

  // The y-directional size of the local grid map for clustering.
  uint32_t size_clustering_grid_y_meter_;

  // The number of local grid cells in x direction.
  uint32_t num_clustering_grid_cell_per_meter_x_;

  // The number of local grid cells in y direction.
  uint32_t num_clustering_grid_cell_per_meter_y_;

  // Minimum number of points threshold in a clustering cell.
  uint32_t min_num_points_in_clustering_cell_threshold_;

  // Minimum number of points to be considered a cluster.
  uint32_t min_num_points_in_cluster_threshold_;

  // Maximum allowable distance to connect two clusters.
  float cluster_connection_check_maximum_distance_;

  // Flag indicating wheter to use the pcd processing mode.
  int pcd_process_;

  double pcd_process_timestamp_;
};

class LidarLaneExtraction
{

public:
  LidarLaneExtraction(const LaneExtractionParams& params);
  ~LidarLaneExtraction();

  void pushNewPointCloud(const PointCloudf& input);

  void pushNewGPSINS(const SimpleGPSINSMeasurement& input);

  void extractLanesAtGivenTimestamp(const double& timestamp);

  PclCloudType debug_cloud_full_;

  std::vector<PclCloudType> debug_cloud_lanes_;

  boost::circular_buffer<SimpleGPSINSMeasurement> oxts_buffer_;

  boost::circular_buffer<PclCloudType> pointcloud_buffer_;

  std::array<PointCloudf, 10> ten_point_clouds_;

  boost::circular_buffer<double> yaw_bias_buff_;

  bool pcd_file_saved_;

private:

  PclCloudType transformPointCloudToGlobalCoordinates(const PointCloudf& input_cloud);

  void extractGroundPoints(const PclCloudType& input, PclCloudType& output);

  void xyToLocalGridIndex(const float& x, const float& y, int& x_index, int& y_index);

  void xyToClusteringGridIndex(const float& x, const float& y, int& x_index, int& y_index);

  void xyToNonGroundObjectFilteringGridIndex(const float& x, const float& y, int& x_index, int& y_index);

  void setVizDebugCloudFull(const PclCloudType& viz_cloud);

  void setVizLanes(const std::vector<PclCloudType>& lanes);

  void extractLaneMarkingCandidates(const PointCloudf& cloud, PointCloudf& lane_marking_candidates);

  boost::circular_buffer<double> front_cam_time_stamp_buffer_;

  boost::circular_buffer<double> rear_cam_time_stamp_buffer_;

  Eigen::Vector3d pos_offset_;

  bool pos_offset_set_;

  uint32_t local_grid_size_x_;
  uint32_t local_grid_size_y_;

  std::ofstream fout_front_lanes_;

  LaneExtractionParams params_;
};



#endif
