#include <lane_extraction/lane_extraction.h>
#include <json.hpp>

Eigen::Matrix4f getRotMatrix_(double roll, double pitch, double yaw);
Eigen::Matrix4f getTransformMatrix(double x, double y, double z, double roll, double pitch, double yaw);
pcl::PointXYZI doInverseRotation(const pcl::PointXYZI& p, const double& roll, const double& pitch, const double& yaw);


bool point_comp_in_x_direction(const pcl::PointXYZI& p1, const pcl::PointXYZI& p2);
void xyToImageuv(const float& x, const float& y, int& u, int& v);

LidarLaneExtraction::LidarLaneExtraction()
  : output_folder_{"no_name"},
    pos_offset_set_(false)
{
  oxts_buffer_.set_capacity(oxts_data_hz * (accum_time_sec));
  pointcloud_buffer_.set_capacity(VLS128_NUM_BLOCKS_PER_SEC * accum_time_sec);
  front_cam_time_stamp_buffer_.set_capacity(camera_data_hz * accum_time_sec);
  yaw_bias_buff_.set_capacity(300);

  local_grid_size_x_ = max_grid_x_meter * num_local_grid_cell_per_meter + 1;
  local_grid_size_y_ = max_grid_y_meter * num_local_grid_cell_per_meter + 1;

  pcl::console::setVerbosityLevel(pcl::console::L_ALWAYS);
}

LidarLaneExtraction::~LidarLaneExtraction()
{

}

void LidarLaneExtraction::pushNewPointCloud(const PointCloudf& input)
{
  // Pointcloud will be transformed to gloal coordinates.
  // Minimum two GPS/INS measurements are necessary.
#if USE_MOTION_COMPENSATION
  if(oxts_buffer_.size() > 2)
  {
    pointcloud_buffer_.push_front(convertPointCloudToPclType(input));
  }
#else
  if(oxts_buffer_.size() > 2)
  {
    pointcloud_buffer_.push_front(transformPointCloudToGlobalCoordinates(input));
  }
#endif
}

void LidarLaneExtraction::pushNewGPSINS(const SimpleGPSINSMeasurement& input)
{
  SimpleGPSINSMeasurement _input = input;

  if(pos_offset_set_ == false)
  {
    pos_offset_ << input.utm_x_meters_, input.utm_y_meters_;
    pos_offset_set_ = true;
  }

  double avg_bias(0.0);
  if(oxts_buffer_.size() > 0)
  {
    double estimated_yaw = std::atan2(_input.utm_y_meters_ - oxts_buffer_[0].utm_y_meters_, _input.utm_x_meters_ - oxts_buffer_[0].utm_x_meters_);

    if(estimated_yaw < 0)
    {
      estimated_yaw += 2 * M_PI;
    }

    double yaw_bias = estimated_yaw - _input.yaw_rad_;
    if(yaw_bias > M_PI)
    {
      yaw_bias -= 2 * M_PI;
    }
    else if(yaw_bias < -M_PI)
    {
      yaw_bias += 2 * M_PI;
    }

    yaw_bias_buff_.push_back(yaw_bias);


    for(size_t i = 0; i < yaw_bias_buff_.size(); ++i)
    {
      avg_bias += yaw_bias_buff_[i];
    }
    avg_bias = avg_bias / static_cast<double>(yaw_bias_buff_.size());

  }

  _input.yaw_rad_ += avg_bias;
  oxts_buffer_.push_front(_input);
}

void LidarLaneExtraction::pushFrontCamTimestamp(const double& input)
{
  front_cam_time_stamp_buffer_.push_front(input);

  if(oxts_buffer_.size() == oxts_buffer_.capacity() && pointcloud_buffer_.size() == pointcloud_buffer_.capacity())
  {
    double past_cam_timestamp = front_cam_time_stamp_buffer_[front_cam_time_stamp_buffer_.capacity()/2];
    extractLanesAtGivenTimestamp(past_cam_timestamp);
  }
}

void LidarLaneExtraction::pushRearCamTimestamp(const double& input)
{
  rear_cam_time_stamp_buffer_.push_front(input);
}

PclCloudType LidarLaneExtraction::convertPointCloudToPclType(const PointCloudf& input_cloud)
{
  PclCloudType ret_cloud;
  size_t num_points = input_cloud.getSize();
  ret_cloud.reserve(num_points);
  ret_cloud.header.stamp = static_cast<uint64_t>(1E6 * input_cloud.points_[0].timestamp);

  for(size_t i = 0; i < num_points; ++i)
  {
    const float& x = input_cloud.points_[i].x;
    const float& y = input_cloud.points_[i].y;
    const float& z = input_cloud.points_[i].z;

    // Ignore the points corresponding to the ego-vehicle body.
    if(x > -2.2f && x < 2.79f && y > -0.945f && y < 0.945f)
    {
      continue;
    }

    // Ignore distant or non-ground points.
    else if(z <= -1.0f || z >= 1.0f || x <= -120.0f || x >= 120.0f || y <= -20.0f || y >= 20.0f)
    {
      continue;
    }

    pcl::PointXYZI pcl_point;
    pcl_point.x = x;
    pcl_point.y = y;
    pcl_point.z = z;
    pcl_point.intensity = static_cast<float>(input_cloud.points_[i].intensity);

    ret_cloud.push_back(pcl_point);
  }

  return ret_cloud;
}


PclCloudType LidarLaneExtraction::transformPointCloudToGlobalCoordinates(const PointCloudf& input_cloud)
{
  // PCL format pointcloud to return.
  PclCloudType ret_cloud;

  // Initialize the return pointcloud.
  size_t num_points = input_cloud.getSize();
  ret_cloud.reserve(num_points);
  ret_cloud.header.stamp = static_cast<uint64_t>(1E6 * input_cloud.points_[0].timestamp);

  // Nearest two GPS/INS measurements.
  SimpleGPSINSMeasurement oxts0 = oxts_buffer_[0];
  SimpleGPSINSMeasurement oxts1 = oxts_buffer_[1];

  // Oxts time gap.
  double oxts_time_gap = oxts0.timestamp_ - oxts1.timestamp_;

  // GPS/INS at point measurement time.
  SimpleGPSINSMeasurement gpsins_at_point;

  // A factor to calculate arithmatical average between two GPS/INS measurements to calculate GPS/INS measurement at point time.
  double r;

  // Point-wise local-to-global transformation.
  for(size_t i = 0; i < num_points; ++i)
  {
    // Eight points are acquired in one instant, so we can calculate GPS/INS every eight times.
    if(i % 8 == 0)
    {
      r = (oxts0.timestamp_ - input_cloud.points_[i].timestamp) / oxts_time_gap;
      gpsins_at_point = (1 - r) * oxts0 + r * oxts1;
    }

    const float& x = input_cloud.points_[i].x;
    const float& y = input_cloud.points_[i].y;
    const float& z = input_cloud.points_[i].z;

    pcl::PointXYZI pcl_point;

    // Ignore the points corresponding to the ego-vehicle body.
    if(x > -2.2f && x < 2.79f && y > -0.945f && y < 0.945f)
    {
#if SAVE_PCD
      continue;
#else
      //continue;
#endif
      pcl_point.x = 0;
      pcl_point.y = 0;
      pcl_point.z = 0;
      pcl_point.intensity = 0;
    }

    // Ignore distant or non-ground points.
#if SAVE_PCD
    else if(z <= -1.5f || z >= 1.5f || x <= -120.0f || x >= 120.0f || y <= -20.0f || y >= 20.0f)
    {
      continue;
#else
    else if(z <= -5.0f || z >= 5.0f || x <= -120.0f || x >= 120.0f || y <= -20.0f || y >= 20.0f)
    {
      //continue;
#endif
      pcl_point.x = 0;
      pcl_point.y = 0;
      pcl_point.z = 0;
      pcl_point.intensity = 0;
    }

    else
    {
      Eigen::Vector4f translation(
            static_cast<float>(gpsins_at_point.utm_x_meters_ - pos_offset_[0]),
          static_cast<float>(gpsins_at_point.utm_y_meters_ - pos_offset_[1]),
          static_cast<float>(gpsins_at_point.utm_z_meters_), 0);
      Eigen::Matrix4f rotation = getRotMatrix_(gpsins_at_point.roll_rad_, gpsins_at_point.pitch_rad_, gpsins_at_point.yaw_rad_);
      Eigen::Vector4f point_local(input_cloud.points_[i].x, input_cloud.points_[i].y, input_cloud.points_[i].z, 1);
      Eigen::Vector4f point_world = rotation * point_local + translation;


      pcl_point.x = point_world[0];
      pcl_point.y = point_world[1];
      pcl_point.z = point_world[2];
      pcl_point.intensity = static_cast<float>(input_cloud.points_[i].intensity);
    }

    ret_cloud.push_back(pcl_point);
  }
  return ret_cloud;
}


void LidarLaneExtraction::extractLanesAtGivenTimestamp(const double& timestamp)
{
  std::cout << "Extract lanes at " << timestamp << std::endl;

  size_t num_points;
  PclCloudType cloud;

  /* Find nearest previous GPS/INS measurement. */
  SimpleGPSINSMeasurement oxts0, oxts1;
  for(size_t i = 1; i < oxts_buffer_.size(); ++i)
  {
    if(timestamp > oxts_buffer_[i].timestamp_)
    {
      oxts1 = oxts_buffer_[i];
      oxts0 = oxts_buffer_[i - 1];
      break;
    }
  }

  /* Aggregate the pointclouds. */
  double oxts_time_gap = oxts0.timestamp_ - oxts1.timestamp_;

  SimpleGPSINSMeasurement gpsins_at_cam_timestamp;
  double r = (oxts0.timestamp_ - timestamp) / oxts_time_gap;
  gpsins_at_cam_timestamp = (1 - r) * oxts0 + r * oxts1;

  for(size_t i = 0; i < pointcloud_buffer_.size(); ++i)
  {
//    if(1E-6 * pointcloud_buffer_[i].header.stamp >= timestamp - static_cast<float>(accum_time_sec / 2.0f))
    {
//      std::cout << timestamp - 1E-6 * pointcloud_buffer_[i].header.stamp << std::endl;
      cloud += pointcloud_buffer_[i];
//    }
//    else
//    {
////      std::cout << "break: " << timestamp - 1E-6 * pointcloud_buffer_[i].header.stamp << std::endl;
//      break;
    }
  }

//  debug_cloud_full_ = cloud;
//  return;

#if SAVE_PCD
  {
    if(output_folder_.back() == '/')
    {
      output_folder_.resize(output_folder_.size() - 1);
    }

    uint64_t time_sec = static_cast<uint64_t>(timestamp);
    uint64_t time_nsec = static_cast<uint64_t>(static_cast<uint64_t>(1E3 * timestamp)) - 1E3 * time_sec;
    time_nsec *= 1E6;

    std::string str_timestamp_sec = std::to_string(time_sec);
    std::string str_timestamp_nsec = std::to_string(time_nsec);

    for(size_t i = 0; i < 9 - str_timestamp_nsec.size(); ++i)
    {
      str_timestamp_nsec.insert(str_timestamp_nsec.begin(), 1, '0');
    }

    std::string str_timestamp = str_timestamp_sec + "_" + str_timestamp_nsec;

    pcl::io::savePCDFileASCII (output_folder_ + "/" + str_timestamp + ".pcd", cloud);


    return;
  }
#endif

  // Extract lane candidates.
  num_points = cloud.size();

  PclCloudType cloud_global = cloud;
  cloud.clear();
  for(int i = 0; i < num_points; ++i)
  {
    if(cloud_global.points[i].intensity == 0)
    {
      continue;
    }

    if(i > 128 * 5 && i < num_points - 128 * 5 - 1)
    {
      bool is_flat(true);
      float avg_intensity(0.0f);

      for(int j = 2; j <= 5; ++j)
      {
        if(fabs(cloud_global.points[i + 128 * (j - 1)].z - cloud_global.points[i + 128 * j].z) > 0.1f)
        {
          is_flat = false;
          break;
        }
        avg_intensity += static_cast<float>(cloud_global.points[i + 128 * j].intensity) / 8.0f;
      }

      for(int j = -2; j >= -5; --j)
      {
        if(fabs(cloud_global.points[i + 128 * (j + 1)].z - cloud_global.points[i + 128 * j].z) > 0.1f)
        {
          is_flat = false;
          break;
        }
        avg_intensity += static_cast<float>(cloud_global.points[i + 128 * j].intensity) / 8.0f;
      }

      if(is_flat == true && cloud_global.points[i].intensity > 2.0 * avg_intensity
         && cloud_global.points[i].intensity >= 30.0f
         && avg_intensity < 20.0f)
      {
        //cloud_global.points[i].intensity = 255;
        cloud.push_back(cloud_global.points[i]);
      }
    }
  }

  // Filter out object points
  std::vector<std::vector<LocalGridCell>> local_grid;
  local_grid.resize(local_grid_size_x_);
  for(size_t i = 0; i < local_grid.size(); ++i)
  {
    local_grid[i].resize(local_grid_size_y_);
  }

  double veh_pos_x = gpsins_at_cam_timestamp.utm_x_meters_ - pos_offset_[0];
  double veh_pos_y = gpsins_at_cam_timestamp.utm_y_meters_ - pos_offset_[1];

  // Build a local grid and fill the points.
  for(size_t i = 0; i < num_points; ++i)
  {
    if(cloud_global.points[i].x == 0 && cloud_global.points[i].y == 0
       || cloud_global.points[i].intensity == 0)
    {
      continue;
    }
    size_t x_index, y_index;
    float x = cloud_global.points[i].x - veh_pos_x;
    float y = cloud_global.points[i].y - veh_pos_y;

    if(fabs(x) < 150.0f && fabs(y) < 150.0f)
    {
      xyToLocalGridIndex(x, y, x_index, y_index);
      if(x_index >= 0 && x_index < local_grid_size_x_
         && y_index >= 0 && y_index < local_grid_size_y_)
      {
        local_grid[x_index][y_index].points_.push_back(cloud_global.points[i]);
      }
    }
  }

  // Calculate minimum, maximum and average height of the cells.
  for(size_t grid_x = 0; grid_x < local_grid_size_x_; ++grid_x)
  {
    for(size_t grid_y = 0; grid_y < local_grid_size_y_; ++grid_y)
    {
      float min_z = 9999.0f;
      float max_z = -9999.0f;
      float sum_z = 0.0f;

      for(size_t j = 0; j < local_grid[grid_x][grid_y].points_.size(); ++j)
      {
        sum_z += local_grid[grid_x][grid_y].points_[j].z;
        if(local_grid[grid_x][grid_y].points_[j].z < min_z)
        {
          min_z = local_grid[grid_x][grid_y].points_[j].z;
        }

        if(local_grid[grid_x][grid_y].points_[j].z > max_z)
        {
          max_z = local_grid[grid_x][grid_y].points_[j].z;
        }
      }
      float mean_z = sum_z / static_cast<float>(local_grid[grid_x][grid_y].points_.size());

      local_grid[grid_x][grid_y].max_z_ = max_z;
      local_grid[grid_x][grid_y].min_z_ = min_z;
      local_grid[grid_x][grid_y].mean_z_ = mean_z;
    }
  }

  for(size_t grid_x = 0; grid_x < local_grid_size_x_; ++grid_x)
  {
    for(size_t grid_y = 0; grid_y < local_grid_size_y_; ++grid_y)
    {
      if(local_grid[grid_x][grid_y].max_z_ - local_grid[grid_x][grid_y].min_z_ < 0.5f)
      {
        local_grid[grid_x][grid_y].is_ground_ = true;
      }
    }
  }


  PclCloudType copied_cloud = cloud;
  cloud.clear();
  for(size_t i = 0; i < copied_cloud.size(); ++i)
  {
    float x = copied_cloud.points[i].x - veh_pos_x;
    float y = copied_cloud.points[i].y - veh_pos_y;

    if(fabs(x) >= 150.0f || fabs(y) >= 150.0f)
      continue;

    size_t idx_x, idx_y;
    xyToLocalGridIndex(x, y, idx_x, idx_y);

    if(local_grid[idx_x][idx_y].is_ground_ == true)
    {
      cloud.push_back(copied_cloud.points[i]);
    }

  }


  /* Transform to vehicle coordinates at camera timestamp. */
  Eigen::Vector3f translation(gpsins_at_cam_timestamp.utm_x_meters_ - pos_offset_[0],
      gpsins_at_cam_timestamp.utm_y_meters_ - pos_offset_[1], gpsins_at_cam_timestamp.utm_z_meters_);
//  Eigen::Matrix4f rotation = getRotMatrix_(gpsins_at_cam_timestamp.roll_rad_, gpsins_at_cam_timestamp.pitch_rad_, gpsins_at_cam_timestamp.yaw_rad_);
//  rotation = rotation.inverse();

  Eigen::Matrix3f rotation = getInverseRotMatrix3f(gpsins_at_cam_timestamp.roll_rad_, gpsins_at_cam_timestamp.pitch_rad_, gpsins_at_cam_timestamp.yaw_rad_);



  num_points = cloud.size();
//#pragma omp parallel for
  for(size_t i = 0; i < num_points; ++i)
  {
    if(cloud.points[i].intensity > 0)
    {
      Eigen::Vector3f global_point(cloud.points[i].x, cloud.points[i].y, cloud.points[i].z);
      Eigen::Vector3f diff_point = global_point - translation;

      Eigen::Vector3f local_point = rotation * diff_point;
      cloud.points[i].x = local_point[0];
      cloud.points[i].y = local_point[1];
      cloud.points[i].z = local_point[2];
    }
    else
    {
      cloud.points[i].x = 0.0f;
      cloud.points[i].y = 0.0f;
      cloud.points[i].z = 0.0f;
    }
  }




  /* Ground and lane marking extraction. */
//  std::vector<std::vector<LocalGridCell>> local_grid;
//  local_grid.resize(local_grid_size_x_);
//  for(size_t i = 0; i < local_grid.size(); ++i)
//  {
//    local_grid[i].resize(local_grid_size_y_);
//  }

//  // Build a local grid and fill the points.
//  for(size_t i = 0; i < num_points; ++i)
//  {
//    if(cloud_global.points[i].x == 0.0f && cloud_global.points[i].y == 0.0f)
//    {
//      continue;
//    }
//    size_t x_index, y_index;
//    if(fabs(cloud_global.points[i].x) < 150.0f && fabs(cloud_global.points[i].y) < 30.0f)
//    {
//      xyToLocalGridIndex(cloud_global.points[i].x, cloud_global.points[i].y, x_index, y_index);
//      local_grid[x_index][y_index].points_.push_back(cloud_global.points[i]);
//    }
//  }

//  // Calculate minimum, maximum and average height of the cells.
//  for(size_t grid_x = 0; grid_x < local_grid_size_x_; ++grid_x)
//  {
//    for(size_t grid_y = 0; grid_y < local_grid_size_y_; ++grid_y)
//    {
//      float min_z = 999.0f;
//      float max_z = -999.0f;
//      float sum_z = 0.0f;

//      for(size_t j = 0; j < local_grid[grid_x][grid_y].points_.size(); ++j)
//      {
//        sum_z += local_grid[grid_x][grid_y].points_[j].z;
//        if(local_grid[grid_x][grid_y].points_[j].z < min_z)
//        {
//          min_z = local_grid[grid_x][grid_y].points_[j].z;
//        }

//        if(local_grid[grid_x][grid_y].points_[j].z > max_z)
//        {
//          max_z = local_grid[grid_x][grid_y].points_[j].z;
//        }
//      }
//      float mean_z = sum_z / static_cast<float>(local_grid[grid_x][grid_y].points_.size());

//      local_grid[grid_x][grid_y].max_z_ = max_z;
//      local_grid[grid_x][grid_y].min_z_ = min_z;
//      local_grid[grid_x][grid_y].mean_z_ = mean_z;
//    }
//  }

  // Filter out non-ground points or low intensity points.
//  cloud.clear();
//  for(size_t grid_x = 0; grid_x < local_grid_size_x_; ++grid_x)
//  {
//    for(size_t grid_y = 0; grid_y < local_grid_size_y_; ++grid_y)
//    {
//      if(local_grid[grid_x][grid_y].max_z_ - local_grid[grid_x][grid_y].min_z_ < 0.2f)
//      {
//        local_grid[grid_x][grid_y].is_ground_ = true;
////        for(size_t j = 0; j < local_grid[grid_x][grid_y].points_.size(); ++j)
////        {
////          if(local_grid[grid_x][grid_y].points_[j].intensity > 80.0f)
////          {
////            cloud.push_back(local_grid[grid_x][grid_y].points_[j]);
////          }
////        }
//      }
//    }
//  }




  /* Outlier removal */
  PclCloudType::Ptr cloud_ptr(new PclCloudType(cloud));
  *cloud_ptr = cloud;
  cloud.clear();

  pcl::StatisticalOutlierRemoval<pcl::PointXYZI> sor;
  sor.setInputCloud (cloud_ptr);
  sor.setMeanK (30);
  sor.setStddevMulThresh (1.0);
  sor.filter (cloud);





  /* Clustering */

  // Regenerate the local grid with filtered cloud.
  std::vector<std::vector<LocalGridCell>> filtered_local_grid;
  size_t clustering_grid_size_x = max_grid_x_meter_clustering * num_clustering_grid_cell_per_meter_x + 1;
  size_t clustering_grid_size_y = max_grid_y_meter_clustering * num_clustering_grid_cell_per_meter_y + 1;

  filtered_local_grid.resize(clustering_grid_size_x);
  for(size_t i = 0; i < filtered_local_grid.size(); ++i)
  {
    filtered_local_grid[i].resize(clustering_grid_size_y);
  }

  for(size_t i = 0; i < cloud.size(); ++i)
  {
    size_t x_index, y_index;
    xyToClusteringGridIndex(cloud.points[i].x, cloud.points[i].y, x_index, y_index);
    if(x_index < clustering_grid_size_x && y_index < clustering_grid_size_y)
    {
      filtered_local_grid[x_index][y_index].points_.push_back(cloud.points[i]);
    }

  }

  // Perform clustering
  std::vector<PclCloudType> dist_clusters;
  for(size_t grid_x = 0; grid_x < clustering_grid_size_x; ++grid_x)
  {
    for(size_t grid_y = 0; grid_y < clustering_grid_size_y; ++grid_y)
    {
      if(filtered_local_grid[grid_x][grid_y].is_clustered_ == false && filtered_local_grid[grid_x][grid_y].points_.size() > 0)
      {
        filtered_local_grid[grid_x][grid_y].is_clustered_ = true;
        std::vector<Index2D> clustered_cell_indices;
        std::vector<Index2D> newly_clustered_cell_indices;
        clustered_cell_indices.emplace_back(grid_x, grid_y);
        size_t start_index_to_explorer = 0;

        while(true)
        {
          for(size_t i = start_index_to_explorer; i < clustered_cell_indices.size(); ++i)
          {
            for(int dx = -2; dx <= 2; ++dx)
            {
              int int_grid_x = static_cast<int>(clustered_cell_indices[i].x_);
              if(int_grid_x + dx < 0 || int_grid_x + dx >= static_cast<int>(clustering_grid_size_x))
              {
                continue;
              }
              size_t x_index = static_cast<size_t>(int_grid_x + dx);

              for(int dy = -2; dy <= 2; ++dy)
              {
                int int_grid_y = static_cast<int>(clustered_cell_indices[i].y_);
                if(int_grid_y + dy < 0 || int_grid_y + dy >= static_cast<int>(clustering_grid_size_y))
                {
                  continue;
                }
                size_t y_index = static_cast<size_t>(int_grid_y + dy);

                if(filtered_local_grid[x_index][y_index].is_clustered_ == false && filtered_local_grid[x_index][y_index].points_.size() > 0)
                {
                  newly_clustered_cell_indices.emplace_back(x_index, y_index);
                  filtered_local_grid[x_index][y_index].is_clustered_ = true;
                }
              }
            }
          }
          if(newly_clustered_cell_indices.size() > 0)
          {
            start_index_to_explorer = clustered_cell_indices.size();
            for(size_t i = 0; i < newly_clustered_cell_indices.size(); ++i)
            {
              clustered_cell_indices.push_back(newly_clustered_cell_indices[i]);
            }
            newly_clustered_cell_indices.clear();
          }
          else
          {
            PclCloudType cluster_cloud;
            for(size_t i = 0; i < clustered_cell_indices.size(); ++i)
            {
              const size_t& x_index = clustered_cell_indices[i].x_;
              const size_t& y_index = clustered_cell_indices[i].y_;
              cluster_cloud += filtered_local_grid[x_index][y_index].points_;
            }

            if(cluster_cloud.size() > min_num_points_in_cluster_threshold)
            {
              dist_clusters.push_back(cluster_cloud);
            }
            break;
          }
        }
      }
    }
  }


  // Sorting points in x direction.
  for(size_t i = 0; i < dist_clusters.size(); ++i)
  {
    std::sort(dist_clusters[i].begin(), dist_clusters[i].end(), point_comp_in_x_direction);
  }

  // Filter out clusters that are too narrow in x direction.
//  std::vector<PclCloudType> copied_dist_clusters = dist_clusters;
//  dist_clusters.clear();
//  for(size_t i = 0; i < copied_dist_clusters.size(); ++i)
//  {
//    if(copied_dist_clusters[i].points.back().x - copied_dist_clusters[i].points[0].x >= 1.0)
//    {
//      dist_clusters.push_back(copied_dist_clusters[i]);
//    }
//  }



  // Lane clustering
  std::vector<PclCloudType> lane_clusters;
  std::vector<PclCloudType> left_lanes;
  std::vector<PclCloudType> right_lanes;

  if(dist_clusters.size() > 1)
  {
    std::vector<size_t> cluster_connectivity(dist_clusters.size(), 999);
    std::vector<size_t> is_connected(dist_clusters.size(), 0);

    for(size_t i = 0; i < dist_clusters.size() - 1; ++i)
    {
      for(size_t j = i + 1; j < dist_clusters.size(); ++j)
      {
        if(dist_clusters[j].front().x - dist_clusters[i].back().x < 30.0f && dist_clusters[j].front().x - dist_clusters[i].back().x > 0.0f
           && fabs(dist_clusters[j].front().y - dist_clusters[i].back().y) < 2.0)
        {
          cluster_connectivity[i] = j;
          break;
        }
      }
    }
    for(size_t i = 0; i < dist_clusters.size(); ++i)
    {
      if(is_connected[i] == 1)
      {
        continue;
      }

      if(cluster_connectivity[i] == 999)
      {
        lane_clusters.push_back(dist_clusters[i]);
      }
      else
      {
        lane_clusters.push_back(dist_clusters[i]);
        size_t idx_next_connected_cluster = cluster_connectivity[i];
        while(true)
        {
          lane_clusters.back() += dist_clusters[idx_next_connected_cluster];
          is_connected[idx_next_connected_cluster] = 1;
          if(cluster_connectivity[idx_next_connected_cluster] == 999)
          {
            break;
          }
          else
          {
            idx_next_connected_cluster = cluster_connectivity[idx_next_connected_cluster];
          }
        }
      }
    }
  }

  std::vector<float> y0_of_cluster_curves;
  std::vector<Eigen::Vector3f> curve_cooeficients;
  std::vector<bool> is_combined;
  std::vector<PclCloudType> copied_lane_clusters = lane_clusters;
  lane_clusters.clear();
  for(size_t i = 0; i < copied_lane_clusters.size(); ++i)
  {
    PclCloudType downsampled_points;

    if(copied_lane_clusters[i].size() > 1000)
    {
      // Find a point that is closest to the vehicle position.
      float min_dist = 999.0f;
      size_t min_idx = 999999;
      for(size_t j = 0; j < copied_lane_clusters[i].size(); ++j)
      {
        float dist = std::sqrt(std::pow(copied_lane_clusters[i].points[j].x, 2) + std::pow(copied_lane_clusters[i].points[j].y, 2));
        if(dist < min_dist)
        {
          min_dist = dist;
          min_idx = j;
        }
      }

      downsampled_points.push_back(copied_lane_clusters[i][min_idx]);
      for(size_t j = min_idx; j < copied_lane_clusters[i].size(); ++j)
      {
        if(copied_lane_clusters[i][j].x - downsampled_points.back().x > 0.5
           && fabs(copied_lane_clusters[i][j].y - downsampled_points.back().y) < 0.3)
        {
          downsampled_points.push_back(copied_lane_clusters[i][j]);
        }
      }

      for(size_t j = min_idx; j > 0; --j)
      {
        if(copied_lane_clusters[i][j].x - downsampled_points.back().x < -0.5
           && fabs(copied_lane_clusters[i][j].y - downsampled_points.back().y) < 0.3)
        {
          downsampled_points.push_back(copied_lane_clusters[i][j]);
        }
      }
    }
    else
    {
      downsampled_points = copied_lane_clusters[i];
    }

    //Check if this cluster includes lane points or outliers.
    Eigen::MatrixXf A(downsampled_points.size(), 3);
    Eigen::VectorXf b(downsampled_points.size());

    for(size_t j = 0; j < downsampled_points.size(); ++j)
    {
      A(j, 0) = std::pow(downsampled_points.points[j].x, 2);
      A(j, 1) = downsampled_points.points[j].x;
      A(j, 2) = 1.0f;
      b(j) = downsampled_points.points[j].y;
    }

    Eigen::VectorXf c = (A.transpose() * A).inverse() * A.transpose() * b;

    PclCloudType inliers;
    for(size_t j = 0; j < copied_lane_clusters[i].size(); ++j)
    {
      float px = copied_lane_clusters[i].points[j].x;
      float py = copied_lane_clusters[i].points[j].y;

      float curve_y = c[0] * std::pow(px, 2) + c[1] * px + c[2];
      if(fabs(curve_y - py) < 0.3f)
      {
        inliers.push_back(copied_lane_clusters[i].points[j]);
      }
    }
//    if(static_cast<float>(inliers.size()) > 0.5f * copied_lane_clusters[i].size())
    if(inliers.size() > 0)
    {
      lane_clusters.push_back(inliers);
      y0_of_cluster_curves.push_back(c[2]);
      curve_cooeficients.push_back(c);
      is_combined.push_back(false);
    }
  }



  if(lane_clusters.size() > 1)
  {
    // Final clustering
    for(size_t i = 0; i < lane_clusters.size() - 1; ++i)
    {
      for(size_t j = i + 1; j < lane_clusters.size(); ++j)
      {
        const float x0 = lane_clusters[j][0].x;
        const float y0 = lane_clusters[j][0].y;
        const float xe = lane_clusters[j].back().x;
        const float ye = lane_clusters[j].back().y;

        if(is_combined[j] == false
          && fabs(curve_cooeficients[i][0] * std::pow(x0, 2) + curve_cooeficients[i][1] * x0 + curve_cooeficients[i][2] - y0) < 1.5
          && fabs(curve_cooeficients[i][0] * std::pow(xe, 2) + curve_cooeficients[i][1] * xe + curve_cooeficients[i][2] - ye) < 1.5)
        {
          lane_clusters[i] += lane_clusters[j];
          is_combined[j] = true;
        }
      }
    }



    // Eliminate too small clusters.
    copied_lane_clusters = lane_clusters;
    std::vector<float> copied_y0_of_cluster_curves = y0_of_cluster_curves;
    y0_of_cluster_curves.clear();
    lane_clusters.clear();
    for(size_t i = 0; i < is_combined.size(); ++i)
    {
      if(is_combined[i] == false && copied_lane_clusters[i].size() > 30)
      {
        lane_clusters.push_back(copied_lane_clusters[i]);
        y0_of_cluster_curves.push_back(copied_y0_of_cluster_curves[i]);
      }
    }



    // Filter out clusters that are too narrow in x direction.
    copied_lane_clusters = lane_clusters;
    lane_clusters.clear();
    copied_y0_of_cluster_curves = y0_of_cluster_curves;
    y0_of_cluster_curves.clear();
    for(size_t i = 0; i < copied_lane_clusters.size(); ++i)
    {
      if(copied_lane_clusters[i].points.back().x - copied_lane_clusters[i].points[0].x >= 10.0)
      {
        lane_clusters.push_back(copied_lane_clusters[i]);
        y0_of_cluster_curves.push_back(copied_y0_of_cluster_curves[i]);
      }
    }



    // Outlier removal -- again
    copied_lane_clusters = lane_clusters;
    lane_clusters.clear();
    y0_of_cluster_curves.clear();
    for(size_t i = 0; i < copied_lane_clusters.size(); ++i)
    {
      PclCloudType downsampled_points;
      downsampled_points.push_back(copied_lane_clusters[i][0]);
      if(copied_lane_clusters[i].size() > 1000)
      {
        for(size_t j = 0; j < copied_lane_clusters[i].size(); ++j)
        {
          if(copied_lane_clusters[i][j].x - downsampled_points.back().x > 0.5
             && fabs(copied_lane_clusters[i][j].y - downsampled_points.back().y) < 0.5)
          {
            downsampled_points.push_back(copied_lane_clusters[i][j]);
          }
        }
      }
      else if(copied_lane_clusters[i].size() > 100)
      {
        for(size_t j = 0; j < copied_lane_clusters[i].size(); ++j)
        {
          if(copied_lane_clusters[i][j].x - downsampled_points.back().x > 0.1
             && fabs(copied_lane_clusters[i][j].y - downsampled_points.back().y) < 0.5
             || copied_lane_clusters[i][j].x - downsampled_points.back().x > 10)
          {
            downsampled_points.push_back(copied_lane_clusters[i][j]);
          }
        }
      }
      else
      {
        downsampled_points = copied_lane_clusters[i];
      }

      //Check if this cluster includes lane points or outliers.
      Eigen::MatrixXf A(downsampled_points.size(), 3);
      Eigen::VectorXf b(downsampled_points.size());

      for(size_t j = 0; j < downsampled_points.size(); ++j)
      {
        A(j, 0) = std::pow(downsampled_points.points[j].x, 2);
        A(j, 1) = downsampled_points.points[j].x;
        A(j, 2) = 1.0f;
        b(j) = downsampled_points.points[j].y;
      }

      Eigen::VectorXf c = (A.transpose() * A).inverse() * A.transpose() * b;

      PclCloudType inliers;
      for(size_t j = 0; j < copied_lane_clusters[i].size(); ++j)
      {
        float px = copied_lane_clusters[i].points[j].x;
        float py = copied_lane_clusters[i].points[j].y;

        float curve_y = c[0] * std::pow(px, 2) + c[1] * px + c[2];
        if(fabs(curve_y - py) < 0.3f)
        {
          inliers.push_back(copied_lane_clusters[i].points[j]);
        }
      }
      if(static_cast<float>(inliers.size()) > 0.6f * copied_lane_clusters[i].size())
      {
        lane_clusters.push_back(inliers);
        y0_of_cluster_curves.push_back(c[2]);
      }
    }

    // Sort the clusters along y direction.
    copied_lane_clusters = lane_clusters;
    copied_y0_of_cluster_curves = y0_of_cluster_curves;
    y0_of_cluster_curves.clear();
    lane_clusters.clear();
    lane_clusters.push_back(copied_lane_clusters[0]);
    y0_of_cluster_curves.push_back(copied_y0_of_cluster_curves[0]);
    std::vector<PclCloudType>::iterator it;
    std::vector<float>::iterator it_y0;

    if(copied_lane_clusters.size() > 1)
    {
      for(size_t i = 1; i < copied_lane_clusters.size(); ++i)
      {
        it = lane_clusters.begin();
        it_y0 = y0_of_cluster_curves.begin();
        size_t insert_index = 0;
        for(size_t j = 0; j < lane_clusters.size(); ++j)
        {
          if(j == 0)
          {
            if(copied_y0_of_cluster_curves[i] < y0_of_cluster_curves[j])
            {
              insert_index = j;
              break;
            }
          }

          if(j == lane_clusters.size() - 1)
          {
            if(copied_y0_of_cluster_curves[i] > y0_of_cluster_curves[j])
            {
              insert_index = j + 1;
              break;
            }
          }

          if(copied_y0_of_cluster_curves[i] > y0_of_cluster_curves[j]
             && copied_y0_of_cluster_curves[i] < y0_of_cluster_curves[j + 1])
          {
            insert_index = j + 1;
          }
        }

        lane_clusters.insert(it + insert_index, copied_lane_clusters[i]);
        y0_of_cluster_curves.insert(it_y0 + insert_index, copied_y0_of_cluster_curves[i]);
      }
    }


    std::vector<float> y0_left, y0_right;

    for(size_t i = 0; i < lane_clusters.size(); ++i)
    {
      if(y0_of_cluster_curves[i] > 0)
      {
        if(i - 1 >= 0)
        {
          for(int j = i - 1; j >= 0; --j)
          {
            right_lanes.push_back(lane_clusters[j]);
            y0_right.push_back(y0_of_cluster_curves[j]);
          }
        }

        for(size_t j = i; j < lane_clusters.size(); ++j)
        {
          left_lanes.push_back(lane_clusters[j]);
          y0_left.push_back(y0_of_cluster_curves[j]);
        }

        break;
      }
    }

//    if(y0_left[0] > 4.0)
//    {
//      left_lanes.clear();
//    }

//    if(y0_right[0] < -4.0)
//    {
//      right_lanes.clear();
//    }


    lane_clusters.clear();

    if(right_lanes.size() > 0)
    {
      lane_clusters.push_back(right_lanes[0]);

      if(right_lanes.size() > 1)
      {
        for(size_t i = 1; i < right_lanes.size(); ++i)
        {
          if(y0_right[i - 1] - y0_right[i] > 5.0)
          {
            std::vector<PclCloudType>::iterator it_start = right_lanes.begin() + i;
            right_lanes.erase(it_start, right_lanes.end());
            break;
          }
          else
          {
            lane_clusters.push_back(right_lanes[i]);
          }
        }
      }
    }

    if(left_lanes.size() > 0)
    {
      lane_clusters.push_back(left_lanes[0]);

      if(left_lanes.size() > 1)
      {
        for(size_t i = 1; i < left_lanes.size(); ++i)
        {
          if(y0_left[i] - y0_left[i - 1] > 5.0)
          {
            std::vector<PclCloudType>::iterator it_start = left_lanes.begin() + i;
            left_lanes.erase(it_start, left_lanes.end());
            break;
          }
          else
          {
            lane_clusters.push_back(left_lanes[i]);
          }
        }
      }
    }

    std::cout << "Number of lanes: left - " << left_lanes.size() << ", right - " << right_lanes.size() << std::endl;

  }




  cloud.clear();
#if DISPLAY_DEBUG_INFO
  for(size_t i = 0; i < lane_clusters.size(); ++i)
  {
    for(size_t j = 0; j < lane_clusters[i].points.size(); ++j)
    {
      float inten = 255.0f * static_cast<float>(i) / static_cast<float>(lane_clusters.size());
      lane_clusters[i].points[j].intensity = inten;
    }
    cloud += lane_clusters[i];
  }
  debug_cloud_lanes_ = cloud;
#endif

  if(output_folder_.compare("no_name") != 0 && lane_clusters.size() > 0)
  {
    if(output_folder_.back() == '/')
    {
      output_folder_.resize(output_folder_.size() - 1);
    }

#if SAVE_RESULT_IMAGE
    // Save image.
    cv::Mat image(600, 2000, CV_8UC3, cv::Scalar(0,0,0));

    cv::line(image, cv::Point(1000, 300), cv::Point(1050, 300), cv::Scalar(0,0,255));
    cv::line(image, cv::Point(1000, 300), cv::Point(1000, 250), cv::Scalar(0,0,255));

    int num_clusters = static_cast<int>(lane_clusters.size());
    for(int i = 0; i < num_clusters; ++i)
    {
      float i_f = static_cast<float>(i);
      float num_f = static_cast<float>(num_clusters);

      int r, g, b;

      float ratio = i_f / num_f;
      int a = static_cast<int>((1.0f - ratio) / 0.25f);	//invert and group
      int X = static_cast<int>(a);	//this is the integer part
      int Y = static_cast<int>(255.0f * (a - X)); //fractional part from 0 to 255
      switch(X)
      {
          case 4: r=255; g=Y; b=0; break;
          case 3: r=255-Y; g=255; b=0; break;
          case 2: r=0; g=255; b=Y; break;
          case 1: r=0; g=255-Y; b=255; break;
          case 0: r=0; g=0; b=255; break;
      }

      for(size_t j = 0; j < lane_clusters[i].size(); ++j)
      {
        const float& x = lane_clusters[i].points[j].x;
        const float& y = lane_clusters[i].points[j].y;
        if(x < -100.0f || y < -100.0f || x >= 100.0f || y >= 100.0f)
          continue;

        int u, v;
        xyToImageuv(x, y, u, v);

        image.at<cv::Vec3b>(v, u)[0] = static_cast<unsigned char>(b);
        image.at<cv::Vec3b>(v, u)[1] = static_cast<unsigned char>(g);
        image.at<cv::Vec3b>(v, u)[2] = static_cast<unsigned char>(r);
      }
    }
#endif

    fout_front_lanes_ << std::fixed;
    fout_front_lanes_.precision(3);

    uint64_t time_sec = static_cast<uint64_t>(timestamp);
    uint64_t time_nsec = static_cast<uint64_t>(static_cast<uint64_t>(1E3 * timestamp)) - 1E3 * time_sec;
    time_nsec *= 1E6;

    std::string str_timestamp_sec = std::to_string(time_sec);
    std::string str_timestamp_nsec = std::to_string(time_nsec);

    int diff_str_length = 9 - static_cast<int>(str_timestamp_nsec.size());

    for(int i = 0; i < diff_str_length; ++i)
    {
      str_timestamp_nsec.insert(str_timestamp_nsec.begin(), 1, '0');
    }

    std::string str_timestamp = str_timestamp_sec + "_" + str_timestamp_nsec;


    fout_front_lanes_.open(output_folder_ + "/" + str_timestamp + ".txt", std::ios::out|std::ios::trunc);

#if SAVE_RESULT_IMAGE
    cv::imwrite(output_folder_ + "/" + str_timestamp + ".jpg", image);
#endif




    for(size_t i = 0; i < left_lanes.size(); ++i)
    {
      fout_front_lanes_ << i + 1;
      for(size_t j = 0; j < left_lanes[i].size(); ++j)
      {
        fout_front_lanes_ << "\t" << left_lanes[i].points[j].x << "\t" << left_lanes[i].points[j].y;
      }
      fout_front_lanes_ << std::endl;
    }

    for(size_t i = 0; i < right_lanes.size(); ++i)
    {
      fout_front_lanes_ << -static_cast<int>(i) - 1;
      for(size_t j = 0; j < right_lanes[i].size(); ++j)
      {
        fout_front_lanes_ << "\t" << right_lanes[i].points[j].x << "\t" << right_lanes[i].points[j].y;
      }
      fout_front_lanes_ << std::endl;
    }


    fout_front_lanes_.close();
  }

#if DISPLAY_PHANTOMVISION_LANES
  // Find corresponding json files.
  std::string front_json_file, rear_json_file;
  double min_time_diff(999.0);
  for(const auto& pair : front_phantomvision_lane_json_list_)
  {
    double time_diff = fabs(pair.timestamp_ - timestamp);
    if(time_diff < min_time_diff)
    {
      min_time_diff = time_diff;
      front_json_file = pair.file_path_;
    }
  }

  min_time_diff = 999.0;
  for(const auto& pair : rear_phantomvision_lane_json_list_)
  {
    double time_diff = fabs(pair.timestamp_ - timestamp);
    if(time_diff < min_time_diff)
    {
      min_time_diff = time_diff;
      rear_json_file = pair.file_path_;
    }
  }

  std::cout << front_json_file << std::endl;
  std::cout << rear_json_file << std::endl;

  // Rear the json files and extract the lanes.
  std::ifstream fin_front(front_json_file);
  if(!fin_front.is_open())
  {
    std::cout << "Fail to read the front phantomvision json file." << std::endl;
    return;
  }

  std::ifstream fin_rear(rear_json_file);
  if(!fin_rear.is_open())
  {
    std::cout << "Fail to read the rear phantomvision json file." << std::endl;
    return;
  }

  nlohmann::json j_front, j_rear;
  fin_front >> j_front;
  fin_rear >> j_rear;

  std::vector<std::array<double, 4>> front_lane_coeffs;
  std::vector<std::array<double, 4>> rear_lane_coeffs;

  for(const auto& lane : j_front["lanes"])
  {
    std::array<double, 4> coeff;
    coeff[0] = lane["c0"];
    coeff[1] = lane["c1"];
    coeff[2] = lane["c2"];
    coeff[3] = lane["c3"];
    front_lane_coeffs.push_back(coeff);
  }

  for(const auto& lane : j_rear["lanes"])
  {
    std::array<double, 4> coeff;
    coeff[0] = lane["c0"];
    coeff[1] = lane["c1"];
    coeff[2] = lane["c2"];
    coeff[3] = lane["c3"];
    rear_lane_coeffs.push_back(coeff);
  }

  // Convert to point cloud in vehicle_frame.
  PclCloudType front_lane_points;
  PclCloudType rear_lane_points;
  for(const auto& coeff : front_lane_coeffs)
  {
    for(double x = 5.0; x <= 100.0; x += 0.1)
    {

      double y(0.0);
      for(size_t i = 0; i < 4; ++i)
      {
        y += coeff[i] * std::pow(x, i);
      }
      pcl::PointXYZI point;
      point.x = x;
      point.y = y;
      point.z = 0.0;
      point.intensity = 50;
      front_lane_points.push_back(point);
    }
  }

  for(const auto& coeff : rear_lane_coeffs)
  {
    for(double x = -100.0; x <= -5.0; x += 0.1)
    {

      double y(0.0);
      for(size_t i = 0; i < 4; ++i)
      {
        y += coeff[i] * std::pow(x, i);
      }
      pcl::PointXYZI point;
      point.x = x;
      point.y = y;
      point.z = 0.0;
      point.intensity = 200;
      rear_lane_points.push_back(point);
    }
  }


//   Transform rear point_cloud to camera frame.
//  double theta = degree_to_radian(-0.4);
//  for(auto& point : rear_lane_points.points)
//  {
//    point.x = std::cos(theta) * point.x - std::sin(theta) * point.y;
//    point.y = std::sin(theta) * point.x + std::cos(theta) * point.y;
//  }


  debug_phantom_vision_lanes_ = front_lane_points;
  debug_phantom_vision_lanes_ += rear_lane_points;

#endif
}

void LidarLaneExtraction::extractGroundPoints(const PclCloudType& input, PclCloudType& output)
{
  std::cout << "Start ground extraction." << std::endl;

  pcl::PointCloud<pcl::PointXYZI>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZI>);
  pcl::PointIndicesPtr ground (new pcl::PointIndices);

  pcl::ProgressiveMorphologicalFilter<pcl::PointXYZI> pmf;
  PclCloudType::Ptr cloud(new PclCloudType(input));

  pmf.setInputCloud(cloud);
  pmf.setMaxWindowSize (20);
  pmf.setSlope (1.0f);
  pmf.setInitialDistance (0.5f);
  pmf.setMaxDistance (3.0f);
  pmf.extract(ground->indices);

  // Create the filtering object
  pcl::ExtractIndices<pcl::PointXYZI> extract;
  extract.setInputCloud (cloud);
  extract.setIndices (ground);
  extract.filter (*cloud_filtered);

  output = *cloud_filtered;

  std::cout << "Ground extraction finished." << std::endl;
}

void LidarLaneExtraction::xyToLocalGridIndex(const float& x, const float& y, size_t& x_index, size_t& y_index)
{
  x_index = static_cast<size_t>(static_cast<float>(num_local_grid_cell_per_meter) * (x + static_cast<float>(max_grid_x_meter / 2)));
  y_index = static_cast<size_t>(static_cast<float>(num_local_grid_cell_per_meter) * (y + static_cast<float>(max_grid_y_meter / 2)));
}

void LidarLaneExtraction::xyToClusteringGridIndex(const float& x, const float& y, size_t& x_index, size_t& y_index)
{
  x_index = static_cast<size_t>(static_cast<float>(num_clustering_grid_cell_per_meter_x) * (x + static_cast<float>(max_grid_x_meter_clustering / 2)));
  y_index = static_cast<size_t>(static_cast<float>(num_clustering_grid_cell_per_meter_y) * (y + static_cast<float>(max_grid_y_meter_clustering / 2)));
}

void LidarLaneExtraction::setOutputFolder(const std::string& input)
{
  output_folder_ = input;
}

bool point_comp_in_x_direction(const pcl::PointXYZI& p1, const pcl::PointXYZI& p2)
{
  if(p1.x < p2.x)
  {
    return true;
  }
  else
  {
    return false;
  }
}

Eigen::Matrix4f getRotMatrix_(double roll, double pitch, double yaw)
{
  Eigen::Matrix4f matrix;
  double cos_yaw = std::cos(yaw);
  double sin_yaw = std::sin(yaw);
  double cos_pitch = std::cos(pitch);
  double sin_pitch = std::sin(pitch);
  double cos_roll = std::cos(roll);
  double sin_roll = std::sin(roll);

  double comb_a = cos_yaw * sin_pitch;
  double comb_b = sin_yaw * sin_pitch;
  matrix(0, 0) = cos_yaw * cos_pitch;
  matrix(0, 1) = comb_a * sin_roll - sin_yaw * cos_roll;
  matrix(0, 2) = comb_a * cos_roll + sin_yaw * sin_roll;

  matrix(1, 0) = sin_yaw * cos_pitch;
  matrix(1, 1) = comb_b * sin_roll + cos_yaw * cos_roll;
  matrix(1, 2) = comb_b * cos_roll - cos_yaw * sin_roll;

  matrix(2, 0) = -sin_pitch;
  matrix(2, 1) = cos_pitch * sin_roll;
  matrix(2, 2) = cos_pitch * cos_roll;

  matrix(3, 0) = 0;
  matrix(3, 1) = 0;
  matrix(3, 2) = 0;

  matrix(0, 3) = 0;
  matrix(1, 3) = 0;
  matrix(2, 3) = 0;
  matrix(3, 3) = 1;

  return matrix;
}

Eigen::Matrix4f getTransformMatrix(double x, double y, double z, double roll, double pitch, double yaw)
{
  Eigen::Matrix4f matrix;
  double cos_yaw = std::cos(yaw);
  double sin_yaw = std::sin(yaw);
  double cos_pitch = std::cos(pitch);
  double sin_pitch = std::sin(pitch);
  double cos_roll = std::cos(roll);
  double sin_roll = std::sin(roll);


  double comb_a = cos_yaw * sin_pitch;
  double comb_b = sin_yaw * sin_pitch;
  matrix(0, 0) = cos_yaw * cos_pitch;
  matrix(0, 1) = comb_a * sin_roll - sin_yaw * cos_roll;
  matrix(0, 2) = comb_a * cos_roll + sin_yaw * sin_roll;

  matrix(1, 0) = sin_yaw * cos_pitch;
  matrix(1, 1) = comb_b * sin_roll + cos_yaw * cos_roll;
  matrix(1, 2) = comb_b * cos_roll - cos_yaw * sin_roll;

  matrix(2, 0) = -sin_pitch;
  matrix(2, 1) = cos_pitch * sin_roll;
  matrix(2, 2) = cos_pitch * cos_roll;

  matrix(3, 0) = 0;
  matrix(3, 1) = 0;
  matrix(3, 2) = 0;

  matrix(0, 3) = x;
  matrix(1, 3) = y;
  matrix(2, 3) = z;
  matrix(3, 3) = 1;

  return matrix;
}

void xyToImageuv(const float& x, const float& y, int& u, int& v)
{
  u = static_cast<int>(10 * x) + 1000;
  v = -static_cast<int>(10 * y) + 300;
}

pcl::PointXYZI doInverseRotation(const pcl::PointXYZI& p, const double& roll, const double& pitch, const double& yaw)
{
    const float& x = p.x;
    const float& y = p.y;
    const float& z = p.z;

    double x1 = cos(yaw) * x + sin(yaw) * y;
    double y1 = -sin(yaw) * x + cos(yaw) * y;
    double z1 = z;

    double x2 = cos(pitch) * x1 - sin(pitch) * z1;
    double y2 = y1;
    double z2 = +sin(pitch) * x1 + cos(pitch) * z1;

    pcl::PointXYZI out;
    out.x = x2;
    out.y = cos(roll) * y2 + sin(roll) * z2;
    out.z = -sin(roll) * y2 + cos(roll) * z2;
    out.intensity = p.intensity;

    return out;
}
