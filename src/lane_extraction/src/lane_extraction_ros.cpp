#include <lane_extraction/lane_extraction.h>

// ROS headers
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>

// std headers
#include <iostream>
#include <string>
#include <vector>
#include <stdint.h>
#include <fstream>

// Message headers
#include <phantom_ros/NetworkFrame.h>
#include <phantom_ros/PoseMeasurement.h>
#include <sensor_msgs/Image.h>
#include <phantom_ros/PhantomVisionMeasurement.h>

std::shared_ptr<LidarLaneExtraction> lane_extraction_ptr{nullptr};
std::shared_ptr<ros::Publisher> pub_debug_cloud_full{nullptr};
std::shared_ptr<ros::Publisher> pub_debug_lanes{nullptr};
std::shared_ptr<ros::Publisher> pub_debug_phantomvision_lanes{nullptr};

SimpleGPSINSMeasurement convertOXTSMessageToSimpleGPSINSMeasurement(const phantom_ros::PoseMeasurement::ConstPtr oxts_msg);
void publishPointCloudCallback(const PointCloudf& point_cloud);
void publishDebugInfo(const pcl::PointCloud<pcl::PointXYZI>& cloud_full, const pcl::PointCloud<pcl::PointXYZI>& cloud_lanes);

const float roll_offset = 0.0f;
const float pitch_offset = 0.0f;
const float yaw_offset = 0.0f;

//const std::string bag_folder_name = "/media/gpgwon/Samsung-T3/recording/2019-11-18-21-26-58_Evaluation_WestCoast";
//const std::string bag_file_prefix = "2019-11-18-12-52-34_Evaluation_WestCost";
const std::string bag_folder_name = "/media/gpgwon/L4_SSD2/eval_data/2019-11-19-09-37-25_Evaluation_WestCoast";
const std::string bag_file_prefix = "2019-11-19-09-37-25_Evaluation_WestCoast";


size_t bag_start_index = 21;
size_t bag_end_index = 29;

int main(int argc, char* argv[])
{

  // Read arguments and store the bag file list.
  std::vector<std::string> bag_file_list;
  std::string out_file_folder;
  std::string timestamp_file_path;
  std::string node_name;
  bool node_name_set(false);

  if(argc == 1)
  {
    std::cout << "\n\n" << std::endl;
    std::cout << "Options: " << std::endl;
    std::cout << "-i [input file list]" << std::endl;
    std::cout << "-o [output directory path]" << std::endl;
    std::cout << "-t [timestamp list file path]" << std::endl << std::endl;
    std::cout << "Usage example: " << std::endl;
    std::cout << "lane_extraction_ros -i /home/bagfiles/*.bag -o /home/results -t timestamps.txt" << std::endl;
  }
  else
  {
    int option;
    for (int i = 1; i < argc; i++)
    {
      std::string str(argv[i]);

      if(str.compare("-i") == 0)
      {
        option = 0;
        continue;
      }
      else if(str.compare("-o") == 0)
      {
        option = 1;
        continue;
      }
      else if(str.compare("-t") == 0)
      {
        option = 2;
        continue;
      }
      else if(str.compare("-n") == 0)
      {
        option = 3;
        continue;
      }


      if(option == 0)
      {
        bag_file_list.push_back(str);
      }
      else if(option == 1)
      {
        out_file_folder = str;
      }
      else if(option == 2)
      {
        timestamp_file_path = str;
      }
      else if(option == 3)
      {
        node_name = str;
        node_name_set = true;
      }
    }
  }

  if(node_name_set == true)
  {
    ros::init(argc, argv, node_name);
  }
  else
  {
    ros::init(argc, argv, "lidar_lane_extraction");
  }

  ros::NodeHandle nh;

  pub_debug_cloud_full = std::make_shared<ros::Publisher>(nh.advertise<sensor_msgs::PointCloud2>("debug_cloud_full", 5));
  pub_debug_lanes = std::make_shared<ros::Publisher>(nh.advertise<sensor_msgs::PointCloud2>("debug_lanes", 5));
  pub_debug_phantomvision_lanes = std::make_shared<ros::Publisher>(nh.advertise<sensor_msgs::PointCloud2>("debug_phantomvision_lanes", 5));


#if READ_TIMESTAMP_FILE_MODE

  std::vector<double> cam_timestamp;

  std::ifstream fin(timestamp_file_path, std::ios::in);

  if(fin.is_open() == false)
  {
    std::cout << "Fail to read the timestamp file." << std::endl;
    return 0;
  }

  size_t cam_timestamp_index = 0;
  uint64_t timestamp;
  while(fin >> timestamp)
  {
    double timestamp_double = 1E-3 * static_cast<double>(timestamp);
    cam_timestamp.push_back(timestamp_double);
  }

  if(cam_timestamp.size() < 1)
  {
    std::cout << "No timestamp exist in the timestamp file." << std::endl;
  }

  std::sort(cam_timestamp.begin(), cam_timestamp.end());
  fin.close();
#endif

#if DISPLAY_PHANTOMVISION_LANES
  std::string front_phantomvision_lane_dir = "/media/gpgwon/L4_SSD2/eval_data/phantomvision_lane/gipoong_timestamp_2/front";
  std::string rear_phantomvision_lane_dir = "/media/gpgwon/L4_SSD2/eval_data/phantomvision_lane/gipoong_timestamp_2/rear";

  std::vector<std::string> front_list;
  std::vector<std::string> rear_list;

  getFileListInTargetFolder(front_phantomvision_lane_dir, front_list);
  getFileListInTargetFolder(rear_phantomvision_lane_dir, rear_list);

  std::vector<TimeStampFileNamePair> front_phantomvision_json_list;
  std::vector<TimeStampFileNamePair> rear_phantomvision_json_list;

  for(const auto& file_path : front_list)
  {
    std::string last_branch = extractLastBranchNameOfPath(file_path);
    std::string timestamp_str = removeFileExtention(last_branch);

    double timestamp_double = convertSec_nSecTimeToDouble(timestamp_str);

    TimeStampFileNamePair pair;
    pair.timestamp_ = timestamp_double;
    pair.file_path_ = file_path;

    front_phantomvision_json_list.push_back(pair);
  }

  for(const auto& file_path : rear_list)
  {
    std::string last_branch = extractLastBranchNameOfPath(file_path);
    std::string timestamp_str = removeFileExtention(last_branch);

    double timestamp_double = convertSec_nSecTimeToDouble(timestamp_str);

    TimeStampFileNamePair pair;
    pair.timestamp_ = timestamp_double;
    pair.file_path_ = file_path;

    rear_phantomvision_json_list.push_back(pair);
  }

#endif

#if PCD_VISUALIZATION
  for(size_t i = 0; i < cam_timestamp.size(); ++i)
  {
    if(!ros::ok())
    {
      return 0;
    }
    uint64_t time_sec = static_cast<uint64_t>(cam_timestamp[i]);
    uint64_t time_nsec = static_cast<uint64_t>(static_cast<uint64_t>(1E3 * cam_timestamp[i])) - 1E3 * time_sec;
    time_nsec *= 1E6;

    std::string str_timestamp_sec = std::to_string(time_sec);
    std::string str_timestamp_nsec = std::to_string(time_nsec);

    for(size_t i = 0; i < 9 - str_timestamp_nsec.size(); ++i)
    {
      str_timestamp_nsec.insert(str_timestamp_nsec.begin(), 1, '0');
    }

    std::string str_timestamp = str_timestamp_sec + "_" + str_timestamp_nsec;

    pcl::PointCloud<pcl::PointXYZI> cloud;
    pcl::io::loadPCDFile (out_file_folder + "/" + str_timestamp + ".pcd", cloud);

    publishDebugInfo(cloud, lane_extraction_ptr->debug_cloud_lanes_);

    std::cout << str_timestamp << " published" << std::endl;
  }

  return 1;
#endif

#if MANUAL_BAG_FILE_LIST_SET

  for(size_t i = bag_start_index; i <= bag_end_index; ++i)
  {
    std::string file_num_str = std::to_string(i);
    std::string file_path;
    if(i == 0)
    {
      file_path = bag_folder_name + "/" + bag_file_prefix + "_000.bag";
    }
    else if(i < 10)
    {
      file_path = bag_folder_name + "/" + bag_file_prefix + "_00" + file_num_str + ".bag";
    }
    else if(i < 100)
    {
      file_path = bag_folder_name + "/" + bag_file_prefix + "_0" + file_num_str + ".bag";
    }
    else
    {
      file_path = bag_folder_name + "/" + bag_file_prefix + "_" + file_num_str + ".bag";
    }

    bag_file_list.push_back(file_path);
  }
#endif

  // Declare a Velodyne128 parser object.
  VelodyneVLS128Parser velodyne_parser;

  // Initialize the parser.
  velodyne_parser.initialize();
  velodyne_parser.setPublishFullScan(false);

  // Register the publish pointcloud callback.
  velodyne_parser.registerPublishCallback(publishPointCloudCallback);

  // Declare LidarLaneExtraction object.
  lane_extraction_ptr = std::make_shared<LidarLaneExtraction>();

  lane_extraction_ptr->setOutputFolder(out_file_folder);


#if DISPLAY_PHANTOMVISION_LANES
  lane_extraction_ptr->front_phantomvision_lane_json_list_ = front_phantomvision_json_list;
  lane_extraction_ptr->rear_phantomvision_lane_json_list_ = rear_phantomvision_json_list;
#endif


  std::vector<std::string> topics;
  topics.push_back("/network/vls_128_data");
  topics.push_back("/oxts");
  topics.push_back("/csi_cam/front_center/image_raw");
  topics.push_back("/csi_cam/rear_center/image_raw");

  std::cout << std::fixed << std::endl;

  bool initial_ts_index_set(false);

  /* Read the bag files one-by-one, parse the velodyne packets in the bag files, and save pointclouds to files. */
  for(size_t i = 0; i < bag_file_list.size(); ++i)
  {
    if(!ros::ok())
    {
      break;
    }

    std::cout << "Processing file - " << bag_file_list[i] << std::endl;

    // Read the bag file.
    rosbag::Bag bag_read(bag_file_list[i], rosbag::bagmode::Read);

    // An object to capture topics of interest from the ROS bag file.
    rosbag::View view(bag_read, rosbag::TopicQuery(topics));

    // Get the velodyne packet topics from the bag file and parse them.
    for(rosbag::MessageInstance const msg: view)
    {
      if(!ros::ok())
      {
        break;
      }

#if READ_TIMESTAMP_FILE_MODE
      if(initial_ts_index_set == false)
      {
        while(ros::ok())
        {
          if(cam_timestamp[cam_timestamp_index] < msg.getTime().toSec() + 2.0)
          {
            cam_timestamp_index++;
            if(cam_timestamp_index >= cam_timestamp.size())
            {
              std::cout << "No more timestamp to process exists." << std::endl;
              return 1;
            }
          }
          else
          {
            std::cout << "Extracting lanes at - " << cam_timestamp[cam_timestamp_index] << std::endl;
            initial_ts_index_set = true;
            break;
          }
        }
      }

      if(msg.getTime().toSec() < cam_timestamp[cam_timestamp_index] - 4.1)
      {
        lane_extraction_ptr->oxts_buffer_.clear();
        lane_extraction_ptr->pointcloud_buffer_.clear();
        continue;
      }

      if(msg.getTime().toSec() >= cam_timestamp[cam_timestamp_index] + 4.0)
      {
        lane_extraction_ptr->extractLanesAtGivenTimestamp(cam_timestamp[cam_timestamp_index]);
#if DISPLAY_DEBUG_INFO
        publishDebugInfo(lane_extraction_ptr->debug_cloud_full_, lane_extraction_ptr->debug_cloud_lanes_);        
#endif

#if DISPLAY_PHANTOMVISION_LANES
        sensor_msgs::PointCloud2 cloud_msg;
        pcl::toROSMsg(lane_extraction_ptr->debug_phantom_vision_lanes_, cloud_msg);

        cloud_msg.header.frame_id = "map";
        cloud_msg.header.stamp = ros::Time::now();

        pub_debug_phantomvision_lanes->publish(cloud_msg);
#endif
        cam_timestamp_index++;
        if(cam_timestamp_index >= cam_timestamp.size())
        {
          std::cout << "No more timestamp to process exists." << std::endl;
          return 1;
        }

        std::cout << "Extracting lanes at - " << cam_timestamp[cam_timestamp_index] << std::endl;
      }
#endif

      if(msg.getTopic() == "/network/vls_128_data")
      {
        phantom_ros::NetworkFrame::ConstPtr input_msg = msg.instantiate<phantom_ros::NetworkFrame>();
        double timestamp = msg.getTime().toSec();
        velodyne_parser.parseDataPacket(input_msg->data, timestamp);
      }

      else if(msg.getTopic() == "/oxts")
      {
        phantom_ros::PoseMeasurement::ConstPtr input_msg = msg.instantiate<phantom_ros::PoseMeasurement>();
        lane_extraction_ptr->pushNewGPSINS(convertOXTSMessageToSimpleGPSINSMeasurement(input_msg));
      }

#if (READ_TIMESTAMP_FILE_MODE == false)
      else if(msg.getTopic() == "/csi_cam/front_center/image_raw")
      {
        sensor_msgs::Image::ConstPtr input_msg = msg.instantiate<sensor_msgs::Image>();
        lane_extraction_ptr->pushFrontCamTimestamp(input_msg->header.stamp.toSec());

        publishDebugInfo(lane_extraction_ptr->debug_cloud_full_, lane_extraction_ptr->debug_cloud_lanes_);
      }

      else if(msg.getTopic() == "/csi_cam/rear_center/image_raw")
      {
        sensor_msgs::Image::ConstPtr input_msg = msg.instantiate<sensor_msgs::Image>();
        lane_extraction_ptr->pushRearCamTimestamp(input_msg->header.stamp.toSec());
      }
#endif
    }
  }



  return 1;
}


SimpleGPSINSMeasurement convertOXTSMessageToSimpleGPSINSMeasurement(const phantom_ros::PoseMeasurement::ConstPtr input)
{
  SimpleGPSINSMeasurement ret;

  ret.timestamp_ = input->utc_time.toSec();
//  ret.timestamp_ = input->header.stamp.toSec();

  double calculated_utm_x, calculated_utm_y;
  long utm_zone;
  char utm_hemisphere;
  Convert_Geodetic_To_UTM(degree_to_radian(input->position.latitude),
    degree_to_radian(input->position.longitude),
    &utm_zone,
    &utm_hemisphere,
    &calculated_utm_x, &calculated_utm_y);

  ret.utm_x_meters_ = calculated_utm_x;
  ret.utm_y_meters_ = calculated_utm_y;
  ret.utm_z_meters_ = input->position.altitude;

  ret.position_covariance_ = input->position.position_covariance[0];

  ret.velocity_x_mps_ = -input->velocity.y;
  ret.velocity_y_mps_ = input->velocity.x;
  ret.velocity_z_mps_ = input->velocity.z;

  ret.xy_speed_mps_ = std::sqrt(std::pow(ret.velocity_x_mps_, 2) + std::pow(ret.velocity_y_mps_, 2));

  ret.roll_rad_  = input->imu.orientation.x + degree_to_radian(roll_offset);
  ret.pitch_rad_ = -input->imu.orientation.y + degree_to_radian(pitch_offset);
  ret.yaw_rad_   = input->imu.orientation.z + HalfPI + degree_to_radian(yaw_offset);
  if(ret.yaw_rad_ > TwoPI)
    ret.yaw_rad_ -= TwoPI;
  else if(ret.yaw_rad_ < 0.0)
    ret.yaw_rad_ +=  TwoPI;

  ret.acc_x_mpss_ = input->imu.linear_acceleration.x;
  ret.acc_y_mpss_ = -input->imu.linear_acceleration.y;
  ret.acc_z_mpss_ = input->imu.linear_acceleration.z;

  ret.gyro_x_rps_ = input->imu.angular_velocity.x;
  ret.gyro_y_rps_ = -input->imu.angular_velocity.y;
  ret.gyro_z_rps_ = input->imu.angular_velocity.z;

  return ret;
}

void publishPointCloudCallback(const PointCloudf& point_cloud)
{
  lane_extraction_ptr->pushNewPointCloud(point_cloud);
}

void publishDebugInfo(const pcl::PointCloud<pcl::PointXYZI>& cloud_full, const pcl::PointCloud<pcl::PointXYZI>& cloud_lanes)
{
  sensor_msgs::PointCloud2 cloud_full_msg;
  pcl::toROSMsg(cloud_full, cloud_full_msg);

  cloud_full_msg.header.frame_id = "map";
  cloud_full_msg.header.stamp = ros::Time::now();

  pub_debug_cloud_full->publish(cloud_full_msg);

  sensor_msgs::PointCloud2 cloud_lane_msg;
  pcl::toROSMsg(cloud_lanes, cloud_lane_msg);

  cloud_lane_msg.header.frame_id = "map";
  cloud_lane_msg.header.stamp = ros::Time::now();

  pub_debug_lanes->publish(cloud_lane_msg);
}
