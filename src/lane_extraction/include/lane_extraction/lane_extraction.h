#ifndef LIDAR_LANE_EXTRACTION_H
#define LIDAR_LANE_EXTRACTION_H

#include <common/file_system.h>
#include <common/utils.h>
#include <velodyne_parser/velodyne_vls_128_parser.h>
#include <lane_extraction/simple_gps_ins_measurement.h>
#include <vector>
#include <deque>
#include <thread>
#include <fstream>

#include <boost/circular_buffer.hpp>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/segmentation/progressive_morphological_filter.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/common/transforms.h>
#include <pcl/filters/radius_outlier_removal.h>
#include <pcl/filters/conditional_removal.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/surface/mls.h>
#include <pcl/io/pcd_io.h>

#include <utm/utm.h>
#include <Eigen/Dense>
#include <opencv2/opencv.hpp>

#define VLS128_NUM_BLOCKS_PER_SEC  18000
#define USE_MOTION_COMPENSATION false

#define READ_TIMESTAMP_FILE_MODE true
#define MANUAL_BAG_FILE_LIST_SET true
#define DISPLAY_PHANTOMVISION_LANES true

#define SAVE_RESULT_IMAGE true
#define DISPLAY_DEBUG_INFO true
#define SAVE_PCD false
#define PCD_VISUALIZATION false

typedef pcl::PointCloud<pcl::PointXYZI> PclCloudType;

struct TimeStampFileNamePair
{
  double timestamp_;
  std::string file_path_;
};

struct LocalGridCell
{
  bool is_ground_;
  PclCloudType points_;
  float max_z_;
  float min_z_;
  float mean_z_;
  bool is_clustered_;

  LocalGridCell() : is_ground_(false), max_z_(0.0), min_z_(0.0), mean_z_(0.0), is_clustered_(false)
  {}
};

struct Index2D
{
  size_t x_;
  size_t y_;

  Index2D(const size_t& x, const size_t& y) : x_(x), y_(y)
  {}
};

class LidarLaneExtraction
{  
  const size_t oxts_data_hz = 100;
  const size_t camera_data_hz = 15;
  const size_t accum_time_sec = 8;
  const size_t max_grid_x_meter = 300;
  const size_t max_grid_y_meter = 300;
  const size_t max_grid_x_meter_clustering = 300;
  const size_t max_grid_y_meter_clustering = 60;
  const size_t num_local_grid_cell_per_meter = 4;
  const size_t num_clustering_grid_cell_per_meter_x = 1;
  const size_t num_clustering_grid_cell_per_meter_y = 4;
  const size_t min_num_points_in_cluster_threshold = 3;

public:
  LidarLaneExtraction();
  ~LidarLaneExtraction();

  void pushNewPointCloud(const PointCloudf& input);

  void pushNewGPSINS(const SimpleGPSINSMeasurement& input);

  void pushFrontCamTimestamp(const double& input);

  void pushRearCamTimestamp(const double& input);

  void setOutputFolder(const std::string& input);

  void extractLanesAtGivenTimestamp(const double& timestamp);

  PclCloudType debug_cloud_full_;

  PclCloudType debug_cloud_lanes_;

  PclCloudType debug_phantom_vision_lanes_;

  boost::circular_buffer<SimpleGPSINSMeasurement> oxts_buffer_;

  boost::circular_buffer<PclCloudType> pointcloud_buffer_;

  boost::circular_buffer<double> yaw_bias_buff_;

  std::vector<TimeStampFileNamePair> front_phantomvision_lane_json_list_;
  std::vector<TimeStampFileNamePair> rear_phantomvision_lane_json_list_;

private:

  PclCloudType transformPointCloudToGlobalCoordinates(const PointCloudf& input_cloud);

  PclCloudType convertPointCloudToPclType(const PointCloudf& input_cloud);

  void extractGroundPoints(const PclCloudType& input, PclCloudType& output);

  void xyToLocalGridIndex(const float& x, const float& y, size_t& x_index, size_t& y_index);

  void xyToClusteringGridIndex(const float& x, const float& y, size_t& x_index, size_t& y_index);

  void pointCloudMotionCompensation(const double& curr_timestamp, PclCloudType& output_cloud);

  boost::circular_buffer<double> front_cam_time_stamp_buffer_;

  boost::circular_buffer<double> rear_cam_time_stamp_buffer_;

  std::string output_folder_;

  Eigen::Vector2d pos_offset_;

  bool pos_offset_set_;

  size_t local_grid_size_x_;
  size_t local_grid_size_y_;

  std::ofstream fout_front_lanes_;

};



#endif
