#include <convert_velodyne_packet_bags_to_pointcloud_files/convert_velodyne_packet_bags_to_pointcloud_files.h>

// ROS headers
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>

// std headers
#include <iostream>
#include <string>
#include <vector>
#include <stdint.h>
#include <fstream>

// 3rd party
#include <yaml-cpp/yaml.h>

// Message headers
#include <convert_velodyne_packet_bags_to_pointcloud_files/NetworkFrame.h>

#define PUBLISH_DEBUG_CLOUD false

std::string out_file_folder("");
#if PUBLISH_DEBUG_CLOUD
std::shared_ptr<ros::Publisher> pub_debug_cloud{nullptr};
#endif

struct Config
{
  // Rosbag parent folder path.
  std::string rosbag_parent_folder_;

  std::string rosbag_file_start_date_;

  // Path of the folder where the output results are stored.
  std::string output_folder_;
};


void getParsedLidarPointCloudCallback(const PointCloudf& point_cloud)
{
  size_t num_points = point_cloud.getSize();


#if PUBLISH_DEBUG_CLOUD
  pcl::PointCloud<pcl::PointXYZI> pcl_cloud;
  pcl_cloud.reserve(num_points);
  for(size_t i = 0; i < num_points; ++i)
  {
    pcl::PointXYZI pcl_point;
    pcl_point.x = point_cloud.points_[i].x;
    pcl_point.y = point_cloud.points_[i].y;
    pcl_point.z = point_cloud.points_[i].z;
    pcl_point.intensity = static_cast<float>(point_cloud.points_[i].intensity);

    pcl_cloud.push_back(pcl_point);
  }

  sensor_msgs::PointCloud2 cloud_msg;
  pcl::toROSMsg(pcl_cloud, cloud_msg);
  cloud_msg.header.frame_id = "vehicle_frame";
  cloud_msg.header.stamp = ros::Time::now();
  pub_debug_cloud->publish(cloud_msg);
#endif

  if(out_file_folder.compare("") == 0)
  {
    return;
  }

  std::string file_name;
  uint64_t first_point_timestamp_nsec = static_cast<uint64_t>(1.0E9 * point_cloud.points_[0].timestamp);

  uint64_t time_sec = static_cast<uint64_t>(1.0E-9 * first_point_timestamp_nsec);
  uint64_t time_nsec = first_point_timestamp_nsec - 1.0E9 * time_sec;

  if(time_nsec < 1000000)
  {
    file_name = std::to_string(time_sec) + "_000" + std::to_string(time_nsec) + ".pc";
  }
  else if(time_nsec < 10000000)
  {
    file_name = std::to_string(time_sec) + "_00" + std::to_string(time_nsec) + ".pc";
  }
  else if(time_nsec < 100000000)
  {
    file_name = std::to_string(time_sec) + "_0" + std::to_string(time_nsec) + ".pc";
  }
  else
  {
    file_name = std::to_string(time_sec) + "_" + std::to_string(time_nsec) + ".pc";
  }

  std::vector<char> write_data(17 * num_points);
  size_t pos = 0;
  for(size_t i = 0; i < num_points; ++i)
  {
    float x = point_cloud.points_[i].x;
    float y = point_cloud.points_[i].y;
    float z = point_cloud.points_[i].z;
    uint8_t intensity = point_cloud.points_[i].intensity;
    uint32_t time_offset_nsec = static_cast<uint32_t>(static_cast<uint64_t>(1.0E9 * point_cloud.points_[i].timestamp) - first_point_timestamp_nsec);

    std::memcpy(&write_data[pos], &x, 4);
    std::memcpy(&write_data[pos + 4], &y, 4);
    std::memcpy(&write_data[pos + 8], &z, 4);
    std::memcpy(&write_data[pos + 12], &intensity, 1);
    std::memcpy(&write_data[pos + 13], &time_offset_nsec, 4);
    pos += 17;
  }


  if(out_file_folder.back() == '/')
  {
    out_file_folder.resize(out_file_folder.size() - 1);
  }

  std::ofstream fout(out_file_folder + "/" + file_name, std::ios::out | std::ios::binary);
  fout.write(&write_data[0], 17 * num_points);

  fout.close();
}

void loadBagToPCFilesConfigs(Config& params)
{
  YAML::Node yaml_node;
  try
  {
    std::cout << "Try to read a yaml file - " << std::string(LIDAR_BASED_VISION_EVALUATION_PARAM_DIR)
                 + "/lane_extraction_map_building_first/lane_extraction_map_building_first_params.yaml" << std::endl;

    yaml_node = YAML::LoadFile(std::string(LIDAR_BASED_VISION_EVALUATION_PARAM_DIR)
                               + "/convert_velodyne_packet_bags_to_pointcloud_files/convert_velodyne_packet_bags_to_pointcloud_files.yaml");

  }
  catch (const YAML::Exception& e)
  {
    std::cout << "Fail to read the yaml file." << std::endl;
  }
  params.output_folder_ = yaml_node["output_folder"].as<std::string>();
  params.rosbag_parent_folder_ = yaml_node["rosbag_parent_folder"].as<std::string>();
  params.rosbag_file_start_date_ = yaml_node["rosbag_file_start_date"].as<std::string>();
}

int main(int argc, char* argv[])
{
#if PUBLISH_DEBUG_CLOUD
  ros::init(argc, argv, "lidar_lane_extraction");
  ros::NodeHandle nh;
  pub_debug_cloud = std::make_shared<ros::Publisher>(nh.advertise<sensor_msgs::PointCloud2>("debug_cloud", 5));
#endif

  bool batch_process(false);

  Config params;
  loadBagToPCFilesConfigs(params);

  // Read arguments and store the bag file list.
  std::vector<std::string> bag_file_list;
  if(argc == 1)
  {
    std::cout << "\n\n" << std::endl;
    std::cout << "Usage Examples:" << std::endl;
    std::cout << "vel_bag_to_pc_files [in_file0] [in_file1] ... [in_filen] -o [output_folder_path]" << std::endl;
    std::cout << "vel_bag_to_pc_files *.bag -o [output_folder_path]" << std::endl << std::endl;
    std::cout << "Output file format:\n";
    std::cout << "  File name - Timestamp at the first point of the point cloud. [nsec.]" << std::endl;
    std::cout << "  File contents - A Binary fomrat with repeated point format below:" << std::endl;;
    std::cout << "    x y z intensity time_offset (time offset from the first point)" << std::endl;;
    std::cout << "    where x, y and z - 4 bytes float32 [meters]" << std::endl;;
    std::cout << "    intensity - 1 byte uint8_t" << std::endl;;
    std::cout << "    time_offset - 4 bytes uint32_t [nsec.]" << std::endl;
  }
  else
  {
    for (int i = 1; i < argc; i++)
    {
      std::string str(argv[i]);
      if(str.compare("-o") == 0)
      {
        if(i + 1 < argc)
        {
          out_file_folder = argv[i + 1];
        }
        else
        {
          std::cout << "Input the path of the folder to store point cloud files." << std::endl;
        }
        break;
      }
      else if(str.compare("-b") == 0)
      {
        batch_process = true;
      }

      bag_file_list.push_back(str);
    }
  }


  // Get the list of dates
  std::vector<std::string> bag_file_child_folders;
  std::vector<boost::filesystem::directory_entry> v;
  std::vector<std::string> date_folder_list;
  if(batch_process == true)
  {
    copy(boost::filesystem::directory_iterator(params.rosbag_parent_folder_),
      boost::filesystem::directory_iterator(), back_inserter(v));

    for(std::vector<boost::filesystem::directory_entry>::const_iterator it = v.begin(); it != v.end();  ++it)
    {
      date_folder_list.push_back((*it).path().string());
    }

    std::string rosbag_start_date_str = params.rosbag_parent_folder_ + "/" + params.rosbag_file_start_date_;
    std::sort(date_folder_list.begin(), date_folder_list.end());
    std::vector<std::string> temp_foler_list = date_folder_list;
    date_folder_list.clear();
    for(const auto& folder : temp_foler_list)
    {
      if(folder.compare(rosbag_start_date_str) >= 0)
      {
        date_folder_list.push_back(folder);
      }
    }

    for(const auto& date_folder : date_folder_list)
    {
      std::vector<std::string> dataset_folder_list;
      v.clear();

      copy(boost::filesystem::directory_iterator(date_folder),
        boost::filesystem::directory_iterator(), back_inserter(v));

      for(std::vector<boost::filesystem::directory_entry>::const_iterator it = v.begin(); it != v.end();  ++it)
      {
        std::string child_folder = (*it).path().string();

        std::string saved_child_folder = child_folder;

        // Extract the dataset name.
        std::string token = child_folder.substr(0, child_folder.find('/'));
        size_t pos = 0;
        while ((pos = child_folder.find('/')) != std::string::npos) {
          token = child_folder.substr(0, pos);
          child_folder.erase(0, pos + 1);
        }
        token = child_folder.substr(0, pos);

        out_file_folder = params.output_folder_ + "/" + token;

        // Create the output folder if the folder doesn't exist.
        if(!boost::filesystem::exists(out_file_folder))
        {
          boost::filesystem::path out_folder_boost_path(out_file_folder);
          boost::filesystem::create_directory(out_folder_boost_path);

          std::cout << "Processing -- " << token << std::endl;
        }
        else
        {
          continue;
        }

        // Declare a Velodyne128 parser object.
        VelodyneVLS128Parser velodyne_parser;

        // Initialize the parser.
        velodyne_parser.initialize();

        // Register the publish pointcloud callback.
        velodyne_parser.registerPublishCallback(getParsedLidarPointCloudCallback);


        std::vector<boost::filesystem::directory_entry> v_file;
        copy(boost::filesystem::directory_iterator(saved_child_folder),
          boost::filesystem::directory_iterator(), back_inserter(v_file));

        std::vector<std::string> bag_file_list;
        for(std::vector<boost::filesystem::directory_entry>::const_iterator it = v_file.begin(); it != v_file.end();  ++it)
        {
          std::string rosbag_file_name = (*it).path().string();
          std::string file_extention = rosbag_file_name.substr(rosbag_file_name.size() - 3, 3);

          if(file_extention.compare("bag") == 0)
          {
            bag_file_list.push_back(rosbag_file_name);
          }
        }

        std::sort(bag_file_list.begin(), bag_file_list.end());

        for(const auto& bag_file_name : bag_file_list)
        {
          // Read the bag file.
          rosbag::Bag bag_read(bag_file_name, rosbag::bagmode::Read);

          // An object to capture topics of interest from the ROS bag file.
          rosbag::View view(bag_read, rosbag::TopicQuery("/network/vls_128_data"));

          // Get the velodyne packet topics from the bag file and parse them.
          for(rosbag::MessageInstance const msg: view)
          {
            phantom_ros::NetworkFrame::ConstPtr velodyne_packet = msg.instantiate<phantom_ros::NetworkFrame>();
            double timestamp = msg.getTime().toSec();
            velodyne_parser.parseDataPacket(velodyne_packet->data, timestamp);
          }
        }
      }
    }
  }


  return 0;

  // Extract the dataset name.
  std::string full_path = bag_file_list[0];
  std::string token = full_path.substr(0, full_path.find('/'));
  size_t pos = 0;
  while ((pos = full_path.find('/')) != std::string::npos) {
    token = full_path.substr(0, pos);
    full_path.erase(0, pos + 1);
  }

  if(out_file_folder.compare("") == 0)
  {
    out_file_folder = params.output_folder_ + "/" + token;
  }

  std::cout << out_file_folder << std::endl;


  // Create the output folder if the folder doesn't exist.
  if(!boost::filesystem::exists(out_file_folder))
  {
    boost::filesystem::path out_folder_boost_path(out_file_folder);
    boost::filesystem::create_directory(out_folder_boost_path);
  }

  // Declare a Velodyne128 parser object.
  VelodyneVLS128Parser velodyne_parser;

  // Initialize the parser.
  velodyne_parser.initialize();

  // Register the publish pointcloud callback.
  velodyne_parser.registerPublishCallback(getParsedLidarPointCloudCallback);

  /* Read the bag files one-by-one, parse the velodyne packets in the bag files, and save pointclouds to files. */
  for(size_t i = 0; i < bag_file_list.size(); ++i)
  {
#if PUBLISH_DEBUG_CLOUD
    if(!ros::ok())
    {
      break;
    }
#endif
    std::cout << "Processing file " << bag_file_list[i] << std::endl;

    // Read the bag file.
    rosbag::Bag bag_read(bag_file_list[i], rosbag::bagmode::Read);

    // An object to capture topics of interest from the ROS bag file.
    rosbag::View view(bag_read, rosbag::TopicQuery("/network/vls_128_data"));

    // Get the velodyne packet topics from the bag file and parse them.
    for(rosbag::MessageInstance const msg: view)
    {
#if PUBLISH_DEBUG_CLOUD
      if(!ros::ok())
      {
        break;
      }
#endif
      phantom_ros::NetworkFrame::ConstPtr velodyne_packet = msg.instantiate<phantom_ros::NetworkFrame>();
      double timestamp = msg.getTime().toSec();
      velodyne_parser.parseDataPacket(velodyne_packet->data, timestamp);
    }
  }



  return 1;
}


