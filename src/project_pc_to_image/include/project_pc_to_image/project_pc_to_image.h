#ifndef PROJECT_PC_TO_IMAGE_H_
#define PROJECT_PC_TO_IMAGE_H_

#include <common/geometry.h>
#include <common/cam_perspective_mapping.h>
#include <common/utils.h>
#include <common/point_cloud.h>
#include <common/utils.h>

#include <vector>
#include <iostream>
#include <fstream>

#include <common/cam_perspective_mapping.h>
#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <yaml-cpp/yaml.h>
#include <json.hpp>

class LidarLaneSegmentationBasedOnAnnotImage
{

public:
  struct Config
  {
    std::string master_json_directory_;

    std::string annotation_data_directory_;

    std::string image_directory_;

    std::vector<double> lat_long_lidar_point_searh_lengths_;

    bool display_debug_image_;
  };

  LidarLaneSegmentationBasedOnAnnotImage(const YAML::Node& yaml_node)
    : yaml_node_{yaml_node}
  {
    loadConfig();

    YAML::Node cam_yaml_node;
    try
    {
      cam_yaml_node = YAML::LoadFile(std::string(LIDAR_BASED_VISION_EVALUATION_PARAM_DIR)
                                 + "/project_pc_to_image/front_center_cam.yaml");
    }
    catch (const YAML::Exception& e)
    {
      std::cout << "The yaml file for the front camera is not found." << std::endl;
    }
    fc_cam_mapping_ = std::make_shared<CamPerspectiveMapping>(cam_yaml_node);

    try
    {
      cam_yaml_node = YAML::LoadFile(std::string(LIDAR_BASED_VISION_EVALUATION_PARAM_DIR)
                                 + "/project_pc_to_image/rear_center_cam.yaml");
    }
    catch (const YAML::Exception& e)
    {
      std::cout << "The yaml file for the front camera is not found." << std::endl;
    }
    rc_cam_mapping_ = std::make_shared<CamPerspectiveMapping>(cam_yaml_node);
  }

  ~LidarLaneSegmentationBasedOnAnnotImage()
  {}

  bool doLaneSegmentation(const std::string& session_name, const double& timestamp,
    const PointCloudf& fc_lidar_points, std::vector<PointCloudf>& fc_lane_segments,
    const PointCloudf& rc_lidar_points, std::vector<PointCloudf>& rc_lane_segments,
    const PointCloudf& raw_pointcloud, std::vector<PointCloudf>& object_points, cv::Mat& out_image);

private:
  YAML::Node yaml_node_;

  Config params_;

  std::shared_ptr<CamPerspectiveMapping> fc_cam_mapping_;

  std::shared_ptr<CamPerspectiveMapping> rc_cam_mapping_;

  void loadConfig();

  bool parseMasterJson(const std::string& session_name, const double& timestamp,
    std::string& fc_image_file_name, std::string& fc_annotation_file_name,
    std::string& rc_image_file_name, std::string& rc_annotation_file_name);

  bool getAnnotatedLanes(const std::string& file_name, std::vector<std::vector<cv::Point>>& annot_lanes);

  bool getAnnotatedObjectBoxes(const std::string& file_name, std::vector<std::vector<cv::Point>>& annot_boxes);

  bool loadImage(const std::string& file_name, cv::Mat& image);

  void polynomialRegression(const std::vector<cv::Point>& input, std::vector<double>& coeff);
};



#endif
