#include <project_pc_to_image/project_pc_to_image.h>

bool compTwoCvPointsInVDirection(const cv::Point& p1, const cv::Point&p2)
{
  if(p1.y < p2.y)
  {
    return true;
  }
  else
  {
    return false;
  }
}

bool compTwoBoundingBoxesInVDirection(const std::vector<cv::Point>& box1, const std::vector<cv::Point>& box2)
{
  if(box1[2].y > box2[2].y)
  {
    return true;
  }
  else
  {
    return false;
  }
}

int main(int argc, char* argv[])
{  
//  YAML::Node yaml_node;
//  try
//  {
//    yaml_node = YAML::LoadFile(std::string(LIDAR_BASED_VISION_EVALUATION_PARAM_DIR)
//                               + "/lane_extraction_map_building_first/lane_extraction_map_building_first_params.yaml");
//  }
//  catch (const YAML::Exception& e)
//  {
//    std::cout << "The yaml file is not found." << std::endl;
//  }


//  LidarLaneSegmentationBasedOnAnnotImage lidar_lane_seg(yaml_node);



//  std::string session_name = "2019-11-19-09-55-28_Evaluation_WestCoast";
//  double timestamp = 1574188147.613;

//  std::vector<Point3f> fc_lidar_points, rc_lidar_points;
//  std::vector<std::vector<Point3f>> fc_lane_segments, rc_lane_segments;

//  lidar_lane_seg.doLaneSegmentation(session_name, timestamp, fc_lidar_points, fc_lane_segments, rc_lidar_points, rc_lane_segments);

//  cv::waitKey(0);
//  return 0;

  YAML::Node fc_cam_yaml_node;
  try
  {
    fc_cam_yaml_node = YAML::LoadFile(std::string(LIDAR_BASED_VISION_EVALUATION_PARAM_DIR)
                               + "/project_pc_to_image/front_center_cam.yaml");
  }
  catch (const YAML::Exception& e)
  {
    std::cout << "The yaml file is not found." << std::endl;
  }

  CamPerspectiveMapping fc_cam_mapping(fc_cam_yaml_node);

  std::string timestamp = "1574131705_542000000";

  // Read a lidar point file.
  std::ifstream fin_lidar("/home/gpgwon/mounts/eval_pc_gipoong/data/" + timestamp + ".lc");

  if(!fin_lidar.is_open())
  {
    std::cout << "Fail to read the pc file. " << std::endl;
    return 0;
  }
  PointCloudf lidar_points;

  while(!fin_lidar.eof())
  {
    Point3If point;

    fin_lidar >> point.x;
    fin_lidar >> point.y;
    fin_lidar >> point.z;

    if(point.x >= 0)
    {
      lidar_points.push_back(point);
    }
  }

  // Read a annotation file.
  std::vector<std::vector<cv::Point>> ann_lines;
  std::ifstream fin_ann("/home/gpgwon/mounts/eval_pc_gipoong/data/" + timestamp + ".ann");

  if(!fin_ann.is_open())
  {
    std::cout << "Fail to open the annotation file." << std::endl;
    return 0;
  }
  nlohmann::json j;
  fin_ann >> j;

  for(const auto entity : j["annotation_entities"])
  {
    if(entity["annotation_type"] == "line")
    {
      std::vector<cv::Point> ann_line;
      for(const auto& point : entity["coordinates"])
      {
        cv::Point cvpoint;
        cvpoint.x = point["x"];
        cvpoint.y = point["y"];
        ann_line.push_back(cvpoint);
      }
      ann_lines.push_back(ann_line);
    }
  }

  std::vector<cv::Point> img_points;
  fc_cam_mapping.doPerspectiveMapping(lidar_points, img_points);


  // Read an image file.
  cv::Mat image;
  image = cv::imread("/home/gpgwon/mounts/eval_pc_gipoong/data/" + timestamp + ".png", cv::IMREAD_COLOR);

  for(auto& img_point : img_points)
  {
    if(img_point.x >= 0 && img_point.y < 960 && img_point.y >= 0 && img_point.y < 960)
    {
      cv::circle(image, img_point, 1, CV_RGB(255, 0, 0), 1, cv::LineTypes::LINE_AA);
    }
  }

  for(const auto& ann_line : ann_lines)
  {
    for(const auto& ann_point : ann_line)
    {
      cv::circle(image, ann_point, 3, CV_RGB(0, 0, 255), 1, cv::LineTypes::LINE_AA);
    }
  }


  cv::imshow("image", image);
  cv::waitKey(0);




//  // Cam extrinsic parameters.
//  Eigen::Vector3d cam_position;
//  cam_position << params.cam_position_[0], params.cam_position_[1], params.cam_position_[2];

//  //  Eigen::Vector3d cam_orientation;
//  //  cam_orientation << degree_to_radian(90.8), degree_to_radian(0.0), degree_to_radian(90.0);
//  //  Eigen::Matrix3d R = getInverseRotMatrix3d(cam_orientation[0], cam_orientation[1], cam_orientation[2]);

//  double pan = degree_to_radian(params.cam_orientation_[0]);
//  double tilt = degree_to_radian(params.cam_orientation_[1]);

//  double cpan = std::cos(pan);
//  double span = std::sin(pan);
//  double ctilt = std::cos(tilt);
//  double stilt = std::sin(tilt);

//  Eigen::Matrix3d R;
//  R << cpan, span, 0,
//      -span * stilt, cpan * stilt, -ctilt,
//      -span * ctilt, cpan * ctilt, stilt;

//  // Transform vehicle frame to cam xyz coordinate.
//  pcl::PointCloud<pcl::PointXYZ> points_in_camera;
//  for(auto& point : points)
//  {
//    double x = point.x - cam_position[0];
//    double y = point.y - cam_position[1];
//    double z = point.z - cam_position[2];
//    pcl::PointXYZ point_in_camera;
//    point_in_camera.x = R(0,0) * x + R(0,1) * y + R(0,2) * z;
//    point_in_camera.y = R(1,0) * x + R(1,1) * y + R(1,2) * z;
//    point_in_camera.z = R(2,0) * x + R(2,1) * y + R(2,2) * z;

//    points_in_camera.push_back(point_in_camera);
//  }


//  // Normalization
//  for(auto& point : points_in_camera)
//  {
//    point.x /= point.z;
//    point.y /= point.z;
//    point.z = 1.0;
//  }


//  //  cv::Mat norm_image(500, 500, CV_8UC3, cv::Scalar(0, 0, 0));

//  for(auto& point : points_in_camera)
//  {
//    //    std::cout << point.x << ", " << point.y << std::endl;
//    if(point.x > -1 && point.x < 1 && point.y > -1 && point.y < 1)
//    {
//      cv::Point point_image(point.x * 250 + 250, point.y * 250 + 250);
//      //      cv::circle(norm_image, point_image, 3, CV_RGB(255, 0, 0), 1, cv::LineTypes::LINE_AA);
//    }
//  }

//  //  cv::imshow("aa", norm_image);
//  //  cv::waitKey(0);
//  //  return 0;



//  Eigen::Vector3f kc; kc << params.distortion_[0], params.distortion_[1], params.distortion_[2];
//  // Apply distortion
//  for(auto& point : points_in_camera)
//  {
//    double r2 = std::pow(point.x, 2) + std::pow(point.y, 2);
//    double k = 1 + kc[0] * r2 + kc[1] * std::pow(r2, 2);

//    point.x = static_cast<double>(k * point.x);
//    point.y = static_cast<double>(k * point.y);
//  }

//  Eigen::Matrix3f cam_intrisic_mat;
//  cam_intrisic_mat << 2052.316, 0, 955.694, 0, 2062.006, 667.516, 0, 0, 1;

//  std::vector<cv::Point> points_in_image;
//  for(auto& point_camera : points_in_camera)
//  {
//    cv::Point point_in_image;
//    point_in_image.x = static_cast<int>((params.cam_intrinsic_[0] * point_camera.x + params.cam_intrinsic_[2])/2);
//    point_in_image.y = static_cast<int>((params.cam_intrinsic_[4] * point_camera.y + params.cam_intrinsic_[5] - params.crop_roi_[1])/2);

//    if(point_in_image.x >= 0 && point_in_image.x < 960
//       && point_in_image.y >= 0 && point_in_image.y < 540)
//    {
//      //      std::cout << point_in_image.x << ", " << point_in_image.y << std::endl;
//      points_in_image.push_back(point_in_image);
//    }
//  }


//  // Read an image file.
//  cv::Mat image;
//  image = cv::imread("/home/gpgwon/mounts/eval_pc_gipoong/data/" + timestamp + ".png", cv::IMREAD_COLOR);

//  for(auto& point_image : points_in_image)
//  {
//    cv::circle(image, point_image, 1, CV_RGB(255, 0, 0), 1, cv::LineTypes::LINE_AA);
//  }

//  for(const auto& ann_line : ann_lines)
//  {
//    for(const auto& ann_point : ann_line)
//    {
//      cv::circle(image, ann_point, 3, CV_RGB(0, 0, 255), 1, cv::LineTypes::LINE_AA);
//    }
//  }


//  cv::imshow("image", image);
//  cv::waitKey(0);




  return 0;
}

void LidarLaneSegmentationBasedOnAnnotImage::loadConfig()
{
  params_.master_json_directory_ = yaml_node_["master_json_directory"].as<std::string>();
  params_.annotation_data_directory_ = yaml_node_["annotation_data_directory"].as<std::string>();
  params_.image_directory_ = yaml_node_["image_directory"].as<std::string>();
  params_.lat_long_lidar_point_searh_lengths_ = yaml_node_["lat_long_lidar_point_searh_lengths"].as<std::vector<double>>();
  params_.display_debug_image_ = yaml_node_["display_debug_image"].as<bool>();
}

bool LidarLaneSegmentationBasedOnAnnotImage::parseMasterJson(const std::string& session_name, const double& timestamp,
  std::string& fc_image_file_name, std::string& fc_annotation_file_name,
  std::string& rc_image_file_name, std::string& rc_annotation_file_name)
{
  std::string timestamp_str = convertDoubleTimestampTo13DigitString(timestamp);
  std::string file_name = params_.master_json_directory_ + "/" + session_name
    + "_csi_cam_front_master_image_raw-" + timestamp_str + ".json";

  std::ifstream fin(file_name);
  if(!fin.is_open())
  {
    std::cout << "Fail to read the master json file." << std::endl;
    return false;
  }

  nlohmann::json j;
  fin >> j;

  fc_image_file_name = j["related_images"][2]["/csi_cam/front_center/image_raw"];
  fc_annotation_file_name = j["related_images"][2]["annotation_md5"];
  fc_annotation_file_name += ".ann";

  rc_image_file_name = j["related_images"][5]["/csi_cam/rear_center/image_raw"];
  rc_annotation_file_name = j["related_images"][5]["annotation_md5"];
  rc_annotation_file_name += ".ann";

  return true;
}

bool LidarLaneSegmentationBasedOnAnnotImage::getAnnotatedLanes(const std::string& file_name, std::vector<std::vector<cv::Point>>& annot_lanes)
{
  std::string file_full_path = params_.annotation_data_directory_ + "/phantom_" + file_name;
  std::ifstream fin(file_full_path);
  if(!fin.is_open())
  {
    std::cout << "Fail to read the annotation file" << std::endl;
    return false;
  }

  nlohmann::json j;
  fin >> j;

  for(const auto entity : j["annotation_entities"])
  {
    if(entity["annotation_type"] == "line")
    {
      std::vector<cv::Point> ann_line;
      for(const auto& point : entity["coordinates"])
      {
        cv::Point cvpoint;
        cvpoint.x = point["x"];
        cvpoint.y = point["y"];
        ann_line.push_back(cvpoint);
      }
      annot_lanes.push_back(ann_line);
    }
  }
}

bool LidarLaneSegmentationBasedOnAnnotImage::getAnnotatedObjectBoxes(const std::string& file_name,
  std::vector<std::vector<cv::Point>>& annot_boxes)
{
  std::string file_full_path = params_.annotation_data_directory_ + "/phantom_" + file_name;

  std::ifstream fin(file_full_path);
  if(!fin.is_open())
  {
    std::cout << "Fail to read the annotation file" << std::endl;
    return false;
  }

  nlohmann::json j;
  fin >> j;

  for(const auto entity : j["annotation_entities"])
  {
    if(entity["annotation_type"] == "bounding_box")
    {
      std::vector<cv::Point> ann_obj;
      for(const auto& point : entity["coordinates"])
      {
        cv::Point cvpoint;
        cvpoint.x = point["x"];
        cvpoint.y = point["y"];
        ann_obj.push_back(cvpoint);
      }
      annot_boxes.push_back(ann_obj);
    }
  }

  return true;
}

bool LidarLaneSegmentationBasedOnAnnotImage::loadImage(const std::string& file_name, cv::Mat& image)
{
  std::string file_full_path = params_.image_directory_ + "/" + file_name;

  try
  {
    image = cv::imread(file_full_path, cv::IMREAD_COLOR);
  }
  catch(const cv::Exception& e)
  {
    std::cout << "Fail to read the image file" << std::endl;
    return false;
  }

  return true;
}

void LidarLaneSegmentationBasedOnAnnotImage::polynomialRegression(const std::vector<cv::Point>& input, std::vector<double>& coeff)
{
  if(input.size() == 2)
  {
    Eigen::MatrixXf A(input.size(), 2);
    Eigen::VectorXf b(input.size());
    Eigen::VectorXf c(2);

    for(size_t i = 0; i < input.size(); ++i)
    {
      A(i, 0) = static_cast<double>(input[i].y);
      A(i, 1) = 1.0f;

      b(i) = static_cast<double>(input[i].x);
    }

    c = (A.transpose() * A).inverse() * A.transpose() * b;

    coeff.resize(2);
    coeff[0] = c(0);
    coeff[1] = c(1);
  }
  else
  {
    Eigen::MatrixXf A(input.size(), 3);
    Eigen::VectorXf b(input.size());
    Eigen::VectorXf c(3);

    for(size_t i = 0; i < input.size(); ++i)
    {
      A(i, 0) = std::pow(static_cast<double>(input[i].y), 2);
      A(i, 1) = static_cast<double>(input[i].y);
      A(i, 2) = 1.0f;

      b(i) = static_cast<double>(input[i].x);
    }

    c = (A.transpose() * A).inverse() * A.transpose() * b;

    coeff.resize(3);
    coeff[0] = c(0);
    coeff[1] = c(1);
    coeff[2] = c(2);
  }
}

bool LidarLaneSegmentationBasedOnAnnotImage::doLaneSegmentation(const std::string& session_name, const double& timestamp,
  const PointCloudf& fc_lidar_points, std::vector<PointCloudf>& fc_lane_segments,
  const PointCloudf& rc_lidar_points, std::vector<PointCloudf>& rc_lane_segments,
  const PointCloudf& raw_pointcloud, std::vector<PointCloudf>& object_points, cv::Mat& out_image)
{
  // Parse the master json file.
  std::string fc_image_file_name, fc_annotation_file_name, rc_image_file_name, rc_annotation_file_name;
  parseMasterJson(session_name, timestamp, fc_image_file_name, fc_annotation_file_name,
    rc_image_file_name, rc_annotation_file_name);

  /* Process front lane.
   */
  // Get image annotation data.
  std::vector<std::vector<cv::Point>> fc_annot_lanes;
  getAnnotatedLanes(fc_annotation_file_name, fc_annot_lanes);

  std::vector<std::vector<cv::Point>> fc_annot_obj_points;
  getAnnotatedObjectBoxes(fc_annotation_file_name, fc_annot_obj_points);
  std::sort(fc_annot_obj_points.begin(), fc_annot_obj_points.end(), compTwoBoundingBoxesInVDirection);


  for(auto& fc_annot_lane : fc_annot_lanes)
  {
    std::sort(fc_annot_lane.begin(), fc_annot_lane.end(), compTwoCvPointsInVDirection);
  }

  // Load the image.
  cv::Mat fc_image;
  loadImage(fc_image_file_name, fc_image);

  // Perspective Mapping
  std::vector<cv::Point> fc_lidar_img_points;
  fc_cam_mapping_->doPerspectiveMapping(fc_lidar_points, fc_lidar_img_points);

  std::vector<cv::Point> current_lidar_img_points;
  fc_cam_mapping_->doPerspectiveMapping(raw_pointcloud, current_lidar_img_points);

  std::vector<bool> flag_used_points(current_lidar_img_points.size(), false);
  std::vector<std::vector<cv::Point>> obj_img_points;
  for(const auto& obj_bounding_box : fc_annot_obj_points)
  {
    PointCloudf points_of_an_object;
    std::vector<cv::Point> img_points_in_an_object;
    for(size_t i = 0; i < current_lidar_img_points.size(); ++i)
    {
      if(flag_used_points[i] == true)
      {
        continue;
      }
      const float& px = current_lidar_img_points[i].x;
      const float& py = current_lidar_img_points[i].y;

      if(px > obj_bounding_box[0].x && px < obj_bounding_box[2].x
        && py > obj_bounding_box[0].y && py < obj_bounding_box[2].y)
      {
        points_of_an_object.push_back(raw_pointcloud.points_[i]);
        img_points_in_an_object.push_back(current_lidar_img_points[i]);
        flag_used_points[i] = true;
      }
    }
    object_points.push_back(points_of_an_object);
    obj_img_points.push_back(img_points_in_an_object);
  }

  /* Segment lanes
   */
  std::vector<PointCloudf> lane_segments;
  std::vector<std::vector<cv::Point>> lane_segments_in_img;
  for(const auto& ann_line : fc_annot_lanes)
  {
    PointCloudf lane_segment;
    std::vector<cv::Point> lane_segment_in_img;

//    if(ann_line.size() < 2)
//    {
//      continue;
//    }
//    std::vector<double> coeff;
//    polynomialRegression(ann_line, coeff);

//    for(size_t i = 0; i < coeff.size(); ++i)
//    {
//      std::cout << coeff[i] << ", ";
//    }
//    std::cout << std::endl;

//    std::vector<cv::Point> augmented_ann_line;
//    for(int v = 280; v < 540; v+=5)
//    {
//      double u_double = 0.0f;
//      for(size_t i = 0; i < coeff.size(); ++i)
//      {
//        u_double += coeff[i] * std::pow(static_cast<double>(v), static_cast<int>(coeff.size()) - i - 1);
//        std::cout << std::pow(static_cast<double>(v), static_cast<int>(coeff.size()) - i - 1) << std::endl;
//      }
//      int u = static_cast<int>(u_double);
//      cv::Point cvpoint(u, v);
//      augmented_ann_line.push_back(cvpoint);

//      cv::circle(fc_image, cvpoint, 2, CV_RGB(0, 255, 0), -1, cv::LineTypes::LINE_AA);
//    }

    if(ann_line.size() < 2)
    {
      continue;
    }

    for(const auto& ann_point : ann_line)
//    for(size_t i = 0; i < ann_line.size() - 1; ++i)
    {
//      int u0 = ann_line[i].x;
//      int v0 = ann_line[i].y;
//      int ue = ann_line[i + 1].x;
//      int ve = ann_line[i + 1].y;

//      double r = static_cast<double>(ve - v0) / static_cast<double>(ue - u0);

//      for(int v = v0; v < ve; v += 1)
//      {
//        int u = static_cast<int>((v - v0) / r + u0);
//        cv::Point ann_point(u, v);
        int du, dv;
        fc_cam_mapping_->getdUdVPerUnitDistance(ann_point, params_.lat_long_lidar_point_searh_lengths_[0],
          params_.lat_long_lidar_point_searh_lengths_[1], du, dv);

        double min_dist = 999;
        size_t selected_idx = 99999;
        for(size_t i = 0; i < fc_lidar_img_points.size(); ++i)
        {
          const cv::Point& point = fc_lidar_img_points[i];
          if(point.x >= ann_point.x - du && point.x <= ann_point.x + du
            && point.y >= ann_point.y - dv && point.y <= ann_point.y + dv)
          {
            double dist = std::sqrt(std::pow(static_cast<double>(ann_point.x - point.x), 2) + std::pow(static_cast<double>(ann_point.y - point.y), 2));
            if(dist < min_dist)
            {
              min_dist = dist;
              selected_idx = i;
            }
          }
        }

        if(selected_idx < 99999)
        {
          lane_segment.push_back(fc_lidar_points.points_[selected_idx]);
          lane_segment_in_img.push_back(fc_lidar_img_points[selected_idx]);
        }
//      }

    }
    lane_segments.push_back(lane_segment);
    lane_segments_in_img.push_back(lane_segment_in_img);
  }

//  fc_lane_segments.clear();
//  // Eliminate duplicated points.
//  for(const auto& lane_seg : lane_segments)
//  {
//    std::vector<Point3f> fc_lane_seg;
//    fc_lane_seg.push_back(lane_seg[0]);
//    for(size_t i = 1; i < lane_seg.size(); ++i)
//    {
//      if((fc_lane_seg.back().x != lane_seg[i].x) && (fc_lane_seg.back().y != lane_seg[i].y))
//      {
//        fc_lane_seg.push_back(lane_seg[i]);
//      }
//    }
//    fc_lane_segments.push_back(fc_lane_seg);
//  }

//  int num_prev(0);
//  int num_new(0);
//  for(size_t i = 0; i < lane_segments.size(); ++i)
//  {
//    num_prev += lane_segments[i].size();
//    num_new += fc_lane_segments[i].size();
//  }

  fc_lane_segments = lane_segments;


  for(const auto& ann_line : fc_annot_lanes)
  {
    for(const auto& ann_point : ann_line)
    {      
      cv::circle(fc_image, ann_point, 2, CV_RGB(0, 0, 255), -1, cv::LineTypes::LINE_AA);
    }
  }

  for(const auto& img_point : fc_lidar_img_points)
  {
    cv::circle(fc_image, img_point, 1, CV_RGB(255, 0, 0), -1, cv::LineTypes::LINE_AA);
  }

  for(const auto& lane_segment_in_img : lane_segments_in_img)
  {
    for(const auto& lidar_lane_point_in_img : lane_segment_in_img)
    {
      cv::circle(fc_image, lidar_lane_point_in_img, 3, CV_RGB(0, 255, 0), -1, cv::LineTypes::LINE_AA);
    }
  }

  for(size_t i = 0; i < obj_img_points.size(); ++i)
  {
    const auto& points_in_an_object = obj_img_points[i];
    int r, g, b;
    intensityToRGB(i, obj_img_points.size(), r, g, b);

    for(const auto& point : points_in_an_object)
    {
      cv::circle(fc_image, point, 1, CV_RGB(r, g, b), -1, cv::LineTypes::LINE_AA);
    }
  }

  for(size_t i = 0; i < fc_annot_obj_points.size(); ++i)
  {
    const auto& obj_bounding_box = fc_annot_obj_points[i];
    int r, g, b;
    intensityToRGB(i, fc_annot_obj_points.size(), r, g, b);

    cv::line(fc_image, obj_bounding_box[0], obj_bounding_box[1], CV_RGB(r, g, b), 1, cv::LineTypes::LINE_AA);
    cv::line(fc_image, obj_bounding_box[1], obj_bounding_box[2], CV_RGB(r, g, b), 1, cv::LineTypes::LINE_AA);
    cv::line(fc_image, obj_bounding_box[2], obj_bounding_box[3], CV_RGB(r, g, b), 1, cv::LineTypes::LINE_AA);
    cv::line(fc_image, obj_bounding_box[3], obj_bounding_box[0], CV_RGB(r, g, b), 1, cv::LineTypes::LINE_AA);
  }

  out_image = fc_image;

  if( params_.display_debug_image_)
  {
    cv::imshow("image", fc_image);
  }


  return true;
}
