#ifndef CAM_PERSPECTIVE_MAPPING_H_
#define CAM_PERSPECTIVE_MAPPING_H_

#include <yaml-cpp/yaml.h>
#include <common/point3.h>
#include <opencv2/opencv.hpp>
#include <common/geometry.h>
#include <common/point_cloud.h>

class CamPerspectiveMapping
{
  struct Config
  {
    std::vector<float> cam_position_; // x, y, z in the vehicle frame.
    std::vector<float> cam_orientation_; // pan, tilt angles.
    std::vector<double> distortion_; // k1, k2, k3
    std::vector<double> camera_matrix_; // serialized 3x3 matrix
    std::vector<int> crop_roi_; // u0, v0, width, height of the roi.
    std::vector<int> resized_img_size_;
  };

public:
  CamPerspectiveMapping(const YAML::Node& yaml_node)
    : yaml_node_{yaml_node}
  {
    loadConfigs();

    double roll = degree_to_radian(params_.cam_orientation_[0]);
    double pitch = degree_to_radian(params_.cam_orientation_[1]);
    double yaw = degree_to_radian(params_.cam_orientation_[2]);
    rot_vehicle_to_cam_ = getInverseRotVector(roll, pitch, yaw);

    img_resize_factors_.x = static_cast<float>(params_.resized_img_size_[0]) / params_.crop_roi_[2];
    img_resize_factors_.y = static_cast<float>(params_.resized_img_size_[1]) / params_.crop_roi_[3];

    ru_ = params_.camera_matrix_[0] * img_resize_factors_.x;
    rv_ = params_.camera_matrix_[4] * img_resize_factors_.y;
    vanishing_point_.x = (params_.camera_matrix_[2] - params_.crop_roi_[0]) / 2;
    vanishing_point_.y = (params_.camera_matrix_[5] - params_.crop_roi_[1]) / 2;
  }

  void doPerspectiveMapping(const Point3f& input_point, cv::Point& img_point)
  {
    // Vehicle frame to camera frame transformation.
    Point3f point_in_cam;
    float dx = input_point.x - params_.cam_position_[0];
    float dy = input_point.y - params_.cam_position_[1];
    float dz = input_point.z - params_.cam_position_[2];

    point_in_cam.x = rot_vehicle_to_cam_[0] * dx + rot_vehicle_to_cam_[1] * dy + rot_vehicle_to_cam_[2] * dz;
    point_in_cam.y = rot_vehicle_to_cam_[3] * dx + rot_vehicle_to_cam_[4] * dy + rot_vehicle_to_cam_[5] * dz;
    point_in_cam.z = rot_vehicle_to_cam_[6] * dx + rot_vehicle_to_cam_[7] * dy + rot_vehicle_to_cam_[8] * dz;

    // Normalization
    point_in_cam.x /= point_in_cam.z;
    point_in_cam.y /= point_in_cam.z;
    point_in_cam.z = 1.0f;

    // Distortion correction
    double r2 = std::pow(point_in_cam.x, 2) + std::pow(point_in_cam.y, 2);
    double k = 1 + params_.distortion_[0] * r2 + params_.distortion_[1] * std::pow(r2, 2);
    point_in_cam.x = static_cast<float>(k * point_in_cam.x);
    point_in_cam.y = static_cast<float>(k * point_in_cam.y);

    // camera frame to image plane projection.
    int x_resize_factor(params_.crop_roi_[2] / params_.resized_img_size_[0]);
    int y_resize_factor(params_.crop_roi_[3] / params_.resized_img_size_[1]);
    img_point.x = static_cast<int>(ru_ * point_in_cam.x + vanishing_point_.x);
    img_point.y = static_cast<int>(rv_ * point_in_cam.y + vanishing_point_.y);
  }

  void doPerspectiveMapping(const PointCloudf& input_points, std::vector<cv::Point>& img_points)
  {
    img_points.reserve(input_points.getSize());
    for(const auto& input_point : input_points.points_)
    {      
      Point3f point;
      point.x = input_point.x;
      point.y = input_point.y;
      point.z = input_point.z;
      cv::Point img_point;
      doPerspectiveMapping(point, img_point);
      img_points.push_back(img_point);
    }
  }

  void getdUdVPerUnitDistance(const cv::Point& ref_img_point, const float& d_lat_meters, const float& d_lon_meters,
    int& du, int& dv)
  {
    // Assume that the all points are on the ground. Therefore, the y coordinates of the points in the camera frame is
    // equal to the height of the camera.
    float y0 = params_.cam_position_[2];

    float roll = degree_to_radian(params_.cam_orientation_[0] + 90.0);

    // Calculate the longitudinal distance of the point from the camera.
    float z = (y0 * rv_) / (ref_img_point.y - vanishing_point_.y - rv_ * tan(roll));

    float du_dx = ru_ / z;
    float dv_dz = (-y0 * rv_) / std::pow(z,2);

    du = static_cast<int>(fabs(d_lat_meters * du_dx));
    dv = static_cast<int>(fabs(d_lon_meters * dv_dz));

    du = std::max(du, 2);
    dv = std::max(dv, 2);
  }


private:
  YAML::Node yaml_node_;

  Config params_;

  std::vector<double> rot_vehicle_to_cam_;

  Point2f img_resize_factors_;

  float ru_;

  float rv_;

  Point2f vanishing_point_;

  void loadConfigs()
  {
    params_.cam_position_ = yaml_node_["cam_position"].as<std::vector<float>>();
    params_.cam_orientation_ = yaml_node_["cam_orientation"].as<std::vector<float>>();
    params_.distortion_ = yaml_node_["distortion"].as<std::vector<double>>();
    params_.camera_matrix_ = yaml_node_["camera_matrix"].as<std::vector<double>>();
    params_.crop_roi_ = yaml_node_["crop_roi"].as<std::vector<int>>();
    params_.resized_img_size_ = yaml_node_["resized_img_size"].as<std::vector<int>>();
  }

};


#endif
