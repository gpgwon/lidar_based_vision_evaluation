/*******************************************************************************
* @file    cloud_point_types.h
* @date    03/27/2019
**
* @attention Copyright (c) 2018
* @attention Phantom AI, Inc.
* @attention All rights reserved.
*******************************************************************************/

#ifndef PHANTOM_AI_CLOUD_POINT_TYPES_H_
#define PHANTOM_AI_CLOUD_POINT_TYPES_H_


#include <common/point3.h>


template <typename T>
class Point3I : public Point3T<T>
{
public:

  // Intensity of the point
  uint8_t intensity;

  // Timestamp when the point is measured
  double timestamp;


  /**
   * @brief Default constructor.
   */
  Point3I()
    : Point3T<T>(0.0, 0.0, 0.0),
      intensity(0),
      timestamp(0.0)
  {}

  /**
   * @brief Constructor with member variables.
   */
  Point3I(const T& x_arg, const T& y_arg, const T& z_arg, const uint8_t& intensity_arg = 0, double timestamp_arg = 0.0):
    Point3T<T>(x_arg, y_arg, z_arg),
    intensity(intensity_arg),
    timestamp(timestamp_arg)
  {}

  /**
   * @brief Copy constructor.
   */
  Point3I(const Point3I& b):
    Point3T<T>(b.x, b.y, b.z),
    intensity(b.intensity),
    timestamp(b.timestamp)
  {}

  /**
   * @brief Function to set the member variables.
   * @param x value.
   * @param y value.
   * @param z value.
   * @param intensity value.
   * @param timestamp value.
   */
  void set(const T x_arg, const T y_arg, const T z_arg, uint8_t intensity_arg = 0, double timestamp_arg = 0.0)
  {
    Point3T<T>::set(x_arg, y_arg, z_arg);
    intensity = intensity_arg;
    timestamp = timestamp_arg;
  }

  /**
   * @brief Stream Operator. Writes the 3D point object to the passed stream.
   */
  friend std::ostream& operator<<(std::ostream& out, const Point3I& a)
  {
    return out << "( " << a.x << ", " << a.y << ", " << a.z << ", " << static_cast<uint16_t>(a.intensity) << " )";
  }
};

typedef Point3I<float> Point3If;
typedef Point3I<double> Point3Id;



#endif
