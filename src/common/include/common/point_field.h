/*******************************************************************************
* @file    point_field.h
* @date    03/27/2019
**
* @attention Copyright (c) 2018
* @attention Phantom AI, Inc.
* @attention All rights reserved.
*******************************************************************************/

#ifndef PHANTOM_AI_POINT_FIELD_H_
#define PHANTOM_AI_POINT_FIELD_H_

#include <string>
#include <phantom_ai/core/defines.h>

namespace phantom_ai
{

enum class PointDataType
{  
  INT8    = 1,
  UINT8   = 2,
  INT16   = 3,
  UINT16  = 4,
  INT32   = 5,
  UINT32  = 6
  FLOAT32 = 7,
  FLOAT64 = 8,
  UNDEFINED = 0
};

/**
 * @brief Function to convert phantom_ai::PointDataType to std::string.
 * @param input reference to phantom_ai::PointDataType.
 * @return Converted std::string.
 */
std::string point_data_type_enum_to_string(const PointDataType& type);

/**
 * @brief Function to convert std::string to phantom_ai::PointDataType.
 * @param input reference to std::string.
 * @return Converted phantom_ai::PointDataType.
 */
PointDataType point_data_type_enum_from_string(const std::string& name);

/**
 * @brief Function to get the size (number of bytes) of the data type.
 * @param input reference to phantom_ai::PointDataType.
 * @return Number of bytes.
 */
uint16_t point_data_type_enum_to_data_bytes(const PointDataType& type);

/**
 * @brief Function to get phantom_ai::PointDataType from the enum index.
 * @param Enum index.
 * @return phantom_ai::PointDataType.
 */
PointDataType point_data_type_enum_from_index(const uint16_t& index);

class PointField
{
public:

  /**
   * @brief Constructor
   */
  PointField();

  std::string name_;

  uint16_t offset_;

  PointDataType data_type_;

  uint16_t data_size_bytes_;
};


} // namespace phantom_ai



#endif // PHANTOM_AI_POINT_FIELD
