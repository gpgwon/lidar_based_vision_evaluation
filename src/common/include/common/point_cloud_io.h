#ifndef POINT_CLOUD_IO_H_
#define POINT_CLOUD_IO_H_

#include <fstream>
#include <iostream>
#include <iomanip>
#include <common/point_cloud.h>
#include <boost/filesystem.hpp>
#include <common/file_system.h>

bool writePointCloudToFile(const std::string& out_folder, const PointCloudf& cloud)
{
  // Check if the output folder exist.
  boost::filesystem::path out_folder_path(out_folder);
  if(!boost::filesystem::exists(out_folder_path) || !boost::filesystem::is_directory(out_folder_path))
  {
    std::cout << "Could not find the output folder to store the point cloud file. [" << out_folder << "]" << std::endl;
    return false;
  }

  std::string copied_out_folder = out_folder;
  if(copied_out_folder.back() == '/')
  {
    copied_out_folder.resize(copied_out_folder.size() - 1);
  }
  std::cout.precision(9);
  std::cout << cloud.points_[0].timestamp << std::endl;

  std::string file_name;
  uint64_t first_point_timestamp_nsec = static_cast<uint64_t>(1.0E9 * cloud.points_[0].timestamp);

  uint64_t time_sec = static_cast<uint64_t>(1.0E-9 * first_point_timestamp_nsec);
  uint64_t time_nsec = first_point_timestamp_nsec - 1.0E9 * time_sec;

  std::string time_sec_str = std::to_string(time_sec);
  std::string time_nsec_str = std::to_string(time_nsec);

  if(time_nsec_str.size() < 9)
  {
    for(size_t i = time_nsec_str.size(); i < 9; ++i)
    {
      time_nsec_str = "0" + time_nsec_str;
    }
  }

  file_name = time_sec_str + "_" + time_nsec_str;

  std::ofstream fout(copied_out_folder + "/" + file_name + ".pc" , std::ios::out | std::ios::binary);

  size_t num_points = cloud.getSize();
  std::vector<char> write_data(17 * num_points);
  size_t pos = 0;
  for(size_t i = 0; i < num_points; ++i)
  {
    float x = cloud.points_[i].x;
    float y = cloud.points_[i].y;
    float z = cloud.points_[i].z;
    uint8_t intensity = cloud.points_[i].intensity;
    uint32_t time_offset_nsec = static_cast<uint32_t>(static_cast<uint64_t>(1.0E9 * cloud.points_[i].timestamp) - first_point_timestamp_nsec);

    std::memcpy(&write_data[pos], &x, 4);
    std::memcpy(&write_data[pos + 4], &y, 4);
    std::memcpy(&write_data[pos + 8], &z, 4);
    std::memcpy(&write_data[pos + 12], &intensity, 1);
    std::memcpy(&write_data[pos + 13], &time_offset_nsec, 4);
    pos += 17;
  }
  fout.write(&write_data[0], 17 * num_points);

  return true;
}

bool readPointCloudFromFile(const std::string& file_path, PointCloudf& cloud)
{
  // Get the timestamp at the first point.
  std::string file_path_wo_extension = removeFileExtention(file_path);
  std::string file_name_only = extractLastBranchNameOfPath(file_path_wo_extension);

  double timestamp_at_first_point = convertTimeStringToDouble(file_name_only);

  // Read the file.
  std::ifstream fin(file_path, std::ios::in | std::ios::binary);
  if(!fin.is_open())
  {
    std::cout << "Fail to open the point cloud file [" << file_path << "]" << std::endl;
    return false;
  }

  // Get length of the file:
  fin.seekg (0, std::ios::end);
  int length = fin.tellg();
  fin.seekg (0, std::ios::beg);

  // Copy to a buffer.
  //char* buff = new char[length];
  std::vector<char> buff(length);
  fin.read(buff.data(), length);
  fin.close();

  // Copy to the out pointcloud.
  uint32_t num_points = length / 17;
  cloud.resize(num_points);
  int pos = 0;
  for(size_t i = 0; i < num_points; ++i)
  {
    std::memcpy(&(cloud.points_[i].x), &buff[pos], 4);
    std::memcpy(&(cloud.points_[i].y), &buff[pos + 4], 4);
    std::memcpy(&(cloud.points_[i].z), &buff[pos + 8], 4);
    std::memcpy(&(cloud.points_[i].intensity), &buff[pos + 12], 1);

    uint32_t time_offset_nsec;
    std::memcpy(&time_offset_nsec, &buff[pos + 13], 4);
    cloud.points_[i].timestamp = timestamp_at_first_point + 1E-9 * time_offset_nsec;

    pos += 17;
  }
  return true;
}




#endif
