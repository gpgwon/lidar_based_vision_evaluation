#ifndef VISION_EVALUATION_FILE_SYSTEM_
#define VISION_EVALUATION_FILE_SYSTEM_

#include <iostream>
#include <algorithm>
#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>

bool getFileListInTargetFolder(const std::string& target_folder, std::vector<std::string>& file_list)
{
  namespace fs = boost::filesystem;

  file_list.clear();

  fs::path target_folder_path(target_folder);

  if(fs::exists(target_folder_path) && fs::is_directory(target_folder_path))
  {
    fs::directory_iterator it(target_folder_path), eod;
    BOOST_FOREACH(fs::path const &p, std::make_pair(it, eod))
    {
//      if(fs::is_regular_file(p))
      {
        file_list.push_back(p.string());
      }
    }

    std::sort(file_list.begin(), file_list.end());

    return true;
  }
  else
  {
    std::cout << "Fail to read the list of files in the folder [" << target_folder << "]" << std::endl;
    return false;
  }
}

bool getFileListInTargetFolder(const std::string& target_folder, const std::string& file_extension, std::vector<std::string>& file_list)
{
  namespace fs = boost::filesystem;

  file_list.clear();

  fs::path target_folder_path(target_folder);

  if(fs::exists(target_folder) && fs::is_directory(target_folder))
  {
    fs::directory_iterator it(target_folder_path), eod;
    BOOST_FOREACH(fs::path const &p, std::make_pair(it, eod))
    {
      if(fs::is_regular_file(p))
      {
        int extention_lenth = static_cast<int>(file_extension.size());
        std::string sub_str = p.string().substr(p.string().size() - extention_lenth, extention_lenth);

        if(sub_str.compare(file_extension) == 0)
        {
          file_list.push_back(p.string());
        }
      }
    }

    std::sort(file_list.begin(), file_list.end());

    return true;
  }
  else
  {
    std::cout << "Fail to read the list of files in the folder [" << target_folder << "]" << std::endl;
    return false;
  }
}

bool getChildFolderList(const std::string& target_folder, std::vector<std::string>& child_folder_list)
{
  namespace fs = boost::filesystem;
  child_folder_list.clear();

  boost::filesystem::path target_folder_path(target_folder);

  if(fs::exists(target_folder_path) && fs::is_directory(target_folder_path))
  {
    fs::directory_iterator it(target_folder_path), eod;
    BOOST_FOREACH(fs::path const &p, std::make_pair(it, eod))
    {
      if(fs::is_directory(p))
      {
        child_folder_list.push_back(p.string());
      }
    }

    std::sort(child_folder_list.begin(), child_folder_list.end());
    return true;
  }
  else
  {
    std::cout << "Fail to read the list of child folders in the folder [" << target_folder << "]" << std::endl;
    return false;
  }
}

std::string removeFileExtention(const std::string& file_name)
{
  size_t lastdot = file_name.find_last_of(".");
  if(lastdot == std::string::npos)
  {
    return file_name;
  }
  else
  {
    return file_name.substr(0, lastdot);
  }
}

std::string extractLastBranchNameOfPath(const std::string& path)
{
  std::string temp_path_str = path;
  if(temp_path_str.back() == '/')
  {
    temp_path_str.resize(temp_path_str.size() - 1);
  }

  size_t last_slash = temp_path_str.find_last_of("/");
  if(last_slash == std::string::npos)
  {
    return path;
  }
  else
  {
    return temp_path_str.substr(last_slash + 1, temp_path_str.size() - last_slash - 1);
  }
}

double convertTimeStringToDouble(const std::string& time_string)
{
  size_t pos_underscore = time_string.find_first_of("_");
  std::string sec_part = time_string.substr(0, pos_underscore);
  std::string nsec_part = time_string.substr(pos_underscore + 1, time_string.size() - pos_underscore - 1);

  return std::stod(sec_part) + 1E-9 * std::stod(nsec_part);
}

#endif
