#ifndef UTILS_H_
#define UTILS_H_

#include <string>
#include <iostream>
std::string convertDoubleTimestampToString(const double& timestamp)
{
  uint64_t time_sec = static_cast<uint64_t>(timestamp);
  uint64_t time_nsec = static_cast<uint64_t>(static_cast<uint64_t>(1E3 * timestamp)) - 1E3 * time_sec;
  time_nsec *= 1E6;

  std::string str_timestamp_sec = std::to_string(time_sec);
  std::string str_timestamp_nsec = std::to_string(time_nsec);

  int diff_str_length = 9 - static_cast<int>(str_timestamp_nsec.size());

  for(int i = 0; i < diff_str_length; ++i)
  {
    str_timestamp_nsec.insert(str_timestamp_nsec.begin(), 1, '0');
  }

  return str_timestamp_sec + "_" + str_timestamp_nsec;
}

std::string convertDoubleTimestampTo13DigitString(const double& timestamp)
{
  uint64_t timestamp_msec = static_cast<uint64_t>(1000 * timestamp);
  return std::to_string(timestamp_msec);
}

double convertSec_nSecTimeToDouble(std::string& timestamp_str)
{
  size_t underscore_pos = timestamp_str.find_last_of("_");
  std::string sec_str = timestamp_str.substr(0, underscore_pos);
  std::string nsec_str = timestamp_str.substr(underscore_pos + 1, timestamp_str.size() - underscore_pos - 1);

  double timestamp_double = std::stod(sec_str) + 1E-9 * std::stod(nsec_str);
  return timestamp_double;
}

void intensityToRGB(const uint8_t& intensity, const int& max_inten, int& r, int& g, int& b)
{
  double i_f = static_cast<double>(intensity);
  double num_f = static_cast<double>(max_inten);

  double ratio = i_f / num_f;
  int a = static_cast<int>((1.0 - ratio) / 0.25);	//invert and group
  int X = static_cast<int>(a);	//this is the integer part
  int Y = static_cast<int>(255.0 * (a - X)); //fractional part from 0 to 255
  switch(X)
  {
  case 4: r=255; g=Y; b=0; break;
  case 3: r=255-Y; g=255; b=0; break;
  case 2: r=0; g=255; b=Y; break;
  case 1: r=0; g=255-Y; b=255; break;
  case 0: r=0; g=0; b=255; break;
  }
}

#endif
