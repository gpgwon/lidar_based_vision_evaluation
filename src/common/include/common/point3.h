#ifndef _GEOMETRY_POINT3D_H
#define _GEOMETRY_POINT3D_H

/*******************************************************************************
* @file    point_3d.h
* @date    07/03/2017
*
* @description Phantom Geometry Core Library
*              Contains implementation of a 3D point class
*
* @attention Copyright (c) 2017
* @attention Phantom AI, Inc.
* @attention All rights reserved.
*******************************************************************************/

#include <common/point2.h>

#include <cmath>
#include <iostream>

// Forward Declaration
template<typename T> class Point2T;


/*
* @brief A 3D point
*
* A Point3T class represents a point in 3D cartesian space. Basic linear
* algebraic operations are defined for adding/subtracting and multiplying/dividing
* by a scalar. Point3T objects can also be operated upon by 3D transforms,
* poses and quaternions to produce new 3D points.
*
*/
template<typename T>
class Point3T
{
public:
  T x; // The x coordinate of the point
  T y; // The y coordinate of the point
  T z; // The z coordinate of the point

  // Object wide constant
  static constexpr T tolerance = 1e-6; // tolerance for which two Point3T objects are considered equal

  /*
   * Constructors
   * Create a Point3T object.
   */

  // Default Constructor, the x, y and z coordinates are set to zero
  Point3T(void):x(0.0), y(0.0), z(0.0) { }

  // Sets the x, y and z coordinates to the passed x, y and z values
  Point3T(T inx, T iny, T inz):x(inx), y(iny), z(inz) { }

  // Converts a 2D point to a 3D one by setting the z coordinate of the 3D point to zero.
  Point3T(const Point2T<T>& b):x(b.x), y(b.y), z(0.0) { }

  // Copy Constructor
  Point3T(const Point3T& b) = default;

  // Accessors/setters to remain backwards compatibility with the old point type.
  void set(T inx, T iny, T inz)
  {
    x = inx;
    y = iny;
    z = inz;
  }

  void set_x(T inx)
  {
    x = inx;
  }

  const T& get_x() const
  {
    return x;
  }

  void set_y(T iny)
  {
    y = iny;
  }

  const T& get_y() const
  {
    return y;
  }

  void set_z(T inz)
  {
    z = inz;
  }

  const T& get_z() const
  {
    return z;
  }

  /*
   * Arithmetic Operators
   * Methods for operating on 2D points.
  */

  // Point Addition
  void operator+=(const Point3T& b)
  {
    x += b.x;
    y += b.y;
    z += b.z;
  }

  // Point Subtraction
  void operator-=(const Point3T& b)
  {
    x -= b.x;
    y -= b.y;
    z -= b.z;
  }

  // Multiplication by scalar
  void operator*=(T b)
  {
    x *= b;
    y *= b;
    z *= b;
  }

  // Division by scalar
  void operator/=(T b)
  {
    if(b == 0.0)
    {
      std::cerr << "Attempting to divide by zero in 3D point division." << std::endl;
      return;
    }

    x /= b;
    y /= b;
    z /= b;
  }

  // Point Assignment
  Point3T& operator=(const Point3T& b) = default;

  /*
   *  Friend Arithmetic Operators
   */

  // Division by scalar
  friend Point3T operator/(const Point3T& a, T b)
  {
    Point3T c(a);
    if (b != 0)
    {
      c /= b;
    }
    return c;
  }

  // Multiplication by scalar
  friend Point3T operator*(const Point3T& a, T b)
  {
    Point3T c(a);
    c *= b;
    return c;
  }

  // Multiplication by scalar
  friend Point3T operator*(T a, const Point3T& b)
  {
    return b * a;
  }

  // Point Addition
  friend Point3T operator+(const Point3T& a, const Point3T& b)
  {
    Point3T c(a);
    c += b;
    return c;
  }

  // Point Subtraction
  friend Point3T operator-(const Point3T& a, const Point3T& b)
  {
    Point3T c(a);
    c -= b;
    return c;
  }

  // Point Comparison
  friend bool operator==(const Point3T& a, const Point3T& b)
  {
    return (std::abs(a.x - b.x) < Point3T::tolerance) &&
           (std::abs(a.y - b.y) < Point3T::tolerance) &&
           (std::abs(a.z - b.z) < Point3T::tolerance);
  }

  friend bool operator!=(const Point3T& a, const Point3T& b)
  {
    return !(a == b);
  }

  /*
   * Other Methods
   */

  // Returns the square of the cartesian distance between this point and the passed argument
  T distanceSq(const Point3T& a) const
  {
    return((x - a.x) * (x - a.x) + (y - a.y) * (y - a.y) +
           (z - a.z) * (z - a.z));
  }

  // Returns the cartesian distance between this point and the passed argument
  T distance(const Point3T& a) const
  {
    return std::sqrt(distanceSq(a));
  }

  T distance2dSq(const Point3T& a) const
  {
    return (x - a.x) * (x - a.x) + (y - a.y) * (y - a.y);
  }

  T distance2d(const Point3T& a) const
  {
    return std::sqrt(distance2dSq(a));
  }

  // Stream Operators
  // writes the 3D point object to the passed stream
  friend std::ostream& operator<<(std::ostream& out, const Point3T& a)
  {
    return out << "( " << a.x << ", " << a.y << ", " << a.z << " )";
  }
};

typedef Point3T<float>  Point3f;
typedef Point3T<double> Point3d;


#endif
