/*******************************************************************************
* @file    time_stamp.h
* @date    10/17/2017
*
* @attention Copyright (c) 2017
* @attention Phantom AI, Inc.
* @attention All rights reserved.
*******************************************************************************/
#ifndef PHANTOM_AI_TIME_STAMP_H_
#define PHANTOM_AI_TIME_STAMP_H_

#include <iostream>
#include <string>


// Number of nano, micro, and milliseconds in one second.
const uint32_t kNanosecondsInSecond = 1e9;
const uint32_t kMicrosecondsInSecond = 1e6;
const uint32_t kMillisecondsInSecond = 1e3;

// Number of nanoseconds in one microsecond.
const uint32_t kNanosecondsInMicroSecond = (kNanosecondsInSecond / kMicrosecondsInSecond);

// Display three decimal places when printing the string form of the timestamp.
const size_t kDefaultTimestampStringPrecision = 3;

class TimeStamp
{
 public:
  /** @brief Constructor that initializes the timestamp to the epoch.
  *   @public
  */
  constexpr TimeStamp() : epoch_sec_(0.0) {}

  /** @brief Constructor that initializes the timestamp using the specified epoch seconds.
  *   @param s Seconds since epoch.
  *   @public
  */
  TimeStamp(double time_sec);

  /** @brief Constructor that initializes the timestamp using the specified epoch seconds, nanoseconds.
  *   @param s Whole seconds since epoch.
  *   @param ns Number of nanoseconds to be added to the `s` arg.
  *   @public
  */
  TimeStamp(int64_t s, int64_t ns);

  /** @brief Get the number of seconds since epoch.
  *   @return The number of seconds since epoch.
  *   @public
  */
  double toSec() const;

  /** @brief Get the number of microseconds since epoch.
  *   @return The number of microseconds since epoch.
  *   @public
  */
  uint64_t toUSec() const;

  /** @brief Test if the timestamp is zero (epoch).
  *   @return Boolean indicating if the timestamp is equal to the epoch.
  *   @public
  */
  bool isZero() const;

  /** @brief Get a formatted string of the current time in UTC.
  *   @return The timestamp as a formatted string in the form 'YYYY-MM-DD-hh-mm-ss'.
  *   @public
  */
  std::string toUtcString() const;

  /** @brief Get a formatted string of the current time in ISO8616 format.
  *   @return The timestamp as a ISO-8601 formatted string, including timezone offset.
  *   @public
  */
  std::string toIso8601String() const;

  /** @brief Get a formatted string of the current time in the local timezome.
  *   @return The timestamp as a formatted string in the form 'Day Mon DD HH:MM:SS AM YYYY'.
  *   @public
  */
  std::string toLocaleString() const;

  /** @brief Operator+ overload to add two timestamps.
  *   @return Timestamp from the two timestamps added together.
  */
  TimeStamp operator+(const TimeStamp& rhs) const;

  /** @brief Operator- overload to subtract two timestamps.
  *   @return Timestamp from the difference of the two timestamps.
  *   @public
  */
  TimeStamp operator-(const TimeStamp& rhs) const;

  /** @brief Operator+= overload to add two timestamps.
  *   @return Ref to current timestamp instance with the operand added to it.
  *   @public
  */
  TimeStamp& operator+=(const TimeStamp& rhs);

  /** @brief Operator+= overload to subtract two timestamps.
  *   @return Ref to current timestamp instance with the operand subtracted from it.
  *   @public
  */
  TimeStamp& operator-=(const TimeStamp& rhs);

  /** @brief Operator== overload to compare two timestamps.
  *   @return Boolean indicating if the two timestamps are equal.
  *   @public
  */
  bool operator==(const TimeStamp& rhs) const;

  /** @brief Operator!= overload to compare two timestamps.
  *   @return Boolean indicating if the two timestamps are not equal.
  *   @public
  */
  bool operator!=(const TimeStamp& rhs) const;

  /** @brief Construct a timestamp from the number of microseconds since epoch.
  *   @return Timestamp instance.
  *   @public
  */
  static TimeStamp FromUsec(uint64_t);

  /** @brief Construct a timestamp from the current time. This prefers the simulation time if available, otherwise
  *          it defaults to the wall clock.
  *   @return Timestamp instance.
  *   @public
  */
  static TimeStamp Now();

  /** @brief Construct a timestamp from the wall clock.
  *   @return Timestamp instance.
  *   @public
  */
  static TimeStamp  WallClockNow();

  /** @brief Get largest representable timestamp.
  *   @return Timestamp instance.
  *   @public
  */
  static TimeStamp End();

  /** @brief Overload for operator> to compare two timestamps.
  *   @return Bool if TimeStamp a is more recent than TimeStamp b.
  *   @public
  */
  friend bool operator>(const TimeStamp& a, const TimeStamp& b);

  /** @brief Overload for operator>= to compare two timestamps.
  *   @return Bool if TimeStamp a is more recent or equal to TimeStamp b.
  *   @public
  */
  friend bool operator>=(const TimeStamp& a, const TimeStamp& b);

  /** @brief Overload for operator< to compare two timestamps.
  *   @return Bool if TimeStamp a is prior to TimeStamp b.
  *   @public
  */
  friend bool operator<(const TimeStamp& a, const TimeStamp& b);

  /** @brief Overload for operator<= to compare two timestamps.
  *   @return Bool if TimeStamp a is prior or equal to TimeStamp b.
  *   @public
  */
  friend bool operator<=(const TimeStamp& a, const TimeStamp& b);

  /** @brief String representation of a TimeStamp instance.
  *   @return Ref to supplied output stream.
  *   @public
  */
  friend std::ostream& operator<<(std::ostream& os, const TimeStamp& ts);

 private:
  // Number of seconds since epoch.
  double epoch_sec_;
};


#endif

