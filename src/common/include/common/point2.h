#ifndef _GEOMETRY_POINT2D_H
#define _GEOMETRY_POINT2D_H

/*******************************************************************************
* @file    point_2d.h
* @date    07/03/2017
*
* @description Phantom Geometry Core Library
*              2D Point class
*
* @attention Copyright (c) 2017
* @attention Phantom AI, Inc.
* @attention All rights reserved.
*******************************************************************************/

#include <cmath>
#include <iostream>


// Forward Declaration


/*
* @brief A 2D point
*
* A Point2T class represents a point in 2D cartesian space.  Basic linear
* algebraic operations are defined for adding/subtracting and multiplying/dividing
* by a scalar. Point2T objects can also be operated upon by 2D transforms
* and poses to produce new 2D points.
*
*/
template<typename T>
class Point2T
{
 public:
  T x; // The x coordinate of the point
  T y; // The y coordinate of the point

  // Object wide constant
  static constexpr T tolerance = 1e-6; // tolerance for which two Point2T objects are considered equal

  /*
   * Constructors
   * Create a Point2T object.
   */

  // Default Constructor, both the x and y coordinates are set to zero
  constexpr Point2T(): x(0.0), y(0.0) { }


  // Sets the x and y coordinates to the passed x and y values
  constexpr Point2T(T inx, T iny): x(inx), y(iny) { }

  // Copy Constructor
  constexpr Point2T(const Point2T& pt2D) = default;

  // Sets the x and y coordinates to the passed x and y values.  Used to retain backwards compatibility with the old
  // point types.
  void set(T inx, T iny)
  {
    x = inx;
    y = iny;
  }

  // Accessors for backwards compatibility with the old point types.
  T get_x() const
  {
    return x;
  }

  void set_x(T inx)
  {
    x = inx;
  }

  T get_y() const
  {
    return y;
  }

  void set_y(T iny)
  {
    y = iny;
  }


  /*
   * Arithmetic Operators
   * Methods for operating on 2D points.
   */

  // Point Addition
  void operator+=(const Point2T& b)
  {
    x += b.x;
    y += b.y;
  }

  // Point Subtraction
  void operator-=(const Point2T& b)
  {
    x -= b.x;
    y -= b.y;
  }

  // Division by a scalar
  void operator/=(T b)
  {
    if(b == 0.0)
    {
      std::cerr << "Attempting to divide by zero in 2D point division." << std::endl;
      return;
    }

    x /= b;
    y /= b;
  }

  // Multiplication by a scalar
  void operator*=(T b)
  {
    x *= b;
    y *= b;
  }

  // Point Assignment
  Point2T& operator=(const Point2T& b) = default;

  /*
   * Friend Arithmetic Operators
   */

  // Division by a scalar
  friend Point2T operator/(const Point2T& a, T b)
  {
    Point2T c(a);
    c /= b;
    return c;
  }

  // Multiplication by a scalar
  friend Point2T operator*(const Point2T& a, T b)
  {
    Point2T c(a);
    c *= b;
    return c;
  }

  // Multiplication by a scalar
  friend Point2T operator*(T a, const Point2T& b)
  {
    return a * b;
  }

  // Point Addition
  friend Point2T operator+(const Point2T& a, const Point2T& b)
  {
    Point2T c(a);
    c += b;
    return c;
  }

  // point Subtraction
  friend Point2T operator-(const Point2T& a, const Point2T& b)
  {
    Point2T c(a);
    c -= b;
    return c;
  }


  // point comparison
  friend bool operator==(const Point2T& a, const Point2T& b)
  {
    return (std::abs(a.x - b.x) < Point2T::tolerance) &&
           (std::abs(a.y - b.y) < Point2T::tolerance);
  }
  friend bool operator!=(const Point2T& a, const Point2T& b)
  {
    return !(a == b);
  }

  // Other Methods
  // Returns the cartesian distance between this point and the passed argument
  T distance(const Point2T& a) const
  {
    return std::sqrt(distanceSq(a));
  }

  T distance(const T& a_x, const T& a_y)
  {
    return std::sqrt(distanceSq(a_x, a_y));
  }

  // Returns the square of the cartesian distance between this point and the passed argument
  T distanceSq(const Point2T& a) const
  {
    return((x - a.x) * (x - a.x) + (y - a.y) * (y - a.y));
  }

  T distanceSq(const T& a_x, const T& a_y)
  {
    return (x - a_x) * (x - a_x) + (y - a_y) * (y - a_y);
  }

  T angle(const Point2T& to) const
  {
    return std::atan2(y - to.y, x - to.x);
  }

  T angle(const T& to_x, const T& to_y) const
  {
    return std::atan2(y - to_y, x - to_x);
  }

  // Stream Operators
  // writes the 2D point object to the passed stream
  friend std::ostream& operator<<(std::ostream& out, const Point2T& a)
  {
    return out << "( " << a.x << ", " << a.y << " )";
  }
};

typedef Point2T<float>  Point2f;
typedef Point2T<double> Point2d;


#endif
