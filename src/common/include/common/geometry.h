#ifndef GEOMETRY_H_
#define GEOMETRY_H_

#include <common/simple_gps_ins_measurement.h>
#include <array>
#include <vector>
#include <Eigen/Dense>

#define M_PI_float 3.141592f
#define TwoPI 6.2831853072
#define TwoPI_float 6.2831853072f
#define HalfPI 1.5707963268
#define HalfPI_float 1.5707963268f
#define degree_to_radian(deg) (deg * M_PI / 180.0)
#define degree_to_radian_float(deg) (deg * M_PI_float / 180.0f)
#define radian_to_degree(rad) (rad * 180.0 / M_PI)
#define radian_to_degree_float(rad) (rad * 180.0f / M_PI_float)

Eigen::Matrix3d getRotMatrix(double roll, double pitch, double yaw);

Eigen::Matrix3d getInverseRotMatrix3d(double roll, double pitch, double yaw);

Eigen::Matrix3f getInverseRotMatrix3f(double roll, double pitch, double yaw);

std::vector<double> getInverseRotVector(double roll, double pitch, double yaw);

std::array<std::array<double, 3>, 3> getRotMatrixArrayFormat(double roll, double pitch, double yaw);

Eigen::Matrix4f getTransformMatrix(float x, float y, float z, float roll, float pitch, float yaw);

#endif
