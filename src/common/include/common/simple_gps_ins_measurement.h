/*******************************************************************************
* @file    simple_gps_ins_measurement.h
* @date    02/07/2019
*
* @attention Copyright (c) 2019
* @attention Phantom AI, Inc.
* @attention All rights reserved.
*******************************************************************************/

#ifndef PHANTOM_AI_SIMPLE_GPS_INS_MEASUREMENT_H_
#define PHANTOM_AI_SIMPLE_GPS_INS_MEASUREMENT_H_

#include <string>

class SimpleGPSINSMeasurement
{
  const double two_pi = 6.28318530718;
public:
  SimpleGPSINSMeasurement() :
    utm_x_meters_(0.0), utm_y_meters_(0.0), utm_z_meters_(0.0),
    position_covariance_(0.0),
    velocity_x_mps_(0.0), velocity_y_mps_(0.0), velocity_z_mps_(0.0),
    roll_rad_(0.0), pitch_rad_(0.0), yaw_rad_(0.0),
    acc_x_mpss_(0.0), acc_y_mpss_(0.0), acc_z_mpss_(0.0),
    gyro_x_rps_(0.0), gyro_y_rps_(0.0), gyro_z_rps_(0.0)
  {}

  ~SimpleGPSINSMeasurement(){}

  void operator=(const SimpleGPSINSMeasurement& b)
  {
    timestamp_ = b.timestamp_;
    utm_x_meters_ = b.utm_x_meters_;
    utm_y_meters_ = b.utm_y_meters_;
    utm_z_meters_ = b.utm_z_meters_;
    position_covariance_ = b.position_covariance_;
    velocity_x_mps_ = b.velocity_x_mps_;
    velocity_y_mps_ = b.velocity_y_mps_;
    velocity_z_mps_ = b.velocity_z_mps_;
    xy_speed_mps_ = b.xy_speed_mps_;
    roll_rad_ = b.roll_rad_;
    pitch_rad_ = b.pitch_rad_;
    yaw_rad_ = b.yaw_rad_;
    acc_x_mpss_ = b.acc_x_mpss_;
    acc_y_mpss_ = b.acc_y_mpss_;
    acc_z_mpss_ = b.acc_z_mpss_;
    gyro_x_rps_ = b.gyro_x_rps_;
    gyro_y_rps_ = b.gyro_y_rps_;
    gyro_z_rps_ = b.gyro_z_rps_;
    gps_status_ = b.gps_status_;    
  }

  void operator+=(const SimpleGPSINSMeasurement& b)
  {
    utm_x_meters_ += b.utm_x_meters_;
    utm_y_meters_ += b.utm_y_meters_;
    utm_z_meters_ += b.utm_z_meters_;
    position_covariance_ = b.position_covariance_;
    velocity_x_mps_ += b.velocity_x_mps_;
    velocity_y_mps_ += b.velocity_y_mps_;
    velocity_z_mps_ += b.velocity_z_mps_;
    xy_speed_mps_ += b.xy_speed_mps_;
    roll_rad_ += b.roll_rad_;
    pitch_rad_ += b.pitch_rad_;
    yaw_rad_ += b.yaw_rad_;
    if(yaw_rad_ > two_pi)
    {
      yaw_rad_ -= two_pi;
    }
    else if(yaw_rad_ < 0.0)
    {
      yaw_rad_ += two_pi;
    }

    acc_x_mpss_ += b.acc_x_mpss_;
    acc_y_mpss_ += b.acc_y_mpss_;
    acc_z_mpss_ += b.acc_z_mpss_;
    gyro_x_rps_ += b.gyro_x_rps_;
    gyro_y_rps_ += b.gyro_y_rps_;
    gyro_z_rps_ += b.gyro_z_rps_;
    gps_status_ = b.gps_status_;
  }

  void operator-=(const SimpleGPSINSMeasurement& b)
  {
    utm_x_meters_ -= b.utm_x_meters_;
    utm_y_meters_ -= b.utm_y_meters_;
    utm_z_meters_ -= b.utm_z_meters_;
    position_covariance_ = b.position_covariance_;
    velocity_x_mps_ -= b.velocity_x_mps_;
    velocity_y_mps_ -= b.velocity_y_mps_;
    velocity_z_mps_ -= b.velocity_z_mps_;
    xy_speed_mps_ -= b.xy_speed_mps_;
    roll_rad_ -= b.roll_rad_;
    pitch_rad_ -= b.pitch_rad_;
    yaw_rad_ -= b.yaw_rad_;
    if(yaw_rad_ > two_pi)
    {
      yaw_rad_ -= two_pi;
    }
    else if(yaw_rad_ < 0.0)
    {
      yaw_rad_ += two_pi;
    }
    acc_x_mpss_ -= b.acc_x_mpss_;
    acc_y_mpss_ -= b.acc_y_mpss_;
    acc_z_mpss_ -= b.acc_z_mpss_;
    gyro_x_rps_ -= b.gyro_x_rps_;
    gyro_y_rps_ -= b.gyro_y_rps_;
    gyro_z_rps_ -= b.gyro_z_rps_;
    gps_status_ = b.gps_status_;
  }

  void operator*=(const double r)
  {
    utm_x_meters_ *= r;
    utm_y_meters_ *= r;
    utm_z_meters_ *= r;
    velocity_x_mps_ *= r;
    velocity_y_mps_ *= r;
    velocity_z_mps_ *= r;
    xy_speed_mps_ *= r;
    roll_rad_ *= r;
    pitch_rad_ *= r;
    yaw_rad_ *= r;
    acc_x_mpss_ *= r;
    acc_y_mpss_ *= r;
    acc_z_mpss_ *= r;
    gyro_x_rps_ *= r;
    gyro_y_rps_ *= r;
    gyro_z_rps_ *= r;
  }

  friend SimpleGPSINSMeasurement operator+(const SimpleGPSINSMeasurement& a, const SimpleGPSINSMeasurement& b)
  {
    SimpleGPSINSMeasurement c(a);
    c += b;
    return c;
  }

  friend SimpleGPSINSMeasurement operator-(const SimpleGPSINSMeasurement& a, const SimpleGPSINSMeasurement& b)
  {
    SimpleGPSINSMeasurement c(a);
    c -= b;
    return c;
  }

  friend SimpleGPSINSMeasurement operator*(const double a, const SimpleGPSINSMeasurement& b)
  {
    SimpleGPSINSMeasurement c(b);
    c *= a;
    return c;
  }

  // Timestamp.
  double timestamp_;

  // UTM_x (East).
  double utm_x_meters_;

  // UTM_y (North).
  double utm_y_meters_;

  // UTM_z (Up).
  double utm_z_meters_;

  // Position covariance.
  double position_covariance_;

  // Velocity in utm_x (east) direction.
  double velocity_x_mps_;

  // Velocity in utm_y (north) direction.
  double velocity_y_mps_;

  // Velocity in utm_z (up) direction.
  double velocity_z_mps_;

  // Speed in the XY-plane.
  double xy_speed_mps_;

  // Rotation angle about x-axis (forward) of vehicle body.
  double roll_rad_;

  // Rotation angle about y-axis (left) of vehicle body.
  double pitch_rad_;

  // Rotation angle about utm_z-axis. Note that this value is 0 in the east direction, and PI/2 in the north direction.
  double yaw_rad_;

  // Acceleration in x direction of vehicle body.
  double acc_x_mpss_;

  // Acceleration in y direction of vehicle body.
  double acc_y_mpss_;

  // Acceleration in z direction of vehicle body.
  double acc_z_mpss_;

  // Angular velocity about x-axis of vehicle body.
  double gyro_x_rps_;

  // Angular velocity about y-axis of vehicle body.
  double gyro_y_rps_;

  // Angular velocity about utm_z-axis.
  double gyro_z_rps_;

  std::string gps_status_;
};



#endif //PHANTOM_AI_SIMPLE_GPS_INS_MEASUREMENT_H_

