/*******************************************************************************
* @file    point_cloud.h
* @date    03/27/2019
**
* @attention Copyright (c) 2018
* @attention Phantom AI, Inc.
* @attention All rights reserved.
*******************************************************************************/

#ifndef PHANTOM_AI_POINT_CLOUD_H_
#define PHANTOM_AI_POINT_CLOUD_H_

#include <common/cloud_point_types.h>
#include <common/time_stamp.h>

#include <vector>
#include <algorithm>
#include <iterator>


template <typename T = Point3If>
class PointCloud
{
public:
  // Height of the point cloud
  uint32_t height_;

  // Width of the point cloud
  uint32_t width_;

  // A flag indicating that the point cloud is 2-dimensional.
  bool is_2D_structure_;

  // A flag to check if all the data in the point cloud is finite (not Inf nor NAN).
  bool is_dense_;

  // Point vector
  std::vector<T> points_;


  /**
   * @brief Default constructor.
   */
  PointCloud() :
    height_(1),
    width_(0),
    is_2D_structure_(false),
    is_dense_(false),
    points_{}
  {
  }

  /**
   * @brief Copy consturctor.
   */
  PointCloud(const PointCloud& b) :
    height_(b.height_),
    width_(b.width_),
    is_2D_structure_(b.is_2D_structure_),
    is_dense_(b.is_dense_),
    points_{b.points_}
  {
  }

  /**
   * @brief Function to get capacity of the point vector.
   * @return Capacity of the point vector.
   */
  size_t getCapacity() const
  {
    return points_.capacity();
  }

  /**
   * @brief Function to get size of the point vector.
   * @return Size of the point vector.
   */
  // Get size of the point vector.
  size_t getSize() const
  {
    return points_.size();
  }

  /**
   * @brief Function to access to a point with 2D indices.
   * @param height index.
   * @param width index.
   * @return Point instance.
   */
  T& at(const uint32_t& height_idx, const uint32_t& width_idx)
  {
    if(height_ > 1)
      return points_.at(height_idx + width_idx * height_);
    else
      std::cout << "1D point cloud cannot be referenced by 2D indices." << std::endl;
  }

  /**
   * @brief Function to access to a point with 1D index.
   * @param point index
   * @return Point instance.
   */
  T& at(const uint32_t& idx)
  {
    return points_.at(idx);
  }

  /**
   * @brief Function to check if the point cloud is empty (whether point.size() == 0 or not).
   * @return True : empty, False : not empty.
   */
  bool isempty() const
  {
    return points_.empty();
  }

  /**
   * @brief Function to reserve memory space for the point vector.
   * @param Number of bytes to reserve.
   */
  void reserve (const uint32_t& n)
  {
    points_.reserve(n);
  }

  /**
   * @brief Function to resize the point cloud with 2D size.
   * @param height of the point cloud.
   * @param width of the point cloud.
   */
  void resize (const uint32_t& height, const uint32_t& width)
  {
    points_.resize(width * height);
    height_ = height;
    width_ = width;
    if(height_ > 1)
    {
      is_2D_structure_ = true;
    }
  }

  /**
   * @brief Function to resize the point cloud with 1D size.
   * @param size of the point cloud.
   */
  void resize (const uint32_t& size)
  {
    points_.resize(size);
    width_ = size;
    height_ = 1;
    is_2D_structure_ = false;
  }

  /**
   * @brief Function to clear the point cloud.
   */
  void clear ()
  {
    points_.clear();
    width_ = 0;
    height_ = 1;
  }

  /**
   * @brief Function to push back a point. Once pushed a point, the 2D structure of the point cloud is broken.
   * @param a point to be added.
   */
  void push_back (const T& pt)
  {
    points_.push_back(pt);
    width_ = points_.size();
    height_ = 1;

    if(height_ > 1)
    {
      std::cout << "By pushing back a point, 2D structure of the point cloud was broken.";
    }
  }

  /**
   * @brief Function to emplace back a point. In common with the push_back function, the 2D structure of the point cloud is broken.
   * @param parameters to initialize the point instance.
   */
  // Emplace back. In common with the push_back function, the 2D structure of the point cloud is broken.
  template<typename... Args>
  void emplace_back (Args&&... args)
  {
    points_.emplace_back(args...);
    width_ = points_.size();
    height_ = 1;
  }

  /**
   * @brief Function to append the point cloud with another point cloud.
   * @param new point cloud to append.
   */
  void append(const PointCloud& b)
  {
    // If the heights of both point clouds are identical and the value is larger than 1, keep the 2D structure.
    // If not, set the point cloud as 1D structure.
    if(height_ == b.height_ && height_ > 1)
    {
      width_ += b.width_;
      is_2D_structure_ = true;
    }
    else
    {
      height_ = 1;
      width_ = height_ * width_ + b.height_ * b.width_;
      is_2D_structure_ = false;
    }

    // If both point clouds are dense the new point cloud will be dense.
    if(is_dense_ && b.is_dense_)
    {
      is_dense_ = true;
    }
    else
    {
      is_dense_ = false;
    }

    // Reserve memory space.
    points_.reserve(points_.size() + b.points_.size());

    // Append the input points to the end of the point cloud.
    points_.insert(points_.end(), b.points_.begin(), b.points_.end());
  }

  /**
   * @brief operator for point cloud appending.
   */
  PointCloud& operator+=(const PointCloud& b)
  {
    append(b);

    return(*this);
  }

  /**
   * @brief operator for merging two point clouds.
   */
  PointCloud& operator+(const PointCloud& b)
  {
    return (*this) += b;
  }

  /**
   * @brief Functino to fill the field information depending on the data type T.
   */
  void generatePointFieldInformation();

};

  typedef PointCloud<Point3If> PointCloudf;
  typedef PointCloud<Point3Id> PointCloudd;


#endif
