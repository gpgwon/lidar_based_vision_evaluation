#include <common/geometry.h>


Eigen::Matrix3d getInverseRotMatrix3d(double roll, double pitch, double yaw)
{
  Eigen::Matrix3d ret_matrix;
  Eigen::Matrix3d R, P, Y;

  double sroll = std::sin(roll);
  double croll = std::cos(roll);
  double spitch = std::sin(pitch);
  double cpitch = std::cos(pitch);
  double syaw = std::sin(yaw);
  double cyaw = std::cos(yaw);

  R << 1, 0, 0, 0, croll, sroll, 0, -sroll, croll;
  P << cpitch, 0, -spitch, 0, 1, 0, spitch, 0, cpitch;
  Y << cyaw, syaw, 0, -syaw, cyaw, 0, 0, 0, 1;

  ret_matrix = R * P * Y;

  return ret_matrix;
}

Eigen::Matrix3f getInverseRotMatrix3f(double roll, double pitch, double yaw)
{
  Eigen::Matrix3d ret_matrixd;
  Eigen::Matrix3f ret_matrix;
  Eigen::Matrix3d R, P, Y;

  double sroll = std::sin(roll);
  double croll = std::cos(roll);
  double spitch = std::sin(pitch);
  double cpitch = std::cos(pitch);
  double syaw = std::sin(yaw);
  double cyaw = std::cos(yaw);

  R << 1, 0, 0, 0, croll, sroll, 0, -sroll, croll;
  P << cpitch, 0, -spitch, 0, 1, 0, spitch, 0, cpitch;
  Y << cyaw, syaw, 0, -syaw, cyaw, 0, 0, 0, 1;

  ret_matrixd = R * P * Y;

  for(size_t i = 0; i < 3; ++i)
  {
    for(size_t j = 0; j < 3; ++j)
    {
      ret_matrix(i, j) = static_cast<float>(ret_matrixd(i, j));
    }
  }

  return ret_matrix;
}

std::vector<double> getInverseRotVector(double roll, double pitch, double yaw)
{
  std::vector<double> ret_vec;
  Eigen::Matrix3d R, P, Y;

  double sroll = std::sin(roll);
  double croll = std::cos(roll);
  double spitch = std::sin(pitch);
  double cpitch = std::cos(pitch);
  double syaw = std::sin(yaw);
  double cyaw = std::cos(yaw);

  R << 1, 0, 0, 0, croll, sroll, 0, -sroll, croll;
  P << cpitch, 0, -spitch, 0, 1, 0, spitch, 0, cpitch;
  Y << cyaw, syaw, 0, -syaw, cyaw, 0, 0, 0, 1;

  Eigen::Matrix3d rot_mat = R * P * Y;

  for(size_t i = 0; i < 3; ++i)
  {
    for(size_t j = 0; j < 3; ++j)
    {
      ret_vec.push_back(rot_mat(i,j));
    }
  }

  return ret_vec;
}

Eigen::Matrix3d getRotMatrix(double roll, double pitch, double yaw)
{
  Eigen::Matrix3d matrix;
  double cos_yaw = std::cos(yaw);
  double sin_yaw = std::sin(yaw);
  double cos_pitch = std::cos(pitch);
  double sin_pitch = std::sin(pitch);
  double cos_roll = std::cos(roll);
  double sin_roll = std::sin(roll);

  double comb_a = cos_yaw * sin_pitch;
  double comb_b = sin_yaw * sin_pitch;
  matrix(0, 0) = cos_yaw * cos_pitch;
  matrix(0, 1) = comb_a * sin_roll - sin_yaw * cos_roll;
  matrix(0, 2) = comb_a * cos_roll + sin_yaw * sin_roll;

  matrix(1, 0) = sin_yaw * cos_pitch;
  matrix(1, 1) = comb_b * sin_roll + cos_yaw * cos_roll;
  matrix(1, 2) = comb_b * cos_roll - cos_yaw * sin_roll;

  matrix(2, 0) = -sin_pitch;
  matrix(2, 1) = cos_pitch * sin_roll;
  matrix(2, 2) = cos_pitch * cos_roll;

  return matrix;
}

std::array<std::array<double, 3>, 3> getRotMatrixArrayFormat(double roll, double pitch, double yaw)
{
  std::array<std::array<double, 3>, 3> matrix;
  double cos_yaw = std::cos(yaw);
  double sin_yaw = std::sin(yaw);
  double cos_pitch = std::cos(pitch);
  double sin_pitch = std::sin(pitch);
  double cos_roll = std::cos(roll);
  double sin_roll = std::sin(roll);

  double comb_a = cos_yaw * sin_pitch;
  double comb_b = sin_yaw * sin_pitch;
  matrix[0][0] = cos_yaw * cos_pitch;
  matrix[0][1] = comb_a * sin_roll - sin_yaw * cos_roll;
  matrix[0][2] = comb_a * cos_roll + sin_yaw * sin_roll;

  matrix[1][0] = sin_yaw * cos_pitch;
  matrix[1][1] = comb_b * sin_roll + cos_yaw * cos_roll;
  matrix[1][2] = comb_b * cos_roll - cos_yaw * sin_roll;

  matrix[2][0] = -sin_pitch;
  matrix[2][1] = cos_pitch * sin_roll;
  matrix[2][2] = cos_pitch * cos_roll;

  return matrix;
}

Eigen::Matrix4f getTransformMatrix(float x, float y, float z, float roll, float pitch, float yaw)
{
  Eigen::Matrix4f matrix;
  double cos_yaw = std::cos(yaw);
  double sin_yaw = std::sin(yaw);
  double cos_pitch = std::cos(pitch);
  double sin_pitch = std::sin(pitch);
  double cos_roll = std::cos(roll);
  double sin_roll = std::sin(roll);


  double comb_a = cos_yaw * sin_pitch;
  double comb_b = sin_yaw * sin_pitch;
  matrix(0, 0) = cos_yaw * cos_pitch;
  matrix(0, 1) = comb_a * sin_roll - sin_yaw * cos_roll;
  matrix(0, 2) = comb_a * cos_roll + sin_yaw * sin_roll;

  matrix(1, 0) = sin_yaw * cos_pitch;
  matrix(1, 1) = comb_b * sin_roll + cos_yaw * cos_roll;
  matrix(1, 2) = comb_b * cos_roll - cos_yaw * sin_roll;

  matrix(2, 0) = -sin_pitch;
  matrix(2, 1) = cos_pitch * sin_roll;
  matrix(2, 2) = cos_pitch * cos_roll;

  matrix(3, 0) = 0;
  matrix(3, 1) = 0;
  matrix(3, 2) = 0;

  matrix(0, 3) = x;
  matrix(1, 3) = y;
  matrix(2, 3) = z;
  matrix(3, 3) = 1;

  return matrix;
}

void xyToImageuv(const float& x, const float& y, int& u, int& v)
{
  u = static_cast<int>(10 * x) + 1000;
  v = -static_cast<int>(10 * y) + 300;
}
