/*******************************************************************************
* @file    point_field.cpp
* @date    03/27/2019
**
* @attention Copyright (c) 2018
* @attention Phantom AI, Inc.
* @attention All rights reserved.
*******************************************************************************/

#include <common/point_field.h>

namespace phantom_ai
{
  PointField::PointField():
    name_(), offset_(), data_type_(), data_size_bytes_()
  {}

  std::string point_data_type_enum_to_string(const PointDataType& type)
  {
    switch (type)
    {
      case PointDataType::INT8:
        return "INT8";

      case PointDataType::UINT8:
        return "UINT8";

      case PointDataType::INT16:
        return "INT16";

      case PointDataType::UINT16:
        return "UINT16";

      case PointDataType::INT32:
        return "INT32";

      case PointDataType::UINT32:
        return "UINT32";

      case PointDataType::FLOAT32:
        return "FLOAT32";

      case PointDataType::FLOAT64:
        return "FLOAT64";

      case PointDataType::UNDEFINED:
        return "UNDEFINED";

      default:
        return "UNDEFINED";
    }
  }

  PointDataType point_data_type_enum_from_string(const std::string& name)
  {
    if(name == "INT8")
    {
      return PointDataType::INT8;
    }
    else if(name == "UINT8")
    {
      return PointDataType::UINT8;
    }
    else if(name == "INT16")
    {
      return PointDataType::INT16;
    }
    else if(name == "UINT16")
    {
      return PointDataType::UINT16;
    }
    else if(name == "INT32")
    {
      return PointDataType::INT32;
    }
    else if(name == "UINT32")
    {
      return PointDataType::UINT32;
    }
    else if(name == "FLOAT32")
    {
      return PointDataType::FLOAT32;
    }
    else if(name == "FLOAT64")
    {
      return PointDataType::FLOAT64;
    }
    else
    {
      return PointDataType::UNDEFINED;
    }
  }

  uint16_t point_data_type_enum_to_data_bytes(const PointDataType& type)
  {
    if(type == PointDataType::INT8 || type == PointDataType::UINT8)
    {
      return 1;
    }
    else if(type == PointDataType::INT16 || type == PointDataType::UINT16)
    {
      return 2;
    }
    else if(type == PointDataType::INT32 || type == PointDataType::UINT32 || type == PointDataType::FLOAT32)
    {
      return 4;
    }
    else if(type == PointDataType::FLOAT64)
    {
      return 8;
    }
    else
    {
      return 0;
    }
  }

  PointDataType point_data_type_enum_from_index(const uint16_t& index)
  {
    switch(index)
    {
      case 0:
        return PointDataType::UNDEFINED;

      case 1:
        return PointDataType::INT8;

      case 2:
        return PointDataType::UINT8;

      case 3:
        return PointDataType::INT16;

      case 4:
        return PointDataType::UINT16;

      case 5:
        return PointDataType::INT32;

      case 6:
        return PointDataType::UINT32;

      case 7:
        return PointDataType::FLOAT32;

      case 8:
        return PointDataType::FLOAT64;

      default:
        return PointDataType::UNDEFINED;
    }
  }
}
