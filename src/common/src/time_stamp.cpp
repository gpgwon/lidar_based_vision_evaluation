/////////////////////////////////////////////////////////////////
/// @attention Copyright © Phantom AI
/// @attention Phantom AI Inc.
/// @attention All rights reserved
/// @attention PROPRIETARY AND CONFIDENTIAL
/// @attention Please refer to COPYRIGHT.txt for complete terms of copyright notice.
/////////////////////////////////////////////////////////////////

#include <common/time_stamp.h>

#include <sys/time.h>

#include <iomanip>
#include <sstream>
#include <limits>


TimeStamp::TimeStamp(double time_sec):
  epoch_sec_(time_sec)
{
}

TimeStamp::TimeStamp(int64_t s, int64_t ns) :
  TimeStamp(static_cast<double>(s) + (static_cast<double>(ns) / static_cast<double>(kNanosecondsInSecond)))
{
}

bool operator>(const TimeStamp& a, const TimeStamp& b)
{
  return !(a <= b);
}

bool operator>=(const TimeStamp& a, const TimeStamp& b)
{
  return !(a < b);
}

bool operator<(const TimeStamp& a, const TimeStamp& b)
{
  return a.epoch_sec_ < b.epoch_sec_;
}

bool operator<=(const TimeStamp& a, const TimeStamp& b)
{
  return a.epoch_sec_ <= b.epoch_sec_;
}

std::ostream& operator<<(std::ostream& os, const TimeStamp& t)
{
  os << "(" << t.epoch_sec_ << ")";
  return os;
}

TimeStamp TimeStamp::operator+(const TimeStamp& rhs) const
{
  return TimeStamp(epoch_sec_ + rhs.epoch_sec_);
}

TimeStamp TimeStamp::operator-(const TimeStamp& rhs) const
{
  return TimeStamp(epoch_sec_ - rhs.epoch_sec_);
}

TimeStamp& TimeStamp::operator+=(const TimeStamp& rhs)
{
  epoch_sec_ += rhs.epoch_sec_;
  return *this;
}

TimeStamp& TimeStamp::operator-=(const TimeStamp& rhs)
{
  epoch_sec_ -= rhs.epoch_sec_;
  return *this;
}

bool TimeStamp::operator==(const TimeStamp& rhs) const
{
  return (epoch_sec_ == rhs.epoch_sec_);
}

bool TimeStamp::operator!=(const TimeStamp& rhs) const
{
  return !(*this == rhs);
}

double TimeStamp::toSec() const
{
  return epoch_sec_;
}

uint64_t TimeStamp::toUSec() const
{
  return epoch_sec_ * kMicrosecondsInSecond;
}

bool TimeStamp::isZero() const
{
  return epoch_sec_ == 0.0;
}

TimeStamp TimeStamp::Now()
{  
  return TimeStamp::WallClockNow();
}

TimeStamp TimeStamp::WallClockNow()
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return TimeStamp(tv.tv_sec, tv.tv_usec * kNanosecondsInMicroSecond);
}


TimeStamp TimeStamp::End()
{
  return TimeStamp(std::numeric_limits<double>::max());
}

TimeStamp TimeStamp::FromUsec(uint64_t t)
{
  return TimeStamp(t * kMicrosecondsInSecond);
}

std::string TimeStamp::toUtcString() const
{
  std::stringstream ss;
  struct tm time;

  time_t time_secs = epoch_sec_;

  if (localtime_r(&time_secs, &time) != nullptr)
  {
    ss << std::put_time(&time, "%Y-%m-%d-%H-%M-%S");
  }

  return ss.str();
}

std::string TimeStamp::toIso8601String() const
{
  std::stringstream ss;
  struct tm time;

  time_t time_secs = epoch_sec_;

  if (localtime_r(&time_secs, &time) != nullptr)
  {
    ss << std::put_time(&time, "%FT%T%z");
  }

  return ss.str();
}

std::string TimeStamp::toLocaleString() const
{
  std::stringstream ss;
  struct tm time;

  time_t time_secs = epoch_sec_;

  if (localtime_r(&time_secs, &time) != nullptr) {
    ss << std::put_time(&time, "%a %b %d %I:%M:%S %p %Y");
  }

  return ss.str();
}
