cmake_minimum_required(VERSION 2.8.3)
project(common)

find_package(PkgConfig REQUIRED)
pkg_check_modules(YAML_CPP REQUIRED yaml-cpp>=0.5.2)

find_package(Boost REQUIRED COMPONENTS)

find_package(PCL 1.7 REQUIRED COMPONENTS)
add_definitions(${PCL_DEFINITIONS})

find_package(Eigen3 REQUIRED NO_MODULE)
add_definitions(${EIGEN3_DEFINITIONS})

find_package( OpenCV REQUIRED )

##########################################################
include_directories(
    include
    ${YAML_CPP_INCLUDEDIR}
    ${Boost_INCLUDE_DIRS}
    ${PCL_INCLUDE_DIRS}
    ${EIGEN3_INCLUDE_DIR}
    ${OpenCV_INCLUDE_DIRS}
)
##########################################################
add_library(common
    src/cloud_point_types.cpp
    src/point_cloud.cpp
    src/time_stamp.cpp    
    src/geometry.cpp
    )

target_link_libraries(common
    ${PCL_LIBRARIES}
    ${OpenCV_LIBS}
    )

# ##########################################################

## Expose variables
set(COMMON_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/include CACHE INTERNAL "" FORCE)
set(COMMON_LIBRARIES common CACHE INTERNAL "" FORCE)

