#include <lane_extraction/lane_extraction.h>
#include <common/file_system.h>
#include <common/point_cloud_io.h>
#include <common/time_stamp.h>

// ROS headers
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>

// std headers
#include <iostream>
#include <string>
#include <vector>
#include <stdint.h>
#include <fstream>
#include <exception>
#include <sstream>
#include <mutex>

// Message headers
#include <lane_extraction/NetworkFrame.h>
#include <lane_extraction/PoseMeasurement.h>
#include <sensor_msgs/Image.h>

// 3rd party headers
#include <boost/filesystem.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/tokenizer.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/posix_time/posix_time_io.hpp>
#include <boost/thread.hpp>
#include <boost/asio.hpp>

struct DatasetNameAndTimestampList
{
  std::string dataset_name_;
  std::vector<double> timestamp_list_;
};


// Constants
const uint32_t kTimeDifferenceBetweenGPSAndUTCTimes = 315964800;
const uint32_t kVeryOldGPSINSMeasurementSkipTime = 20;
const uint32_t kOldGPSINSMeasurementSkipTime = 10;

// Global variables
bool is_gps_utc_time_offset_set(false);
int gps_utc_time_offset_sec;

YAML::Node yaml_node;

uint32_t cnt_processed_timestamps(0);
std::mutex mtx_cnt;

std::ofstream log_file;

std::vector<bool> threads_terminated;

typedef boost::tokenizer<boost::escaped_list_separator<char>> Tokenizer;

// Shared pointer to ROS Nodehandle.
std::shared_ptr<ros::NodeHandle> ros_node_handler_ptr{nullptr};

// Shared pointer to a ROS publisher to display a debug cloud.
std::shared_ptr<ros::Publisher> pub_debug_cloud_full{nullptr};

// Shared pointer to a ROS publisher to display lanes.
std::shared_ptr<ros::Publisher> pub_debug_lanes{nullptr};

// Function to convert the OXTS data to SimpleGPSINSMeasurement format.
bool convertOXTSCSVLineToSimpleGPSINSMeasurement(const std::vector<std::string>& str_vec, SimpleGPSINSMeasurement& simple_gpsins);

// Function to publish the visualization topics for debugging.
void publishDebugInfo(const PointCloudd& cloud_full, const std::vector<PointCloudd>& cloud_lanes);

// Function to load the configuration file.
void loadConfigFile(LaneExtractionParams& params);

// Function to list up the bag files by order.
void listUpBagFiles(LaneExtractionParams& params, std::vector<std::string>& bag_file_list);

// Function to list up the PointCloud files by order.
bool listUpPointCloudFiles(const LaneExtractionParams& params, std::vector<std::string>& pc_file_list,
  double& dataset_start_timestamp, double& dataset_end_timestamp);

bool listUpPointCloudFilesBatchProcess(const std::string& dataset_path, std::vector<std::string>& pc_file_list,
  double& dataset_start_timestamp, double& dataset_end_timestamp);

// Read the timestamp file.
void readTimeStampFile(const LaneExtractionParams& params, const double& dataset_start_timestamp,
  const double& dataset_end_timestamp, std::vector<double>& timestamps, std::vector<uint64_t>& timestamp_uint);

// Read OXTS files.
bool readOXTSFiles(const LaneExtractionParams& params, const double& dataset_start_timestamp, const double& dataset_end_timestamp,
  std::string& gpsins_file_name);

bool searchUnprocessedNextDataset(const LaneExtractionParams& params, std::string& next_dataset_name);

bool ProcessOneDataset(const LaneExtractionParams& params);

bool ProcessOneDatasetBatchMode(const LaneExtractionParams& params, const std::string& dataset_name, const int& thread_number);

void batchProcessThread(const LaneExtractionParams& params, const int& thread_number);

//void bachProcessThreadFunction(const LaneExtractionParams& params);

//void processOneDataset(const LaneExtractionParams& params, )

void listUpDatasetsAndFindNumOfTimestamps(const LaneExtractionParams& params, std::vector<DatasetNameAndTimestampList>& list);

bool moveToStartOfLine(std::ifstream& fs);

std::string getLastLineInFile(std::ifstream& fs);

void skipNLinesOfTextFile(std::ifstream& fin, const size_t& n);

double getGPSINSTimestampFromLineString(const std::string line_str);

double extractTimeStampFromPointCloudFilePath(const std::string& file_path);


std::vector<uint64_t> inlier_timestamps;
std::vector<uint64_t> outlier_timestamps;


int main(int argc, char* argv[])
{
  double start_time = TimeStamp::Now().toSec();
  double check_time = start_time;

  std::cout << std::fixed;
  std::cout.precision(6);

  // Read the config. file.
  LaneExtractionParams params;
  loadConfigFile(params);

  // Set the ROS environment to publish viz topics.
  if(params.publish_viz_topics_)
  {
    // Initialize the ROS node.
    boost::posix_time::ptime timeUTC = boost::posix_time::second_clock::universal_time();
    ros::Time ros_time = ros::Time::fromBoost(timeUTC);
    ros::init(argc, argv, "lane_extraction_" + std::to_string(ros_time.toNSec()));

    ros_node_handler_ptr = std::make_shared<ros::NodeHandle>();

    // Publishers to display debug info.
    pub_debug_cloud_full = std::make_shared<ros::Publisher>(ros_node_handler_ptr->advertise<sensor_msgs::PointCloud2>("debug_cloud_full", 5));
    pub_debug_lanes = std::make_shared<ros::Publisher>(ros_node_handler_ptr->advertise<sensor_msgs::PointCloud2>("debug_lanes", 5));
  }

//  std::vector<DatasetNameAndTimestampList> list__;
//  listUpDatasetsAndFindNumOfTimestamps(params, list__);

//  return 0;

  // Create a log file.
  std::string log_file_name;
  std::string host_name = boost::asio::ip::host_name();

  boost::posix_time::ptime ptime = boost::posix_time::second_clock::local_time();
  std::string date_time = to_simple_string(ptime.date()) + "-" + to_simple_string(ptime.time_of_day());

  log_file_name = "[LOG][lidar_based_lane_extraction][" + host_name + "][" + date_time + "].txt";
  std::string log_file_path = params.log_file_folder_ + "/" + log_file_name;

  log_file.open(log_file_path);

  // Open threads.
  std::vector<boost::thread> threads;
  if(params.batch_process_ == true)
  {
     threads.resize(params.num_of_threads_);
     threads_terminated.resize(params.num_of_threads_, false);
     for(size_t i = 0; i < params.num_of_threads_; ++i)
     {
       threads[i] = boost::thread(batchProcessThread, params, i);
       std::this_thread::sleep_for (std::chrono::seconds(5));
     }


    while(true)
    {
      if(params.publish_viz_topics_)
      {
        if(!ros::ok())
        {
          break;
        }
      }

      bool all_thread_terminated(true);
      for(size_t i = 0; i < params.num_of_threads_; ++i)
      {
        all_thread_terminated &= threads_terminated[i];
      }

      if(all_thread_terminated)
      {
        break;
      }

      double current_time = TimeStamp::Now().toSec();
      double elapsed_time = current_time - check_time;

      if(elapsed_time >= 60.0)
      {
        double elapsed_time_from_start = TimeStamp::Now().toSec() - start_time;
        printf("%d timestamps processed / %f minutes\n", cnt_processed_timestamps, elapsed_time_from_start / 60.0);
        log_file << cnt_processed_timestamps << " timestamps processed / " << elapsed_time_from_start / 60.0 << " hours" << std::endl;
        check_time = current_time;
      }

      std::this_thread::sleep_for (std::chrono::seconds(1));
    }

    std::cout << "Total number of processed timestamps: " << cnt_processed_timestamps << std::endl;
    std::cout << "Elapsed time = " << TimeStamp::Now().toSec() - start_time << std::endl;

    log_file << "Total number of processed timestamps: " << cnt_processed_timestamps << std::endl;
    log_file << "Elapsed time = " << TimeStamp::Now().toSec() - start_time << std::endl;

    std::ofstream fout_inlier("inlier_timestamps.txt");
    std::ofstream fout_outlier("outlier_timestamps.txt");

    std::cout << "num timestamps: " << inlier_timestamps.size() << ", " << outlier_timestamps.size() << std::endl;
    for(const auto& ts : inlier_timestamps)
    {
      fout_inlier << ts << std::endl;
    }
    fout_inlier.close();

    for(const auto& ts : outlier_timestamps)
    {
      fout_outlier << ts << std::endl;
    }
    fout_outlier.close();


  }
  else
  {
    ProcessOneDataset(params);
  }

  std::this_thread::sleep_for (std::chrono::seconds(5));
  return 0;
}


bool convertOXTSCSVLineToSimpleGPSINSMeasurement(const std::vector<std::string>& str_vec, SimpleGPSINSMeasurement& simple_gpsins)
{
//   Check if this measurement is valid.
  if(std::stod(str_vec[2]) == 0 || std::stod(str_vec[3]) == 0)
  {
    return false;
  }

  // Get the timestamp of this measurement.
  uint64_t gps_time = std::stoul(str_vec[0]);
  double gps_ins_timestamp = 1e-9 * (gps_time) + kTimeDifferenceBetweenGPSAndUTCTimes + gps_utc_time_offset_sec;
  simple_gpsins.timestamp_ = gps_ins_timestamp;

  // Calculate the UTM coordinates.
  double calculated_utm_x, calculated_utm_y;
  long utm_zone;
  char utm_hemisphere;  
  Convert_Geodetic_To_UTM(std::stod(str_vec[2]),
                          std::stod(str_vec[3]),
                          &utm_zone,
                          &utm_hemisphere,
                          &calculated_utm_x, &calculated_utm_y);
  simple_gpsins.utm_x_meters_ = calculated_utm_x;
  simple_gpsins.utm_y_meters_ = calculated_utm_y;
  simple_gpsins.utm_z_meters_ = std::stod(str_vec[4]);

  // Position covariance.
  simple_gpsins.position_covariance_ = std::stod(str_vec[9]);

  // Velocities.
//  simple_gpsins.velocity_x_mps_ = std::stod(str_vec[8]);
//  simple_gpsins.velocity_y_mps_ = std::stod(str_vec[7]);
//  simple_gpsins.velocity_z_mps_ = -std::stod(str_vec[9]);

  // Ground-plane speed.
//  simple_gpsins.xy_speed_mps_ = std::sqrt(std::pow(simple_gpsins.velocity_x_mps_, 2) + std::pow(simple_gpsins.velocity_y_mps_, 2));

  // Euler angles.
  simple_gpsins.roll_rad_  = std::stod(str_vec[7]);
  simple_gpsins.pitch_rad_ = -std::stod(str_vec[6]);
  simple_gpsins.yaw_rad_   = -std::stod(str_vec[5]) + HalfPI;//+ std::stod(str_vec[25]))) + HalfPI;
  if(simple_gpsins.yaw_rad_ > TwoPI)
    simple_gpsins.yaw_rad_ -= TwoPI;
  else if(simple_gpsins.yaw_rad_ < 0.0)
    simple_gpsins.yaw_rad_ +=  TwoPI;

  simple_gpsins.gps_status_ = str_vec[8];

  return true;
}


void publishDebugInfo(const PointCloudd& in_cloud_full, const std::vector<PointCloudd>& in_cloud_lanes)
{
  PointCloudd cloud_full = in_cloud_full;
//  for(auto& point : cloud_full.points)
//  {
//    point.y += 40.0f;
//  }

  pcl::PointCloud<pcl::PointXYZI> pcl_cloud;
  pcl_cloud.reserve(cloud_full.getSize());
  for(const auto& point : cloud_full.points_)
  {
    pcl::PointXYZI pcl_point;
    pcl_point.x = static_cast<float>(point.x);
    pcl_point.y = static_cast<float>(point.y);
    pcl_point.z = static_cast<float>(point.z);
    pcl_point.intensity = static_cast<float>(point.intensity);

    pcl_cloud.push_back(pcl_point);
  }
  sensor_msgs::PointCloud2 cloud_full_msg;  
  pcl::toROSMsg(pcl_cloud, cloud_full_msg);

  cloud_full_msg.header.frame_id = "map";
  cloud_full_msg.header.stamp = ros::Time::now();

  pub_debug_cloud_full->publish(cloud_full_msg);


  std::vector<PointCloudd> clustered_cloud_lanes = in_cloud_lanes;
  pcl_cloud.clear();

//  for(const auto cluster : clustered_cloud_lanes)
//  {
//    std::cout << cluster.points[0].x << ", " << cluster.points.back().x << ", "
//              << cluster.points[0].y << ", " << cluster.points.back().y << std::endl;
//  }

  int i = 0;
  for(const auto& lane_points : clustered_cloud_lanes)
  {
    double inten = 255.0f * static_cast<double>(i) / static_cast<double>(clustered_cloud_lanes.size());
    for(const auto& point : lane_points.points_)
    {
      pcl::PointXYZI pcl_point;
      pcl_point.x = static_cast<float>(point.x);
      pcl_point.y = static_cast<float>(point.y);
      pcl_point.z = static_cast<float>(point.z);
      pcl_point.intensity = inten;

      pcl_cloud.push_back(pcl_point);
    }
    ++i;
  }
  sensor_msgs::PointCloud2 cloud_lane_msg;
  pcl::toROSMsg(pcl_cloud, cloud_lane_msg);

  cloud_lane_msg.header.frame_id = "map";
  cloud_lane_msg.header.stamp = ros::Time::now();

  pub_debug_lanes->publish(cloud_lane_msg);
}

void loadConfigFile(LaneExtractionParams& params)
{  
  try
  {
    yaml_node = YAML::LoadFile(std::string(LIDAR_BASED_VISION_EVALUATION_PARAM_DIR)
                               + "/lane_extraction_map_building_first/lane_extraction_map_building_first_params.yaml");
  }
  catch (const YAML::Exception& e)
  {
    std::cout << "The yaml file is not found." << std::endl;
  }

  params.pointcloud_folder_ = yaml_node["pointcloud_folder"].as<std::string>();
  params.pc_file_process_time_range_ = yaml_node["pc_file_process_time_range"].as<std::vector<double>>();
  params.gps_ins_folder_ = yaml_node["gps_ins_folder"].as<std::string>();
  params.output_folder_ = yaml_node["output_folder"].as<std::string>();
  params.use_timestamp_file_ = yaml_node["use_timestamp_file"].as<bool>();
  params.timestamp_file_path_ = yaml_node["timestamp_file_path"].as<std::string>();
  params.save_result_image_ = yaml_node["save_result_image"].as<bool>();
  params.publish_viz_topics_ = yaml_node["publish_viz_topics"].as<bool>();
  params.gps_ins_measurement_frequency_hz_ = yaml_node["gps_ins_measurement_frequency_hz"].as<uint32_t>();
  params.camera_measurement_frequency_hz_ = yaml_node["camera_measurement_frequency_hz"].as<uint32_t>();
  params.lidar_measurement_frequency_hz_ = yaml_node["lidar_measurement_frequency_hz"].as<uint32_t>();
  params.lidar_data_max_accumulation_time_sec_ = yaml_node["lidar_data_max_accumulation_time_sec"].as<uint32_t>();
  params.size_local_grid_x_meter_ = yaml_node["size_local_grid_x_meter"].as<uint32_t>();
  params.size_local_grid_y_meter_ = yaml_node["size_local_grid_y_meter"].as<uint32_t>();
  params.num_local_grid_cell_per_meter_ = yaml_node["num_local_grid_cell_per_meter"].as<uint32_t>();
  params.size_clustering_grid_x_meter_ = yaml_node["size_clustering_grid_x_meter"].as<uint32_t>();
  params.size_clustering_grid_y_meter_ = yaml_node["size_clustering_grid_y_meter"].as<uint32_t>();
  params.num_clustering_grid_cell_per_meter_x_ = yaml_node["num_clustering_grid_cell_per_meter_x"].as<uint32_t>();
  params.num_clustering_grid_cell_per_meter_y_ = yaml_node["num_clustering_grid_cell_per_meter_y"].as<uint32_t>();
  params.min_num_points_in_clustering_cell_threshold_ = yaml_node["min_num_points_in_clustering_cell_threshold"].as<uint32_t>();
  params.min_num_points_in_cluster_threshold_ = yaml_node["min_num_points_in_cluster_threshold"].as<uint32_t>();
  params.cluster_connection_check_maximum_distance_ = yaml_node["cluster_connection_check_maximum_distance"].as<double>();
  params.pcd_process_ = yaml_node["pcd_process"].as<int>();
  params.pcd_process_timestamp_ = yaml_node["pcd_process_timestamp"].as<double>();
  params.pointcloud_parent_folder_ = yaml_node["pointcloud_parent_folder"].as<std::string>();
  params.output_parent_folder_ = yaml_node["output_parent_folder"].as<std::string>();
  params.batch_process_ = false;
  params.num_of_threads_ = yaml_node["num_of_threads"].as<uint32_t>();
  params.log_file_folder_ = yaml_node["log_file_folder"].as<std::string>();
  params.batch_process_ = yaml_node["batch_process"].as<bool>();
  params.intensity_threshold_ = yaml_node["intensity_threshold"].as<double>();
  params.draw_polynomial_ = yaml_node["draw_polynomial"].as<bool>();
  params.factor_for_avg_inten_thld_ = yaml_node["factor_for_avg_inten_thld"].as<double>();
  params.x_bound_to_consider_as_lane_point_ = yaml_node["x_bound_to_consider_as_lane_point"].as<double>();
  params.y_bound_to_consider_as_lane_point_ = yaml_node["y_bound_to_consider_as_lane_point"].as<double>();
  params.dist_thld_to_accept_as_lane_point_ = yaml_node["dist_thld_to_accept_as_lane_point"].as<double>();
  params.dx_to_search_along_polynomial_ = yaml_node["dx_to_search_along_polynomial"].as<double>();
  params.image_delay_ = yaml_node["image_delay"].as<double>();
  params.display_debug_image_ = yaml_node["display_debug_image"].as<bool>();
}

bool listUpPointCloudFiles(const LaneExtractionParams& params, std::vector<std::string>& pc_file_list,
  double& dataset_start_timestamp, double& dataset_end_timestamp)
{
  pc_file_list.clear();
  std::vector<std::string> temp_list;
  if(!getFileListInTargetFolder(params.pointcloud_folder_, "pc", temp_list))
  {
    return false;
  }

  if(params.pc_file_process_time_range_[0] == 0 && params.pc_file_process_time_range_[1] == 0)
  {
    pc_file_list = temp_list;
  }
  else
  {
    for(const auto& file : temp_list)
    {
      std::string file_wo_extension = removeFileExtention(file);
      std::string file_name_only = extractLastBranchNameOfPath(file_wo_extension);
      double file_time = convertTimeStringToDouble(file_name_only);

      if(file_time >= params.pc_file_process_time_range_[0] && file_time <= params.pc_file_process_time_range_[1])
      {
        pc_file_list.push_back(file);
      }
    }
  }

  std::string file_wo_extension = removeFileExtention(pc_file_list[0]);
  std::string file_name_only = extractLastBranchNameOfPath(file_wo_extension);
  dataset_start_timestamp = convertTimeStringToDouble(file_name_only);

  file_wo_extension = removeFileExtention(pc_file_list.back());
  file_name_only = extractLastBranchNameOfPath(file_wo_extension);
  dataset_end_timestamp = convertTimeStringToDouble(file_name_only) + 0.1;

  return true;
}

bool listUpPointCloudFilesBatchProcess(const std::string& dataset_path, std::vector<std::string>& pc_file_list,
  double& dataset_start_timestamp, double& dataset_end_timestamp)
{
  pc_file_list.clear();
  std::vector<std::string> temp_list;
  if(!getFileListInTargetFolder(dataset_path, "pc", temp_list))
  {
    return false;
  }

  pc_file_list = temp_list;

  std::string file_wo_extension = removeFileExtention(pc_file_list[0]);
  std::string file_name_only = extractLastBranchNameOfPath(file_wo_extension);
  dataset_start_timestamp = convertTimeStringToDouble(file_name_only);

  file_wo_extension = removeFileExtention(pc_file_list.back());
  file_name_only = extractLastBranchNameOfPath(file_wo_extension);
  dataset_end_timestamp = convertTimeStringToDouble(file_name_only) + 0.1;

  return true;
}

void readTimeStampFile(const LaneExtractionParams& params, std::vector<double>& timestamps)
{
  std::ifstream fin(params.timestamp_file_path_, std::ios::in);

  if(fin.is_open() == false)
  {
    std::cout << "Attempted to read the timestamp file, but the file doesn't exist." << std::endl;
    log_file << "Attempted to read the timestamp file, but the file doesn't exist." << std::endl;
    return;
  }

  uint64_t timestamp;
  while(fin >> timestamp)
  {
    double timestamp_double = 1E-3 * static_cast<double>(timestamp);
    timestamps.push_back(timestamp_double);
  }
  fin.close();
}

void readTimeStampFile(const LaneExtractionParams& params, const double& dataset_start_timestamp,
  const double& dataset_end_timestamp, std::vector<double>& timestamps, std::vector<uint64_t>& timestamp_uint)
{
  std::ifstream fin(params.timestamp_file_path_, std::ios::in);

  if(fin.is_open() == false)
  {
    std::cout << "Attempted to read the timestamp file, but the file doesn't exist." << std::endl;
    log_file << "Attempted to read the timestamp file, but the file doesn't exist." << std::endl;
    return;
  }

  uint64_t timestamp;
  while(fin >> timestamp)
  {
    double timestamp_double = 1E-3 * static_cast<double>(timestamp);
    if(timestamp_double >= dataset_start_timestamp && timestamp_double <= dataset_end_timestamp)
    {      
      timestamps.push_back(timestamp_double);
      timestamp_uint.push_back(timestamp);
    }
    else if(timestamp_double > dataset_end_timestamp)
    {
      break;
    }
  }
  fin.close();

//  if(timestamps.size() < 1)
//  {
//    std::cout << "No timestamp exist in the timestamp file." << std::endl;
//    log_file << "No timestamp exist in the timestamp file." << std::endl;
//  }

//  std::sort(timestamps.begin(), timestamps.end());
}

bool readOXTSFiles(const LaneExtractionParams& params, const double& dataset_start_timestamp, const double& dataset_end_timestamp,
  std::string& gpsins_file_name)
{
  /* Read all csv files and check the timestamp.
   * Find the gps/ins file corresponding to the dataset.
   */

  boost::filesystem::path targetDir(params.gps_ins_folder_);
  boost::filesystem::directory_iterator it(targetDir), eod;
  BOOST_FOREACH(boost::filesystem::path const &p, std::make_pair(it, eod))
  {
    double gps_ins_start_time_sec, gps_ins_end_time_sec;

    // Get the paht of the file.
    std::string file_full_path = it->path().string();

    // Skip non-csv files.
    std::string file_extention = file_full_path.substr(file_full_path.size() - 3, 3);
    if(file_extention.compare("csv") != 0)
    {
      continue;
    }

    // Open the file
    std::ifstream fin(file_full_path);
    std::string line_str;

    // Skip the first row which include column names.
    std::getline(fin, line_str);

    // Get the start timestamp of the GPS/INS file.
    std::getline(fin, line_str);
    Tokenizer tok(line_str);
    std::vector<std::string> str_vec;
    str_vec.assign(tok.begin(), tok.end());

    if(str_vec.size() < 8) // Skip invalid GPS/INS file.
    {
      continue;
    }

    if(!is_gps_utc_time_offset_set)
    {
      gps_utc_time_offset_sec = std::stoul(str_vec[1]); // GPS to UTC time offset. This is provided by OXTS device.
      is_gps_utc_time_offset_set = true;
    }
    uint64_t gps_time = std::stoul(str_vec[0]);
    gps_ins_start_time_sec = 1e-9 * gps_time + kTimeDifferenceBetweenGPSAndUTCTimes + gps_utc_time_offset_sec;

    // Get the end timestamp of the GPS/INS file.
    line_str = getLastLineInFile(fin);    
    gps_ins_end_time_sec = getGPSINSTimestampFromLineString(line_str);

    fin.close();

    if(gps_ins_start_time_sec < dataset_start_timestamp && gps_ins_end_time_sec > dataset_end_timestamp)
    {      
      gpsins_file_name = file_full_path;
      return true;
    }
  }

  return false;
}

bool moveToStartOfLine(std::ifstream& fs)
{
  fs.seekg(-1, std::ios_base::cur);
  for(long i = fs.tellg(); i > 0; i--)
  {
    if(fs.peek() == '\n')
    {
      fs.get();
      return true;
    }
    fs.seekg(i, std::ios_base::beg);
  }
  return false;
}

std::string getLastLineInFile(std::ifstream& fs)
{
  // Go to the last character before EOF
  fs.seekg(-1, std::ios_base::end);
  if (!moveToStartOfLine(fs))
    return "";

  std::string lastline = "";
  getline(fs, lastline);
  return lastline;
}

void skipNLinesOfTextFile(std::ifstream& fin, const size_t& n)
{
  size_t i = 0;
  while(i++ < n)
  {
    fin.ignore(400, '/n');
  }
}

double getGPSINSTimestampFromLineString(const std::string line_str)
{
  Tokenizer tok(line_str);
  std::vector<std::string> str_vec;
  str_vec.assign(tok.begin(), tok.end());

  uint64_t gps_time = std::stoul(str_vec[0]);
  return 1e-9 * gps_time + kTimeDifferenceBetweenGPSAndUTCTimes + gps_utc_time_offset_sec;
}

double extractTimeStampFromPointCloudFilePath(const std::string& file_path)
{
  std::string file_wo_extension = removeFileExtention(file_path);
  std::string file_name_only = extractLastBranchNameOfPath(file_wo_extension);
  return convertTimeStringToDouble(file_name_only);
}

bool searchUnprocessedNextDataset(const LaneExtractionParams& params, std::string& next_dataset_name)
{
  std::vector<std::string> dataset_folder_list;
  getChildFolderList(params.pointcloud_parent_folder_, dataset_folder_list);

  for(const auto& dataset_folder : dataset_folder_list)
  {
    std::string dataset_name = extractLastBranchNameOfPath(dataset_folder);
    size_t first_dot = dataset_name.find_first_of(".");
    if(first_dot == 0)
    {
      continue;
    }

    std::string output_folder = params.output_parent_folder_ + "/" + dataset_name;
    boost::filesystem::path path(output_folder);
    if(!boost::filesystem::exists(path))
    {
      next_dataset_name = dataset_name;
      return true;
    }
  }

  return false;
}

bool ProcessOneDataset(const LaneExtractionParams& params)
{
  std::string dataset_name = extractLastBranchNameOfPath(params.pointcloud_folder_);
  // Create output folder.
  boost::filesystem::path output_path(params.output_folder_ + "/" + dataset_name);
  if(!boost::filesystem::exists(output_path))
  {
    boost::filesystem::create_directories(output_path);
  }


  // List up the pointcloud files.
  std::vector<std::string> pc_file_list;
  double dataset_start_timestamp, dataset_end_timestamp;
  if(!listUpPointCloudFiles(params, pc_file_list, dataset_start_timestamp, dataset_end_timestamp))
  {
    printf("Failed to list up the pointcloud files.\n");
    log_file << "Failed to list up the pointcloud files." << std::endl;
    return false;
  }

  // Read the timestamp file.
  std::vector<double> extraction_timestamp_list;
  std::vector<uint64_t> uint_timestamps;
  bool initial_ts_index_set(false);
  if(params.use_timestamp_file_)
  {
    readTimeStampFile(params, dataset_start_timestamp, dataset_end_timestamp, extraction_timestamp_list, uint_timestamps);

    std::cout << "Number of timestamps: " << extraction_timestamp_list.size() << std::endl;
    if(extraction_timestamp_list.size() == 0)
    {
      printf("No corresponding timestamps.\n");
      log_file << "No corresponding timestamps." << std::endl;
      return false;
    }
  }

  // Find corresponding OXTS file
  std::string gpsins_file_path;
  if(!readOXTSFiles(params, dataset_start_timestamp, dataset_end_timestamp, gpsins_file_path))
  {
    printf("Failed to find corresponding GPS/INS file.\n");
    log_file << "Failed to find corresponding GPS/INS file." << std::endl;
    return false;
  }
  std::cout << "Corresponding GPS/INS file: " << gpsins_file_path << std::endl;

  if(extraction_timestamp_list.size() == 0)
  {
    std::cout << "No timestamp found" << std::endl;    
    return false;
  }

  // Declare a Velodyne128 parser object.
  VelodyneVLS128Parser velodyne_parser;

  // Initialize the Velodyne parser.
  velodyne_parser.initialize();
  velodyne_parser.setPublishFullScan(false);

  // Declare LidarLaneExtraction object.
  LidarLaneExtraction lane_extractor(params, yaml_node);

  // Read the GPS/INS file.
  std::ifstream fin(gpsins_file_path);
  std::string line_str;
  std::getline(fin, line_str); // Skip the first row (names of columns)
  std::getline(fin, line_str);
  double gps_ins_timestamp = getGPSINSTimestampFromLineString(line_str);


  /* Iterate over the timestamp list and extract lanes at each timestamp */
  size_t pointcloud_read_start_index(0);
  for(const auto& extraction_timestamp : extraction_timestamp_list)
  {
    if(params.publish_viz_topics_)
    {
      if(!ros::ok())
      {
        break;
      }
    }
//    std::cout << "Processing timestamp - " << extraction_timestamp << std::endl;

    // Read and push the point clouds and GPS/INS measurements corresponding to the extraction timestamp into the buffer.
    for(size_t i = pointcloud_read_start_index; i < pc_file_list.size(); ++i)
    {
      if(params.publish_viz_topics_)
      {
        if(!ros::ok())
        {
          break;
        }
      }

      double cloud_timestamp = extractTimeStampFromPointCloudFilePath(pc_file_list[i]) + 0.12;

      if(cloud_timestamp >= (extraction_timestamp - params.lidar_data_max_accumulation_time_sec_ / 2)
        && cloud_timestamp <= (extraction_timestamp + params.lidar_data_max_accumulation_time_sec_ / 2))
      {
        // Before push the point cloud, read and push the GPS/INS measurements up to the pointcloud timestamp.
        while(true)
        {
          if(params.publish_viz_topics_)
          {
            if(!ros::ok())
            {
              break;
            }
          }

          // Skip GPS/INS measurements that are too old from current time.
          if(cloud_timestamp - gps_ins_timestamp > kVeryOldGPSINSMeasurementSkipTime)
          {
            // Skip about 10 sec.
            fin.ignore(300000);
            fin.ignore(400, '\n');
            std::getline(fin, line_str);
            gps_ins_timestamp = getGPSINSTimestampFromLineString(line_str);
            continue;
          }
          else if(cloud_timestamp - gps_ins_timestamp > kOldGPSINSMeasurementSkipTime)
          {
            // Skip about 1 sec.
            fin.ignore(30000);
            fin.ignore(400, '\n');
            std::getline(fin, line_str);
            gps_ins_timestamp = getGPSINSTimestampFromLineString(line_str);
            continue;
          }
          else if(gps_ins_timestamp >= cloud_timestamp)
          {
            break;
          }

          // Read a GPS/INS measurement and push it to the buffer.
          std::getline(fin, line_str);
          Tokenizer tok(line_str);
          std::vector<std::string> str_vec;
          str_vec.assign(tok.begin(), tok.end());
          uint64_t gps_time = std::stoul(str_vec[0]);
          gps_ins_timestamp = 1e-9 * (gps_time) + kTimeDifferenceBetweenGPSAndUTCTimes + gps_utc_time_offset_sec;

          SimpleGPSINSMeasurement simple_gpsins;
          convertOXTSCSVLineToSimpleGPSINSMeasurement(str_vec, simple_gpsins);
          lane_extractor.pushNewGPSINS(simple_gpsins);
        }

        // Read the pointcloud.
        PointCloudf input_pc;
        readPointCloudFromFile(pc_file_list[i], input_pc);

        // Push the pontcloud into the buffer.
        lane_extractor.pushNewPointCloud(input_pc);
      }
      else if(cloud_timestamp > (extraction_timestamp + params.lidar_data_max_accumulation_time_sec_ / 2))
      {
        pointcloud_read_start_index = i;
        break;
      }
    }

    lane_extractor.extractLanesAtGivenTimestamp(dataset_name, extraction_timestamp, output_path.string());

    if(params.publish_viz_topics_)
    {
      publishDebugInfo(lane_extractor.debug_cloud_full_, lane_extractor.debug_cloud_lanes_);      
      ros::spinOnce();
    }

    if(params.display_debug_image_)
    {
      cv::waitKey(0);
    }

    mtx_cnt.lock();
    cnt_processed_timestamps++;
    mtx_cnt.unlock();
  }
  return true;
}

bool ProcessOneDatasetBatchMode(const LaneExtractionParams& params, const std::string& dataset_name, const int& thread_number)
{
  // Create output folder.
  boost::filesystem::path output_path(params.output_parent_folder_ + "/" + dataset_name);
  boost::filesystem::create_directories(output_path);

  // List up the pointcloud files.
  std::vector<std::string> pc_file_list;
  double dataset_start_timestamp, dataset_end_timestamp;
  std::string input_dataset_folder = params.pointcloud_parent_folder_ + "/" + dataset_name;
  if(!listUpPointCloudFilesBatchProcess(input_dataset_folder, pc_file_list, dataset_start_timestamp, dataset_end_timestamp))
  {    
    printf("[Thread %d] Failed to list up the pointcloud files.\n", thread_number);
    log_file << "[Thread " << thread_number << "] Failed to list up the pointcloud files." << std::endl;
    return false;
  }

  // Read the timestamp file.
  std::vector<double> extraction_timestamp_list;
  std::vector<uint64_t> uint_timestamps;
  bool initial_ts_index_set(false);
  if(params.use_timestamp_file_)
  {
    readTimeStampFile(params, dataset_start_timestamp, dataset_end_timestamp, extraction_timestamp_list, uint_timestamps);
    if(extraction_timestamp_list.size() == 0)
    {
      printf("[Thread %d] No corresponding timestamps.\n", thread_number);
      log_file << "[Thread " << thread_number << "] No corresponding timestamps." << std::endl;
      return false;
    }
  }

  // Find corresponding OXTS file
  std::string gpsins_file_path;
  if(!readOXTSFiles(params, dataset_start_timestamp, dataset_end_timestamp, gpsins_file_path))
  {
    printf("[Thread %d] Failed to find corresponding GPS/INS file.\n", thread_number);
    log_file << "[Thread " << thread_number << "] Failed to find corresponding GPS/INS file." << std::endl;    
    outlier_timestamps.insert(outlier_timestamps.end(), uint_timestamps.begin(), uint_timestamps.end());
    return false;
  }
  std::cout << "Corresponding GPS/INS file: " << gpsins_file_path << std::endl;

  inlier_timestamps.insert(inlier_timestamps.end(), uint_timestamps.begin(), uint_timestamps.end());
  return false;

  if(extraction_timestamp_list.size() == 0)
  {
    std::cout << "No timestamp found" << std::endl;    
    return false;
  }  

  // Declare a Velodyne128 parser object.
  VelodyneVLS128Parser velodyne_parser;

  // Initialize the Velodyne parser.
  velodyne_parser.initialize();
  velodyne_parser.setPublishFullScan(false);

  // Declare LidarLaneExtraction object.
  LidarLaneExtraction lane_extractor(params, yaml_node);

  // Read the GPS/INS file.
  std::ifstream fin(gpsins_file_path);
  std::string line_str;
  std::getline(fin, line_str); // Skip the first row (names of columns)
  std::getline(fin, line_str);
  double gps_ins_timestamp = getGPSINSTimestampFromLineString(line_str);


  /* Iterate over the timestamp list and extract lanes at each timestamp */
  size_t pointcloud_read_start_index(0);
  for(const auto& extraction_timestamp : extraction_timestamp_list)
  {
    if(params.publish_viz_topics_)
    {
      if(!ros::ok())
      {
        break;
      }
    }
//    std::cout << "Processing timestamp - " << extraction_timestamp << std::endl;

    // Read and push the point clouds and GPS/INS measurements corresponding to the extraction timestamp into the buffer.
    for(size_t i = pointcloud_read_start_index; i < pc_file_list.size(); ++i)
    {
      if(params.publish_viz_topics_)
      {
        if(!ros::ok())
        {
          break;
        }
      }

      double cloud_timestamp = extractTimeStampFromPointCloudFilePath(pc_file_list[i]) + 0.12;

      if(cloud_timestamp >= (extraction_timestamp - params.lidar_data_max_accumulation_time_sec_ / 2)
        && cloud_timestamp <= (extraction_timestamp + params.lidar_data_max_accumulation_time_sec_ / 2))
      {
        // Before push the point cloud, read and push the GPS/INS measurements up to the pointcloud timestamp.
        while(true)
        {
          if(params.publish_viz_topics_)
          {
            if(!ros::ok())
            {
              break;
            }
          }

          // Skip GPS/INS measurements that are too old from current time.
          if(cloud_timestamp - gps_ins_timestamp > kVeryOldGPSINSMeasurementSkipTime)
          {
            // Skip about 10 sec.
            fin.ignore(300000);
            fin.ignore(400, '\n');
            std::getline(fin, line_str);
            gps_ins_timestamp = getGPSINSTimestampFromLineString(line_str);
            continue;
          }
          else if(cloud_timestamp - gps_ins_timestamp > kOldGPSINSMeasurementSkipTime)
          {
            // Skip about 1 sec.
            fin.ignore(30000);
            fin.ignore(400, '\n');
            std::getline(fin, line_str);
            gps_ins_timestamp = getGPSINSTimestampFromLineString(line_str);
            continue;
          }
          else if(gps_ins_timestamp >= cloud_timestamp)
          {
            break;
          }

          // Read a GPS/INS measurement and push it to the buffer.
          std::getline(fin, line_str);
          Tokenizer tok(line_str);
          std::vector<std::string> str_vec;
          str_vec.assign(tok.begin(), tok.end());
          uint64_t gps_time = std::stoul(str_vec[0]);
          gps_ins_timestamp = 1e-9 * (gps_time) + kTimeDifferenceBetweenGPSAndUTCTimes + gps_utc_time_offset_sec;

          SimpleGPSINSMeasurement simple_gpsins;
          convertOXTSCSVLineToSimpleGPSINSMeasurement(str_vec, simple_gpsins);
          lane_extractor.pushNewGPSINS(simple_gpsins);
        }

        // Read the pointcloud.
        PointCloudf input_pc;
        readPointCloudFromFile(pc_file_list[i], input_pc);

        // Push the pontcloud into the buffer.
        lane_extractor.pushNewPointCloud(input_pc);
      }
      else if(cloud_timestamp > (extraction_timestamp + params.lidar_data_max_accumulation_time_sec_ / 2))
      {
        pointcloud_read_start_index = i;
        break;
      }
    }

    lane_extractor.extractLanesAtGivenTimestamp(dataset_name, extraction_timestamp, output_path.string());

    if(params.publish_viz_topics_)
    {
      publishDebugInfo(lane_extractor.debug_cloud_full_, lane_extractor.debug_cloud_lanes_);
      ros::spinOnce();
    }

    if(params.display_debug_image_)
    {
      cv::waitKey(0);
    }

    mtx_cnt.lock();
    cnt_processed_timestamps++;
    mtx_cnt.unlock();
  }

  return true;
}

void batchProcessThread(const LaneExtractionParams& params, const int& thread_number)
{
  while(true)
  {
    if(params.publish_viz_topics_)
    {
      if(!ros::ok())
      {
        break;
      }
    }

    std::string next_dataset_name;
    if(!searchUnprocessedNextDataset(params, next_dataset_name))
    {
      printf("[Thread %d] No more dataset found. Thread is terminated.\n", thread_number);
      log_file << "[Thread " << thread_number << "] No more dataset found. Thread is terminated." << std::endl;
      break;
    }

    printf("[Thread %d] Starts to process dataset [%s]\n", thread_number, next_dataset_name.c_str());
    log_file << "[Thread " << thread_number << "] Starts to process dataset [" << next_dataset_name << "]." << std::endl;
    if(!ProcessOneDatasetBatchMode(params, next_dataset_name, thread_number))
    {
      continue;
    }
  }

  threads_terminated[thread_number] = true;
}


void listUpDatasetsAndFindNumOfTimestamps(const LaneExtractionParams& params, std::vector<DatasetNameAndTimestampList>& list)
{
  std::vector<std::string> dataset_folder_list;
  getChildFolderList(params.pointcloud_parent_folder_, dataset_folder_list);

  // List up regular dataset folders.
  std::vector<std::string> regular_dataset_folder_list;
  for(const auto& dataset_folder : dataset_folder_list)
  {
    std::string dataset_name = extractLastBranchNameOfPath(dataset_folder);
    size_t first_dot = dataset_name.find_first_of(".");
    if(first_dot == 0)
    {
      continue;
    }

    regular_dataset_folder_list.push_back(dataset_folder);
  }

  // Calculate the number of corresponding timestamps.
  for(const auto& dataset_folder : regular_dataset_folder_list)
  {
    // List up the pointcloud files in the dataset folder.
    std::vector<std::string> pc_file_list;

    getFileListInTargetFolder(dataset_folder, pc_file_list);

    double dataset_start_timestamp = extractTimeStampFromPointCloudFilePath(pc_file_list[0]);
    double dataset_end_timestamp = extractTimeStampFromPointCloudFilePath(pc_file_list.back()) + 0.1;

    DatasetNameAndTimestampList dataset_name_ts_list;
    dataset_name_ts_list.dataset_name_ = extractLastBranchNameOfPath(dataset_folder);

    std::vector<double> extraction_timestamp_list;
    std::vector<uint64_t> uint_timestamps;
    bool initial_ts_index_set(false);

    readTimeStampFile(params, dataset_start_timestamp, dataset_end_timestamp, extraction_timestamp_list, uint_timestamps);

    std::cout << dataset_name_ts_list.dataset_name_ << " / "
              << dataset_start_timestamp << ", " << dataset_end_timestamp << " / "
              << extraction_timestamp_list.size() << std::endl;

    dataset_name_ts_list.timestamp_list_ = extraction_timestamp_list;
    list.push_back(dataset_name_ts_list);
  }


}








