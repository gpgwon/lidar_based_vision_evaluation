#include <lane_extraction/lane_extraction.h>
//#include <pcl/io/pcd_io.h>

const uint32_t kImprobableBigClusterIndexNumber = 9999;
const double kTimeDurationPerVLS128FiringCycle = 53.3E-6;
const uint32_t kNumOfFiringCyclePer10Milliseconds = 43 * static_cast<uint32_t>(0.01 / kTimeDurationPerVLS128FiringCycle);
const std::array<uint32_t, 128> vls128_channel_vertical_angle_order =
{36, 69, 54, 87, 0, 97, 18, 115, 44, 77, 62, 95, 8, 105, 26, 123, 100, 21, 118, 39, 64, 49, 82, 3, 108, 29, 126, 47,
 72, 57, 90, 11, 52, 85, 6, 103, 16, 113, 34, 67, 60, 93, 14, 111, 24, 121, 42, 75, 116, 37, 70, 55, 80, 1, 98, 19,
 124, 45, 78, 63, 88, 9, 106, 27, 4, 101, 22, 119, 32, 65, 50, 83, 12, 33, 109, 30, 127, 40, 73, 58, 91, 68, 53, 86,
 7, 96, 17, 114, 35, 76, 61, 94, 15, 104, 25, 122, 43, 20, 117, 38, 71, 48, 81, 2, 99, 28, 125, 46, 79, 56, 89, 10,
  107, 84, 5, 102, 23, 112, 66, 51, 92, 13, 110, 31, 120, 41, 74, 59};


void xyToImageuv(const double& x, const double& y, int& u, int& v);

LidarLaneExtraction::LidarLaneExtraction(const LaneExtractionParams& params, const YAML::Node& yaml_node)
  : pcd_file_saved_(false),
    params_(params),
    yaml_node_(yaml_node)
{
  oxts_buffer_.set_capacity((params_.gps_ins_measurement_frequency_hz_ * params_.lidar_data_max_accumulation_time_sec_));
  pointcloud_buffer_.set_capacity((params_.lidar_measurement_frequency_hz_ * params_.lidar_data_max_accumulation_time_sec_));
  raw_pointcloud_buffer_.set_capacity((params_.lidar_measurement_frequency_hz_ * params_.lidar_data_max_accumulation_time_sec_));
  front_cam_time_stamp_buffer_.set_capacity((params_.camera_measurement_frequency_hz_ * params_.lidar_data_max_accumulation_time_sec_));
  yaw_bias_buff_.set_capacity(100);

  local_grid_size_x_ = params_.size_local_grid_x_meter_ * params_.num_local_grid_cell_per_meter_ + 1;
  local_grid_size_y_ = params_.size_local_grid_y_meter_ * params_.num_local_grid_cell_per_meter_ + 1;

  // Do not print pcl-related warning messages.
//  pcl::console::setVerbosityLevel(pcl::console::L_ALWAYS);
}

LidarLaneExtraction::~LidarLaneExtraction()
{
}

void LidarLaneExtraction::pushNewPointCloud(const PointCloudf& input)
{
  // Pointcloud will be transformed to gloal coordinates.
  // Minimum two GPS/INS measurements are necessary.

  if(oxts_buffer_.size() > 2)
  {
    raw_pointcloud_buffer_.push_front(input);
    pointcloud_buffer_.push_front(transformPointCloudToGlobalCoordinates(input));
  }
}

void LidarLaneExtraction::pushNewGPSINS(const SimpleGPSINSMeasurement& input)
{  
  static Point3d pos_offset;
  static bool pos_offset_set(false);

  SimpleGPSINSMeasurement _input = input;

  if(pos_offset_set == false)
  {
    pos_offset = Point3d(input.utm_x_meters_, input.utm_y_meters_, input.utm_z_meters_);
    pos_offset_set = true;
    return;
  }

  _input.utm_x_meters_ -= pos_offset.x;
  _input.utm_y_meters_ -= pos_offset.y;
  _input.utm_z_meters_ -= pos_offset.z;

  // Calculate the yaw bias.
  double avg_bias(0.0);
  if(oxts_buffer_.size() > 0)
  {
    double estimated_yaw = std::atan2(_input.utm_y_meters_ - oxts_buffer_[0].utm_y_meters_, _input.utm_x_meters_ - oxts_buffer_[0].utm_x_meters_);

    if(estimated_yaw < 0)
    {
      estimated_yaw += 2 * M_PI;
    }

    double yaw_bias = estimated_yaw - _input.yaw_rad_;
    if(yaw_bias > M_PI)
    {
      yaw_bias -= 2 * M_PI;
    }
    else if(yaw_bias < -M_PI)
    {
      yaw_bias += 2 * M_PI;
    }

    yaw_bias_buff_.push_back(yaw_bias);

    for(size_t i = 0; i < yaw_bias_buff_.size(); ++i)
    {
      avg_bias += yaw_bias_buff_[i];
    }
    avg_bias = avg_bias / static_cast<double>(yaw_bias_buff_.size());

  }

  _input.yaw_rad_ += avg_bias;
  oxts_buffer_.push_front(_input);
}

PointCloudd LidarLaneExtraction::transformPointCloudToGlobalCoordinates(const PointCloudf& input_cloud)
{
//   Extract only low channels.
  size_t num_total_points = input_cloud.getSize();
  size_t num_points_per_ch = num_total_points / 128;
  PointCloudf cloud_43ch;
  cloud_43ch.reserve(num_points_per_ch * 43);
  std::vector<uint32_t> origin_index;
  origin_index.reserve(num_points_per_ch * 43);

  for(size_t i = 0; i < num_points_per_ch; ++i)
  {
    for(size_t j = 1; j <= 43; ++j)
    {
      cloud_43ch.push_back(input_cloud.points_[128 * i + vls128_channel_vertical_angle_order[j]]);
      origin_index.push_back(128 * i + vls128_channel_vertical_angle_order[j]);
    }
  }

  // PCL format pointcloud to return.
  PointCloudd ret_cloud;

  // Initialize the return pointcloud.
  size_t num_points = cloud_43ch.getSize();
//  size_t num_points = input_cloud.getSize();
  ret_cloud.reserve(num_points);
  //ret_cloud.header.stamp = static_cast<uint64_t>(1E6 * input_cloud.points_[0].timestamp);

  // Nearest two GPS/INS measurements.
  SimpleGPSINSMeasurement oxts0, oxts1;
  double oxts_time_gap;


  for(size_t j = 0; j < oxts_buffer_.size() - 1; ++j)
  {
    if(cloud_43ch.points_[0].timestamp > oxts_buffer_[j + 1].timestamp_ && cloud_43ch.points_[0].timestamp <= oxts_buffer_[j].timestamp_)
    {
      oxts0 = oxts_buffer_[j];
      oxts1 = oxts_buffer_[j + 1];
      oxts_time_gap = oxts0.timestamp_ - oxts1.timestamp_;
      break;
    }
  }


  // Point-wise local-to-global transformation.
  for(size_t i = 0; i < num_points; ++i)
  {
    double point_time = cloud_43ch.points_[i].timestamp;

    // Every about 10 ms, find adjacent OXTS measurements.
    if(i % kNumOfFiringCyclePer10Milliseconds == 0)
    {      
      for(size_t j = 0; j < oxts_buffer_.size() - 1; ++j)
      {        
        if(point_time > oxts_buffer_[j + 1].timestamp_ && point_time <= oxts_buffer_[j].timestamp_)
        {
          oxts0 = oxts_buffer_[j];
          oxts1 = oxts_buffer_[j + 1];
          oxts_time_gap = oxts0.timestamp_ - oxts1.timestamp_;
          break;
        }
      }
    }

    // GPS/INS at point measurement time.
    SimpleGPSINSMeasurement gpsins_at_point;
    std::array<std::array<double, 3>, 3> rotation;
    std::array<double, 3> translation;

    gpsins_at_point = calculateInterpolatedGPSINSData(oxts1, oxts0, point_time);
    rotation = getRotMatrixArrayFormat(gpsins_at_point.roll_rad_, gpsins_at_point.pitch_rad_, gpsins_at_point.yaw_rad_);

    const double& x = cloud_43ch.points_[i].x;
    const double& y = cloud_43ch.points_[i].y;
    const double& z = cloud_43ch.points_[i].z;

    Point3Id global_point;

    // Ignore the points corresponding to the ego-vehicle body.
    if(x > -2.5 && x < 3.0 && y > -1.0 && y < 1.0)
    {
      global_point.x = 0;
      global_point.y = 0;
      global_point.z = 0;
      global_point.intensity = 255;
    }

    // Ignore distant or non-ground points.
    else if(z <= -5.0 || z >= 5.0 || y <= -20.0 || y >= 20.0 || x <= -120.0 || x >= 120.0)
    {
      global_point.x = 0;
      global_point.y = 0;
      global_point.z = 0;
      global_point.intensity = 255;
    }
    else
    {
      global_point.x = rotation[0][0] * x + rotation[0][1] * y + rotation[0][2] * z + gpsins_at_point.utm_x_meters_;
      global_point.y = rotation[1][0] * x + rotation[1][1] * y + rotation[1][2] * z + gpsins_at_point.utm_y_meters_;
      global_point.z = rotation[2][0] * x + rotation[2][1] * y + rotation[2][2] * z + gpsins_at_point.utm_z_meters_;
      global_point.intensity = cloud_43ch.points_[i].intensity;
    }

    ret_cloud.push_back(global_point);
  }

  return ret_cloud;
}

void LidarLaneExtraction::extractLanesAtGivenTimestamp(const std::string& session_name, const double& timestamp, const std::string& output_folder)
{
  if(params_.batch_process_ == false)
  {
    std::cout << "Extract lanes at " << timestamp << std::endl;
  }

  // Declare some variables used in this function.
  PointCloudd aggregated_cloud;
//  PointCloudd cloud;
  size_t num_points;

  // Get string format of the timestamp.
  std::string str_timestamp = convertDoubleTimestampToString(timestamp);

  // Get a raw point cloud that is closest to the timestamp.
  double min_time_gap(999.0);
  PointCloudf current_raw_pointcloud;
  std::vector<Point3f> current_lidar_points;
  for(const auto& raw_pointcloud : raw_pointcloud_buffer_)
  {
    double time_gap = fabs(raw_pointcloud.points_.back().timestamp - timestamp);
    if(time_gap < min_time_gap)
    {
      current_raw_pointcloud = raw_pointcloud;
      min_time_gap = time_gap;
    }
  }

  PointCloudf copied_current_raw_pointcloud = current_raw_pointcloud;
  current_raw_pointcloud.clear();
  for(const auto& lidar_point : copied_current_raw_pointcloud.points_)
  {
    const float& x = lidar_point.x;
    const float& y = lidar_point.y;
    const float& z = lidar_point.z;

    double dist = std::sqrt(std::pow(x, 2) + std::pow(y, 2));

    if(x > 2.0 && z < 5.0 && dist < 200.0)
    {
      current_raw_pointcloud.push_back(lidar_point);
    }
  }

  // Get GPS/INS measurement at the given timestamp.
  double delay_compensated_timestamp = timestamp - params_.image_delay_;
  SimpleGPSINSMeasurement gpsins_at_cam_timestamp = calculateGPSINSMeasurementAtGivenTimestamp(delay_compensated_timestamp);

  // Aggregate the point clouds.
  aggregated_cloud.clear();
  for(const auto& one_scan : pointcloud_buffer_)
  {
    aggregated_cloud += one_scan;
  }
  num_points = aggregated_cloud.getSize();

  // Extract ground points.
  std::vector<std::vector<LocalGridCell>> local_grid;
  FindGroundRegion(aggregated_cloud, gpsins_at_cam_timestamp, local_grid);

  // Algorithm B
  PointCloudd lane_marking_candidates;
  lane_marking_candidates.clear();
  std::vector<double> vec_avg_adj_points_intensity;

  for(int i = 0; i < num_points; ++i)
  {
    if(aggregated_cloud.points_[i].intensity == 255)
    {
      continue;
    }

    double x = aggregated_cloud.points_[i].x - gpsins_at_cam_timestamp.utm_x_meters_;
    double y = aggregated_cloud.points_[i].y - gpsins_at_cam_timestamp.utm_y_meters_;

    if(x < -150.0 || x > 150.0 || y < -150.0 || y > 150.0)
    {
      continue;
    }

    int idx_x, idx_y;
    xyToLocalGridIndex(x, y, idx_x, idx_y);

    if(local_grid[idx_x][idx_y].is_ground_ != true || fabs(local_grid[idx_x][idx_y].min_z_ - aggregated_cloud.points_[i].z) > 0.1)
    {
      continue;
    }

    bool skip_point(false);
    if(i > 43 * 20 && i < num_points - 43 * 20 - 1)
    {
      double avg_adj_points_intensity(0.0);
      int added_point_cnt(0);
      for(int j = -1; j > -20; --j)
      {
        if(aggregated_cloud.points_[i + 43 * j].intensity == 255)
        {
          continue;
        }

        double xy_distance_bw_two_points
          = aggregated_cloud.points_[i].distance2d(aggregated_cloud.points_[i + 43 * j]);
        if(xy_distance_bw_two_points >= 0.1)
        {
          avg_adj_points_intensity += static_cast<double>(aggregated_cloud.points_[i + 43 * j].intensity);
          added_point_cnt++;
        }
        else if(xy_distance_bw_two_points >= 0.15)
        {
          avg_adj_points_intensity += static_cast<double>(aggregated_cloud.points_[i + 43 * j].intensity);
          added_point_cnt++;
        }
        else if(xy_distance_bw_two_points > 0.2)
        {
          avg_adj_points_intensity += static_cast<double>(aggregated_cloud.points_[i + 43 * j].intensity);
          added_point_cnt++;
          break;
        }
      }

      for(int j = 1; j < 20; ++j)
      {
        if(aggregated_cloud.points_[i + 43 * j].intensity == 255)
        {
          continue;
        }

        double xy_distance_bw_two_points
          = aggregated_cloud.points_[i].distance2d(aggregated_cloud.points_[i + 43 * j]);
        if(xy_distance_bw_two_points >= 0.1)
        {
          avg_adj_points_intensity += static_cast<double>(aggregated_cloud.points_[i + 43 * j].intensity);
          added_point_cnt++;
        }
        else if(xy_distance_bw_two_points >= 0.15)
        {
          avg_adj_points_intensity += static_cast<double>(aggregated_cloud.points_[i + 43 * j].intensity);
          added_point_cnt++;
        }
        else if(xy_distance_bw_two_points > 0.2)
        {
          avg_adj_points_intensity += static_cast<double>(aggregated_cloud.points_[i + 43 * j].intensity);
          added_point_cnt++;
          break;
        }
      }

      if(added_point_cnt > 0)
      {
        avg_adj_points_intensity /= static_cast<double>(added_point_cnt);
      }
      else
      {
        continue;
      }

      if(0.5 * aggregated_cloud.points_[i].intensity > avg_adj_points_intensity
         && aggregated_cloud.points_[i].intensity > 0.0)
      {
        lane_marking_candidates.push_back(aggregated_cloud.points_[i]);
        vec_avg_adj_points_intensity.push_back(avg_adj_points_intensity);
      }
    }
  }


  // Average intensity
  double avg_inten(0.0);
  for(const auto& point : lane_marking_candidates.points_)
  {
    avg_inten += point.intensity;
  }

  if(lane_marking_candidates.getSize() == 0)
  {
    fout_front_lanes_.open(output_folder + "/" + str_timestamp + ".txt", std::ios::out|std::ios::trunc);
    fout_front_lanes_.close();
    cv::Mat image(1200, 2000, CV_8UC3, cv::Scalar(0,0,0));
    cv::imwrite(output_folder + "/" + str_timestamp + ".jpg", image);
    return;
  }
  avg_inten = avg_inten / lane_marking_candidates.getSize();

  double peak_thld = 0.0075 * (avg_inten - 10) + 0.1;
  if(peak_thld < 0.1)
  {
    peak_thld = 0.1;
  }
  else if(peak_thld > 0.4)
  {
    peak_thld = 0.4;
  }

  peak_thld = 0.5;

//  std::cout << "Average intensity / peak threshold: " << avg_inten << ", " << peak_thld << std::endl;

  PointCloudd copied_lane_marking_candidates = lane_marking_candidates;
  lane_marking_candidates.clear();
  for(size_t i = 0; i < copied_lane_marking_candidates.getSize(); ++i)
  {
    if(peak_thld * copied_lane_marking_candidates.points_[i].intensity > vec_avg_adj_points_intensity[i]
      && copied_lane_marking_candidates.points_[i].intensity >= params_.factor_for_avg_inten_thld_ * avg_inten)
    {
      lane_marking_candidates.push_back(copied_lane_marking_candidates.points_[i]);
    }
  }


//  pcl::RadiusOutlierRemoval<Point3Id> outrem;
//  outrem.setInputCloud(PointCloudd::ConstPtr(new PointCloudd(lane_marking_candidates)));
//  outrem.setRadiusSearch(0.2);
//  outrem.setMinNeighborsInRadius (5);
//  outrem.filter (cloud);
//##############################################################################################


  /* Transform to vehicle coordinates at camera timestamp. */
  Eigen::Vector3d translation(
    gpsins_at_cam_timestamp.utm_x_meters_, gpsins_at_cam_timestamp.utm_y_meters_, gpsins_at_cam_timestamp.utm_z_meters_);
  Eigen::Matrix3d rotation = getInverseRotMatrix3d(gpsins_at_cam_timestamp.roll_rad_, gpsins_at_cam_timestamp.pitch_rad_, gpsins_at_cam_timestamp.yaw_rad_);

  num_points = lane_marking_candidates.getSize();
  PointCloudd temp_cloud = lane_marking_candidates;
  lane_marking_candidates.clear();
  lane_marking_candidates.reserve(num_points);
  //#pragma omp parallel for
  for(const auto& point : temp_cloud.points_)
  {
    if(point.intensity > 0)
    {
      Eigen::Vector3d global_point(point.x, point.y, point.z);
      Eigen::Vector3d diff_point = global_point - translation;

      Eigen::Vector3d local_point = rotation * diff_point;
      Point3Id ret_point;
      ret_point.x = local_point[0];
      ret_point.y = local_point[1];
      ret_point.z = local_point[2];
      ret_point.intensity = point.intensity;
      lane_marking_candidates.push_back(ret_point);
    }   
  }


  pcl::PointCloud<pcl::PointXYZI> pcl_cloud = convertPointClouddToPclCloud(lane_marking_candidates);
  pcl::RadiusOutlierRemoval<pcl::PointXYZI> outrem;
  outrem.setInputCloud(pcl::PointCloud<pcl::PointXYZI>::ConstPtr(new pcl::PointCloud<pcl::PointXYZI>(pcl_cloud)));
  outrem.setRadiusSearch(0.2);
  outrem.setMinNeighborsInRadius (3);
  outrem.filter(pcl_cloud);

  lane_marking_candidates = convertPclCloudToPointCloudd(pcl_cloud);

  // Lane segmentation based on image annotation data.
  LidarLaneSegmentationBasedOnAnnotImage lane_seg(yaml_node_);
  PointCloudf fc_lidar_points, rc_lidar_points;
  std::vector<PointCloudf> fc_lane_segments, rc_lane_segments, fc_object_points;

  for(const auto& point : lane_marking_candidates.points_)
  {
    if(point.x >= 2.0)
    {
      fc_lidar_points.emplace_back(point.x, point.y, point.z);
    }
  }

  cv::Mat debug_image;
  lane_seg.doLaneSegmentation(session_name, timestamp, fc_lidar_points, fc_lane_segments,
    rc_lidar_points, rc_lane_segments, current_raw_pointcloud, fc_object_points, debug_image);

  std::vector<PointCloudd> fc_lanes;
  for(const auto& lane_seg : fc_lane_segments)
  {
    PointCloudd lane_cloud;
    for(const auto& lane_point : lane_seg.points_)
    {
      Point3Id point;
      point.x = lane_point.x;
      point.y = lane_point.y;
      point.z = lane_point.z;

      lane_cloud.push_back(point);
    }
    fc_lanes.push_back(lane_cloud);
  }

  for(auto& lane_seg : fc_lanes)
  {
    std::sort(lane_seg.points_.begin(), lane_seg.points_.end(), pointCompAlongXDirection);
  }

//   Find y0 and accept only the left and right lanes.
  std::vector<PointCloudd> copied_fc_lanes = fc_lanes;
  fc_lanes.clear();
  double y0_left = 99.0;
  double y0_right = -99.0;
  PointCloudd left_lane_seg;
  PointCloudd right_lane_seg;
  for(const auto& lane_seg : copied_fc_lanes)
  {
    if(lane_seg.getSize() < 2)
    {
      continue;
    }
    double r = (lane_seg.points_[1].y - lane_seg.points_[0].y) / (lane_seg.points_[1].x - lane_seg.points_[0].x);
    double y0 = -r * lane_seg.points_[0].x + lane_seg.points_[0].y;
    if(y0 >= 0 && y0 < y0_left)
    {
      y0_left = y0;
      left_lane_seg = lane_seg;
    }
    else if(y0 < 0 && y0 > y0_right)
    {
      y0_right = y0;
      right_lane_seg = lane_seg;
    }
  }
  if(y0_left < 4.0)
  {
    fc_lanes.push_back(left_lane_seg);
  }

  if(y0_right > -4.0)
  {
    fc_lanes.push_back(right_lane_seg);
  }

  // Use polygon to extract initial lane marking candidates.
  copied_fc_lanes = fc_lanes;
  fc_lanes.clear();
  for(const auto& lane_seg : copied_fc_lanes)
  {
    PointCloudd new_lane_seg;
    for(size_t i = 0; i < lane_seg.getSize() - 1; ++i)
    {
      PointCloudd extracted_points;
      double x0 = lane_seg.points_[i].x;
      double y0 = lane_seg.points_[i].y;
      double xe = lane_seg.points_[i + 1].x;
      double ye = lane_seg.points_[i + 1].y;

      double r = (ye - y0) / (xe - x0);
      std::vector<double> coeff;
      coeff.push_back(-r * x0 + y0);
      coeff.push_back(r);
      if(i == 0)
      {
        findPointsOnAPolynomial(lane_marking_candidates, coeff, 0.0, xe, 0.1, 0.5, extracted_points);
      }
      else
      {
        findPointsOnAPolynomial(lane_marking_candidates, coeff, x0, xe, 0.1, 0.5, extracted_points);
      }

      new_lane_seg += extracted_points;
    }
    fc_lanes.push_back(new_lane_seg);
  }


  // Extract more lane points.
  std::vector<std::vector<double>> coefficients;
  copied_fc_lanes = fc_lanes;
  fc_lanes.clear();
  for(const auto& copied_lane_seg : copied_fc_lanes)
  {
    if(copied_lane_seg.getSize() < 2)
    {
      continue;
    }

    PointCloudd lane_seg;
    std::vector<double> coeff;
    polynomialRegression(copied_lane_seg, coeff);

    double x0 = copied_lane_seg.points_[0].x;
    double xe = copied_lane_seg.points_.back().x;

    findPointsOnAPolynomial(lane_marking_candidates, coeff, x0, xe, 0.1, 0.1, lane_seg);
    polynomialRegression(lane_seg, coeff);

//    findPointsOnAPolynomial(lane_marking_candidates, coeff, 0.0, xe, 0.1, 0.5, lane_seg);
//    polynomialRegression(lane_seg, coeff);

    int num_prev_lane_seg_points = lane_seg.getSize();
    double prev_xe = xe;
    for(double xe_new = xe + 20.0; xe_new <= 120.0; xe_new += 20.0)
    {
      PointCloudd new_lane_seg_points;
      findPointsOnAPolynomial(lane_marking_candidates, coeff, prev_xe, xe_new, 0.1, 0.5, new_lane_seg_points);
      if(new_lane_seg_points.getSize() > 0)
      {
        lane_seg += new_lane_seg_points;
        polynomialRegression(lane_seg, coeff);
        prev_xe = xe_new;
      }
      else
      {
        break;
      }
    }

    findPointsOnAPolynomial(lane_marking_candidates, coeff, 0.0, 120.0, 0.1, 0.1, lane_seg);
    polynomialRegression(lane_seg, coeff);

    coefficients.push_back(coeff);

    fc_lanes.push_back(lane_seg);
  }

  setVizLanes(fc_lanes);

  setVizDebugCloudFull(lane_marking_candidates);

  PointCloudd polynomial_points;
  for(const auto& coeff : coefficients)
  {
    for(double x = 0.0; x < 100.0; x += 0.1)
    {
      double y(coeff[0]);
      for(size_t i = 1; i < coeff.size(); ++i)
      {
        y += coeff[i] * std::pow(x, i);
      }

      Point3Id p;
      p.x = x;
      p.y = y;
      p.z = 0.0;
      p.intensity = 255.0;

      polynomial_points.push_back(p);
    }
  }
  debug_cloud_full_ += polynomial_points;


  PointCloudd filtered_out_points;


  int ip = 1;
  for(const auto& points_on_an_object : fc_object_points)
  {
    uint8_t intensity = static_cast<uint8_t>(255 * ip / fc_object_points.size());
    ip++;
    for(const auto& point : points_on_an_object.points_)
    {      
      debug_cloud_full_.emplace_back(point.x, point.y, point.z, intensity);
    }
  }


//  /* Clustering
//   */
//  // Regenerate the local grid with filtered cloud.
//  std::vector<std::vector<LocalGridCell>> filtered_local_grid;
//  int clustering_grid_size_x = params_.size_clustering_grid_x_meter_ * params_.num_clustering_grid_cell_per_meter_x_ + 1;
//  int clustering_grid_size_y = params_.size_clustering_grid_y_meter_ * params_.num_clustering_grid_cell_per_meter_y_ + 1;

//  filtered_local_grid.resize(clustering_grid_size_x);
//  for(size_t i = 0; i < filtered_local_grid.size(); ++i)
//  {
//    filtered_local_grid[i].resize(clustering_grid_size_y);
//  }

//  for(size_t i = 0; i < cloud.size(); ++i)
//  {
//    int x_index, y_index;
//    xyToClusteringGridIndex(cloud.points[i].x, cloud.points[i].y, x_index, y_index);
//    if(x_index >= 0 && x_index < clustering_grid_size_x && y_index >= 0 && y_index < clustering_grid_size_y)
//    {
//      filtered_local_grid[x_index][y_index].points_.push_back(cloud.points[i]);
//    }
//  }

//  // Distance-based naive clustering
//  std::vector<PointCloudd> dist_clusters;
//  for(size_t grid_x = 0; grid_x < clustering_grid_size_x; ++grid_x)
//  {
//    for(size_t grid_y = 0; grid_y < clustering_grid_size_y; ++grid_y)
//    {
//      if(filtered_local_grid[grid_x][grid_y].is_clustered_ == false
//         && filtered_local_grid[grid_x][grid_y].points_.size() > params_.min_num_points_in_clustering_cell_threshold_)
//      {
//        filtered_local_grid[grid_x][grid_y].is_clustered_ = true;
//        std::vector<CellIndices> clustered_cell_indices;
//        std::vector<CellIndices> prev_clustered_cell_indices;
//        std::vector<CellIndices> newly_clustered_cell_indices;
//        clustered_cell_indices.emplace_back(grid_x, grid_y);
//        prev_clustered_cell_indices.emplace_back(grid_x, grid_y);
//        size_t start_index_to_explore = 0;

//        while(true)
//        {
//          for(size_t i = 0; i < prev_clustered_cell_indices.size(); ++i)
//          {
//            for(int dx = 0; dx <= 2; ++dx)
//            {
//              int x_index = prev_clustered_cell_indices[i].x_index_ + dx;
//              if(x_index < 0 || x_index >= clustering_grid_size_x)
//              {
//                continue;
//              }

//              for(int dy = -1; dy <= 1; ++dy)
//              {
//                if(dx == 0 && dy == 0)
//                {
//                  continue;
//                }

//                int y_index = prev_clustered_cell_indices[i].y_index_ + dy;
//                if(y_index < 0 || y_index >= static_cast<int>(clustering_grid_size_y))
//                {
//                  continue;
//                }

//                if(filtered_local_grid[x_index][y_index].is_clustered_ == false
//                  && filtered_local_grid[x_index][y_index].points_.size() > params_.min_num_points_in_clustering_cell_threshold_)
//                {
//                  newly_clustered_cell_indices.emplace_back(x_index, y_index);
//                  filtered_local_grid[x_index][y_index].is_clustered_ = true;
//                }
//              }
//            }
//          }

//          if(newly_clustered_cell_indices.size() > 0)
//          {
//            clustered_cell_indices.insert(clustered_cell_indices.end(),
//              newly_clustered_cell_indices.begin(), newly_clustered_cell_indices.end());
//            prev_clustered_cell_indices = newly_clustered_cell_indices;
//            newly_clustered_cell_indices.clear();
//          }
//          else
//          {
//            PointCloudd cluster_cloud;
//            for(size_t i = 0; i < clustered_cell_indices.size(); ++i)
//            {
//              const int& x_index = clustered_cell_indices[i].x_index_;
//              const int& y_index = clustered_cell_indices[i].y_index_;
//              cluster_cloud += filtered_local_grid[x_index][y_index].points_;
//            }

//            if(cluster_cloud.size() > params_.min_num_points_in_cluster_threshold_)
//            {
//              dist_clusters.push_back(cluster_cloud);
//            }
//            break;
//          }
//        }
//      }
//    }
//  }


//  // Sorting points in x direction.
//  for(size_t i = 0; i < dist_clusters.size(); ++i)
//  {
//    std::sort(dist_clusters[i].begin(), dist_clusters[i].end(), pointCompAlongXDirection);
//  }


////  debug_cloud_full_.clear();
////  for(const auto& dist_cluster : dist_clusters)
////  {
////    debug_cloud_full_ += dist_cluster;
////  }


//// Filter out clusters that are too narrow in x direction.
////  std::vector<PointCloudd> copied_dist_clusters = dist_clusters;
////  dist_clusters.clear();
////  for(size_t i = 0; i < copied_dist_clusters.size(); ++i)
////  {
////    if(copied_dist_clusters[i].points.back().x - copied_dist_clusters[i].points[0].x >= 0.3)
////    {
////      dist_clusters.push_back(copied_dist_clusters[i]);
////    }
////  }

//  // Lane clustering
//  std::vector<PointCloudd> lane_clusters;
//  std::vector<PointCloudd> left_lanes;
//  std::vector<PointCloudd> right_lanes;
//  std::vector<uint32_t> inlier_indices;

//  if(dist_clusters.size() > 1)
//  {
//    std::vector<size_t> cluster_connectivity(dist_clusters.size(), kImprobableBigClusterIndexNumber);
//    std::vector<bool> is_connected(dist_clusters.size(), false);

//    for(size_t i = 0; i < dist_clusters.size() - 1; ++i)
//    {
//      for(size_t j = i + 1; j < dist_clusters.size(); ++j)
//      {
//        if((dist_clusters[j].front().x - dist_clusters[i].back().x) < params_.cluster_connection_check_maximum_distance_
//          && (dist_clusters[j].front().x - dist_clusters[i].back().x) > 0.0
//          && fabs(dist_clusters[j].front().y - dist_clusters[i].back().y) < 2.0)
//        {
////          if(is_connected[j] == false)
////          {
//          cluster_connectivity[i] = j;
////            is_connected[j] = true;
//            break;
////          }
//        }
//      }
//    }

//    for(size_t i = 0; i < dist_clusters.size() - 1; ++i)
//    {
//      for(size_t j = i + 1; j < dist_clusters.size(); ++j)
//      {
//        if(cluster_connectivity[i] != kImprobableBigClusterIndexNumber && cluster_connectivity[i] == cluster_connectivity[j])
//        {
//          double dist_i = xyDistanceBetweenTwoPclPoints(dist_clusters[i].points.back(), dist_clusters[cluster_connectivity[i]].points[0]);
//          double dist_j = xyDistanceBetweenTwoPclPoints(dist_clusters[j].points.back(), dist_clusters[cluster_connectivity[j]].points[0]);

//          if(dist_i <= dist_j)
//          {
//            cluster_connectivity[j] = kImprobableBigClusterIndexNumber;
//          }
//          else
//          {
//            cluster_connectivity[i] = kImprobableBigClusterIndexNumber;
//          }
//        }
//      }
//    }


//    is_connected = std::vector<bool>(dist_clusters.size(), false);
//    for(size_t i = 0; i < dist_clusters.size(); ++i)
//    {
//      if(is_connected[i] == true)
//      {
//        continue;
//      }

//      if(cluster_connectivity[i] != kImprobableBigClusterIndexNumber)
//      {
//        is_connected[i] = true;
//        lane_clusters.push_back(dist_clusters[i]);
//        inlier_indices.push_back(i);
//        size_t idx_next_connected_cluster = cluster_connectivity[i];

//        while(true)
//        {
//          lane_clusters.back() += dist_clusters[idx_next_connected_cluster];
//          inlier_indices.push_back(idx_next_connected_cluster);
//          is_connected[idx_next_connected_cluster] = true;
//          if(cluster_connectivity[idx_next_connected_cluster] == kImprobableBigClusterIndexNumber)
//          {
//            break;
//          }
//          else
//          {
//            idx_next_connected_cluster = cluster_connectivity[idx_next_connected_cluster];
//          }
//        }
//      }
//    }

//    for(size_t i = 0; i < dist_clusters.size(); ++i)
//    {
//      if(is_connected[i] == false)
//      {
//        lane_clusters.push_back(dist_clusters[i]);
//        inlier_indices.push_back(i);
//      }
//    }
//  }

//  // Store the filtered out points.
//  for(size_t i = 0; i < dist_clusters.size(); ++i)
//  {
//    bool inlier(false);
//    for(const auto& inlier_idx : inlier_indices)
//    {
//      if(i == inlier_idx)
//      {
//        inlier = true;
//      }
//    }

//    if(!inlier)
//    {
//      filtered_out_points += dist_clusters[i];
//    }
//  }




//  // Outlier removal and lane segmentation
//  int poly_order = 3;
//  std::vector<double> y0_of_cluster_curves;
//  std::vector<Eigen::VectorXd> curve_cooeficients;
//  std::vector<bool> is_combined;
//  std::vector<PointCloudd> copied_lane_clusters = lane_clusters;
//  lane_clusters.clear();
//  inlier_indices.clear();

//  for(const auto& lane_cluster : copied_lane_clusters)
//  {
//    if(lane_cluster.size() > 30)
//    {
//      lane_clusters.push_back(lane_cluster);
//    }
//    else
//    {
//      filtered_out_points += lane_cluster;
//    }
//  }

//  // Sort lane clusters by number of points;
//  std::sort(lane_clusters.begin(), lane_clusters.end(), clusterComptByNumberOfPoints);

//  // Sorting points in x direction.
//  for(size_t i = 0; i < lane_clusters.size(); ++i)
//  {
//    std::sort(lane_clusters[i].begin(), lane_clusters[i].end(), pointCompAlongXDirection);
//  }

//  copied_lane_clusters = lane_clusters;
//  lane_clusters.clear();
//  for(size_t i = 0; i < copied_lane_clusters.size(); ++i)
//  {
//    PointCloudd downsampled_points;

//    if(copied_lane_clusters[i].size() > 1000)
//    {
//      // Find a point that is closest to the vehicle position.
//      double min_dist = 999.0;
//      size_t min_idx = 999999;
//      for(size_t j = 0; j < copied_lane_clusters[i].size(); ++j)
//      {
//        double dist = std::sqrt(std::pow(copied_lane_clusters[i].points[j].x, 2) + std::pow(copied_lane_clusters[i].points[j].y, 2));
//        if(dist < min_dist)
//        {
//          min_dist = dist;
//          min_idx = j;
//        }
//      }

//      downsampled_points.push_back(copied_lane_clusters[i][min_idx]);

//      if(min_idx < copied_lane_clusters[i].size() - 1)
//      {
//        for(int j = min_idx + 1; j < copied_lane_clusters[i].size(); ++j)
//        {
//          if(copied_lane_clusters[i][j].x - downsampled_points.back().x > 0.1)
//          {
//            downsampled_points.push_back(copied_lane_clusters[i][j]);
//          }
//        }
//      }

//      if(min_idx > 0)
//      {
//        for(int j = min_idx - 1; j > 0; --j)
//        {
//          if(copied_lane_clusters[i][j].x - downsampled_points.back().x < -0.1)
//          {
//            downsampled_points.push_back(copied_lane_clusters[i][j]);
//          }
//        }
//      }
//    }
//    else
//    {
//      downsampled_points = copied_lane_clusters[i];
//    }

//    //Check if this cluster includes lane points or outliers.
//    Eigen::MatrixXd A(downsampled_points.size(), poly_order + 1);
//    Eigen::VectorXd b(downsampled_points.size());

//    for(size_t j = 0; j < downsampled_points.size(); ++j)
//    {
//      for(int k = 0; k < poly_order + 1; ++k)
//      {
//        A(j, k) = std::pow(downsampled_points.points[j].x, poly_order - k);
//      }

//      b(j) = downsampled_points.points[j].y;
//    }

//    Eigen::VectorXd c = (A.transpose() * A).inverse() * A.transpose() * b;

//    PointCloudd inliers;
//    for(size_t j = 0; j < copied_lane_clusters[i].size(); ++j)
//    {
//      double px = copied_lane_clusters[i].points[j].x;
//      double py = copied_lane_clusters[i].points[j].y;

//      double curve_y = 0.0;// = c[0] * std::pow(px, 2) + c[1] * px + c[2];
//      for(size_t k = 0; k < poly_order + 1; ++k)
//      {
//        curve_y += c[k] * std::pow(px, poly_order - k);
//      }

//      if(fabs(curve_y - py) < 0.5)
//      {
//        inliers.push_back(copied_lane_clusters[i].points[j]);
//      }
//      else
//      {
//        filtered_out_points.push_back(copied_lane_clusters[i].points[j]);
//      }
//    }
//    //    if(static_cast<double>(inliers.size()) > 0.5 * copied_lane_clusters[i].size())
//    if(inliers.size() > 0)
//    {
//      lane_clusters.push_back(inliers);
//      y0_of_cluster_curves.push_back(c[poly_order]);
//      curve_cooeficients.push_back(c);
//      is_combined.push_back(false);
//    }
//  }

////   Draw polynomial lines.
//  if(params_.draw_polynomial_)
//  {
//    PointCloudd polynomial_points;
//    for(size_t i = 0; i < curve_cooeficients.size(); ++i)
//    {
//      Eigen::VectorXd c = curve_cooeficients[i];

//      for(double x = lane_clusters[i].points[0].x; x <= lane_clusters[i].points.back().x; x += 0.1)
//      //for(double x = -150.0; x <= 150.0; x += 0.1)
//      {
//        double y = 0.0;// = c[0] * std::pow(px, 2) + c[1] * px + c[2];
//        for(size_t k = 0; k < poly_order + 1; ++k)
//        {
//          y += c[k] * std::pow(x, poly_order - k);
//        }
//        Point3Id point;
//        point.x = x;
//        point.y = y;
//        point.z = 0.0;
//        point.intensity = 255;

//        polynomial_points.push_back(point);
//      }
//    }

//    debug_cloud_full_ += polynomial_points;
//  }


//  // Final segmentation
//  if(lane_clusters.size() > 1)
//  {
//    for(size_t i = 0; i < lane_clusters.size() - 1; ++i)
//    {
//      const double& cluster1_x0 = lane_clusters[i].points[0].x;
//      const double& cluster1_y0 = lane_clusters[i].points[0].y;
//      const double& cluster1_xe = lane_clusters[i].points.back().x;
//      const double& cluster1_ye = lane_clusters[i].points.back().y;

//      if(cluster1_xe - cluster1_x0 < 10.0)
//      {
//        continue;
//      }

//      PointCloudd appeding_clusters;

//      for(size_t j = i + 1; j < lane_clusters.size(); ++j)
//      {
//        const double& cluster2_x0 = lane_clusters[j].points[0].x;
//        const double& cluster2_y0 = lane_clusters[j].points[0].y;
//        const double& cluster2_xe = lane_clusters[j].points.back().x;
//        const double& cluster2_ye = lane_clusters[j].points.back().y;

//        if(is_combined[j] == false)
//        {
//          double dx_12 = cluster2_x0 - cluster1_xe;
//          double dx_21 = cluster1_x0 - cluster2_xe;

//          if(dx_12 > 0 && dx_12 < 30.0)
//          {
//            double y_at_cluster2_x0 = 0.0;
//            for(size_t k = 0; k < poly_order + 1; ++k)
//            {
//              y_at_cluster2_x0 += curve_cooeficients[i][k] * std::pow(cluster2_x0, poly_order - k);
//            }

//            for(double n = -1.0; n <= 1.0; n += 0.1)
//            {
//              Point3Id pcl_point;
//              pcl_point.x = cluster2_x0;
//              pcl_point.y = y_at_cluster2_x0 + n;
//              pcl_point.z = 0.5;
//              pcl_point.intensity = 255;
//              debug_cloud_full_.push_back(pcl_point);
//            }


//            if(fabs(y_at_cluster2_x0 - cluster2_y0) < 1.5)
//            {
//              appeding_clusters += lane_clusters[j];
//              is_combined[j] = true;
//            }
//          }
//          else if(dx_21 > 0 && dx_21 < 30.0)
//          {
//            double y_at_cluster2_xe = 0.0;
//            for(size_t k = 0; k < poly_order + 1; ++k)
//            {
//              y_at_cluster2_xe += curve_cooeficients[i][k] * std::pow(cluster2_xe, poly_order - k);
//            }

//            for(double n = -1.0; n <= 1.0; n += 0.1)
//            {
//              Point3Id pcl_point;
//              pcl_point.x = cluster2_xe;
//              pcl_point.y = y_at_cluster2_xe + n;
//              pcl_point.z = 0.5;
//              pcl_point.intensity = 255;
//              debug_cloud_full_.push_back(pcl_point);
//            }

//            if(fabs(y_at_cluster2_xe - cluster2_ye) < 1.5)
//            {
//              appeding_clusters += lane_clusters[j];
//              is_combined[j] = true;
//            }
//          }
//        }
//      }
//      lane_clusters[i] += appeding_clusters;
//    }
//  }

//  copied_lane_clusters = lane_clusters;
//  std::vector<double> copied_y0_of_cluster_curves = y0_of_cluster_curves;
//  std::vector<Eigen::VectorXd> copied_curve_coefficients = curve_cooeficients;
//  y0_of_cluster_curves.clear();
//  lane_clusters.clear();
//  curve_cooeficients.clear();
//  for(size_t i = 0; i < copied_lane_clusters.size(); ++i)
//  {
//    if(is_combined[i] == false)
//    {
//      lane_clusters.push_back(copied_lane_clusters[i]);
//      y0_of_cluster_curves.push_back(copied_y0_of_cluster_curves[i]);
//      curve_cooeficients.push_back(copied_curve_coefficients[i]);
//    }
//  }


//  is_combined.resize(lane_clusters.size());
//  for(size_t i = 0; i < lane_clusters.size(); ++i)
//  {
//    is_combined.push_back(false);
//  }

//  if(lane_clusters.size() > 1)
//  {
//    for(size_t i = 0; i < lane_clusters.size() - 1; ++i)
//    {
//      PointCloudd appeding_clusters;
//      Eigen::VectorXd ci = curve_cooeficients[i];
//      for(size_t j = i + 1; j < lane_clusters.size(); ++j)
//      {
//        Eigen::VectorXd cj = curve_cooeficients[j];
//        if(is_combined[j] == false)
//        {
//          if(fabs(ci[poly_order] - cj[poly_order]) < 1.5
//            && fabs(ci[poly_order - 1] - cj[poly_order - 1]) < 0.1)
//          {
//            appeding_clusters += lane_clusters[j];
//            is_combined[j] = true;
//          }
//        }
//      }

//      lane_clusters[i] += appeding_clusters;
//    }
//  }

//  copied_lane_clusters = lane_clusters;
//  copied_y0_of_cluster_curves = y0_of_cluster_curves;
//  y0_of_cluster_curves.clear();
//  lane_clusters.clear();
//  curve_cooeficients.clear();
//  for(size_t i = 0; i < copied_lane_clusters.size(); ++i)
//  {
//    if(is_combined[i] == false)
//    {
//      lane_clusters.push_back(copied_lane_clusters[i]);
//      y0_of_cluster_curves.push_back(copied_y0_of_cluster_curves[i]);
//    }
//  }



//  // Eliminate too small lane segments
//  copied_lane_clusters = lane_clusters;
//  lane_clusters.clear();
//  copied_y0_of_cluster_curves = y0_of_cluster_curves;
//  y0_of_cluster_curves.clear();
//  for(size_t i = 0; i < copied_lane_clusters.size(); ++i)
//  {
//    if(copied_lane_clusters[i].size() > 50)
//    {
//      lane_clusters.push_back(copied_lane_clusters[i]);
//      y0_of_cluster_curves.push_back(copied_y0_of_cluster_curves[i]);
//    }
//    else
//    {
//      filtered_out_points += copied_lane_clusters[i];
//    }
//  }

//  // Sorting points in x direction.
//  for(size_t i = 0; i < lane_clusters.size(); ++i)
//  {
//    std::sort(lane_clusters[i].begin(), lane_clusters[i].end(), pointCompAlongXDirection);
//  }


//  // Filter out clusters that are too narrow in x direction.
//  copied_lane_clusters = lane_clusters;
//  lane_clusters.clear();
//  copied_y0_of_cluster_curves = y0_of_cluster_curves;
//  y0_of_cluster_curves.clear();

//  for(size_t i = 0; i < copied_lane_clusters.size(); ++i)
//  {
//    if(copied_lane_clusters[i].points[0].x > 50.0 || copied_lane_clusters[i].points.back().x < -50.0)
//    {
//      filtered_out_points += copied_lane_clusters[i];
//      continue;
//    }

//    if(copied_lane_clusters[i].points.back().x - copied_lane_clusters[i].points[0].x >= 30.0)
//    {
//      lane_clusters.push_back(copied_lane_clusters[i]);
//      y0_of_cluster_curves.push_back(copied_y0_of_cluster_curves[i]);
//    }
//    else
//    {
//      filtered_out_points += copied_lane_clusters[i];
//    }
//  }

//  // Sort the clusters along y direction.
//  copied_lane_clusters = lane_clusters;
//  copied_y0_of_cluster_curves = y0_of_cluster_curves;
//  y0_of_cluster_curves.clear();
//  lane_clusters.clear();

//  if(copied_lane_clusters.size() > 0)
//  {
//    lane_clusters.push_back(copied_lane_clusters[0]);
//    y0_of_cluster_curves.push_back(copied_y0_of_cluster_curves[0]);

//    std::vector<double> y0_of_left_lanes;
//    std::vector<double> y0_of_right_lanes;

//    std::vector<PointCloudd>::iterator it;
//    std::vector<double>::iterator it_y0;

//    if(copied_lane_clusters.size() > 1)
//    {
//      for(size_t i = 1; i < copied_lane_clusters.size(); ++i)
//      {
//        it = lane_clusters.begin();
//        it_y0 = y0_of_cluster_curves.begin();
//        size_t insert_index = 0;
//        for(size_t j = 0; j < lane_clusters.size(); ++j)
//        {
//          if(j == 0)
//          {
//            if(copied_y0_of_cluster_curves[i] < y0_of_cluster_curves[j])
//            {
//              insert_index = j;
//              break;
//            }
//          }

//          if(j == lane_clusters.size() - 1)
//          {
//            if(copied_y0_of_cluster_curves[i] > y0_of_cluster_curves[j])
//            {
//              insert_index = j + 1;
//              break;
//            }
//          }

//          if(copied_y0_of_cluster_curves[i] > y0_of_cluster_curves[j]
//             && copied_y0_of_cluster_curves[i] < y0_of_cluster_curves[j + 1])
//          {
//            insert_index = j + 1;
//          }
//        }

//        lane_clusters.insert(it + insert_index, copied_lane_clusters[i]);
//        y0_of_cluster_curves.insert(it_y0 + insert_index, copied_y0_of_cluster_curves[i]);
//      }
//    }
//  }


//  std::vector<double> y0_left, y0_right;

//  for(size_t i = 0; i < lane_clusters.size(); ++i)
//  {
//    if(y0_of_cluster_curves[i] > 0)
//    {
//      if(i >= 1)
//      {
//        for(int j = i - 1; j >= 0; --j)
//        {
//          right_lanes.push_back(lane_clusters[j]);
//          y0_right.push_back(y0_of_cluster_curves[j]);
//        }
//      }

//      for(size_t j = i; j < lane_clusters.size(); ++j)
//      {
//        left_lanes.push_back(lane_clusters[j]);
//        y0_left.push_back(y0_of_cluster_curves[j]);
//      }

//      break;
//    }
//  }


//  lane_clusters.clear();
//  if(left_lanes.size() > 0)
//  {
//    if(y0_left[0] > 4.0)
//    {
//      for(size_t i = 0; i < left_lanes.size(); ++i)
//      {
//        filtered_out_points += left_lanes[i];
//      }
//      left_lanes.clear();
//    }
//  }

//  if(right_lanes.size() > 0)
//  {
//    if(y0_right[0] < -4.0)
//    {
//      for(size_t i = 0; i < right_lanes.size(); ++i)
//      {
//        filtered_out_points += right_lanes[i];
//      }
//      right_lanes.clear();
//    }
//  }


//  if(right_lanes.size() > 0)
//  {
//    lane_clusters.push_back(right_lanes[0]);

//    if(right_lanes.size() > 1)
//    {
//      for(size_t i = 1; i < right_lanes.size(); ++i)
//      {
//        if(y0_right[i - 1] - y0_right[i] > 5.0)
//        {
//          filtered_out_points += right_lanes[i];
//          std::vector<PointCloudd>::iterator it_start = right_lanes.begin() + i;
//          right_lanes.erase(it_start, right_lanes.end());
//          break;
//        }
//        else
//        {
//          lane_clusters.push_back(right_lanes[i]);
//        }
//      }
//    }
//  }

//  if(left_lanes.size() > 0)
//  {
//    lane_clusters.push_back(left_lanes[0]);

//    if(left_lanes.size() > 1)
//    {
//      for(size_t i = 1; i < left_lanes.size(); ++i)
//      {
//        if(y0_left[i] - y0_left[i - 1] > 5.0)
//        {
//          filtered_out_points += left_lanes[i];
//          std::vector<PointCloudd>::iterator it_start = left_lanes.begin() + i;
//          left_lanes.erase(it_start, left_lanes.end());
//          break;
//        }
//        else
//        {
//          lane_clusters.push_back(left_lanes[i]);
//        }
//      }
//    }
//  }

//  if(params_.batch_process_ == false)
//  {
//    std::cout << "Number of lanes: left - " << left_lanes.size() << ", right - " << right_lanes.size() << std::endl;
//  }


//  cloud.clear();
//  if(params_.publish_viz_topics_)
//  {
//    for(auto& point : filtered_out_points.points)
//    {
//      point.intensity = 255.0;
//    }

//    setVizLanes(lane_clusters);
//  }


//  fout_front_lanes_.open(output_folder + "/" + str_timestamp + ".txt", std::ios::out|std::ios::trunc);

////  std::ofstream fout_inten_dist(output_folder + "/" + str_timestamp + "_inten_dist.txt");
////  for(const auto& point : debug_cloud_full_.points)
////  {
////    fout_inten_dist << point.intensity << std::endl;
////  }
////  fout_inten_dist.close();

//  fout_pos_accuracy_.open(output_folder + "/position_accuracy", std::ios::out|std::ios::app);
//  fout_pos_accuracy_ << str_timestamp << "\t" << gpsins_at_cam_timestamp.gps_status_ << "\t" << std::to_string(gpsins_at_cam_timestamp.position_covariance_) << std::endl;
//  fout_pos_accuracy_.close();

  if(params_.save_result_image_)
  {
    // Save image.
    cv::Mat image(1200, 2400, CV_8UC3, cv::Scalar(0,0,0));

    cv::line(image, cv::Point(1000, 300), cv::Point(1050, 300), cv::Scalar(0,0,255));
    cv::line(image, cv::Point(1000, 300), cv::Point(1000, 250), cv::Scalar(0,0,255));

    cv::putText(image, cv::String("Position mode: " + gpsins_at_cam_timestamp.gps_status_), cv::Point(1200, 100), cv::FONT_HERSHEY_COMPLEX_SMALL, 2.0, cv::Scalar(255,255,255), 1);
    cv::putText(image, cv::String("Position accuracy: " + std::to_string(gpsins_at_cam_timestamp.position_covariance_)), cv::Point(1200, 170), cv::FONT_HERSHEY_COMPLEX_SMALL, 2.0, cv::Scalar(255,255,255), 1);

    int num_clusters = static_cast<int>(fc_lanes.size());
    for(int i = 0; i < num_clusters; ++i)
    {
      int r, g, b;
      intensityToRGB(i, num_clusters, r, g, b);

      for(size_t j = 0; j < fc_lanes[i].getSize(); ++j)
      {
        const double& x = fc_lanes[i].points_[j].x;
        const double& y = fc_lanes[i].points_[j].y;
        if(x < -120.0 || y < -30.0 || x >= 120.0 || y >= 30.0)
          continue;

        int u, v;
        xyToImageuv(x, y, u, v);
        cv::circle(image, cv::Point(u, v), 2, CV_RGB(r, g, b), -1, cv::LineTypes::LINE_AA);
      }
    }

    debug_image.copyTo(image(cv::Rect(500, 600, debug_image.cols, debug_image.rows)));


    int ip = 1;
    for(const auto& points_on_an_object : fc_object_points)
    {
      int r, g, b;
      intensityToRGB(ip, fc_object_points.size(), r, g, b);
      ip++;
      for(const auto& point : points_on_an_object.points_)
      {
        if(point.x > -120.0 && point.x < 120.0 && point.y > -30.0 && point.y < 30.0)
        {
          int u, v;
          xyToImageuv(point.x, point.y, u, v);
          cv::circle(image, cv::Point(u, v), 1, CV_RGB(r, g, b), -1, cv::LineTypes::LINE_AA);
        }
      }
    }

//    for(int i = 0; i < debug_cloud_full_.getSize(); ++i)
//    {
//      double& x = debug_cloud_full_.points_[i].x;
//      double& y = debug_cloud_full_.points_[i].y;
//      if(x < -120.0 || y < -30.0 || x >= 120.0 || y >= 30.0)
//        continue;

//      int u, v;
//      xyToImageuv(x, y, u, v);

////      v += 600;

//      int r, g, b;
//      intensityToRGB(debug_cloud_full_.points_[i].intensity, 255, r, g, b);

//      cv::circle(image, cv::Point(u, v), 2, CV_RGB(r, g, b), -1, cv::LineTypes::LINE_AA);

////      image.at<cv::Vec3b>(v, u)[0] = static_cast<unsigned char>(r);
////      image.at<cv::Vec3b>(v, u)[1] = static_cast<unsigned char>(g);
////      image.at<cv::Vec3b>(v, u)[2] = static_cast<unsigned char>(b);
//    }

    cv::imwrite(output_folder + "/" + str_timestamp + ".jpg", image);
  }



//  if(fc_lanes.size() > 1)
//  {
//    fout_front_lanes_ << std::fixed;
//    fout_front_lanes_.precision(3);

//    fout_front_lanes_ << 1;
//    for(size_t j = 0; j < left_lanes[i].size(); ++j)
//    {
//      fout_front_lanes_ << "\t" << left_lanes[i].points[j].x << "\t" << left_lanes[i].points[j].y;
//    }
//    fout_front_lanes_ << std::endl;


//    for(size_t i = 0; i < right_lanes.size(); ++i)
//    {
//      fout_front_lanes_ << -static_cast<int>(i) - 1;
//      for(size_t j = 0; j < right_lanes[i].size(); ++j)
//      {
//        fout_front_lanes_ << "\t" << right_lanes[i].points[j].x << "\t" << right_lanes[i].points[j].y;
//      }
//      fout_front_lanes_ << std::endl;
//    }

//    if(filtered_out_points.size() > 0)
//    {
//      fout_front_lanes_ << 99;
//      for(size_t i = 0; i < filtered_out_points.size(); ++i)
//      {
//        fout_front_lanes_ << "\t" << filtered_out_points.points[i].x << "\t" << filtered_out_points.points[i].y;
//      }
//      fout_front_lanes_ << std::endl;
//    }
//  }
//  fout_front_lanes_.close();
}

void LidarLaneExtraction::xyToLocalGridIndex(const double& x, const double& y, int& x_index, int& y_index)
{
  x_index = static_cast<int>(static_cast<double>(params_.num_local_grid_cell_per_meter_) * (x + static_cast<double>(params_.size_local_grid_x_meter_ / 2)));
  y_index = static_cast<int>(static_cast<double>(params_.num_local_grid_cell_per_meter_) * (y + static_cast<double>(params_.size_local_grid_y_meter_ / 2)));
}

void LidarLaneExtraction::xyToClusteringGridIndex(const double& x, const double& y, int& x_index, int& y_index)
{
  x_index = static_cast<int>(static_cast<double>(params_.num_clustering_grid_cell_per_meter_x_) * (x + static_cast<double>(params_.size_clustering_grid_x_meter_ / 2)));
  y_index = static_cast<int>(static_cast<double>(params_.num_clustering_grid_cell_per_meter_y_) * (y + static_cast<double>(params_.size_clustering_grid_y_meter_ / 2)));
}

void LidarLaneExtraction::extractLaneCandidates(PointCloudd& cloud)
{

}

void LidarLaneExtraction::setVizDebugCloudFull(const PointCloudd& viz_cloud)
{
  debug_cloud_full_ = viz_cloud;
}

void LidarLaneExtraction::setVizLanes(const std::vector<PointCloudd>& lanes)
{
  debug_cloud_lanes_ = lanes;
}

SimpleGPSINSMeasurement LidarLaneExtraction::calculateGPSINSMeasurementAtGivenTimestamp(const double& timestamp)
{
  SimpleGPSINSMeasurement oxts0, oxts1;
  for(size_t i = 1; i < oxts_buffer_.size(); ++i)
  {
    if(timestamp > oxts_buffer_[i].timestamp_)
    {
      oxts1 = oxts_buffer_[i];
      oxts0 = oxts_buffer_[i - 1];
      break;
    }
  }

  double ts = timestamp;
  SimpleGPSINSMeasurement ret = calculateInterpolatedGPSINSData(oxts1, oxts0, ts);
  ret.gps_status_ = oxts1.gps_status_;
  ret.position_covariance_ = oxts1.position_covariance_;

  return ret;
}

void LidarLaneExtraction::polynomialRegression(const PointCloudd& points, std::vector<double>& coeff)
{
  coeff.clear();
  int num_points = points.getSize();
  int poly_order = 3;
  if(num_points < 4)
  {
    poly_order = num_points - 1;
  }

  Eigen::MatrixXd X(num_points, poly_order + 1);
  Eigen::VectorXd y(num_points);
  Eigen::VectorXd c(poly_order + 1);

  for(size_t i = 0; i < num_points; ++i)
  {
    X(i, 0) = 1.0;
    for(int j = 1; j < poly_order + 1; ++j)
    {
      X(i, j) = std::pow(static_cast<double>(points.points_[i].x), j);
    }
    y(i) = static_cast<double>(points.points_[i].y);
  }

  c = (X.transpose() * X).inverse() * X.transpose() * y;
  for(size_t i = 0; i < poly_order + 1; ++i)
  {
    coeff.emplace_back(c(i));
  }
}

void LidarLaneExtraction::findPointsOnAPolynomial(const PointCloudd& input_points, const std::vector<double>& coeff,
  const double& x0, const double& xe, const double& x_bound, const double& y_bound, PointCloudd& inliers)
{
  inliers.clear();

  const double& dx = params_.dx_to_search_along_polynomial_;
//  const double& x_bound = params_.x_bound_to_consider_as_lane_point_;
//  const double& y_bound = params_.y_bound_to_consider_as_lane_point_;

  for(double x = x0; x < xe; x += dx)
  {
    double y(coeff[0]);
    for(size_t i = 1; i < coeff.size(); ++i)
    {
      y += coeff[i] * std::pow(x, i);
    }

    Point3Id closest_point;
    double min_dist(9999.0);
    for(const auto& lidar_point : input_points.points_)
    {
      if(lidar_point.x < x - x_bound || lidar_point.x > x + x_bound
        || lidar_point.y < y - y_bound || lidar_point.y > y + y_bound)
      {
        continue;
      }

      double dist = std::sqrt(std::pow(lidar_point.x - x, 2) + std::pow(lidar_point.y - y, 2));
      if(dist < y_bound && dist < min_dist)
      {
        min_dist = dist;
        closest_point = lidar_point;
      }
    }
    if(min_dist < 9999.0)
    {
      inliers.push_back(closest_point);
    }
  }
}

void LidarLaneExtraction::FindGroundRegion(const PointCloudd& cloud, const SimpleGPSINSMeasurement& gpsins, std::vector<std::vector<LocalGridCell>>& local_grid)
{
  // Initialize the grids.
  local_grid.resize(local_grid_size_x_);
  for(size_t i = 0; i < local_grid.size(); ++i)
  {
    local_grid[i].resize(local_grid_size_y_);
  }

  // Number of total points.
  uint32_t num_points = cloud.getSize();

  // Build a local grid and fill the points.

  for(const auto& point : cloud.points_)
  {
    // Skip useless points.
    if(point.intensity == 255)
    {
      continue;
    }

    int x_index, y_index;
    double x = point.x - gpsins.utm_x_meters_;
    double y = point.y - gpsins.utm_y_meters_;
    double z = point.z - gpsins.utm_z_meters_;

    xyToLocalGridIndex(x, y, x_index, y_index);
    if(x_index >= 0 && x_index < local_grid_size_x_ && y_index >= 0 && y_index < local_grid_size_y_)
    {
      local_grid[x_index][y_index].points_.push_back(point);
    }
  }


  // Calculate maximum, minimum z values of the points in each cell.
  for(auto& row_cells : local_grid)
  {
    for(auto& cell : row_cells)
    {
      double sum_z = 0.0;
      for(const auto& point : cell.points_.points_)
      {
        sum_z += point.z;
        if(point.z < cell.min_z_)
        {
          cell.min_z_ = point.z;
        }

        if(point.z > cell.max_z_)
        {
          cell.max_z_ = point.z;
        }
      }
    }
  }


  // Remove high-height points within each cell
  for(auto& row_cells : local_grid)
  {
    for(auto& cell : row_cells)
    {
      PointCloudd inlier_points;
      for(const auto& point : cell.points_.points_)
      {
        if(point.z < cell.min_z_ + 0.05)
        {
          inlier_points.push_back(point);
        }
      }

      if(inlier_points.getSize() > 0)
      {
        cell.points_ = inlier_points;
      }
      else
      {
        cell.points_.clear();
      }
    }
  }


  // Recalculate the maximum z in each cell, and calculate the mean z of the cell.
  for(auto& row_cells : local_grid)
  {
    for(auto& cell : row_cells)
    {
      cell.max_z_ = -9999.0;
      for(const auto& point : cell.points_.points_)
      {
        cell.mean_z_ += point.z;
        if(point.z > cell.max_z_)
        {
          cell.max_z_ = point.z;
        }
      }

      cell.mean_z_ /= static_cast<double>(cell.points_.getSize());
    }
  }

  // Find ground cells using cell propagation method.
  // Firstly select a apparent ground cell.
  int max_num_of_cells_to_search = 2 * params_.num_local_grid_cell_per_meter_;
  int x_index_at_veh_pos = (local_grid_size_x_ - 1) / 2;
  int y_index_at_veh_pos = (local_grid_size_y_ - 1) / 2;

  CellIndices seed_gnd_cell_indices;
  bool gnd_cell_found(false);
  for(int grid_x = x_index_at_veh_pos - max_num_of_cells_to_search; grid_x <= x_index_at_veh_pos + max_num_of_cells_to_search; ++grid_x)
  {
    for(int grid_y = y_index_at_veh_pos - max_num_of_cells_to_search; grid_y <= y_index_at_veh_pos + max_num_of_cells_to_search; ++grid_y)
    {
      bool is_ground(true);
      for(int i = -1; i <= 1; ++i)
      {
        for(int j = -1; j <= 1; ++j)
        {
          if(i == 0 && j == 0)
          {
            continue;
          }

          if(fabs(local_grid[grid_x][grid_y].min_z_ - local_grid[grid_x + i][grid_y + j].max_z_) > 0.05
            && fabs(local_grid[grid_x][grid_y].max_z_ - local_grid[grid_x + i][grid_y + j].min_z_) > 0.05)
          {
            is_ground = false;
            break;
          }
        }
        if(is_ground == false)
        {
          break;
        }
      }

      if(is_ground == true)
      {
        seed_gnd_cell_indices.x_index_ = grid_x;
        seed_gnd_cell_indices.y_index_ = grid_y;
        gnd_cell_found = true;
        local_grid[grid_x][grid_y].is_ground_ = true;
        break;
      }
    }

    if(gnd_cell_found == true)
    {
      break;
    }
  }

  // Find ground cells starting from the seed ground cell.
  std::vector<CellIndices> found_gnd_cells;
  std::vector<CellIndices> newly_found_gnd_cells;
  std::vector<CellIndices> gnd_cells_at_prev_step;
  found_gnd_cells.push_back(seed_gnd_cell_indices);
  gnd_cells_at_prev_step.push_back(seed_gnd_cell_indices);

  while(true)
  {
    for(const auto cell_indices : gnd_cells_at_prev_step)
    {
      for(int i = -1; i <= 1; ++i)
      {
        for(int j = -1; j <= 1; ++j)
        {
          if((i == 0 && j == 0)
            || (cell_indices.x_index_ + i) < 0 || (cell_indices.x_index_ + i) >= local_grid_size_x_
            || (cell_indices.y_index_ + j) < 0 || (cell_indices.y_index_ + j) >= local_grid_size_y_)
          {
            continue;
          }
          else if(local_grid[cell_indices.x_index_ + i][cell_indices.y_index_ + j].is_ground_ == true)
          {
             continue;
          }

          if(fabs(local_grid[cell_indices.x_index_][cell_indices.y_index_].min_z_ - local_grid[cell_indices.x_index_ + i][cell_indices.y_index_ + j].max_z_) < 0.05
            || fabs(local_grid[cell_indices.x_index_][cell_indices.y_index_].max_z_ - local_grid[cell_indices.x_index_ + i][cell_indices.y_index_ + j].min_z_) < 0.05)
          {
            local_grid[cell_indices.x_index_ + i][cell_indices.y_index_ + j].is_ground_ = true;
            newly_found_gnd_cells.emplace_back(cell_indices.x_index_ + i, cell_indices.y_index_ + j);
          }
        }
      }
    }

    if(newly_found_gnd_cells.size() == 0)
    {
      break;
    }

    found_gnd_cells.insert(found_gnd_cells.end(), newly_found_gnd_cells.begin(), newly_found_gnd_cells.end());
    gnd_cells_at_prev_step = newly_found_gnd_cells;
    newly_found_gnd_cells.clear();
  }

}


Point3Id doInverseRotation(const Point3Id& p, const double& roll, const double& pitch, const double& yaw)
{
  const double& x = p.x;
  const double& y = p.y;
  const double& z = p.z;

  double x1 = cos(yaw) * x + sin(yaw) * y;
  double y1 = -sin(yaw) * x + cos(yaw) * y;
  double z1 = z;

  double x2 = cos(pitch) * x1 - sin(pitch) * z1;
  double y2 = y1;
  double z2 = +sin(pitch) * x1 + cos(pitch) * z1;

  Point3Id out;
  out.x = x2;
  out.y = cos(roll) * y2 + sin(roll) * z2;
  out.z = -sin(roll) * y2 + cos(roll) * z2;
  out.intensity = p.intensity;

  return out;
}

double xyDistanceBetweenTwoPoints(const Point3Id& p1, const Point3Id& p2)
{
  return std::sqrt(std::pow((p1.x - p2.x), 2) + std::pow((p1.y - p2.y), 2));
}

SimpleGPSINSMeasurement calculateInterpolatedGPSINSData(const SimpleGPSINSMeasurement& gps_ins1, const SimpleGPSINSMeasurement& gps_ins2, double& ref_timestamps)
{
  SimpleGPSINSMeasurement ret;


  double r;
  double t1, y1, t2, y2;
  t1 = gps_ins1.timestamp_;
  t2 = gps_ins2.timestamp_;
  double dt = t2 - t1;

  ret.timestamp_ = ref_timestamps;

  // X
  y1 = gps_ins1.utm_x_meters_;
  y2 = gps_ins2.utm_x_meters_;
  r = (y2 - y1) / dt;
  ret.utm_x_meters_ = r * (ref_timestamps - t1) + y1;

  // Y
  y1 = gps_ins1.utm_y_meters_;
  y2 = gps_ins2.utm_y_meters_;
  r = (y2 - y1) / dt;
  ret.utm_y_meters_ = r * (ref_timestamps - t1) + y1;

  // Z
  y1 = gps_ins1.utm_z_meters_;
  y2 = gps_ins2.utm_z_meters_;
  r = (y2 - y1) / dt;
  ret.utm_z_meters_ = r * (ref_timestamps - t1) + y1;

  // roll
  y1 = gps_ins1.roll_rad_;
  y2 = gps_ins2.roll_rad_;
  r = (y2 - y1) / dt;
  ret.roll_rad_ = r * (ref_timestamps - t1) + y1;

  // pitch
  y1 = gps_ins1.pitch_rad_;
  y2 = gps_ins2.pitch_rad_;
  r = (y2 - y1) / dt;
  ret.pitch_rad_ = r * (ref_timestamps - t1) + y1;

  // yaw
  y1 = gps_ins1.yaw_rad_;
  y2 = gps_ins2.yaw_rad_;
  r = (y2 - y1) / dt;
  ret.yaw_rad_ = r * (ref_timestamps - t1) + y1;

  if(ret.yaw_rad_ < 0)
  {
    ret.yaw_rad_ += 2 * M_PI;
  }
  else if(ret.yaw_rad_ > 2 * M_PI)
  {
    ret.yaw_rad_ -= 2 * M_PI;
  }

  return ret;
}

bool pointCompAlongXDirection(const Point3Id& p1, const Point3Id& p2)
{
  if(p1.x < p2.x)
  {
    return true;
  }
  else
  {
    return false;
  }
}

bool clusterComptByNumberOfPoints(const PointCloudd& cluster1, const PointCloudd& cluster2)
{
  if(cluster1.getSize() > cluster2.getSize())
  {
    return true;
  }
  else
  {
    return false;
  }
}


pcl::PointCloud<pcl::PointXYZI> LidarLaneExtraction::convertPointClouddToPclCloud(const PointCloudd& input)
{
  pcl::PointCloud<pcl::PointXYZI> ret_cloud;
  ret_cloud.reserve(input.getSize());
  for(const auto& point : input.points_)
  {
    pcl::PointXYZI pcl_point;
    pcl_point.x = point.x;
    pcl_point.y = point.y;
    pcl_point.z = point.z;
    pcl_point.intensity = static_cast<float>(point.intensity);

    ret_cloud.push_back(pcl_point);
  }
  return ret_cloud;
}

PointCloudd LidarLaneExtraction::convertPclCloudToPointCloudd(const pcl::PointCloud<pcl::PointXYZI>& input)
{
  PointCloudd ret_cloud;
  ret_cloud.reserve(input.size());
  for(const auto& pcl_point : input.points)
  {
    Point3Id point;
    point.x = pcl_point.x;
    point.y = pcl_point.y;
    point.z = pcl_point.z;
    point.intensity = static_cast<uint8_t>(pcl_point.intensity);

    ret_cloud.push_back(point);
  }
  return ret_cloud;
}

void xyToImageuv(const double& x, const double& y, int& u, int& v)
{
  u = static_cast<int>(10 * x) + 1000;
  v = -static_cast<int>(10 * y) + 300;
}




