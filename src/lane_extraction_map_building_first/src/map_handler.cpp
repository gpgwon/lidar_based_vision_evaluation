/*******************************************************************************
* @file    tile_map.h
* @date    06/15/2018
*
* @attention Copyright (c) 2017
* @attention Phantom AI, Inc.
* @attention All rights reserved.
*******************************************************************************/

#include <lane_extraction/map_handler.h>
#include <boost/filesystem.hpp>

namespace phantom_ai {


LocalizationGridMap::LocalizationGridMap() :
  tile_map_origin_{},
  tile_size_{},
  map_viz_origin_{},
  tiles_{},
  is_pose_updated_{false},
  pose_x_(0.0),
  pose_y_(0.0),
  map_update_thread_{},
  trigger_thread_stop_{false},
  map_folder_name_(""),
  map_update_timer_(1)
{}

LocalizationGridMap::~LocalizationGridMap()
{
  if(map_update_thread_.joinable())
    map_update_thread_.join();

  for(size_t i = 0; i < tiles_.size(); ++i)
  {
    for(size_t j = 0; j < tiles_.at(i).size(); ++j)
    {
      if(tiles_.at(i).at(j) != NULL)
        delete tiles_.at(i).at(j);
    }
  }
}

void LocalizationGridMap::run()
{
  trigger_thread_stop_ = false;
  map_update_thread_ = std::thread(&LocalizationGridMap::mapUpdateThread, this);
}

void LocalizationGridMap::stop()
{
  trigger_thread_stop_ = true;
  map_update_thread_.join();
}

void LocalizationGridMap::initialize()
{
  map_folder_name_ = phantom_paths_dataset_directory() + "/loc_map/";

  loadYamlFile(map_folder_name_);
  tiles_.resize(tile_size_.x, std::vector<GridMapTile*>(tile_size_.y, NULL));
}

void LocalizationGridMap::mapUpdateThread()
{
  while(true)
  {
    if(trigger_thread_stop_)
      break;

    map_update_timer_.tic();

    if(is_pose_updated_)
    {

      updateLocalMap();

      is_pose_updated_ = false;
    }

    map_update_timer_.toc();
  }

  std::cout << "Map update thread terminated." << std::endl;
}

void LocalizationGridMap::loadWholeMapFiles(const std::string& tile_map_folder_name)
{
  boost::filesystem::path targetDir(tile_map_folder_name);
  boost::filesystem::directory_iterator it(targetDir), eod;
  BOOST_FOREACH(boost::filesystem::path const &p, std::make_pair(it, eod))
  {
    if(boost::filesystem::is_regular_file(p))
    {
      std::string file_full_path = it->path().string();
      std::string folder_path = tile_map_folder_name;

      uint32_t lnth_folder_path = folder_path.size();
      uint32_t lnth_file_name_only = file_full_path.size() - lnth_folder_path - 4;
      std::string file_name = file_full_path.substr(lnth_folder_path, lnth_file_name_only);

      if(file_name == "parameters.")
        continue;

      std::vector<std::string> split_strings;
      boost::split(split_strings, file_name, [](char c){return c == '_';});

      uint32_t tile_x0 = static_cast<uint32_t>(std::stoul(split_strings.at(0)));
      uint32_t tile_y0 = static_cast<uint32_t>(std::stoul(split_strings.at(1)));
      uint32_t tile_x_idx = (tile_x0 - tile_map_origin_.x) / 100;
      uint32_t tile_y_idx = (tile_y0 - tile_map_origin_.y) / 100;

      tiles_.at(tile_x_idx).at(tile_y_idx) = new GridMapTile(tile_x0, tile_y0);

      std::ifstream fin(file_full_path, std::ifstream::binary);

      fin.seekg (0,fin.end);
      uint32_t file_size = fin.tellg();
      fin.seekg (0);

      // allocate memory for file content
      std::vector<char> buff;
      buff.resize(file_size);
      fin.read (&buff.at(0), file_size);

      for(size_t i = 0; i < file_size / 5; ++i)
      {
        uint16_t cell_idx_x;
        uint16_t cell_idx_y;
        uint8_t cell_type;

        std::memcpy((char*)(&cell_idx_x), &buff.at(5 * i), 2);
        std::memcpy((char*)(&cell_idx_y), &buff.at(5 * i + 2), 2);
        std::memcpy((char*)(&cell_type), &buff.at(5 * i + 4), 1);

        tiles_.at(tile_x_idx).at(tile_y_idx)->updateCell(cell_idx_x, cell_idx_y, cell_type);
      }

      fin.close();
    }
  }

  std::cout << "Tile map loading completed." << std::endl;
}

void LocalizationGridMap::loadAMapFile(const uint32_t& tile_idx_x, const uint32_t& tile_idx_y)
{  
  uint32_t tile_x0 = tile_map_origin_.x + 100 * tile_idx_x;
  uint32_t tile_y0 = tile_map_origin_.y + 100 * tile_idx_y;

  std::string open_file_name = map_folder_name_ + std::to_string(tile_x0) + "_" + std::to_string(tile_y0) + ".2dm";

  std::ifstream fin;
  fin.open(open_file_name, std::ifstream::binary);

  if(fin.fail())
    return;

  if(tiles_.at(tile_idx_x).at(tile_idx_y) == NULL)
    tiles_.at(tile_idx_x).at(tile_idx_y) = new GridMapTile(tile_x0, tile_y0);
  else
    return;

  fin.seekg (0,fin.end);
  uint32_t file_size = fin.tellg();
  fin.seekg (0);

  // allocate memory for file content
  std::vector<char> buff;
  buff.resize(file_size);
  fin.read (&buff.at(0), file_size);

  for(size_t i = 0; i < file_size / 5; ++i)
  {
    uint16_t cell_idx_x;
    uint16_t cell_idx_y;
    uint8_t cell_type;

    std::memcpy((char*)(&cell_idx_x), &buff.at(5 * i), 2);
    std::memcpy((char*)(&cell_idx_y), &buff.at(5 * i + 2), 2);
    std::memcpy((char*)(&cell_type), &buff.at(5 * i + 4), 1);

    tiles_.at(tile_idx_x).at(tile_idx_y)->updateCell(cell_idx_x, cell_idx_y, cell_type);
  }
}

void LocalizationGridMap::updateLocalMap()
{
  uint32_t prev_tile_idx_x = UINT_MAX;
  uint32_t prev_tile_idx_y = UINT_MAX;

  uint32_t curr_tile_idx_x, curr_tile_idx_y;
  getTileIndexFromUTMPoint(pose_x_, pose_y_, &curr_tile_idx_x, &curr_tile_idx_y);

  if(prev_tile_idx_x == curr_tile_idx_x && prev_tile_idx_y == curr_tile_idx_y)
  {
    prev_tile_idx_x = curr_tile_idx_x;
    prev_tile_idx_y = curr_tile_idx_y;
    return;
  }
  else
  {
    prev_tile_idx_x = curr_tile_idx_x;
    prev_tile_idx_y = curr_tile_idx_y;
  }

  for(int tile_idx_x = (int)curr_tile_idx_x - 4; tile_idx_x <= (int)curr_tile_idx_x + 4; ++tile_idx_x)
  {
    for(int tile_idx_y = (int)curr_tile_idx_y - 4; tile_idx_y <= (int)curr_tile_idx_y + 4; ++tile_idx_y)
    {
      if(tile_idx_x < 0 || tile_idx_x >= (int)tile_size_.x || tile_idx_y < 0 || tile_idx_y >= (int)tile_size_.y)
      {
        continue;
      }

      if(tile_idx_x <= (int)curr_tile_idx_x - 3 || tile_idx_x >= (int)curr_tile_idx_x + 3
         || tile_idx_y <= (int)curr_tile_idx_y - 3 || tile_idx_y >= (int)curr_tile_idx_y + 3)
      {
        if(tiles_.at(tile_idx_x).at(tile_idx_y) != NULL)
        {
          delete tiles_.at(tile_idx_x).at(tile_idx_y);
          tiles_.at(tile_idx_x).at(tile_idx_y) = NULL;
        }
      }
      else
      {
        if(tiles_.at(tile_idx_x).at(tile_idx_y) == NULL)
        {
          loadAMapFile(tile_idx_x, tile_idx_y);
        }
      }
    }
  }
}


void LocalizationGridMap::saveMap(const std::string& save_map_folder_name)
{
  std::cout << "Save map" << std::endl;
  for(uint32_t tile_x = 0; tile_x < tile_size_.x; ++tile_x)
  {
    for(uint32_t tile_y = 0; tile_y < tile_size_.y; ++tile_y)
    {
      if(tiles_.at(tile_x).at(tile_y) != NULL)
      {
        std::string tile_file_name = std::to_string(tiles_.at(tile_x).at(tile_y)->x0_) +
            "_" + std::to_string(tiles_.at(tile_x).at(tile_y)->y0_) + ".2dm";

        std::vector<char> buff;
        for(uint16_t i = 0; i < 2000; ++i)
        {
          for(uint16_t j = 0; j < 2000; ++j)
          {
            if(tiles_.at(tile_x).at(tile_y)->cell_.at(i).at(j) > 0)
            {
              buff.resize(buff.size() + 5);
              char* pos_buff = &buff.at(buff.size() - 5);
              std::memcpy(pos_buff, &i, 2);
              std::memcpy(pos_buff + 2, &j, 2);
              std::memcpy(pos_buff + 4, &tiles_.at(tile_x).at(tile_y)->cell_.at(i).at(j), 1);
            }
          }
        }

        std::ofstream fout(save_map_folder_name + tile_file_name, std::ofstream::binary);

        fout.write(&buff.at(0), buff.size());
        fout.close();
      }
    }
  }

  std::cout << "Complete." << std::endl;
}

void LocalizationGridMap::clearAllTiles()
{
  if(tiles_.size() == 0)
    return;

  for(uint32_t tile_x = 0; tile_x < tile_size_.x; ++tile_x)
  {
    for(uint32_t tile_y = 0; tile_y < tile_size_.y; ++tile_y)
    {
      if(tiles_.at(tile_x).at(tile_y) != NULL)
      {
        delete tiles_.at(tile_x).at(tile_y);
        tiles_.at(tile_x).at(tile_y) = NULL;
      }
    }
  }
}

void LocalizationGridMap::loadYamlFile(const std::string& tile_map_folder_name)
{
  YamlNode node = load_yaml_file(tile_map_folder_name + "parameters.yaml");

  std::vector<double> map_viz_origin = get_yaml_value(node, "map_viz_origin").as<std::vector<double>>();
  map_viz_origin_ = Point2d(map_viz_origin[0], map_viz_origin[1]);

  std::vector<unsigned int> tile_map_origin = get_yaml_value(node, "tile_map_origin").as<std::vector<unsigned int>>();
  tile_map_origin_ = phantom_ai::Point2T<unsigned int>(tile_map_origin[0], tile_map_origin[1]);

  std::vector<unsigned int> tile_size = get_yaml_value(node, "tile_size").as<std::vector<unsigned int>>();
  tile_size_ = phantom_ai::Point2T<unsigned int>(tile_size[0], tile_size[1]);
}

}
