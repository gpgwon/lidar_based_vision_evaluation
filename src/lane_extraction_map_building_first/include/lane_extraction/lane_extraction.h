#ifndef LIDAR_LANE_EXTRACTION_H
#define LIDAR_LANE_EXTRACTION_H

#include <velodyne_parser/velodyne_vls_128_parser.h>
#include <common/simple_gps_ins_measurement.h>
#include <common/geometry.h>
#include <common/utils.h>
#include <common/point3.h>
#include <common/point_cloud.h>
#include <project_pc_to_image/project_pc_to_image.h>

#include <vector>
#include <deque>
#include <thread>
#include <fstream>

#include <boost/circular_buffer.hpp>

#include <yaml-cpp/yaml.h>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
//#include <pcl/filters/extract_indices.h>
//#include <pcl/segmentation/progressive_morphological_filter.h>
//#include <pcl/filters/voxel_grid.h>
//#include <pcl/ModelCoefficients.h>
//#include <pcl/filters/extract_indices.h>
//#include <pcl/features/normal_3d.h>
//#include <pcl/kdtree/kdtree.h>
//#include <pcl/kdtree/kdtree_flann.h>
//#include <pcl/sample_consensus/method_types.h>
//#include <pcl/sample_consensus/model_types.h>
//#include <pcl/segmentation/sac_segmentation.h>
//#include <pcl/segmentation/extract_clusters.h>
//#include <pcl/common/transforms.h>
#include <pcl/filters/radius_outlier_removal.h>
//#include <pcl/filters/conditional_removal.h>
//#include <pcl/filters/statistical_outlier_removal.h>
//#include <pcl/surface/mls.h>
//#include <pcl/io/pcd_io.h>

#include <utm/utm.h>
#include <Eigen/Dense>
#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>

#define VLS128_NUM_BLOCKS_PER_SEC  18000

Point3Id doInverseRotation(const Point3Id& p, const double& roll, const double& pitch, const double& yaw);

double xyDistanceBetweenTwoPoints(const Point3Id& p1, const Point3Id& p2);

SimpleGPSINSMeasurement calculateInterpolatedGPSINSData(const SimpleGPSINSMeasurement& gps_ins1, const SimpleGPSINSMeasurement& gps_ins2, double& ref_timestamps);

// Return true if p1.x < p2.x
bool pointCompAlongXDirection(const Point3Id& p1, const Point3Id& p2);

// Return true if cluster.size() < cluster2.size()
bool clusterComptByNumberOfPoints(const PointCloudd& cluster1, const PointCloudd& cluster2);

class CellIndices
{
public:
  CellIndices()
  {}

  CellIndices(const int& x_index, const int& y_index)
    : x_index_(x_index), y_index_(y_index)
  {}

  int x_index_;
  int y_index_;
};

struct LocalGridCell
{
  bool is_ground_;
  PointCloudd points_;
  double max_z_;
  double min_z_;
  double mean_z_;
  bool is_clustered_;

  LocalGridCell() : is_ground_(false), max_z_(-9999.0), min_z_(9999.0), mean_z_(0.0), is_clustered_(false)
  {}
};

struct LaneExtractionParams
{
  // Path of the folder where the point cloud fiels are located.
  std::string pointcloud_folder_;

  // Start and end time to process the pointcloud files.
  std::vector<double> pc_file_process_time_range_;

  // Path of the folder where the post-processed GPS/INS files are located.
  std::string gps_ins_folder_;

  // Path of the folder where the output results are stored.
  std::string output_folder_;

  // Flag indicating whether to use a timestamp file.
  bool use_timestamp_file_;

  // File path of the timestamp list file.
  std::string timestamp_file_path_;

  // Flag indicating whether to save result images.
  bool save_result_image_;

  // Flag indicating whether to publish visualization topics for debugging.
  bool publish_viz_topics_;

  // Measurement frequency of the GPS/INS.
  uint32_t gps_ins_measurement_frequency_hz_;

  // Measurement frequency of the camera.
  uint32_t camera_measurement_frequency_hz_;

  // Measurement frequency of the lidar.
  uint32_t lidar_measurement_frequency_hz_;

  // Max time duration to accumulate the lidar data.
  uint32_t lidar_data_max_accumulation_time_sec_;

  // The x-directional size of the local grid map.
  uint32_t size_local_grid_x_meter_;

  // The y-directional size of the local grid map.
  uint32_t size_local_grid_y_meter_;

  // The number of local grid cells in x and y direction.
  uint32_t num_local_grid_cell_per_meter_;

  // The x-directional size of the local grid map for clustering.
  uint32_t size_clustering_grid_x_meter_;

  // The y-directional size of the local grid map for clustering.
  uint32_t size_clustering_grid_y_meter_;

  // The number of local grid cells in x direction.
  uint32_t num_clustering_grid_cell_per_meter_x_;

  // The number of local grid cells in y direction.
  uint32_t num_clustering_grid_cell_per_meter_y_;

  // Minimum number of points threshold in a clustering cell.
  uint32_t min_num_points_in_clustering_cell_threshold_;

  // Minimum number of points to be considered a cluster.
  uint32_t min_num_points_in_cluster_threshold_;

  // Maximum allowable distance to connect two clusters.
  double cluster_connection_check_maximum_distance_;

  // Flag indicating wheter to use the pcd processing mode.
  int pcd_process_;

  double pcd_process_timestamp_;

  std::string pointcloud_parent_folder_;

  std::string output_parent_folder_;

  bool batch_process_;

  uint32_t num_of_threads_;

  std::string log_file_folder_;

  double intensity_threshold_;

  bool draw_polynomial_;

  double factor_for_avg_inten_thld_;

  double x_bound_to_consider_as_lane_point_;

  double y_bound_to_consider_as_lane_point_;

  double dist_thld_to_accept_as_lane_point_;

  double dx_to_search_along_polynomial_;

  double image_delay_;

  bool display_debug_image_;
};

class LidarLaneExtraction
{

public:
  LidarLaneExtraction(const LaneExtractionParams& params, const YAML::Node& yaml_node);
  ~LidarLaneExtraction();

  void pushNewPointCloud(const PointCloudf& input);

  void pushNewGPSINS(const SimpleGPSINSMeasurement& input);

  void extractLanesAtGivenTimestamp(const std::string& session_name, const double& timestamp, const std::string& output_folder);

  PointCloudd debug_cloud_full_;

  std::vector<PointCloudd> debug_cloud_lanes_;

  boost::circular_buffer<SimpleGPSINSMeasurement> oxts_buffer_;

  boost::circular_buffer<PointCloudd> pointcloud_buffer_;

  boost::circular_buffer<PointCloudf> raw_pointcloud_buffer_;;

  boost::circular_buffer<double> yaw_bias_buff_;

  bool pcd_file_saved_;

private:

  PointCloudd transformPointCloudToGlobalCoordinates(const PointCloudf& input_cloud);

  void xyToLocalGridIndex(const double& x, const double& y, int& x_index, int& y_index);

  void xyToClusteringGridIndex(const double& x, const double& y, int& x_index, int& y_index);

  void extractLaneCandidates(PointCloudd& cloud);

  void setVizDebugCloudFull(const PointCloudd& viz_cloud);

  void setVizLanes(const std::vector<PointCloudd>& lanes);

  SimpleGPSINSMeasurement calculateGPSINSMeasurementAtGivenTimestamp(const double& timestamp);

  void FindGroundRegion(const PointCloudd& cloud, const SimpleGPSINSMeasurement& gpsins,
    std::vector<std::vector<LocalGridCell>>& local_grid);

  void polynomialRegression(const PointCloudd& points, std::vector<double>& coeff);

  void findPointsOnAPolynomial(const PointCloudd& input_points, const std::vector<double>& coeff,
    const double& x0, const double& xe, const double& x_bound, const double& y_bound, PointCloudd& inliers);

  pcl::PointCloud<pcl::PointXYZI> convertPointClouddToPclCloud(const PointCloudd& input);

  PointCloudd convertPclCloudToPointCloudd(const pcl::PointCloud<pcl::PointXYZI>& input);

  boost::circular_buffer<double> front_cam_time_stamp_buffer_;

  boost::circular_buffer<double> rear_cam_time_stamp_buffer_;

  uint32_t local_grid_size_x_;
  uint32_t local_grid_size_y_;

  std::ofstream fout_front_lanes_;

  LaneExtractionParams params_;

  std::ofstream fout_pos_accuracy_;

  YAML::Node yaml_node_;
};



#endif
