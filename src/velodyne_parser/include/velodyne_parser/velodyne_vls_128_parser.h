#ifndef VELODYNE_VLS_128_PARSER_H_
#define VELODYNE_VLS_128_PARSER_H_

#include <velodyne_parser/base_velodyne_parser.h> // parent class

class VelodyneVLS128Parser : public BaseVelodyneParser
{
 public:
  VelodyneVLS128Parser() = default;

  /** @brief Parsing method meant to be tailored to specific sensor model and configs for data output from sensor.
  *   @return status of parsing: true = successful, false = failure.
  *   @callergraph
  *   @public
  */
  bool parseDataPacket(const std::vector<uint8_t>& packet, const double& timestamp) override;

  /** @brief Calls callback for publishing a point cloud. Must be registered first with registerPublishCallback.
  *   @param point_cloud Reference to point cloud to be published.
  *   @param frame_id_string Reference to frame id string used for ROS TF.
  *   @callergraph
  *   @public
  */
  void publishPointCloud(const PointCloudf& point_cloud) override;
};

#endif // VELODYNE_VLS_128_PARSER_H_
