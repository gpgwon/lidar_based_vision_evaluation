#ifndef BASE_VELODYNE_PARSER_H_
#define BASE_VELODYNE_PARSER_H_


#include <common/time_stamp.h>
#include <common/point_cloud.h>
#include <common/geometry.h>

#include <velodyne_parser/velodyne_types.h>

#include <functional>
#include <iostream>
#include <array>

#include <eigen3/Eigen/Dense>

#include <boost/timer.hpp>

const float kMaxMeasurableAngleDeg = 359.99f;
const float kMinMeasurableAngleDeg = 0.0f;
const uint16_t kMaxConfigurableAngleDeg = 360;
const uint16_t kMinConfigurableAngleDeg = 0;

class BaseVelodyneParser
{
  struct Config{
    float position_in_veh_x_m;
    float position_in_veh_y_m;
    float position_in_veh_z_m;
    float rotation_in_veh_roll_rad;
    float rotation_in_veh_pitch_rad;
    float rotation_in_veh_yaw_rad;
  };


public:
  /** @brief Trivial constructor for BaseVelodyneParser abstract class.
  *   @callergraph
  *   @public
  */
  BaseVelodyneParser();

  /** @brief Virtual destructor to ensure everything is cleaned up if `delete` is called on a derived type that has
  *          been casted to this base type.
  *   @callergraph
  *   @public
  */
  virtual ~BaseVelodyneParser();

  /** @brief Copy constructor - deleted.
  *   @public
  */
  BaseVelodyneParser(const BaseVelodyneParser&) = delete;

  /** @brief Move constructor - deleted.
  *   @public
  */
  BaseVelodyneParser(const BaseVelodyneParser&&) = delete;

  /**
  *   @brief Copy assignment constructor - deleted.
  *   @public
  */
  BaseVelodyneParser& operator=(BaseVelodyneParser const&) = delete;

  /** @brief Move assignment constructor - deleted.
  *   @public
  */
  BaseVelodyneParser& operator=(BaseVelodyneParser const&&) = delete;

  /** @brief Initialization method meant to be tailored to specific sensor model and configs.
  *   @return status of intialization: true = successful, false = failure.
  *   @callergraph
  *   @public
  */
  bool initialize(void);

  /** @brief Parsing method meant to be tailored to specific sensor model and configs for data output from sensor.
  *   @return status of parsing: true = successful, false = failure.
  *   @callergraph
  *   @public
  */
  virtual bool parseDataPacket(const std::vector<uint8_t>& packet, const double& timestamp) = 0;


  /** @brief Register a callback to be called when a new TCP/UDP packet is received.
  *   @param callback Pointer to a function accepting a ref to a PointCloud instance.
  *   @public
  */
  void registerPublishCallback(std::function<void(const PointCloudf&)> callback_func);

  /** @brief Calls callback for publishing a point cloud. Must be registered first with registerPublishCallback.
  *   @param point_cloud Reference to point cloud to be published.
  *   @param frame_id_string Reference to frame id string used for ROS TF.
  *   @callergraph
  *   @public
  */
  virtual void publishPointCloud(const PointCloudf& point_cloud) = 0;

  /** @brief Checks status to see if the processing of a full scan point cloud is complete.
  *   @note is primarly used for when the output needs to be an accumulated full scan.
  *   @return True = if full scan is complete.
  *   @callergraph
  *   @public
  */
  bool isScanComplete(void);

  /** @brief Gets a const reference to the current parsed points.
  *   @return Const refrence to the current parsed points.
  *   @callergraph
  *   @public
  */
  const PointCloudf& getLastCompletedScan(void);

  /** @brief clears the last completed completed scan point cloud.
  *   @callergraph
  *   @public
  */
  void clearLastCompletedScan(void);

  /** @brief Returns the completion status of the parser initialization.
  *   @return True = parser initialization complete, False = Not complete.
  *   @callergraph
  *   @public
  */
  bool isInitComplete(void);

  virtual void setPublishFullScan(const bool& publish_full_scan);



protected:

  /** @brief Handles setup necessary for a point cloud to be concluded. This includes setting the completition flag,
  *          current point cloud pointer swapping and new current point cloud clean up.
  *   @protected
  */
  void setCurrentPointCloudToCompleted(void);

  //************** Temporarily added to meet DeepMapp's requirements. ***************//
  void transformMinSecimeToFullUTCTimeStamp(const double& min_sec_time, const double& system_time, double& full_utc_timestamp);

  // Function pointer for publishing point clouds.
  std::function<void(const PointCloudf&)> velodyne_parser_point_cloud_output_callback_;

  // Stores current point cloud (single sub scan or accumulated scan based on config)
  std::array<PointCloudf, 2> point_clouds_;

  // Points to the point cloud we are currently putting points into.
  // Only use setCurrentPointCloudToCompleted() to change.
  PointCloudf* current_point_cloud_ptr_;

  // Points to the point cloud that has been completed. Never directly set.
  // Only use setCurrentPointCloudToCompleted() to change.
  PointCloudf* completed_point_cloud_ptr_;

  // Flag indicating the status of the parser initialization
  bool init_complete_;

  // Flag indicating if the full field of view (0 to 360) is being used while parsing.
  bool full_fov_used_;

  // Flag indicating if a full scan has been completed.
  bool full_scan_complete_;

  // Stores the azimuth of the last packet block, used for evaulating when to publish a full scan based on FoV.
  float last_block_azimuth_rad_;

  // Rotation Matrix for transform of lidar position in vehicle coordinates.
  Eigen::Matrix3f tf_rotation_matrix_;

  bool publish_full_scan_;

  float t00;
  float t01;
  float t02;
  float t10;
  float t11;
  float t12;
  float t20;
  float t21;
  float t22;

  // Configuration.
  Config config_;
};


#endif // BASE_VELODYNE_PARSER_H_
