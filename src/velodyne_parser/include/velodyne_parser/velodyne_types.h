#ifndef VELODYNE_TYPES_H_
#define VELODYNE_TYPES_H_

#include <string>

// Payload size of the Velodyne telemetry/position packet.
const size_t kVelodyneTelemetryPacketPayloadSizeBytes = 512;
const size_t kVelodyneTelemetryPacketMaxNMEASizeBytes = 306;

// Types of velodyne models
enum class VelodyneSensorModel : uint8_t
{
  HDL_32E = 0x21,
  VLP_16 = 0x22,
  PUCK_HI_RES = 0x24,
  VLP_32C = 0x28,
  VELARRAY = 0x31,
  VLS_128 = 0xA1, // This is different for the VLP16's defintion.
  UNKNOWN = 0xFF
};

// Modes for sensor's reported returns.
// Same for VLP32c and VLP16
enum class VelodyneReturnMode : uint8_t
{
  STRONGEST = 0x37,
  LAST = 0x38,
  DUAL = 0x39,
  UNKNOWN = 0xFF
};

// PPS Status signal from sensors position (telemetry) packet
// Same for VLP32c and VLP16
enum class VelodynePPSStatus : uint8_t
{
  NO_PPS_DETECTED = 0,
  SYNCING_TO_PPS = 1,
  PPS_LOCKED = 2,
  ERROR = 3
};


// Decoded telemetry/position packet format.
// Same for VLP32c and VLP16
struct __attribute__((__packed__)) VelodyneDecodedTelemetryPacket
{
  uint8_t unused0[198];
  uint32_t timestamp_us; // Microseconds since top of the hour relative to sensor's time.
  VelodynePPSStatus pps_status;
  uint8_t unused1[3];
  char nmea_sentence[kVelodyneTelemetryPacketMaxNMEASizeBytes]; // Payload length is 512 bytes. The GPRMC sentence is
                                                                // terminated with CR/LF and padded to end of payload
                                                                // with null bytes. So max NMEA length is 306.
};


#endif // VELODYNE_TYPES_H_
