#include <velodyne_parser/base_velodyne_parser.h>
#include <boost/function.hpp>


BaseVelodyneParser::BaseVelodyneParser() :
  init_complete_(false),
  full_fov_used_(true),
  full_scan_complete_(false),
  last_block_azimuth_rad_(0.0f),
  publish_full_scan_(true)
{
  // Setup intial states pointers for current and completed point clouds
  current_point_cloud_ptr_ = &point_clouds_[0];
  completed_point_cloud_ptr_ = &point_clouds_[1];
}

BaseVelodyneParser::~BaseVelodyneParser()
{
}

bool BaseVelodyneParser::initialize(void)
{
  config_.position_in_veh_x_m = 1.065;
  config_.position_in_veh_y_m = 0.0f;
  config_.position_in_veh_z_m = 2.14f;
  config_.rotation_in_veh_roll_rad = -0.005f;
  config_.rotation_in_veh_pitch_rad = -0.007f;
  config_.rotation_in_veh_yaw_rad = -1.57254f;

  /* Pre-calculate the 3D rotation matrix for transforming to vehicle coordinates */
  // Roll rotation matrix
  Eigen::Matrix3f roll_rotation_matrix = Eigen::Matrix3f::Identity();
  roll_rotation_matrix(1, 1) = std::cos(config_.rotation_in_veh_roll_rad);
  roll_rotation_matrix(1, 2) = -std::sin(config_.rotation_in_veh_roll_rad);
  roll_rotation_matrix(2, 1) = std::sin(config_.rotation_in_veh_roll_rad);
  roll_rotation_matrix(2, 2) = std::cos(config_.rotation_in_veh_roll_rad);

  // Pitch rotation matrix
  Eigen::Matrix3f pitch_rotation_matrix = Eigen::Matrix3f::Identity();
  pitch_rotation_matrix(0, 0) = std::cos(config_.rotation_in_veh_pitch_rad);
  pitch_rotation_matrix(0, 2) = std::sin(config_.rotation_in_veh_pitch_rad);
  pitch_rotation_matrix(2, 0) = -std::sin(config_.rotation_in_veh_pitch_rad);
  pitch_rotation_matrix(2, 2) = std::cos(config_.rotation_in_veh_pitch_rad);

  // Yaw rotation matrix
  Eigen::Matrix3f yaw_rotation_matrix = Eigen::Matrix3f::Identity();
  yaw_rotation_matrix(0, 0) = std::cos(config_.rotation_in_veh_yaw_rad);
  yaw_rotation_matrix(0, 1) = -std::sin(config_.rotation_in_veh_yaw_rad);
  yaw_rotation_matrix(1, 0) = std::sin(config_.rotation_in_veh_yaw_rad);
  yaw_rotation_matrix(1, 1) = std::cos(config_.rotation_in_veh_yaw_rad);

  tf_rotation_matrix_ = roll_rotation_matrix * pitch_rotation_matrix * yaw_rotation_matrix; // Order matters.

  t00 = tf_rotation_matrix_(0,0);
  t01 = tf_rotation_matrix_(0,1);
  t02 = tf_rotation_matrix_(0,2);
  t10 = tf_rotation_matrix_(1,0);
  t11 = tf_rotation_matrix_(1,1);
  t12 = tf_rotation_matrix_(1,2);
  t20 = tf_rotation_matrix_(2,0);
  t21 = tf_rotation_matrix_(2,1);
  t22 = tf_rotation_matrix_(2,2);

  // lets set the initialization complete flag to true
  init_complete_ = true;

  return true;
}

void BaseVelodyneParser::registerPublishCallback(std::function<void(const PointCloudf&)> callback_func)
{
  velodyne_parser_point_cloud_output_callback_ = callback_func;
}

void BaseVelodyneParser::publishPointCloud(const PointCloudf& point_cloud)
{
  if (velodyne_parser_point_cloud_output_callback_ != nullptr)
  {
    velodyne_parser_point_cloud_output_callback_(point_cloud);
  }

  else
  {
    std::cout << "publishPointCloud called but no callback is registered." << std::endl;
  }
}

bool BaseVelodyneParser::isScanComplete(void)
{
  return full_scan_complete_;
}

bool BaseVelodyneParser::isInitComplete(void)
{
  return init_complete_;
}

const PointCloudf& BaseVelodyneParser::getLastCompletedScan(void)
{
    return *completed_point_cloud_ptr_;
}

void BaseVelodyneParser::clearLastCompletedScan(void)
{
  completed_point_cloud_ptr_->clear();
}

void BaseVelodyneParser::setCurrentPointCloudToCompleted(void)
{
  // set flag that stores status
  full_scan_complete_ = true;

  // Swap the two pointers, current cloud now is the completed cloud adn vice versa.
  std::swap(completed_point_cloud_ptr_, current_point_cloud_ptr_);

  // We also want to clear out any old data that in the previous completed cloud (now the current point cloud)
  // before we start filling it with new data.  This may have already been cleared else where too.
  current_point_cloud_ptr_->clear();
}


//************** Temporarily added to meet DeepMapp's requirements. ***************//
void BaseVelodyneParser::transformMinSecimeToFullUTCTimeStamp(const double& min_sec_time, const double& system_time, double& full_utc_timestamp)
{
  static const double kLargeTimeGapValueNotLikelyComeOut = 3000.0;
  static const double kSecsPerHour = 3600.0;

  double time_hours = kSecsPerHour * static_cast<double>(static_cast<unsigned>(system_time) / static_cast<unsigned>(kSecsPerHour));

  full_utc_timestamp = time_hours + min_sec_time;
  if(full_utc_timestamp - system_time < -kLargeTimeGapValueNotLikelyComeOut)
  {
    full_utc_timestamp += kSecsPerHour;
  }
  else if(full_utc_timestamp - system_time > kLargeTimeGapValueNotLikelyComeOut)
  {
    full_utc_timestamp -= kSecsPerHour;
  }
}

void BaseVelodyneParser::setPublishFullScan(const bool& publish_full_scan)
{
  publish_full_scan_ = publish_full_scan;
}

