#include <velodyne_parser/velodyne_vls_128_parser.h>
#include <chrono>

// Payload size of the VLS128 packet.
const size_t kVLS128DataPacketPayloadSizeBytes = 1206;
const size_t kVLS128NumOfPointsPerFiring = 8;
const size_t kVLS128NumOfPointsPerDataBlock = 32;
const size_t kVLS128NumOfLasers = 128;
const size_t kNumOfBlocksPerFiringSequence = kVLS128NumOfLasers / kVLS128NumOfPointsPerDataBlock; // = 4
const size_t kVLS128NumOfBlocksPerDataPacket = 12;
const size_t kVLS128NumOfFiringSequencesPerPacket = kVLS128NumOfBlocksPerDataPacket / kNumOfBlocksPerFiringSequence; // = 3
const size_t kVLS128NumberOfAugmentedBlocksPerDataPacket = kVLS128NumOfFiringSequencesPerPacket;


// Conversation Constants
const float kVLS128RangeResolutionMeters = 0.004f;
const float kVLS128AzimuthResolutionDeg = 0.01f;

// Timing constants
const double kVLS128TimeBetweenLaserPairFiringsSecs = 2.665E-6;
const double kVLS128FiringCycleTimeSecs = 53.3E-6;
const double kVLS128TimeOffsetFromBlockTimeStamp = -8.7E-6;

// Parsing Constants
const uint8_t kVLS128AzimuthToleranceDeg = 6;

// Azimuth offset for each laser relative to sensor's azimuth.
const std::array<double, kVLS128NumOfLasers> VLS128AzimuthOffsetsRad =
{//     Azimuth offset |  Laser ID
 -0.1108982207,      //	0
 -0.0793775744,      //	1
 -0.0476823952,      //	2
 -0.0158999495,      //	3
 0.0158999495,       //	4
 0.0476823952,       //	5
 0.0793775744,       //	6
 0.1108982207,       //	7
 -0.1108982207,      //	8
 -0.0793775744,      //	9
 -0.0476823952,      //  10
 -0.0158999495,      //  11
 0.0158999495,       //  12
 0.0476823952,       //	13
 0.0793775744,       //	14
 0.1108982207,       //	15
 -0.1108982207,      //	16
 -0.0793775744,      //	17
 -0.0476823952,      //	18
 -0.0158999495,      //	19
 0.0158999495,       //	20
 0.0476823952,       //	21
 0.0793775744,       //	22
 0.1108982207,       //	23
 -0.1108982207,      //	24
 -0.0793775744,      //	25
 -0.0476823952,      //	26
 -0.0158999495,      //	27
 0.0158999495,       //	28
 0.0476823952,       //	29
 0.0793775744,       //	30
 0.1108982207,       //	31
 -0.1108982207,      //	32
 -0.0793775744,      //	33
 -0.0476823952,      //	34
 -0.0158999495,      //	35
 0.0158999495,       //	36
 0.0476823952,       //	37
 0.0793775744,       //	38
 0.1108982207,       //	39
 -0.1108982207,      //	40
 -0.0793775744,      //	41
 -0.0476823952,      //	42
 -0.0158999495,      //	43
 0.0158999495,       //	44
 0.0476823952,       //	45
 0.0793775744,       //	46
 0.1108982207,       //	47
 -0.1108982207,      //	48
 -0.0793775744,      //	49
 -0.0476823952,      //	50
 -0.0158999495,      //	51
 0.0158999495,       //	52
 0.0476823952,       //	53
 0.0793775744,       //	54
 0.1108982207,       //	55
 -0.1108982207,      //	56
 -0.0793775744,      //	57
 -0.0476823952,      //	58
 -0.0158999495,      //	59
 0.0158999495,       //	60
 0.0476823952,       //	61
 0.0793775744,       //	62
 0.1108982207,       //	63
 -0.1108982207,      //	64
 -0.0793775744,      //	65
 -0.0476823952,      //	66
 -0.0158999495,      //	67
 0.0158999495,       //	68
 0.0476823952,       //	69
 0.0793775744,       //	70
 0.1108982207,       //	71
 -0.1108982207,      //	72
 -0.0793775744,      //	73
 -0.0476823952,      //	74
 -0.0158999495,      //	75
 0.0158999495,       //	76
 0.0476823952,       //	77
 0.0793775744,       //	78
 0.1108982207,       //	79
 -0.1108982207,      //	80
 -0.0793775744,      //	81
 -0.0476823952,      //	82
 -0.0158999495,      //	83
 0.0158999495,       //	84
 0.0476823952,       //	85
 0.0793775744,       //	86
 0.1108982207,       //	87
 -0.1108982207,      //	88
 -0.0793775744,      //	89
 -0.0476823952,      //	90
 -0.0158999495,      //	91
 0.0158999495,       //	92
 0.0476823952,       //	93
 0.0793775744,       //	94
 0.1108982207,       //	95
 -0.1108982207,      //	96
 -0.0793775744,      //	97
 -0.0476823952,      //	98
 -0.0158999495,      //	99
 0.0158999495,       //	100
 0.0476823952,       //	101
 0.0793775744,       //	102
 0.1108982207,       //	103
 -0.1108982207,      //	104
 -0.0793775744,      //	105
 -0.0476823952,      //	106
 -0.0158999495,      //	107
 0.0158999495,       //	108
 0.0476823952,       //	109
 0.0793775744,       //	110
 0.1108982207,       //	111
 -0.1108982207,      //	112
 -0.0793775744,      //	113
 -0.0476823952,      //	114
 -0.0158999495,      //	115
 0.0158999495,       //	116
 0.0476823952,       //	117
 0.0793775744,       //	118
 0.1108982207,       //	119
 -0.1108982207,      //	120
 -0.0793775744,      //	121
 -0.0476823952,      //	122
 -0.0158999495,      //	123
 0.0158999495,       //	124
 0.0476823952,       //	125
 0.0793775744,       //	126
 0.1108982207        //	127
};

// Elevation Angle for each laser relative to sensor origin.
const std::array<double, kVLS128NumOfLasers> VLS128ElevationAnglesRad =
{// Elevation Angle  | Laser ID
 -0.2049365608	,	//	0
 -0.0347320521	,	//	1
 0.0593411946	,	//	2
 -0.0923279174	,	//	3
 -0.0136135682	,	//	4
 0.0804596785	,	//	5
 -0.0712094335	,	//	6
 0.0228638132	,	//	7
 -0.1134464014	,	//	8
 -0.0193731547	,	//	9
 0.074700092   , //	10
 -0.07696902   , //	11
 0.0017453293	,	//	12
 0.1130973355	,	//	13
 -0.0558505361	,	//	14
 0.0382227106	,	//	15
 -0.0673697091	,	//	16
 0.0267035376	,	//	17
 -0.1613382361	,	//	18
 -0.0308923278	,	//	19
 0.0478220215	,	//	20
 -0.1038470905	,	//	21
 -0.0097738438	,	//	22
 0.0842994029	,	//	23
 -0.0520108117	,	//	24
 0.042062435	,   //	25
 -0.109606677	,	//	26
 -0.0155334303	,	//	27
 0.0631809189	,	//	28
 -0.0884881931	,	//	29
 0.0055850536	,	//	30
 0.1322959573	,	//	31
 -0.0059341195	,	//	32
 0.0904080553	,	//	33
 -0.0635299848	,	//	34
 0.0305432619	,	//	35
 -0.436332313	,	//	36
 -0.0424115008	,	//	37
 0.0516617459	,	//	38
 -0.1000073661	,	//	39
 0.009424778	,   //	40
 0.1692969374	,	//	41
 -0.0481710874	,	//	42
 0.0459021593	,	//	43
 -0.1335176878	,	//	44
 -0.0270526034	,	//	45
 0.0670206433	,	//	46
 -0.0846484687	,	//	47
 0.0555014702	,	//	48
 -0.0961676418	,	//	49
 -0.0020943951	,	//	50
 0.1000073661	,	//	51
 -0.0750491578	,	//	52
 0.0190240888	,	//	53
 -0.2799857186	,	//	54
 -0.0385717765	,	//	55
 0.0708603676	,	//	56
 -0.0808087444	,	//	57
 0.0132645023	,	//	58
 0.2617993878	,	//	59
 -0.0596902604	,	//	60
 0.0343829863	,	//	61
 -0.1195550538	,	//	62
 -0.0232128791	,	//	63
 -0.098087504	,	//	64
 -0.0040142573	,	//	65
 0.0947713784	,	//	66
 -0.0616101226	,	//	67
 0.0171042267	,	//	68
 -0.3417703741	,	//	69
 -0.0404916386	,	//	70
 0.053581608	,   //	71
 -0.0827286065	,	//	72
 0.0113446401	,	//	73
 0.2050761871	,	//	74
 -0.0462512252	,	//	75
 0.0324631241	,	//	76
 -0.1247910415	,	//	77
 -0.0251327412	,	//	78
 0.0689405055	,	//	79
 -0.0366519143	,	//	80
 0.0574213324	,	//	81
 -0.0942477796	,	//	82
 -0.0001745329	,	//	83
 0.0785398163	,	//	84
 -0.0731292957	,	//	85
 0.020943951	, 	//	86
 -0.236753913	,	//	87
 -0.0212930169	,	//	88
 0.0727802298	,	//	89
 -0.0788888822	,	//	90
 0.0151843645	,	//	91
 0.1061160185	,	//	92
 -0.0577703982	,	//	93
 0.0363028484	,	//	94
 -0.1160643953	,	//	95
 0.0247836754	,	//	96
 -0.1805717644	,	//	97
 -0.0328121899	,	//	98
 0.0612610567	,	//	99
 -0.1057669527	,	//	100
 -0.011693706	,	//	101
 0.0823795407	,	//	102
 -0.0692895713	,	//	103
 0.0401425728	,	//	104
 -0.1115265392	,	//	105
 -0.0174532925	,	//	106
 0.0766199542	,	//	107
 -0.0904080553	,	//	108
 0.0036651914	,	//	109
 0.1218239818	,	//	110
 -0.0539306739	,	//	111
 0.0869173967	,	//	112
 -0.0654498469	,	//	113
 0.0286233997	,	//	114
 -0.1457698991	,	//	115
 -0.044331363	,	//	116
 0.0497418837	,	//	117
 -0.1019272283	,	//	118
 -0.0078539816	,	//	119
 0.1471312559	,	//	120
 -0.0500909495	,	//	121
 0.0439822972	,	//	122
 -0.1076868148	,	//	123
 -0.0289724656	,	//	124
 0.0651007811	,	//	125
 -0.0865683309	,	//	126
 0.0075049158	,	//	127

};


// Data format for one measurement point.
struct __attribute__((__packed__)) DataPoint
{
  uint16_t distance;
  uint8_t reflectivity;
};

// Data block consisting of 32 measurement points.
struct __attribute__((__packed__)) DataBlock
{
  uint16_t flag;
  uint16_t azimuth;
  std::array<DataPoint, kVLS128NumOfPointsPerDataBlock> points;
};

// Augmented data block consisting of 128 measurement points.
// Three DataBlocks are combined into a AugmentedDataBlock.
struct __attribute__((__packed__)) AugmentedDataBlock
{
  uint16_t flag;
  uint16_t azimuth;
  std::array<DataPoint, kVLS128NumOfPointsPerDataBlock> points_0_31;
  uint16_t unused0;
  uint16_t unused1;
  std::array<DataPoint, kVLS128NumOfPointsPerDataBlock> points_32_63;
  uint16_t unused2;
  uint16_t unused3;
  std::array<DataPoint, kVLS128NumOfPointsPerDataBlock> points_64_95;
  uint16_t unused4;
  uint16_t unused5;
  std::array<DataPoint, kVLS128NumOfPointsPerDataBlock> points_96_127;
};

// Decoded velodyne data packet format.
struct __attribute__((__packed__)) VLS128DecodedDataPacket
{
  std::array<AugmentedDataBlock, kVLS128NumOfFiringSequencesPerPacket> data_blocks;
  uint32_t timestamp_us; // Microseconds since top of the hour relative to sensor's time.
  VelodyneReturnMode return_mode;
  VelodyneSensorModel product_id;
};

// Overlay the decoded velodyne data packet and raw packet into same space.
union VLS128DataPacket
{
  VLS128DataPacket()
    : raw{}
  {
  }

  // The decoded packet.
  VLS128DecodedDataPacket decoded;

  // The complete raw packet.
  std::array<uint8_t, kVLS128DataPacketPayloadSizeBytes> raw;
};


// Check that expected structure sizes are correct.
static_assert(sizeof(VLS128DataPacket) == kVLS128DataPacketPayloadSizeBytes,
              "Invalid size of packed VLS128 data packet.");


bool VelodyneVLS128Parser::parseDataPacket(const std::vector<uint8_t>& packet, const double& system_time)
{
  // Lets grab the frame and convert it to a usable format
  VLS128DataPacket sub_scan;
  std::memcpy(&sub_scan.raw[0], &packet[0], kVLS128DataPacketPayloadSizeBytes);


  /* For precision azimuth calculation, we need to calcuate the azimuth gaps between blocks.
     There are two things that need to be considered when calculating the azimuth gaps:
      1) We can only calculate azimuth gaps for blocks we have the azimuth for, this means the last gap cannot be
         directly calculate, since we do no have visibility into the next packet.
      2) We when rollover occurs (either 359 to 0 or corssing FoV limits), we need to be able to correctly handle this
         case.

    When we do encounter a roll over case:
      1) We will offset the azimuth by 360 degs to correctly calculate the interpolated version later. We do this now
         for simplicity.
      2) We need to keep track of the calculated azimuth gap for the rollover block because we need to exclude this
         calculated azimuth gap from the average and then go back and replace it with the average.

    Finally
      regardless of rollover we need to calculate the average azimuth gap because the last block will use the avg
      azimuth gap since, there is no visibility into the next packet.
  */
  bool roll_over_occurred_in_this_packet = false;
  std::vector<float> azimuth_gaps_rad;
  std::vector<size_t> rollover_azimuth_gap_indices;

  for (size_t block_num = 0; block_num < (kVLS128NumberOfAugmentedBlocksPerDataPacket - 1); ++block_num)
  {
    const AugmentedDataBlock& current_block = sub_scan.decoded.data_blocks.at(block_num);

    // Get sensor azimuth for this block (raw azimuth is in 1/100ths of a degree)
    float current_azimuth_rad = static_cast<float>(degree_to_radian_float(current_block.azimuth * kVLS128AzimuthResolutionDeg));

    // Get next block's information.
    AugmentedDataBlock& next_block = sub_scan.decoded.data_blocks.at(block_num + 1);

    /* Now lets look at possible rollover and also calculate the azimuth gaps.
           - We need to check the return mode: Dual returns would be looking at even indexed blocks, where single
             return would be looking at all the indexed blocks
    */

    // Check if 359 to 0 rollover occurred and if so offset the azimuth to allow for proper interpolation.
    if (current_block.azimuth > next_block.azimuth)
    {
      rollover_azimuth_gap_indices.push_back(block_num);

      // lets give this azimuth gap a 0 value for easy processing in the next step.
      azimuth_gaps_rad.push_back(0.0f);

      roll_over_occurred_in_this_packet = true;
    }
    else
    {
      // Calculate the azimuth gap between the current and next block (in radians) and add it to the vector.
      azimuth_gaps_rad.push_back(degree_to_radian_float(next_block.azimuth * kVLS128AzimuthResolutionDeg)
        - current_azimuth_rad);
    }
  }

  /* Timestamp handling

     For timestamp handling we need to consider if the sensor is using GPS timestamp.
     With this we then need to check two things: First the config preference and also we need to look at the current
     status for the sensor PPS based on the last status reading from the telemetry message.
  */
  double start_of_sub_scan_epoch_timestamp_sec = 0.0;

  //************** Temporarily commented out to meet DeepMapp's requirements. ***************//
  //if (config_.use_timestamp_from_sensor && last_telemetry_info_.sys_time_of_last_valid_nmea_sec)

  //************** Temporarily added to meet DeepMapp's requirements. ***************//
  const double kLargeTimeGapValueNotLikelyComeOutInNormalCondition = 1.0;
  double time_of_first_point_min_sec_only = 1.0e-6 * sub_scan.decoded.timestamp_us;
  double time_of_first_point_full_utc;

  transformMinSecimeToFullUTCTimeStamp(time_of_first_point_min_sec_only, system_time, time_of_first_point_full_utc);
  time_of_first_point_full_utc += kVLS128TimeOffsetFromBlockTimeStamp;

  /* Improvement oppurtunities
       - We should think of ways to better handle checking timestamps from the sensor and how to better decide,
         when and when not to use the currently stored telemetry UTC timestamp.
       - Think on how to utilize the PPS status for checking timestamps.
    */

  //************** Temporarily commented out to meet DeepMapp's requirements. ***************//
  //    // Convert the UTC from the last valid telemetry message to epoch time.
  //    // NOTE: we only care up to the hour in terms of resolution.
  //    struct tm temp_time = {};
  //    temp_time.tm_year = last_telemetry_info_.last_valid_nmea_gprmc.utc.year;
  //    temp_time.tm_mon = last_telemetry_info_.last_valid_nmea_gprmc.utc.mon;
  //    temp_time.tm_mday = last_telemetry_info_.last_valid_nmea_gprmc.utc.day;
  //    temp_time.tm_hour = last_telemetry_info_.last_valid_nmea_gprmc.utc.hour;

  //    time_t hour_since_epoch_sec = mktime(&temp_time);

  //    // Now add the Telemetry epoch hour timestamp with the packet start timestamp
  //    start_of_sub_scan_epoch_timestamp_sec = hour_since_epoch_sec + sub_scan.decoded.timestamp_us * 1E-6;

  //************** Temporarily added to meet DeepMapp's requirements. ***************//
  start_of_sub_scan_epoch_timestamp_sec = time_of_first_point_full_utc;

  // Now lets go through the calculated azimuth gaps and get out an average azimuth gap, with the possibility of having
  // to handle the case where there is rollover.
  float accumulated_azimuth_gaps_rad = 0.0f;
  for (size_t i = 0; i < azimuth_gaps_rad.size(); ++i)
  {
    // Skip the azimuth gaps that are zero, these include roll over azimuth gaps and also dual return blocks.
    if (azimuth_gaps_rad[i] == 0.0f)
    {
      continue;
    }

    accumulated_azimuth_gaps_rad += azimuth_gaps_rad[i];
  }

  // If there was rollover than we need to calculate the average azimuth gap without the azimuth gap affected by the
  // rollover, if not, calculate the average with normal size.

  float avg_azimuth_gap_rad = accumulated_azimuth_gaps_rad /
      (azimuth_gaps_rad.size() - rollover_azimuth_gap_indices.size());

  // Now handle the rollover cases if applicable, and replace the bad gaps with the average
  if (roll_over_occurred_in_this_packet)
  {
    for (size_t i = 0; i < rollover_azimuth_gap_indices.size(); ++i)
    {
      azimuth_gaps_rad[rollover_azimuth_gap_indices[i]] = avg_azimuth_gap_rad;
    }
  }

  // Since we can only are able to calculate 2 out of the 3 azimuth gaps needed for proper interpolation. we need
  // to append the average azimuth gap to the end of the vector of azimuth gaps.  We do this by simply copying
  azimuth_gaps_rad.push_back(avg_azimuth_gap_rad);

  /* Actual Parsing and packing */
  for (size_t block_num = 0; block_num < kVLS128NumberOfAugmentedBlocksPerDataPacket; ++block_num)
  {
    const AugmentedDataBlock& current_block = sub_scan.decoded.data_blocks.at(block_num);

    // Get sensor azimuth for this block (raw azimuth is in 1/100ths of a degree)
    float current_azimuth_rad = degree_to_radian_float(current_block.azimuth * kVLS128AzimuthResolutionDeg);

    /* Need to check for roll over from 359.99 deg to 0 deg. */
    // If not last block in scan, check for roll over.
    if((block_num + 1) < kVLS128NumberOfAugmentedBlocksPerDataPacket)
    {
      AugmentedDataBlock& next_block = sub_scan.decoded.data_blocks.at(block_num + 1);
      if (current_block.azimuth > next_block.azimuth)
      {
        // Add 360 degrees in the native azimuth units.  This allows seamless processing.
        next_block.azimuth += 36000;
      }
    }

    /* Since we are now looking at the full 360 degrees for the lidar output, we need to check the last block's azimuth
       and the current azimuth to see if the end of the FoV is crossed.  If so we want to publish the accumulated scan
       if enabled.

       If we want to accumulate subscans before publishing, we will evaulate it at the start of the block processing.
       We do this before instead of after because if the first block in a new packet is needed to
       complete scan, we want to publish the accumulated scan immediately.
    */
    if (publish_full_scan_ && !current_point_cloud_ptr_->isempty())
    {
      float eval_current_azimuth_rad = current_azimuth_rad;
      // If there's wrap around, lets remove it for the full scan publish evaluation
      if (eval_current_azimuth_rad >= 2.0f * M_PI)
      {
        eval_current_azimuth_rad -= 2.0f * M_PI;
      }

      /* This is the special end of FoV evaluation case  when full field of view is being used
         (Start FoV = 0 and End FoV = 360).
      */
      if (full_fov_used_
        && (last_block_azimuth_rad_ > M_PI)
        && (eval_current_azimuth_rad < M_PI))
      {
        // The scan has completed add has transited over the End FoV to Start FoV)

         // Handle some required behind the scenes stuff and set some flags.
        setCurrentPointCloudToCompleted();

        //************** Temporarily added to meet DeepMapp's requirements. ***************//
        completed_point_cloud_ptr_->height_ = kVLS128NumOfLasers;
        completed_point_cloud_ptr_->width_ /= kVLS128NumOfLasers;
        completed_point_cloud_ptr_->is_2D_structure_ = true;
        completed_point_cloud_ptr_->is_dense_ = false;

        // Call a publish on the completed scan.
        publishPointCloud(*completed_point_cloud_ptr_);

        // Assuming it got published, now reset scan complete flag.
        full_scan_complete_ = false;

        // Store this block azimuth for next cycle.
        last_block_azimuth_rad_ = eval_current_azimuth_rad;

        // Since we are looking at the full FoV, we still want to process the current azimuth.
      }


      /* If zero crossing is NOT possible (Start FoV angle < Stop FoV angle), simply compare the current angle to the
         the FoV boundariesDataBlock
      */
      else
      {
        // store this block azimuth for next cycle.
        last_block_azimuth_rad_ = eval_current_azimuth_rad;
      }
    }
    else if(!current_point_cloud_ptr_->isempty())
    {
      setCurrentPointCloudToCompleted();
       //************** Temporarily added to meet DeepMapp's requirements. ***************//
       completed_point_cloud_ptr_->height_ = kVLS128NumOfLasers;
       completed_point_cloud_ptr_->width_ /= kVLS128NumOfLasers;
       completed_point_cloud_ptr_->is_2D_structure_ = true;
       completed_point_cloud_ptr_->is_dense_ = false;

       // Call a publish on the completed scan.
       publishPointCloud(*completed_point_cloud_ptr_);

       // Assuming it got published, now reset scan complete flag.
       full_scan_complete_ = false;
    }

    /* Process points for this block */
    DataPoint current_point;
    for(size_t laser_num = 0; laser_num < kVLS128NumOfLasers; ++laser_num)
    {
      // The AugmentedDataBlock consists of four DataBlocks, and a total of 128 points are
      // deviced into the four blocks.
      if(0 <= laser_num && laser_num < 32)
      {
        current_point = current_block.points_0_31[laser_num % 32];
      }
      else if(32 <= laser_num && laser_num < 64)
      {
        current_point = current_block.points_32_63[laser_num % 32];
      }
      else if(64 <= laser_num && laser_num < 96)
      {
        current_point = current_block.points_64_95[laser_num % 32];
      }
      else
      {
        current_point = current_block.points_96_127[laser_num % 32];
      }

      const size_t current_firing_pair_num = laser_num / kVLS128NumOfPointsPerFiring;

      float laser_azimuth_rad;

      // Make the interpolated version of the azimuth which is at a higher precision.
      laser_azimuth_rad = current_azimuth_rad + (azimuth_gaps_rad[block_num] * kVLS128TimeBetweenLaserPairFiringsSecs
                                                 * (current_firing_pair_num + current_firing_pair_num / 8)) / kVLS128FiringCycleTimeSecs - VLS128AzimuthOffsetsRad[laser_num];

      // Correct azimuth back to 0 to 360 if necessary. This will happen to points that cross over start FoV limit
      if (laser_azimuth_rad >= 2.0f * M_PI)
      {
        laser_azimuth_rad -= 2.0f * M_PI;
      }

      // Check point validity
      //************** Temporarily commented out to meet DeepMapp's requirements. ***************//
      //      if (current_point.distance != 0.0f)
      {
        Point3If processed_point;

        // Get distance (1 unit of the distance value = 4mm and we want it in meters)
        float distance_m = current_point.distance * kVLS128RangeResolutionMeters;

        // Calculate point position
        processed_point.x = distance_m * std::cos(VLS128ElevationAnglesRad[laser_num]) * std::sin(laser_azimuth_rad);
        processed_point.y = distance_m * std::cos(VLS128ElevationAnglesRad[laser_num]) * std::cos(laser_azimuth_rad);
        processed_point.z = distance_m * std::sin(VLS128ElevationAnglesRad[laser_num]);


        // Check for transform config and perform if enabled
 Eigen::Vector3f tf_point;
//        Eigen::Vector3f tf_point = tf_rotation_matrix_
//            * Eigen::Vector3f(processed_point.x, processed_point.y, processed_point.z);

        tf_point[0] = t00 * processed_point.x + t01 * processed_point.y + t02 * processed_point.z;
        tf_point[1] = t10 * processed_point.x + t11 * processed_point.y + t12 * processed_point.z;
        tf_point[2] = t20 * processed_point.x + t21 * processed_point.y + t22 * processed_point.z;

        processed_point.x = tf_point(0) + config_.position_in_veh_x_m;
        processed_point.y = tf_point(1) + config_.position_in_veh_y_m;
        processed_point.z = tf_point(2) + config_.position_in_veh_z_m;


        // Get reflectivity
        processed_point.intensity = current_point.reflectivity;

        // Calculate absolute timestamp (timestamp application to points will depend on return mode)
        double timestamp_offset_us;

        {
          timestamp_offset_us = block_num * kVLS128FiringCycleTimeSecs
              + (current_firing_pair_num + current_firing_pair_num / 8) * kVLS128TimeBetweenLaserPairFiringsSecs;
        }

        // Apply timestamp offset for point to packet's start timestamp.
        processed_point.timestamp = start_of_sub_scan_epoch_timestamp_sec + timestamp_offset_us;

        // Add point to current point cloud
        current_point_cloud_ptr_->push_back(processed_point);
      }
    } // point for loop
  } // block for loop


  return true;
}

void VelodyneVLS128Parser::publishPointCloud(const PointCloudf& point_cloud)
{
  // had to temporarly move the publish point cloud methods the child classes to pack the point cloud in the way that
  // deepmap wants it.

  if (velodyne_parser_point_cloud_output_callback_ != nullptr)
  {
    velodyne_parser_point_cloud_output_callback_(point_cloud);
  }

  else
  {
    std::cout << "publishPointCloud called but no callback is registered." << std::endl;
  }
}

