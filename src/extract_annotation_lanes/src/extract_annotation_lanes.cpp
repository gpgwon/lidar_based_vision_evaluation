#include <extract_annotation_lanes/extract_annotation_lanes.h>

// std headers
#include <iostream>
#include <string>
#include <vector>
#include <stdint.h>
#include <fstream>

// 3rd party
#include <yaml-cpp/yaml.h>

#define PUBLISH_DEBUG_CLOUD false

std::string out_file_folder("");
#if PUBLISH_DEBUG_CLOUD
std::shared_ptr<ros::Publisher> pub_debug_cloud{nullptr};
#endif

struct Config
{
  std::string master_jason_folder_;

  std::string ann_file_folder_;

  std::string image_file_folder_;
};


void loadBagToPCFilesConfigs(Config& params);

int main(int argc, char* argv[])
{

  return 0;
}


void loadBagToPCFilesConfigs(Config& params)
{
  YAML::Node yaml_node;
  try
  {
    std::cout << "Try to read a yaml file - " << std::string(LIDAR_BASED_VISION_EVALUATION_PARAM_DIR)
                 + "/lane_extraction_map_building_first/lane_extraction_map_building_first_params.yaml" << std::endl;

    yaml_node = YAML::LoadFile(std::string(LIDAR_BASED_VISION_EVALUATION_PARAM_DIR)
                               + "/convert_velodyne_packet_bags_to_pointcloud_files/convert_velodyne_packet_bags_to_pointcloud_files.yaml");

  }
  catch (const YAML::Exception& e)
  {
    std::cout << "Fail to read the yaml file." << std::endl;
  }
  params.master_jason_folder_ = yaml_node["master_jason_folder"].as<std::string>();
  params.ann_file_folder_ = yaml_node["ann_file_folder"].as<std::string>();
  params.image_file_folder_ = yaml_node["image_file_folder"].as<std::string>();
}

