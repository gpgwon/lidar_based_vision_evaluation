cmake_minimum_required(VERSION 2.8.3)
project(extract_annotation_lanes)

add_definitions( -DLIDAR_BASED_VISION_EVALUATION_PARAM_DIR="${LIDAR_BASED_VISION_EVALUATION_PARAM_DIR}")


include_directories(
    include
    ${COMMON_INCLUDE_DIR}
    ${YAML_CPP_INCLUDEDIR}
)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
add_executable(
    extract_annotation_lanes
    src/extract_annotation_lanes.cpp
)

target_link_libraries(
    extract_annotation_lanes
    ${YAML_CPP_LIBRARIES}
)

