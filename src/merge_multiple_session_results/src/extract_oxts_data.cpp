
// ROS headers
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <ros/ros.h>

// std headers
#include <iostream>
#include <string>
#include <vector>
#include <stdint.h>
#include <fstream>

// 3rd party
#include <yaml-cpp/yaml.h>

#include <phantom_ros/PoseMeasurement.h>
#include <boost/filesystem.hpp>

#include <common/file_system.h>
#include <common/utils.h>
#include <common/geometry.h>

#define PUBLISH_DEBUG_CLOUD false

std::string out_file_folder("");
std::string input_folder("");

int main(int argc, char* argv[])
{
  // Read arguments and store the bag file list.
  std::vector<std::string> bag_file_list;

  for (int i = 1; i < argc; i++)
  {
    std::string str(argv[i]);
    if(str.compare("-o") == 0)
    {
      if(i + 1 < argc)
      {
        out_file_folder = argv[i + 1];
      }
      else
      {
        std::cout << "Input the path of the folder to store point cloud files." << std::endl;
      }
      break;
    }

    bag_file_list.push_back(str);
  }


  std::string session_name = extractLastBranchNameOfPath(bag_file_list[0]);


  std::ofstream fout(out_file_folder + "/" + session_name + ".csv");


  /* Read the bag files one-by-one, parse the velodyne packets in the bag files, and save pointclouds to files. */
  for(size_t i = 0; i < bag_file_list.size(); ++i)
  {
    std::cout << "Processing file " << bag_file_list[i] << std::endl;

    // Read the bag file.
    rosbag::Bag bag_read(bag_file_list[i], rosbag::bagmode::Read);

    // An object to capture topics of interest from the ROS bag file.
    rosbag::View view(bag_read, rosbag::TopicQuery("/oxts"));

    // Get the velodyne packet topics from the bag file and parse them.
    for(rosbag::MessageInstance const msg: view)
    {
      phantom_ros::PoseMeasurement::ConstPtr oxts = msg.instantiate<phantom_ros::PoseMeasurement>();
      double timestamp = msg.getTime().toSec();

      std::cout << oxts->position.latitude << ", " << oxts->position.longitude << std::endl;

      fout << timestamp << "," << -18 << "," << degree_to_radian(oxts->position.latitude) << "," << degree_to_radian(oxts->position.longitude) << ","
           << oxts->position.altitude << "," << oxts->imu.orientation.x << "," << oxts->imu.orientation.y << "," << oxts->imu.orientation.z << ","
           << "No Data (10)" << "," << oxts->position.position_covariance[0] << "," << oxts->position.position_covariance[4] << std::endl;
    }
  }

  fout.close();

  return 1;
}


