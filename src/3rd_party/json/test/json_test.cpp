#include <json.hpp>
#include <iostream>
#include <fstream>
#include <boost/optional.hpp>

using json = nlohmann::json;
using nlohmann::json;

namespace temp
{

struct Node
{
    int id;
    std::vector<double> point;
    Node()
        :id{0},
          point{}
    {

    }
};

struct Link
{
    int id;
    std::vector<Node> n;
    Link()
        :id{0},
          n{}
    {

    }
};

struct Map
{
    int id;
    std::vector<Link> l;
    Map()
        :id{0},
          l{}
    {

    }
};

}

namespace temp
{
template <typename BasicJsonType>
void from_json(const BasicJsonType& j, Node& n)
{
    n.id = j["id"].template get<int>();
    n.point = j["point"].template get<std::vector<double>>();
}

template <typename BasicJsonType>
void from_json(const BasicJsonType& j, Link& l)
{
    l.id = j["id"].template get<int>();
    l.n = j["node"].template get<std::vector<Node>>();
}

template <typename BasicJsonType>
void from_json(const BasicJsonType& j, Map& m)
{
//    m.id = j["id"].template get<int>();
    m.l = j["link"].template get<std::vector<Link>>();
}

}

int main()
{
    std::stringstream ss;
    ss << \
        "{" \
        "    \"map\" :" \
        "    {" \
        "        \"id\" : 1," \
        "        \"left_bottom\": [-2000.0, -1000.0]," \
        "        \"right_top\": [10.0, 10.0]," \
        "        \"node\" : [" \
        "        {" \
        "            \"id\": 1," \
        "            \"in_link_id\": []," \
        "            \"out_link_id\": [1]," \
        "            \"link_relation\": [[]]," \
        "            \"type\": 0," \
        "            \"position\":[0.000000, 0.000000]" \
        "        }" \
        "        ]," \
        "        \"link\" : [" \
        "            {" \
        "                \"id\": 1," \
        "                \"length\": 100.0," \
        "                \"vertices\": [[], [], []]," \
        "                \"min_speed_limit\": 0," \
        "                \"max_speed_limit\": 100," \
        "                \"one_way\": false," \
        "                \"type\": 0," \
        "                \"start_node_id\": 1," \
        "                \"end_node_id\": 2," \
        "                \"node\": [" \
        "                    {" \
        "                        \"id\": 1," \
        "                        \"point\": [-9.646622, -37.828596]" \
        "                    }" \
        "                ]" \
        "            }" \
        "        ]" \
        "    }" \
        "}";

    json j;

    ss >> j;

    temp::Map m = j["map"].get<temp::Map>();

    std::cout<<m.l.size()<<std::endl;
    std::cout<<m.l[0].id<<std::endl;
    std::cout<<m.l[1].id<<std::endl;
}
