#include <iostream>
#include <cmath>
#include "utm/utm.h"

int main()
{
    double lat = 37.565248;
    double lon = -122.267111;
    long zone = 0;
    char h;
    double x = 0.0;
    double y  = 0.0;
    Convert_Geodetic_To_UTM(lat * M_PI / 180.0,
                            lon * M_PI / 180.0,
                            &zone, &h, &x, &y);

    std::cout<<" utm conversion from ["<<lat<<", "<<lon<<" to "<<std::endl;
    std::cout<<"x : "<<x<<", y: "<<y<<std::endl;
    std::cout<<" zone : "<<zone<<" hemisphere : "<<h<<std::endl;

    return 0;
}
